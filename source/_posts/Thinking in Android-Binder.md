---
title: Thinking in Android -- Binder
date: 2020/5/25 20:46:25
---

[点击查看 Binder.pdf](/mysite/files/Binder.pdf)

{% pdf /mysite/files/Binder.pdf %}

## hexo 博客添加pdf 插件
### 安装插件
```
npm install --save hexo-pdf
```

### 编写
在成的md文件中添加pdf
```
外部链接：
{% pdf http://7xov2f.com1.z0.glb.clouddn.com/bash_freshman.pdf %}
本地连接：
{% pdf ./pdf名字.pdf %}
```
