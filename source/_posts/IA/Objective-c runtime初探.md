运行时，也叫运行时刻，指程序在执行的时刻，是相对于编译时（找符号定义）、链接时（找实现）的一个概念。

对于OC语言，它偏向于将决定尽可能的从编译或链接时推迟到运行时，它总是尽可能地采用动态的方式来解决问题，这意味着OC语言不仅需要一个编译器，还需要一个强大的运行时系统来执行编译好的代码。

同时，在OC中，runtime还可以指运行时API，它是iOS内部核心之一，我们可以通过import\<objc/runtime.h\>引入运行时API，而runtime
system，即运行时系统，负责执行代码系统。

   
为了窥探runtime的大致结构，我们可以通过clang将OC代码转换成C/C++代码来查看其大致结构。

拿这次的PhotoVault的一段代码举例，首先，我定义了一个类，叫PhotoVaultObject，代码如下：

>   //

>   //  PhotoVaultObject.h

>   //  MAX Phone Manager

>   //

>   //  Created by mitsuki on 2018/7/31.

>   //  Copyright © 2018年 ONE App Inc. All rights reserved.

>   //

>   \#import \<Foundation/NSUUID.h\>

>   \#import \<Foundation/NSObject.h\>

>   \#import \<Foundation/NSString.h\>

>   \@interface PhotoVaultObject : NSObject

>   \@property (nonatomic, copy, readonly) NSString \*localIdentifier;

>   \- (instancetype)initWithLocalIdentifier:(NSString \*)localIdentifier;

>   \@end

>   //

>   //  PhotoVaultObject.m

>   //  MAX Phone Manager

>   //

>   //  Created by mitsuki on 2018/7/31.

>   //  Copyright © 2018年 ONE App Inc. All rights reserved.

>   //

>   \#import "PhotoVaultObject.h"

>   \@implementation PhotoVaultObject

>   \- (instancetype)initWithLocalIdentifier:(NSString \*)localIdentifier {

>       if (self = [super init]) {

>           \_localIdentifier = localIdentifier;

>           returnself;

>       }

>       returnnil;

>   }

>   \- (instancetype)init {

>       if (self = [super init]) {

>           \_localIdentifier = [[NSUUID UUID] UUIDString];

>           returnself;

>       }

>       returnnil;

>   }

>   \- (NSString \*)description {

>       return [NSString stringWithFormat:\@"PhotoVaultObject: localIdentifier =
>   %\@", self.localIdentifier];

>   }

>   \@end

然后我定义了PhotoVaultObject的子类——PhotoVaultAssetCollection

>   //

>   //  PhotoVaultAssetCollection.h

>   //  MAX Phone Manager

>   //

>   //  Created by mitsuki on 2018/7/31.

>   //  Copyright © 2018年 ONE App Inc. All rights reserved.

>   //

>   \#import "PhotoVaultObject.h"

>   staticconst NSInteger kPhotoVaultAssetCollectionMaxTitleLength = 20;

>   // 类似系统的PHAssetCollection类

>   \@interface PhotoVaultAssetCollection : PhotoVaultObject

>   \@property (nonatomic, copy, readonly) NSString \*title;

>   \+ (instancetype)assetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                title:(NSString \*)title;

>   \@end

>   //

>   //  PhotoVaultAssetCollection.m

>   //  MAX Phone Manager

>   //

>   //  Created by mitsuki on 2018/7/31.

>   //  Copyright © 2018年 ONE App Inc. All rights reserved.

>   //

>   \#import "PhotoVaultAssetCollection.h"

>   \@implementation PhotoVaultAssetCollection

>   \+ (instancetype)assetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                title:(NSString \*)title {

>       return [[PhotoVaultAssetCollection alloc]
>   initAssetCollectionWithLocalIdentifier:localIdentifier title:title];

>   }

>   \- (instancetype)initAssetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                 title:(NSString \*)title {

>       if (self = [super initWithLocalIdentifier:localIdentifier]) {

>           \_title = title;

>           returnself;

>       }

>       returnnil;

>   }

>   \- (NSString \*)description {

>       return [NSString stringWithFormat:\@"PhotoVaultAssetCollection: title =
>   %\@", self.title];

>   }

>   \@end

>   int main(int argc, constchar \* argv[]) {

>       \@autoreleasepool {

>           PhotoVaultObject \*object = [PhotoVaultAssetCollection
>   assetCollectionWithLocalIdentifier:[[NSUUID UUID] UUIDString]
>   title:\@"Default Album"];

>           NSLog(\@"object = %\@", [object description]);

>       }

>       return0;

>   }

打开终端，执行下面的命令，可以将上面的代码编译成可执行文件hehe.out：

>   clang -fobjc-arc PhotoVaultAssetCollection.m PhotoVaultObject.m -o hehe.out

执行hehe.out可以得到输出：

>   2018-08-30 11:38:27.922 hehe.out[63126:9984470] object =
>   PhotoVaultAssetCollection: title = Default Album

说明代码没问题

然后我们执行下面的命令，将上面的代码转换成C/C++代码：

>   clang -rewrite-objc PhotoVaultAssetCollection.m PhotoVaultObject.m

得到的是 PhotoVaultAssetCollection.cpp 和 PhotoVaultObject.cpp
文件，打开PhotoVaultAssetCollection.cpp文件，先找到我们在代码里定义的main方法：

>   int main(int argc, constchar \* argv[]) {

>       /\* \@autoreleasepool \*/ { \__AtAutoreleasePool \__autoreleasepool;

>           PhotoVaultObject \*object = ((PhotoVaultAssetCollection \*(\*)(id,
>   SEL, NSString \*, NSString \*))(void
>   \*)objc_msgSend)((id)objc_getClass("PhotoVaultAssetCollection"),
>   sel_registerName("assetCollectionWithLocalIdentifier:title:"), ((NSString \*
>   \_Nonnull (\*)(id, SEL))(void \*)objc_msgSend)((id)((NSUUID \* \_Nonnull
>   (\*)(id, SEL))(void \*)objc_msgSend)((id)objc_getClass("NSUUID"),
>   sel_registerName("UUID")), sel_registerName("UUIDString")), (NSString
>   \*)\&__NSConstantStringImpl__var_folders_dg__y6k7j9s4tdflcqsj1dc7r8r0000gn_T_PhotoVaultAssetCollection_7b7567_mi_1);

>           NSLog((NSString
>   \*)&__NSConstantStringImpl__var_folders_dg__y6k7j9s4tdflcqsj1dc7r8r0000gn_T_PhotoVaultAssetCollection_7b7567_mi_2,
>   ((NSString \*(\*)(id, SEL))(void \*)objc_msgSend)((id)object,
>   sel_registerName("description")));

>       }

>       return0;

>   }

可以发现，消息会被转化为：

>   id objc_msgSend(id, SEL, ...)

其中’…’表示方法的参数，也就是说，确定一个消息如何接收，除了方法的参数以外，还需要确定接收消息的id以及选择器SEL；

在继续介绍之前，我们先来了解一下runtime中的一些概念：

【SEL】

SEL表示方法选择器，它的定义为

>   typedefstruct objc_selector \*SEL;

 
  在OC代码中，我们可以通过编译器指令 \@selector(deleteAction:)来表示一个SEL对象；也可以写成
sel_registerName("deleteAction:”)

在iOS以及MacOS中，SEL实质为一个char \*，表示方法的名字，可以在iOS中用
DebugLog(\@“SEL = *%s*",
*\@selector(deleteAction:*));打印出来（编译器会报warning），打印结果为：SEL =
deleteAction:

    带了多个参数的方法：SEL = assetCollectionWithLocalIdentifier:title:

做试验的话可以发现，在不同地方调用 DebugLog(\@"hehe = %p",
\&\@selector(deleteAction:));打印结果是一样的（注意这里打印的是指针），这也就说明拥有相同参数名的方法会被视为同名方法，和类型无关；

【id】

    就是我们熟悉而陌生的id，它的定义为（objc-private.h）

>   typedefstruct objc_object \*id;

>   struct objc_object {

>   private:

>       isa_t isa;

>   public:

>       // ISA() assumes this is NOT a tagged pointer object

>       Class ISA();

>       // getIsa() allows this to be a tagged pointer object

>       Class getIsa();

>       ...省略其他方法

>   }

   
可以发现，id其实是指向objc_object类型的指针，从名字来看相当于一个对象的引用；

    我们再回来看看 objc_msgSend 的定义：

>   id objc_msgSend(id, SEL, ...)

   
即发送消息用到的参数包括id、SEL和方法的参数，C是不支持函数重载的，这也说明了为什么在同一个类里定义了下面代码，会被编译器认为是 Duplicate
declaration of method 'deleteAction:'：

>   \- (void)deleteAction:(PhotoVaultObject \*)sendor {}

>   \- (void)deleteAction:(UITapGestureRecognizer \*)sendor {}

    objc_object的结构体里包含了一个isa变量：

>   isa_t isa;

    isa，拆开来看即is a，字面意思，”this object is a(n) instance of
isa_t"，isa_t是一个union，定义为：

>   union isa_t

>   {

>       isa_t() { }

>       isa_t(uintptr_t value) : bits(value) { }

>       Class cls;

>       uintptr_t bits;

>   … 此处省略struct uintptr_t的定义

>   };

   
isa_t主要还是表示指向类的指针，但是64位的整形单纯用来记类的指针有些浪费，因此，OC2.0在某些环境上（从代码来看似乎只有arm64机支持）将isa指针的某些位抽出来用来记录一些和引用计数相关的数据，所以才会有uintptr_t，详细可以看
<http://yulingtianxia.com/blog/2015/12/06/The-Principle-of-Refenrence-Counting/>。

    简单来说，我们可以认为isa_t就是Class

【Class】

    还是我们熟悉而陌生的Class，它的定义为（objc-runtime-new.h）

>   typedefstruct objc_class \*Class;

>   struct objc_class : objc_object {

>       // Class ISA;

>       Class superclass;

>       cache_t cache;             // formerly cache pointer and vtable

>       class_data_bits_t bits;    // class_rw_t \* plus custom rr/alloc flags

>       class_rw_t \*data() {

>           returnbits.data();

>       }

>       void setData(class_rw_t \*newData) {

>           bits.setData(newData);

>       }

>   ...省略其他方法

>   bool isMetaClass() {

>           assert(this);

>           assert(isRealized());

>           returndata()-\>ro-\>flags & RO_META;

>       }

>   // NOT identical to this-\>ISA when this is a metaclass

>       Class getMeta() {

>           if (isMetaClass()) return (Class)this;

>           elsereturnthis-\>ISA();

>       }

>       bool isRootClass() {

>           returnsuperclass == nil;

>       }

>       bool isRootMetaclass() {

>           returnISA() == (Class)this;

>       }

>   ...省略其他方法

>   }

    从objc_class : objc_object可以看出来，Class也是对象，这也符合了OOP的思想；

    需要注意的是，为了处理类和对象之间的关系，runtime库创建了一个叫做元类(Meta
Class)的概念，元类是指类对象所属的类型。元类的元类还是自己

这里先抄一张图来说明一下实例(typedef struct objc_object \*id)、类(typedef struct
objc_class \*Class)、超类(class superClass)、isa(isa_t
isa)之间的联系（结合上面的代码就能看出其中的规律）

![](media/94aa5e598c2e3d057207606008b8c91b.png)

   
也就是说，id的isa是Class，Class的isa是MetaClass，MetaClass的isa是RootMetaClass；

   
图中RootClass就是NSObject，有意思的是，RootMetaClass也是NSObject的子类。（玩一玩文字游戏就是”All
MetaClasses are NSObject, NSObject Class is a RootMetaClass instance”）

>   // 当前在PhotoVaultMainViewController的-viewDidLoad里

>   (lldb) p objc_getMetaClass(object_getClassName(self))

>   (Class) \$33 = 0x00000001015204e0

>   (lldb) p [objc_getMetaClass(object_getClassName(self)) description]

>   (__NSCFString \*) \$34 = 0x00000001c8241770 \@“PhotoVaultMainViewController"

>   (lldb) p [class_getSuperclass(objc_getMetaClass(object_getClassName(self)))
>   description]

>   (__NSCFConstantString \*) \$35 = 0x00000001ac32eb38 \@"UIViewController"

>   (lldb) p object_getClassName(objc_getMetaClass(object_getClassName(self)))

>   (const char \* \_Nonnull) \$36 = 0x0000000180b62247 "NSObject"

>   …

>   (lldb) p
>   class_getSuperclass(class_getSuperclass(class_getSuperclass(class_getSuperclass(objc_getMetaClass(object_getClassName(self.class))))))

>   (Class) \$40 = NSObject

>   尝试分析下面的输出结果:

>   po [PhotoCleanerMainViewController
>   isKindOfClass:PhotoCleanerMainViewController.class]

>   po [PhotoCleanerMainViewController.class
>   isKindOfClass:PhotoCleanerMainViewController.class]

>   0

>   0

>   po [PhotoCleanerMainViewController isKindOfClass:UIViewController.class]

>   po [PhotoCleanerMainViewController.class
>   isKindOfClass:UIViewController.class]

>   0

>   0

>   po [PhotoCleanerMainViewController isKindOfClass:NSObject.class]

>   po [PhotoCleanerMainViewController.class isKindOfClass:NSObject.class]

>   1

>   1

>   po [PhotoCleanerMainViewController isMemberOfClass:NSObject.class]

>   po [PhotoCleanerMainViewController
>   isMemberOfClass:objc_getMetaClass("PhotoCleanerMainViewController”)]

>   0

>   1

>   如果

>   (lldb) po [[objc_getMetaClass("PhotoCleanerMainViewController") superclass]
>   superclass]

>   UIResponder（注意，这个只是po出来的字符串，实际上是UIResponder的元类）

>   (lldb) po [[[objc_getMetaClass("PhotoCleanerMainViewController") superclass]
>   superclass] superclass]

>   NSObject

>   那么

>   (lldb) po [[[[objc_getMetaClass("PhotoCleanerMainViewController")
>   superclass] superclass] superclass] superclass]

>   ???

>   (lldb) po [[[[[objc_getMetaClass("PhotoCleanerMainViewController")
>   superclass] superclass] superclass] superclass] superclass]

>   ???

    拿网上一个例子：

>   **我们现在新建一个类Parent，继承于NSObject,
>   里面有成员方法-(void)selectorP，类方法+(void)ClassSelectorP。**

>   **再新建一个类Child，继承于Parent，里面有成员方法-(void)selectorC,
>   类方法+(void)ClassSelectorC。**

1.  当我们调用[child class]
    的时候，child就会通过isa指针去找到Child的class。（这里我补充一下，在使用KVO之后，child通过isa找到的是NSKVONotifying_Child的class对象，下同）

2.  当我们调用[child superclass]的时候，child
    通过isa找到Child的class，再通过super_class，找到Parent的class。

3.  接着，调用[child
    SelectorC]，child通过isa找到Child的class，在class（注意看上面 struct
    objc_class 的定义）的方法列表里面找到SelectorC；

4.  再试着调用[child
    SelectorP]，child通过isa找到Child的class，发现class里面并没有这个方法，通过class里面的super_class找到Parent的class，在里面的方法列表找到了SelectorP；

5.  再是类方法[Child
    ClassSelectorC]，Child（请注意，大写）通过isa找到Child的class，通过class的isa找到Child的metaclass，在metaclass的方法列表里面找到了ClassSelectorC；

6.  再试着调用[Child
    ClassSelectorP]，Child通过isa找到Child的class，通过class的isa找到Child的metaclass，发现metaclass里面并没有这个方法，通过metaclass里面的super_class找到Parent的metaclass，在里面的方法列表找到了ClassSelectorP；

关于5、6中的Class，根据转换成C时的代码以及我的理解，我感觉大写C的Child，
它本质是type，它不是Objc对象，在发送消息时编译器会帮我们转换成class对象，例如调用[Child
ClassSelectorC]等价于调用[objc_getClass("Child")
ClassSelector]，而看上面调用[PhotoVaultAssetCollection
assetCollectionWithLocalIdentifier:]静态方法的时候的C代码——((PhotoVaultAssetCollection
\*(\*)(id, SEL, NSString \*, NSString \*))(void
\*)objc_msgSend)((id)objc_getClass("PhotoVaultAssetCollection”),
sel_registerName("assetCollectionWithLocalIdentifier:title:”),…），可以发现，我们写的PhotoVaultAssetCollection，在转换过后变成了(id)objc_getClass("PhotoVaultAssetCollection”)

    ps：网上也找到了对应的说法——当你发出一个类似 [NSObject alloc]
的消息时，你事实上是把这个消息发给了一个类对象 (Class Object)
，这个类对象必须是一个元类的实例，而这个元类同时也是一个根元类 (root meta class)
的实例。

上面的5、6，有一点是可以确定，那就是+号声明的方法（静态方法）是到元类里查询的，两个实验表现可以证明这个结论——

-   同一个类里可以声明同名的静态方法和实例方法（这在java是非法的），而且明显使用类型（或者类对象）调用的是静态方法，使用实例调用的是实例方法；

-   可以用类型（或者类对象）直接调用NSObject里的实例方法，或者拓展的NSObject的实例方法；

    ps：网上说法——所以当 [NSObject alloc]
这条消息发给类对象的时候，objc_msgSend()
会去它的元类里面去查找能够响应消息的方法，如果找到了，然后对这个类对象执行方法调用。

…此处省略无数概念

【Ivar】

    Ivar是一种表示类中实例变量的类型，它的定义为(objc-runtime-new.h)

>   typedefstructivar_t \*Ivar;

>   struct ivar_t {

>   \#if \__x86_64_\_

>       // \*offset was originally 64-bit on some x86_64 platforms.

>       // We read and write only 32 bits of it.

>       // Some metadata provides all 64 bits. This is harmless for unsigned

>       // little-endian values.

>       // Some code uses all 64 bits. class_addIvar() over-allocates the

>       // offset for their benefit.

>   \#endif

>       int32_t \*offset;

>       constchar \*name;

>       constchar \*type;

>       // alignment is sometimes -1; use alignment() instead

>       uint32_t alignment_raw;

>       uint32_t size;

>       uint32_t alignment() const {

>           if (alignment_raw == \~(uint32_t)0) return1U \<\< WORD_SHIFT;

>           return1 \<\< alignment_raw;

>       }

>   };

   
runtime提供了class_copyIvarList方法可以拿到一个类对象的Ivar，object_getIvar(id,
Ivar)可以从一个id中将Ivar所表示的实例变量拿出来，Ivar和android的field有点类似……（此处省略1万字）

【IMP】

    即实现，它的定义为(objc.h)

>   typedefid (\*IMP)(id, SEL, ...); 

    是一个函数指针，由编译器生成

【消息】

Objc中，我们使用中括号（[]）把接收者和消息括起来，知道运行时才会把消息和方法实现绑定。

网上抄的消息发送步骤：

1.  检测这个 selector 是不是要忽略的。比如 Mac OS X
    开发，有了垃圾回收就不理会 retain, release 这些函数了。

2.  检测这个 target 是不是 nil 对象。ObjC
    的特性是允许对一个 nil 对象执行任何一个方法不会 Crash，因为会被忽略掉。

3.  如果上面两个都过了，那就开始查找这个类的 IMP，先从 cache 里面找，完了找得到就跳到对应的函数去执行。

4.  如果 cache 找不到就找一下方法分发表。

5.  如果分发表找不到就到超类的分发表去找，一直找，直到找到NSObject类为止。

6.  如果还找不到就要开始进入**动态方法**解析了，后面会提到。

![](media/4520ac77c3ba69cd3d50a32afb3f3b86.png)

总结一下objc_msgSend(id, SEL, … ,)的步骤就是：

1.  找id的isa指向的类对象（可能是类、也可能是元类）

2.  一路向上（super方向）找IMP，先从cache中找，cache没命中就到方法列表中找

然后我们再来看代码

【id】

>   \#ifndef \_REWRITER_typedef_PhotoVaultObject

>   \#define \_REWRITER_typedef_PhotoVaultObject

>   typedefstruct objc_object PhotoVaultObject;

>   typedefstruct {} \_objc_exc_PhotoVaultObject;

>   \#endif

>   \#ifndef \_REWRITER_typedef_PhotoVaultAssetCollection

>   \#define \_REWRITER_typedef_PhotoVaultAssetCollection

>   typedefstruct objc_object PhotoVaultAssetCollection;

>   typedefstruct {} \_objc_exc_PhotoVaultAssetCollection;

>   \#endif

【实例方法列表】

>   staticstruct/\*_method_list_t\*/ {

>   unsignedint entsize;  // sizeof(struct \_objc_method)

>   unsignedint method_count;

>   struct \_objc_method method_list[4];

>   } \_OBJC_\$_INSTANCE_METHODS_PhotoVaultAssetCollection \__attribute_\_
>   ((used, section ("__DATA,__objc_const"))) = {

>   sizeof(_objc_method),

>   4,

>   {{(struct objc_selector \*)"initAssetCollectionWithLocalIdentifier:title:",
>   "\@32\@0:8\@16\@24", (void
>   \*)_I_PhotoVaultAssetCollection_initAssetCollectionWithLocalIdentifier_title_},

>   {(struct objc_selector \*)"sizeZero", "{MySize=dd}16\@0:8", (void
>   \*)_I_PhotoVaultAssetCollection_sizeZero},

>   {(struct objc_selector \*)"description", "\@16\@0:8", (void
>   \*)_I_PhotoVaultAssetCollection_description},

>   {(struct objc_selector \*)"title", "\@16\@0:8", (void
>   \*)_I_PhotoVaultAssetCollection_title}}

>   };

>   struct \_objc_method {

>   struct objc_selector \* \_cmd;

>   constchar \*method_type;

>   void  \*_imp;

>   };

【SEL】

上面是PhotoVaultAssetCollection的实例方法列表，从上面暴力将char\*强转成SEL其实也能看出来SEL其实就是char\*。

【IMP】

其中类似(void \*)_I_PhotoVaultAssetCollection_title的为函数指针，即IMP，比如——

>   static MySize
>   \_I_PhotoVaultAssetCollection_sizeZero(PhotoVaultAssetCollection \* self,
>   SEL \_cmd) {

>       MySize size;

>       size.width = 0;

>       size.height = 0;

>       return size;

>   }

可以看到，这个IMP函数里默认有两个参数——self和_cmd，就是我们在OC方法里常用到的self和_cmd（self不用说了，_cmd是当前的消息选择器，用的比较少），编译器帮我们识别了self和_cmd，所以我们可以在方法里直接用这两个对象，特别的，在构造方法里——

>   static instancetype
>   \_I_PhotoVaultAssetCollection_initAssetCollectionWithLocalIdentifier_title_(PhotoVaultAssetCollection
>   \* self, SEL \_cmd, NSString \*localIdentifier, NSString \*title) {

>       // self = [super
>   initAssetCollectionWithLocalIdentifier:localIdentifier];

>       // \_title = title;

>       if (self = ((PhotoVaultAssetCollection \*(\*)(__rw_objc_super \*, SEL,
>   NSString \*))(void \*)objc_msgSendSuper)((__rw_objc_super){(id)self,
>   (id)class_getSuperclass(objc_getClass("PhotoVaultAssetCollection"))},
>   sel_registerName("initWithLocalIdentifier:"), (NSString \*)localIdentifier))
>   {

>           (\*(NSString \*\*)((char \*)self +
>   OBJC_IVAR_\$_PhotoVaultAssetCollection\$_title)) = title;

>           return self;

>       }

>       return \__null;

>   }

这是一个有意思的东西，为什么我们在构造函数里要这么写：

>   \- (instancetype)initAssetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                    title:(NSString \*)title {

>       if (self = [super initWithLocalIdentifier:localIdentifier]) {

>           \_title = title;

>       }

>       returnself;

>   }

如果这么写会怎样？

>   \@implementation PhotoVaultAssetCollection

>   \- (instancetype)initAssetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                 title:(NSString \*)title {

>       NSLog(\@"before super self = %\@", self);

>       PhotoVaultAssetCollection \*hehe = [super
>   initWithLocalIdentifier:localIdentifier];

>       NSLog(\@"after super self = %\@", self);

>       if (self) {

>           \_title = title;

>           NSLog(\@"hehe = %\@", hehe);

>           NSLog(\@"self = %\@", self);

>           returnself;

>       }

>       returnnil;

>   }

>   \- (NSString \*)description {

>       return [NSString stringWithFormat:\@"PhotoVaultAssetCollection: title =
>   %\@, id = %\@, %p", self.title, self.localIdentifier, self];

>   }

>   \@end

>   int main(int argc, constchar \* argv[]) {

>       \@autoreleasepool {

>           PhotoVaultAssetCollection \*object = [PhotoVaultAssetCollection
>   assetCollectionWithLocalIdentifier:[[NSUUID UUID] UUIDString]
>   title:\@"Default Album”];

>           NSLog(\@"object = %\@", object);

>       }

>       return0;

>   }

结果：

>   2018-08-31 20:32:10.293 hehe.out[98989:12234390] before super self =
>   PhotoVaultAssetCollection: title = (null), id = (null), 0x7fa105e00b10

>   2018-08-31 20:32:10.293 hehe.out[98989:12234390] after super self =
>   PhotoVaultAssetCollection: title = (null), id =
>   57DEB600-93C5-483F-BD20-6A6733F9C839, 0x7fa105e00b10

>   2018-08-31 20:32:10.293 hehe.out[98989:12234390] hehe =
>   PhotoVaultAssetCollection: title = Default Album, id =
>   57DEB600-93C5-483F-BD20-6A6733F9C839, 0x7fa105e00b10

>   2018-08-31 20:32:10.293 hehe.out[98989:12234390] self =
>   PhotoVaultAssetCollection: title = Default Album, id =
>   57DEB600-93C5-483F-BD20-6A6733F9C839, 0x7fa105e00b10

>   2018-08-31 20:32:10.293 hehe.out[98989:12234390] object =
>   PhotoVaultAssetCollection: title = Default Album, id =
>   57DEB600-93C5-483F-BD20-6A6733F9C839, 0x7fa105e00b10

（我猜的哈）你会发现，hehe、self是同一个东西，在调用init方法时，self的空间已经分配好了，调用super的init实际上就是把self的空间里属于super部分的成员变量填充好，在这个例子中，多次调用super的结果都是一样的。而
\_title=title 这句话，在转换成C语言后为：

>   (\*(NSString \*\*)((char \*)self +
>   OBJC_IVAR_\$_PhotoVaultAssetCollection\$_title)) = title;

>   extern"C"unsignedlongint OBJC_IVAR_\$_PhotoVaultAssetCollection\$_title
>   \__attribute_\_ ((used, section ("__DATA,__objc_ivar"))) =
>   \__OFFSETOFIVAR__(struct PhotoVaultAssetCollection, \_title);

    可以发现，其实成员变量_title是通过指针的偏移量来确定位置的，这和Objc的[Non
Fragile ivars](https://www.jianshu.com/p/3b219ab86b09)性质也有一定的关联，

综上，我们还是老老实实按照苹果推荐的写法写，至于为什么不想java那样：

>   \- (instancetype)initAssetCollectionWithLocalIdentifier:(NSString
>   \*)localIdentifier

>                                                    title:(NSString \*)title {

>       self = [super initWithLocalIdentifier:localIdentifier];

>       \_title = title;

>       returnself;

>   }

那是因为init族方法允许返回nil，也就是说，允许因某个基类创造失败导致子类也创建失败，所以，如果子类发现基类返回nil，也应该立即返回nil，表示初始化失败，如果我们不判空，很明显会访问无效的内存——

>   Segmentation fault: 11
