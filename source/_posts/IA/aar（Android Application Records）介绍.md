1.  aar 包是 android studio 下打包android工程中 src、res、lib 等资源后生成的 aar
文件，aar 包导入其他app 后，其他 app 可以引用 aar 包中的源码和资源文件

2. 与 jar 包的区别：

    
jar包只包含了class文件与清单文件 ，一般不包含资源文件，如图片等所有res中的文件。所以如果构建一个简单的类库（工具库），使用jar包即可。但如果构建的库含有UI或者AndroidManifest.xml等文件，那么使用
aar 包将是一个很好的选择。

    aar 包包含的文件：

-   /AndroidManifest.xml (mandatory)

-   /classes.jar (mandatory)  （jar 包）

-   /res/ (mandatory)

-   /R.txt (mandatory)

-   /assets/ (optional)

-   /libs/\*.jar (optional)

-   /jni//\*.so (optional)

-   /proguard.txt (optional)

-   /lint.jar (optional)

    要使aar包包含 proguard.txt， 需要在创建 aar 的module 加入（
假设proguard-rules.pro 为 proguard 文件），否则 aar 包不会包含 proguard 文件

defaultConfig {

    consumerProguardFiles '[proguard-rules.pro](http://proguard-rules.pro)'

}

jar包是属于java的，并不来源于eclipse或android studio，aar是属于Android
Studio的，必须由Android Studio来支持。

在Android Studio，jar包能做的，aar都能做。

3. 生成 aar 包

![](media/996e44daeee96e705e33adeb25eabeb8.png)

![](media/2f2b13dc8fa34571b0ebbeca906c3656.png)

在新 module 中添加完代码之后

![](media/cbb988a9d1cae36a47cf5bbc82ff6f0a.png)

![](media/51dd0d3bb32f84f8fb0fdee996a37c0d.png)

    生成 aar 包的同时也会生成 jar 包

![](media/9f3c8a10aaaed6d1b33669f4a0064de2.png)

4. 其他 app 引用 aar 包（三种方式）

（1）直接在 app 的 libs 目录下添加

![](media/28908108004a57f524185ee47a9c75e4.png)

   使用这种方法需要在 app 的 build.grade 中添加

repositories {

    flatDir {

        dirs 'libs'

    }

}

dependencies {

    compile(name: 'libchargescreen-debug', ext: 'aar')

    aar 中引入的包也需要加上（必须要做，否则 app 不能通过编译或者crash）

}

![](media/b3313e91146feab4f65afffe31553cb7.png)

(2) 新建一个 module

![](media/11beade6cb76f43fd7f3254fb3326842.png)

![](media/1cf26669279584dc67b7ab7f408500c7.png)

找到你要引入的aar包

![](media/6838ebb8bb6e29da77f368dbd1a9a67f.png)

点击 Finish 后，可以在项目中找到新 module

![](media/6a4a401852ac107911b888c8c0f6ee70.png)

在app build.gradle 添加配置

dependencies {

    compile project(':libchargescreen-debug')

    aar 中引入的包也需要加上 (必须要做, 否则app不能通过编译或者crash)

}

（3）网络加载

   
上面两种方式引用aar本质都是本地加载，另一种方式是通过网络加载。由于网络加载涉及到发布到mavenCentral托管的问题这里不做讨论。

引用的 aar 可以在 app 中找到，目录如下

![](media/11186420349ba949e914d42b4a4b36c3.png)

5. 生成 aar 包注意的一些细节：

  （1）代码精简，特别是 AndroidManifest.xml、gradle 和 Proguard
文件，不必要的设置或者属性都去掉。

  （2）aar 提供的接口简单，尽量少记 flurry，尽量不记 Preference 和不配
remoteConfig.

   (3)如果提供的 aar
包是混淆过后的，那么提供给外部使用的接口一定要做防混淆处理。

  （4）为了避免有时发生引用错误的资源，在创建 aar 时资源名最好以模块名开头。
android 在引用资源或类时，类中都会 import
资源或类所在的包，使用的资源来自引用包内的资源。而在写程序时我们往往不会写
import 语句，如果不对 aar 中的资源名加以区分，项目很可能就会引用错误的资源。

  （5）对其他本地库的依赖类型使用 provided。

   (6)对于 gradle，manifest，proguard
等文件，必须明白每一行的含义，做到配置精简准确。这个特别重要，因为有些配置看起来不起眼但会造成很严重的后果。比如 proguard
含有 -dontobfuscate 将会不混淆输入的类文件。在创建 aar
时，代码最开始往往是从项目中拷贝过来的，一般程序员会更注重代码而忽略了配置，等到如果是因为配置不对而程序出问题了那将会花大量的时间去查原因。为了避免这种时间上的浪费，所以在创建
aar 时一定得把所有配置重新过一遍。

   这里分享一篇有关 proguard
的文章：*http://www.cnblogs.com/0616--ataozhijia/p/3723967.html*

6. 对其他本地库的依赖类型使用 provided 的原因

（1）android studio dependencies的六种依赖

**Compile **

compile 是对所有的 build type以及favlors都会参与编译并且打包到最终的apk文件中。

**Provided **

Provided是对所有的build
type以及favlors只在编译时使用，类似eclipse中的external-libs,只参与编译，不打包到最终apk。

**APK **

只会打包到apk文件中，而不参与编译，所以不能再代码中直接调用 jar 或 aar
中的类或方法，否则在编译时会报错

**Test compile **

Test compile 仅仅是针对单元测试代码的编译编译以及最终打包测试 apk
时有效，而对正常的debug 或者 release apk 包不起作用。

**Debug compile **

Debug compile 仅仅针对 debug 模式的编译和最终的 debug apk 打包。

**Release compile **

Release compile 仅仅针对 Release 模式的编译和最终的Release apk打包。

（2）对其他本地库的依赖类型使用 provided 的原因

如果app直接在libs目录下引用aar,
而app和aar都依赖于相同的其他lib，那么将编译不通过，原因是 [Android Studio
com.android.dex.DexException: Multiple dex files
define(重复引用包)](http://blog.csdn.net/hyr83960944/article/details/41825087)

出现重复引用包的情况时，解决办法有三种：

     一、 app和aar都依赖于相同的其他lib，app对其依赖类型为非
provided，aar对其依赖类型为provided（建议使用）

     二、把app和aar共同依赖的lib 库构建成一个module，app和aar直接引用这个module

     三、把aar构建成一个module, app 通过引用aar所构建成的modulel的方式来引用aar 

7. app 和 aar 出现相同的配置时，android studio编译时用哪个配置？

    一个 app 引用 aar 后, 能对 app 的最终配置产生影响的地方有 aar，main src,
productFlavors, buildTypes. 若是这四个处有相同的配置，android studio 通过 merge
来决定 app 的配置，merge 的优先级从低到高为：libraries/dependencies -\> main src
-\> productFlavors -\> buildTypes.

   但是，manifest 的配置在 merge 的时候可能产生冲突。

For instance:

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme1”/\>

merging with the following declaration will not generate a conflict:

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:screenOrientation=”landscape/\>

same for

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme1”/\>

merging with the following declaration will not generate a conflict, since the
“theme” attribute defined in both elements have the same value.

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme1”

android:screenOrientation=”landscape/\>

however,

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme1”/\>

merging with the following declaration will generate a conflict, since the
“theme” attribute defined in both elements does not have the same value.

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme2”

android:screenOrientation=”landscape/\>

总结 manifest merge时起冲突的原因：app
在不止一处地方对同一个属性进行了配置，且配置的参数不同。

不要急，解决办法还是有的。android studio 提供了强大的工具 tools 来解决这一问题。

针对上面 merged 的时候引起的冲突，把

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme2”

android:screenOrientation=”landscape/\>

改为

\<activity

  android:name=”com.foo.bar.ActivityOne”

android:theme=”\@theme2”

android:screenOrientation=”landscape"

tools:replace="theme”/\>

虽然 tools 提供了merge manifest 时引起冲突的解决方法，但写好 app，特别是 aar 的
manifest 才是王道。

8. 参考资料

<https://guides.codepath.com/android/Building-your-own-Android-library#creating-a-new-android-library>

<http://tools.android.com/tech-docs/new-build-system/user-guide/manifest-merger>
