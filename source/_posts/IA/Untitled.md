解析艺术般的控件—RecyclerView

0. 引言

对于RecyclerView，想必大家都非常熟悉，在日常开发过程中会经常使用。这一充满艺术性的控件自Google推 出以来就逐渐取代了ListView。

这篇文章并不会讲RecyclerView怎么用，而是通过分析RecyclerView内部的运行机制，以便能恰当、正 确的使用RecyclerView。

研究一个现有的控件，先看看这个控件的设计目的是什么。其实就是看看RecyclerView是干什么的。官方对 RecyclerView的介绍是很简短的一句话：

A ﬂexible view for providing a limited window into a large data set. 一个用来为大量数据集合提供有限窗口的灵活的视图。

其中关键的有两点：

providing a limited window into a large data set ﬂexible

针对于这两点我们可以看看RecyclerView的整体设计。

1. RecyclerView的整体设计

1.1 RecyclerView数据展示的设计思路

之前提到，其设计目的的第一点就是展示大量数据。其实RecyclerView在这一点上和ListView等控件具有相同 的设计思路，都是使用了设计模式中的适配器模式。

附上适配器模式简介 https://www.jianshu.com/p/9d0575311214

RecyclerView中的 Adapter 一般来说需要负责以下几部分的工作：

创建View和ViewHolder，后者作为整个复用机制的跟踪单元。 把具体位置的Item和ViewHolder进行绑定，并存储相关的信息。 通知RecyclerView数据变化，支持局部的更新，在提高效率的同时也有效地支持了动画。 Item点击事件的处理。 多类型布局的支持。

首先呢，来看一张巨丑无比简单明了的结构图：

Adapter

首先RecyclerView是一个 ViewGroup ，它和我们常用的各种Layout一样，是用来装很多子View的容器，那么 它里面装的那些View是怎么来的呢？其实是来自 ViewHolder 中的 itemView 的。那么 ViewHolder 是从哪里 生成的呢？显示的数据又是在哪里设置的呢？这就是 Adapter 的作用，它根据要展示数据的内容和类型，生成 相应的 ViewHolder ，并对相应的View进行设置，从而展示出来。

如果从数据源出发就是， Adapter 将要展示的数据根据其内容和类型，转化成对应的 ViewHolder 并对其进 行设置，然后RecyclerView把 ViewHolder 中的itemView展示出来。

如果把这个图套用到适配器模式中，RecyclerView就是其中的 Client ，ViewHolder就是 Target ，Adapter自 然就是 Adapter ，Data就是 Adaptee 。

目标角色（Target）：定义Client使用的与特定领域相关的接口。 客户角色（Client）：与符合Target接口的对象协同。 被适配角色（Adaptee)：定义一个已经存在并已经使用的接口，这个接口需要适配。 适配器角色（Adapter) ：适配器模式的核心。它将对被适配Adaptee角色已有的接口转换为目标角色 Target匹配的接口。对Adaptee的接口与Target接口进行适配.

这个结构其实不通过源码也可以看出，在使用RecyclerView的时候也可以体会到。其中被应用于展示的 ViewHolder 通过通过实现Adapter中的 onCreateViewHolder(ViewGroup parent, int viewType); 创 建，再通过 onBindViewHolder(VH holder, int position); 绑定数据生成itemView。

设计目的中的第一点我们清楚了，那么我们来看第二点。

1.2 RecyclerView ﬂexible的设计思路

在研究和探讨这个问题的之前我们需要具体化ﬂexible。那么RecyclerView有哪些地方体现出了ﬂexible？个人拙 见有以下几点：

布局 动画 装饰

这些大家基本也都知道。那么我们分别看它在每个功能点上面的设计：

1.2.1 LayoutManager（布局）

LayoutManager 一般需要负责以下几部分的工作：

负责View的摆放，可以是线性、宫格、瀑布流式或者任意类型，而RecyclerView不知道也不关 心这些，这是LayoutManager的职责。 对于滚动事件的处理，RecyclerView负责接收事件，但是最终还是由LayoutManager进行处理 滚动后的逻辑，因为只有它在知道View具体摆放的位置。

细心的同学应该在上图中发现，在ViewHolder到RecyclerView的箭头上有三个点，其实就是暗示了这其中还有 很多的猫腻!

还是先上一张巨丑无比简单明了的图：

LayoutManager

RecyclerView布局十分灵活，是因为RecyclerView将自己的布局策略全权交给了LayoutManager。仔细阅读源 码还可以发现，就连View的添加，都是通过LayoutManager完成的。LayoutManager所做的事情就是拿到 ViewHolder中的itemView，然后根据LayoutManager中定义的布局策略，对itemView进行布局，然后添加到 RecyclerView中。

因此使用者可以根据自己的需要，自定义布局策略，而这里系统提供好了三种布局策略，线性布局，网格布局 和瀑布流布局。一般情况下这三种已经满足了我们的需求。如果不能，用户可以自定义布局策略。

1.2.2 ItemAnimator（动画）

RecyclerView作为一种展示大量数据的视图控件，难免会遇到数据变化的情况。例如添加，删除，更改等。当 这些事情发生的时候，往往会通过动画来体现这种变化。那么在RecyclerView中便提供了一种非常灵活的动画 机制。

同样先上一张巨丑无比简单明了的图：

ItemAnimator

首先，达到数据改变触发动画，我们通常使用 Adapter 中的 notifyXXX 方法即可。但是其内部是如何工作的 呢？

其实 notify 系列的方法可以看作是发出一个事件，在这里 Adapter 和 RecyclerView 的工作原理，是一个 典型的观察者模式。

RecyclerView 是观察者， Adapter 是被观察者，在设置Adapter的时候RecyclerView订阅观察事件，当 Adapter中的数据发生改变的时候通知RecyclerView。然后RecyclerView接到通知之后进行了很多处理。并触 发重新布局。在布局过程中又经过一系列处理，将这些动画的信息存储到 ViewInfoStore 中。在布局结束的 时候由 ViewInfoStore 统一处理并通过 CallBack 中的方法调用 ItemAnimator 中的方法执行动画。

另外，RecyclerView对于动画的处理采用了 Predictive 的方式，除了当前已经在RecyclerView布局中的 View（实线框部分），它还需要知道在屏幕意外的信息（虚线框部分），这样在H被删除的时候，它才能够对 J-K进行上移动画，并把原来不在屏幕内的L上移到可视范围之内。

删除H

RecyclerView动画的灵活性是通过 ItemAnimator 实现的，可以通过继承 ItemAnimator ，然后实现里面的 方法，来实现各种各样的动画效果。

推荐Github上的一个RecyclerView的ItemAnimator开源库，感兴趣的话可以参考参考。 https://github.com/wasabeef/recyclerview-animators

1.2.3 ItemDecoration（装饰）

ItemDecoration 主要用来装饰每一个itemView，比如给每个item添加分割线。 主要有以下几个方法：

1. public void onDraw(Canvas c, RecyclerView parent, State state) {

2. //在画Item之前。

3. }

4. public void onDrawOver(Canvas c, RecyclerView parent, State state) {

5. //在画Item之后

6. }

7. public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {

8. // 设置边距

9. outRect.set(left, top, right, bottom);

10. }

用一张图来解释一下这三个方法

ItemDecoration

设置边距,上图中的灰色区域就是变局，分别代表左、上、右、下的边界。 上图中红色999的便签，就是在onDrawOver中画的。 上图中红色五角星背景，则是onDraw画的。

1.3 回收复用机制

结合之前的内容，再将RecyclerView的复用结构补充上。又一张巨丑无比简单明了的图。

回收复用结构图

看一下图中的 Recycler 和 RecyclerViewPool 。这两个可能都不熟悉，那么对这两个类进行一个简单的介 绍：

Recycler

原文：A Recycler is responsible for managing scrapped or detached item views for reuse.

译文：Recycler负责管理报废或分离的项目视图，从而实现View的复用。

RecyclerViewPool

原文：RecycledViewPool lets you share Views between multiple RecyclerViews.

译文：RecycledViewPool可以让你在多个RecyclerView之间分享视图

Recycler作为回收站，item的回收和复用都是由Recycler来控制的。那他是如何来处理的呢？

回收示意图

我们来看上面这张图。图中展示了RecyclerView复用itemView的机制。其中 #、1、2、3、4 分别代表：

“#”：代表 LayoutManager 在处理布局时，需要从RecyclerView中获得每一个item的View，再对子View 完成布局并添加到RecyclerView上。 “1”: Scrap 主要用于对于屏幕内的ChildView的缓存，缓存中的ViewHolder不需要重新Bind，缓存时机 是在onLayout的过程中，并且用完即清空。属于一级缓存。 “2”: Cache 代表当前已经不可见但并没有从布局中移除的View。属于二级缓存，默认size为2。 “3”: ViewCacheExtension 是留给开发者自定义的缓存池。属于第三级缓存。官方并没有给出默认示 例。 “4”: RecycledViewPool 是最终的缓存池，也就是第四级缓存。 RecycledViewPool 提供按照不同 Type缓存不同View的能力，每个type的ViewHolder默认存储5个。

三级缓存

上面这个图，稍微说明一下。

1.当RecyclerView需要更新数据的时候，包括 add 、 move 、 remove 、 change 操作，如果当前数据 在可视范围之内，就会直接从 Scrap 中获取。

2.上下小幅度的滑动的时候，就需要用到 cache 中缓存的View了。

3.如果大幅度滚动， cache 中缓存数据不够用，或者调用了 notifyDataSetChanged 后，需要重新布 局时，这时候就会调用 Pool 中的缓存。

4.值得注意的是， Scrap 和 cache 的数据，是不需要重新绑定的。

下面将通过滑动来具体说明RecyclerView的缓存机制是怎样运行的。

1.3.1 Scrap(一级缓存)

之前提到，Scrap是为了屏幕内 item 快速复用而存在，Scrap有以下两个好处：

RecyclerView（ViewGroup）具有两次 onLayout() 过程，第二次 onLayout() 中直接使用第 一次 onLayout() 缓存的 View，而不必再创建， 当有 add 、 move 、 remove 、 change 操作时，如果当前数据在可视范围之内，就会直接从 Scrap中获取。

Scrap

上图中的 mAttachedScrap 的size大小为3，因为图中有3个可见的item。

1.3.2 Cache(二级缓存)

二缓是通过 position 来匹配相应的 ViewHolder 的，这里的 position 指的是 RecyclerView 预测的、可能进入屏 幕的 item 的 position，它是由当前屏幕滑动方向和可见的 item 位置来共同决定的。例如：屏幕向下滑动，那 么可能进入屏幕的 item 的 position 就是当前可见第一个 item 的 position - 1；屏幕向上滑动，那么可能进入屏 幕的 item 的 position 就是当前可见的最后一个 item 的 position + 1。这样说起来可能有些模糊，举个例子：

Cache

以上述状态来说，如果屏幕上滑，那么预测下一个可能出现在屏幕上的 item 的 position 可能是 4（也就是 Item1『E(layoutPosition:4)』）；而如果屏幕下滑，预测的下一个出现在屏幕上的 item 的 position 是 0（也就 是 Item『A(layoutPosition:0)』）。然后通过将 position 用于与 mCacheViews 中的 ViewHolder 的 layoutPosition 做比较，如果相同则返回该 ViewHolder。

来看动图：

1.

屏幕上滑：

可以看到 target mCacheView position 由 0 变成了 4。与此同时，mCachedViews 将可能出现在屏幕上的 item 的位置从原有的位置调整为 ArrayList 的最后一位。

2.

屏幕下滑：

屏幕下滑

屏幕下滑的话，target mCachedView position 由 4 变成了 0。与此同时，mCachedViews 内部也会做相应的 调整。

1.3.3 RecycledViewPool(四级缓存)

RecycledViewPool性质：

1.

2.

3.

内部维护了一个 SparseArray SparseArray key 为 ViewHolder 的 ViewType，这说明每一套 ViewHolder 都具有自己的缓存数 据 SparseArray value 为 ScrapData 类型，ScrapData 就是关键的缓存数据了，其数据结构简略如 下：

1. static class ScrapData {

2. ArrayList<ViewHolder> mScrapHeap = new ArrayList<>();

3. int mMaxScrap = 5;

4. // ...

5. }

由此可见，针对每一种 ViewHolder，RecycledViewPool 都会维护一个默认大小为 5 的 ArrayList 来用做缓存 它们，当然，这里还需要提及的一点是，ArrayList 的默认大小被限制为 5，但是这个值是可以通过 RecycledViewPool#setMaxRecycledViews(viewType, max) 来替换的，比如想换成大一点的 10、20， 都是可以的（这也是该数据类型为 ArrayList 而不是数组的原因之一）。

二缓 mCachedViews 和四缓 RecyclerViewPool ，都是为了即将给即将入屏的 item 复用而存在。可能有小伙 伴疑惑了，既然意义相同，为何不是只有二缓就足够了，还要多一个四缓来更复杂？缘由在于：可以由开发者 通过 RecycledViewPool#putRecycledView(ViewHolder) 主动向RecycledViewPool内填充数据，技术上可 以实现多个 RecyclerView 通过 RecyclerView#setRecycledViewPool(RecycledViewPool) 共用同一个 RecyclerViewPool

下面看一下当大幅度下滑时，RecyclerView是如何复用的，来看下面的例子：

1. 初始状态

初始状态

可以看到，刚开始屏幕显示的item有：

『C(layoutPosition:2)』 『D(layoutPosition:3)』 『E(layoutPosition:4)』 『F(layoutPosition:5)』的一点点

mCachedViews里存储的是：

『A(layoutPosition:0)』 『B(layoutPosition:1)』 『G(layoutPosition:6)』

2. C滑出屏幕，F完全进入

这时我们把屏幕向下滑动，考虑一下，当『C(layoutPosition:2)』滑出屏幕，会把『C(layoutPosition:2)』存入 mCachedViews，那么此时mCachedViews的组成是什么呢？『A(layoutPosition:0)』将何去何从呢？我们来 看：

C滑出屏幕，F完全进入

可以从动图里看到，当『C(layoutPosition:2)』滑出屏幕时，『C(layoutPosition:2)』进入了mCachedViews 里，此时mCachedViews的存储变成了：

『B(layoutPosition:1)』 『C(layoutPosition:2)』 『G(layoutPosition:6)』

『A(layoutPosition:0)』则进入了 RecycledViewPool ，存储了起来。

3. G滑入屏幕

再接着往下看，『G(layoutPosition:6)』后面的是『H(layoutPosition:7)』，但是 mCachedViews 里并没有 『H(layoutPosition:7)』这个item，那么当『G(layoutPosition:6)』滑入屏幕时，按照预想，会提前把 『H(layoutPosition:7)』存入 mCachedViews ，那么存入的『H(layoutPosition:7)』是通过 onCreateViewHolder 创建的吗？

G滑入屏幕

从动图中看到，『H(layoutPosition:7)』出现时只触发了 BindViewHolder ，而 mRecyclerPool 中存储的A消 失了，即：

『H(layoutPosition:7)』是通过复用 mRecyclerPool 中存储的『A(layoutPosition:0)』的ViewHolder，重 新绑定数据后再存储到 mCachedViews 里，为即将滑入屏幕作准备。

通过上述分析可以得出结论：

在 mCachedViews 达到存储上限后，为了存放即将滑出屏幕并且不在缓存中的ViewHolder，会 把一个ViewHolder存放到RecycledViewPool； RecycledViewPool每个type存储数量是5个，type一致就可以被复用，会调用 onBindViewHolder重新绑定布局。

2. 初始化-绘制之前的准备

我们正常使用RecyclerView时一般都是这样的：

1. RecyclerView recyclerView = findViewById(R.id.recycler_view);

2. GridLayoutManager gridLayoutManager;

3.

4. gridLayoutManager = new GridLayoutManager(ActivityMain.this, 2);

5. gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

6. //设置LayoutManager

7. recyclerView.setLayoutManager(gridLayoutManager);

8.

9. //设置Adapter

10. recyclerView.setAdapter(adapter);

11. adapter.notifyDataSetChanged();

最基本的就是设置 LayoutManager ，设置 Adapter ，如果在设置完成之前 Adapter 中我们没有填充数据， UI界面是不会发生变化的。如果我们设置完成之后填充数据就需要调用 notifyDataSetChanged() 方法请求 绘制刷新。

RecyclerView与Adapter

主要说明一下 setAdapter(Adapter adapter) ，这一步是必须的，不设置 Adapter ，那么数据跟UI就无法 关联起来，也就谈不上什么绘制了，绘制都是要基于数据的。

setAdapter(Adapter adapter) 方法源代码：

1. //为RecyclerView设置Adapter，为RecyclerView提供⼦itemView

2. public void setAdapter(Adapter adapter) {

3. // 设置布局绘制不被冻结，以便刷新界⾯UI

4. setLayoutFrozen(false);

5. //设置Adapter内部实现

6. setAdapterInternal(adapter, false, true);

7. //请求重新布局

8. requestLayout();

9. }

一共调用了三个方法，第一个和最后一个方法都很好理解，我们重点要说 setAdapterInternal 方法，说之 前我要知道 setAdapter 主要是适配数据，当我们设置的数据有变化时，UI界面能够及时刷新。要实现这样的 逻辑，就必须存在数据变更观察者和被观察者，就是 RecyclerView 要观察Adapter的数据变化。

Adapter 类中有一个被观察者：

1. private final AdapterDataObservable mObservable = new AdapterDataObservable(); //默认就创建好了

RecyclerView 中有一个观察者：

1. private final RecyclerViewDataObserver mObserver = new RecyclerViewDataObserver(); //默认就创建好了

在 RecyclerView 的 setAdapterInternal 方法中使观察者和被观察者建立联系：

1. //设置Adapter内部实现

2. private void setAdapterInternal(Adapter adapter, boolean compatibleWithPreviou s, boolean removeAndRecycleViews) {

3. if (mAdapter != null) {

4. //移除之前观察者和被观察者之间的关系

5. mAdapter.unregisterAdapterDataObserver(mObserver);

6. //移除之前RecyclerView和Adapter之间的关系

7. mAdapter.onDetachedFromRecyclerView(this);

8. }

9. ...

10. if (adapter != null) { //新的Adapter不为空

11. //注册观察者跟被观察者之间的关系

12. adapter.registerAdapterDataObserver(mObserver);

13. //注册RecyclerView与Adapter之间的关系

14. adapter.onAttachedToRecyclerView(this);

15. }

16. if (mLayout != null) {

17. //通知LayoutManager Adapter发⽣了变化

18. mLayout.onAdapterChanged(oldAdapter, mAdapter);

19. }

20. ...

21. }

如果在创建 Adapter 时候我们就绑定了数据就不需要调用 notifyDataSetChanged() 来绘制UI界面了。如果 是之后绑定数据那就要手动调用 notifyDataSetChanged() 方法绘制。

Adapter.notifyDataSetChanged() 方法源码阅读:

1. public final void notifyDataSetChanged() {

2. mObservable.notifyChanged();

3. }

我们接着看 AdapterDataObservable.notifyChanged() 方法实现：

1. //mObservers是保存观察者的⼀个ArrayList

2. public void notifyChanged() {

3. for (int i = mObservers.size() - 1; i >= 0; i--) {

4. //通知所有的观察者数据发送改变了

5. mObservers.get(i).onChanged();

6. }

7. }

我们再来看 RecyclerViewDataObserver.onChanged() 方法：

1. Override

2. public void onChanged() {

3. ......

4. //Adapter⽬前没有待更新或者正在更新的操作才可以重新绘制

5. if (!mAdapterHelper.hasPendingUpdates()) {

6. requestLayout();

7. }

8. }

从上面我们可以看到一个 Adapter 中可能存在多个观察者，那就意味着 Adapter 可以同时被多 个 RecyclerView 公用。

调用 requestLayout() 方法后就会走RecyclerView的绘制流程了。

3. 绘制流程

1. c.lang.Object

2.

3.

4.

↳ android.view.View

↳ android.view.ViewGroup

↳ android.support.v7.widget.RecyclerView

看一下 RecyclerView 的继承关系，可以看到，它其实是一个 ViewGroup，而同时又是一个View，既然是一个 View，那么就不可少的要经历 onMeasure()、onLayout()、onDraw() 这三个方法。实际上，RecyclerView 将 onMeasure()、 onLayout() 交给了 LayoutManager 去处理，这样做的好处就是职责清晰，还可以实现 给 RecyclerView 设置不同的 LayoutManager 就可以自由的使用列表、网格、瀑布流等常规的布局，还可以自 定义LayoutManager来满足特殊的列表需求，以实现不同的显示效果，因为 onMeasure()、onLayout() 都不 同了。 上面说RecyclerView 的 measure 和 layout 都是交给了 LayoutManager 去做的，来看一下为什么：

以下分析均以LinearLayoutManager为例

3.1 onMeasure()

由于源码过于复杂，这里选择使用伪代码进行分析。

1. Override

2. protected void onMeasure(int widthSpec, int heightSpec) {

3. if (mLayout == null) {

4. //layoutManager没有设置的话，直接⾛default的⽅法，所以会为空⽩

5. defaultOnMeasure(widthSpec, heightSpec);

6. return;

7. }

8. if (mLayout.mAutoMeasure) {

9. final int widthMode = MeasureSpec.getMode(widthSpec);

10. final int heightMode = MeasureSpec.getMode(heightSpec);

11. final boolean skipMeasure = widthMode == MeasureSpec.EXACTLY

12. && heightMode == MeasureSpec.EXACTLY;

13.

14. mLayout.onMeasure(mRecycler, mState, widthSpec, heightSpec);

15. //如果测量模式是MeasureSpec.EXACTLY，即⾃身宽⾼为固定dp或者match_parent时，则 跳过measure过程直接⾛layout

16. if (skipMeasure || mAdapter == null) {

17. return;

18. }

19. if (mState.mLayoutStep == State.STEP_START) {

20. //mLayoutStep默认值是 State.STEP_START

21. dispatchLayoutStep1();

22. //执⾏完dispatchLayoutStep1()后是State.STEP_LAYOUT

23. }

24. ...

25. //真正执⾏LayoutManager绘制的地⽅

26. dispatchLayoutStep2();

27. //执⾏完后是State.STEP_ANIMATIONS

28. ...

29. //宽⾼都不确定的时候，会绘制两次

30. // if RecyclerView has non-exact width and height and if there is at l east one child

31. // which also has non-exact width & height, we have to re-measure.

32. if (mLayout.shouldMeasureTwice()) {

33. ...

34. dispatchLayoutStep2();

35. ...

36. }

37. } else {

38. if (mHasFixedSize) {

39. mLayout.onMeasure(mRecycler, mState, widthSpec, heightSpec);

40. return;

41. }

42. ...

43. mLayout.onMeasure(mRecycler, mState, widthSpec, heightSpec);

44. ...

45. mState.mInPreLayout = false; // clear

46. }

47. }

一些关键的伪代码已经放上了，可以感觉到 onMeasure 的过程还是相对比较复杂的，测量时又分了两种情 况，一种是RecyclerView的宽高设置为 wrap_content 的情况，另外一种是宽和高的测量值是 绝对值 。

为了便于理解，我们只考虑宽和高的测量值是 绝对值 的情况来分析，即自身宽高为 固定dp 或 者 match_parent 。

注意这个方法：

1. Override

2. protected void onMeasure(int widthSpec, int heightSpec) {

3. ...

4. mLayout.onMeasure(mRecycler, mState, widthSpec, heightSpec);

5. ...

6. }

上面的判断很多，但是无论哪一种判断成立，最终都会执行到 mLayout.onMeasure() 方法中，而这个 mLayout 就是一个 LayoutManager 对象，也就是说，虽然RecyclerView重写了 onMeasure ，但是实际进 行测量的是LayoutManager。

这里通过 mLayout.onMeasure() 测量的是RecyclerView的宽高，不涉及子view的测量。如果 RecyclerView宽高设置的是wrap_content，则会先返回一个最小的宽高值，在后续测量完子view以后再 重新确定自身宽高。

1. @Override

2. protected void onMeasure(int widthSpec, int heightSpec) {

3. ...

4. final boolean skipMeasure = widthMode == MeasureSpec.EXACTLY

5. && heightMode == MeasureSpec.EXACTLY;

6.

7. mLayout.onMeasure(mRecycler, mState, widthSpec, heightSpec);

8. //如果测量模式是MeasureSpec.EXACTLY，即⾃身宽⾼为固定dp或者match_parent时，则跳过 后续measure过程直接⾛layout

9. if (skipMeasure || mAdapter == null) {

10. return;

11. }

12. ...

13. }

接下来有一个判断，如果宽和高的测量值是绝对值时，RecyclerView在执行完自身的测量以后就直接跳过了 onMeasure 方法，没有执行子View的测量，因此onMeasure我们就暂时分析到这里。

那这里可能有疑问了，当RecyclerView宽高设置为wrap_content时，会在onMeasure中执行完子view测 量再给自身确定宽高。但是当我们给RecyclerView设置绝对值大小的时候，如果没有执行onMeasure的 后续方法，那么子View没有测量，会造成空白的情况，但是实际上，子View仍可以正常绘制出来。这 个问题会在后面介绍onLayout流程时解答。（onLayout里会执行子View的绘制）

3.2 onLayout()

1. @Override

2. protected void onLayout(boolean changed, int l, int t, int r, int b) {

3. TraceCompat.beginSection(TRACE_ON_LAYOUT_TAG);

4. dispatchLayout();

5. TraceCompat.endSection();

6. mFirstLayoutComplete = true;

7. }

RecyclerView中的 onLayout() 方法很简短，主要看一下 dispatchLayout()

dispatchLayoutStep 分为三个步骤：

dispatchLayoutStep1: Adapter的更新; 决定该启动哪种动画; 保存当前View的信息; 如果有必 要，先跑一次布局并将信息保存下来。 dispatchLayoutStep2: 真正对子View做布局的地方。 dispatchLayoutStep3: 为动画保存View的相关信息; 触发动画; 相应的清理工作。

1. void dispatchLayout() {

2. ...

3. if (mState.mLayoutStep == State.STEP_START) {

4. dispatchLayoutStep1();

5. ...

6. dispatchLayoutStep2();

7. } else if (mAdapterHelper.hasUpdates() || mLayout.getWidth() != getWidth() || mLayout.getHeight() != getHeight()) {

8. // First 2 steps are done in onMeasure but looks like we have to run a gain due to

9. // changed size.

10. ...

11. dispatchLayoutStep2();

12. } else {

13. ...

14. }

15. dispatchLayoutStep3();

16. }

这里的代码就比较好理解了，并且上面提到的问题也就迎刃而解了，当我们给RecyclerView设置固定的宽高的 时候， onMeasure 在测量完自身后是直接跳过了子View测量的执行，为什么子View仍然能绘制出来。

这里可以看到，如果 onMeasure 没有执行， mState.mLayoutStep == State.STEP_START 就成立，所以仍 然会执行 dispatchLayoutStep1() 、 dispatchLayoutStep2() ，也就对应的会绘制子View。

而后面的注释也比较清楚，由于我们在Layout的时候改变了宽高，也会导致 dispatchLayoutStep2() ，也就 是子View的重新绘制。

如果上面情况都没有，那么 onLayout() 的作用就仅仅是 dispatchLayoutStep3() ，而 dispatchLayoutStep3() 方法的作用除了重置一些参数，外还和执行动画有关。

下面详细的看一下 dispatchLayout() 中的这个三个方法：

3.2.1 dispatchLayoutStep1()

可以看到在执行 dispatchLayoutStep1() 之前，首先要判断 mState.mLayoutStep == State.STEP_START 是否成立， mLayoutStep 的默认值其实就是 State.STEP_START ，并且每次绘制流程 结束后，会重置为 State.STEP_START 。

方法 dispatchLayoutStep1() 就不过多的分析了，直接放上官方对于 dispatchLayoutStep1() 的注释， 这个注释已经很好的解释了这个方法的作用。

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

/** * The first step of a layout where we; * - process adapter updates * - decide which animation should run * - save information about current views * - If necessary, run predictive layout and save its information */ /** * 1.处理Adapter的更新 * 2.决定哪些动画需要执⾏ * 3.保存当前View的信息 * 4.如果必要的话，执⾏预测布局并且保存它的信息 */

3.2.2 dispatchLayoutStep2()

接下来就是LayoutManager执行子View测量与布局的地方 dispatchLayoutStep2() 。

1. private void dispatchLayoutStep2() {

2. ...

3. //重写的getItemCount⽅法

4. mState.mItemCount = mAdapter.getItemCount();

5. ...

6. // Step 2: Run layout

7. mState.mInPreLayout = false;

8. mLayout.onLayoutChildren(mRecycler, mState);

9. ...

10. }

这里注意两个地方，第一个 mAdapter.getItemCount() ，可以看到我们每次重写Adapter时重写的 getItemCount 方法用到的地方了。

onLayoutChildren()

第二个，可以看到 mLayout.onLayoutChildren() 这个方法，为什么说RecyclerView将子View的布局交给 了 LayoutManager ，这里就是最有力的体现，可以看到，这里将RecycleView内部持有的 Recycler 和 state 传给了 LayoutManager 的 onLayoutChildren 方法，单从方法的名字其实就可以看出它的大致作 用。这里我们进入 LayoutManager 里看一看。

1. public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.Stat e state) {

2. // layout algorithm:

3. // 找寻锚点

4. // 1) by checking children and other variables, find an anchor coordinate and an anchor

5. // item position.

6. // 两个⽅向填充，从锚点往上，从锚点往下

7. // 2) fill towards start, stacking from bottom

8. // 3) fill towards end, stacking from top

9. // 4) scroll to fulfill requirements like stack from bottom.

10. // create layout state

11. ...

12. //mStackFromEnd默认是false，除⾮⼿动调⽤setStackFromEnd()⽅法，两个都会false，异 或则为false

13. mAnchorInfo.mLayoutFromEnd = mShouldReverseLayout ^ mStackFromEnd;

14. // calculate anchor position and coordinate

15. //计算锚点的位置和偏移量

16. updateAnchorInfoForLayout(recycler, state, mAnchorInfo);

17. ...

18. //mLayoutFromEnd默认为false

19. if (mAnchorInfo.mLayoutFromEnd) {

20. ...

21. } else {

22. //正常绘制流程的话，先往下绘制，再往上绘制

23. // fill towards end

24. updateLayoutStateToFillEnd(mAnchorInfo);

25. ...

26. fill(recycler, mLayoutState, state, false);

27. ...

28. // fill towards start

29. updateLayoutStateToFillStart(mAnchorInfo);

30. ...

31. fill(recycler, mLayoutState, state, false);

32. ...

33. if (mLayoutState.mAvailable > 0) {

34. ...

35. // start could not consume all it should. add more items towards e nd

36. updateLayoutStateToFillEnd(lastElement, endOffset);

37. ...

38. fill(recycler, mLayoutState, state, false);

39. ...

40. }

41. }

42. ...

43. layoutForPredictiveAnimations(recycler, state, startOffset, endOffset);

44. //完成后重置参数

45. if (!state.isPreLayout()) {

46. mOrientationHelper.onLayoutComplete();

47. } else {

48. mAnchorInfo.reset();

49. }

50. mLastStackFromEnd = mStackFromEnd;

51. }

这里虽然已经尽量删减了很多代码，但是还是很多…但是其实原理理解起来还是比较容易的。简单的说其实可 以总结缩略为：

1.

2.

3.

先寻找页面当前的锚点 以这个锚点为基准，分别向上和向下填充 填充完后，如果还有剩余的可填充大小，再填充一次

布局锚点

这里以垂直布局来说明， mAnchorInfo 为布局锚点信息，包含了子控件在Y轴上起始绘制偏移量 coordinate ，ItemView在Adapter中的索引位置 position 和布局方向 mLayoutFromEnd ——这里是指 start、end方向。

这部分代码的功能就是：确定布局锚点，以此为起点向开始和结束方向填充 ItemView ，如图所示

布局锚点

上图的圆形红点就是通过 updateAnchorInfoForLayout 方法中计算出来的填充锚点位置，但是无论 哪种情况， fill() 方法都会调用两次。 （1）是屏幕显示的位置在RecyclerView的最底部，那么就只有填充方向为 formEnd 时才会填充 （2）是屏幕显示的位置在RecyclerView的顶部，那么就只有填充方向为 formStart 时才会填充 （3）应该是最常见的，屏幕显示的位置在RecyclerView的中间，那么填充方向为 formEnd 和 formStart 时都能正常填充

1. int fill(RecyclerView.Recycler recycler, LayoutState layoutState, RecyclerView .State state, boolean stopOnFocusable) {

2. while (...&&layoutState.hasMore(state)) {

3. ...

4. layoutChunk(recycler, state, layoutState, layoutChunkResult);

5. ...

6. }

7. }

fill() 方法中最重要的就是 layoutChunk() 方法。

layoutChunk()方法

1. void layoutChunk(RecyclerView.Recycler recycler, RecyclerView.State state, Lay outState layoutState, LayoutChunkResult result) {

2. // next⽅法很重要

3. View view = layoutState.next(recycler);

4. ...

5. addView(view);

6. ...

7. // 对ChildView进⾏测量

8. measureChildWithMargins(view, 0, 0);

9. ...

10. // 对ChildView进⾏布局

11. layoutDecoratedWithMargins(view, left, top, right, bottom);

12. ...

13. }

首先是 next 方法，千万不要因为这个 next 短小不起眼，就认为不重要，只能说这个方法很重要。

1. View next(RecyclerView.Recycler recycler) {

2. ...

3. //重要，从recycler获得View,recycler是被RecyclerView持有

4. final View view = recycler.getViewForPosition(mCurrentPosition);

5. mCurrentPosition += mItemDirection;

6. return view;

7. }

注意 View view = recycler.getViewForPosition(mCurrentPosition) ，终于看到RecyclerView中缓存 策略的身影，下面从流程上简单的看一下 getViewForPosition() 这个方法。

1. public View getViewForPosition(int position, boolean dryRun) {

2. return tryGetViewHolderForPositionByDeadline(position, dryRun, FOREVER_NS) .itemView;

3. }

4. /**

5. * Attempts to get the ViewHolder for the given position, either from the Recy cler scrap,

6. * cache, the RecycledViewPool, or creating it directly.

7. */

8. /**

9. * 注释写的很清楚，从Recycler的scrap，cache，RecyclerViewPool中获取ViewHolder,或者直 接创建ViewHolder

10. */

11. @Nullable

12. ViewHolder tryGetViewHolderForPositionByDeadline(int position, boolean dryRun, long deadlineNs) {

13. ...

14. //经过各种判断，尝试从Recycler的scrap，cache，RecyclerViewPool中获取ViewHolder， 如果没有获取到，则创建⼀个新的ViewHolder

15. holder = mAdapter.createViewHolder(RecyclerView.this, type);

16. ...

17. }

getViewForPosition 会调用 tryGetViewHolderForPositionByDeadline 方 法， tryGetViewHolderForPositionByDeadline 方法的注释写的很清楚:

会尝试从 Recycler 的 scrap 、 cache 、 RecyclerViewPool 获取 ViewHolder ，或者直接创建 ViewHolder

这里我们也看到了我们最熟悉的 mAdapter.createViewHolder() 方法！

next() 方法分析结束后，其实就比较容易了。

取到View后会通过 addView() 的方法把View添加到RecyclerView中。

ChildView的测量

1. //对ChildView进⾏测量

2. public void measureChildWithMargins(View child, int widthUsed, int heightUsed) {

3. ...

4. //设置分割线中的回调⽅法

5. final Rect insets = mRecyclerView.getItemDecorInsetsForChild(child);

6. ...

7. if (shouldMeasureChild(child, widthSpec, heightSpec, lp)) {

8. //⼦View的测量

9. child.measure(widthSpec, heightSpec);

10. }

11. }

从这个方法里我们看到了子View的测量，当然还有一个需要我们注意的地方那就 是 mRecyclerView.getItemDecorInsetsForChild(child)

1. Rect getItemDecorInsetsForChild(View child) {

2. ...

3. final int decorCount = mItemDecorations.size();

4. for (int i = 0; i < decorCount; i++) {

5. mTempRect.set(0, 0, 0, 0);

6. //getItemOffsets()实现分割线的回调⽅法！

7. mItemDecorations.get(i).getItemOffsets(mTempRect, child, this, mSt ate);

8. insets.left += mTempRect.left;

9. insets.top += mTempRect.top;

10. insets.right += mTempRect.right;

11. insets.bottom += mTempRect.bottom;

12. }

13. ...

14. return insets;

15. }

其实可以看到这里在测量子View的时候是将我们实现自定义分割线重写的 getItemOffsets 方法。这里其实也 就可以理解了自定义分割线的原理就是在子View的测量过程前给上下左右加上自定义分割线所对应设置给这个 child的边距。

设置itemView的边距(padding)

ChildView的布局

测量完成后，紧接着就调用了 layoutDecoratedWithMargins(view, left, top, right, bottom) 对子 View完成了布局。

1. //对ChildView进⾏布局

2. public void layoutDecoratedWithMargins(View child, int left, int top, int righ t, int bottom) {

3. final LayoutParams lp = (LayoutParams) child.getLayoutParams();

4. final Rect insets = lp.mDecorInsets;

5. //layout

6. child.layout(left + insets.left + lp.leftMargin, top + insets.top + lp.top Margin, right - insets.right - lp.rightMargin, bottom - insets.bottom - lp.bot tomMargin);

7. }

在取到子View之后，LayoutManager通过addView添加到RecyclerView中，再经过对子View测量和布 局，子View就显示了出来。

3.2.3 dispatchLayoutStep3()

最后再简单的看一下 dispatchLayoutStep3()

1. private void dispatchLayoutStep3() {

2. //重置参数

3. if (mState.mRunSimpleAnimations) {

4. // Step 3: Find out where things are now, and process change animation

s.

5. // traverse list in reverse because we may call animateChange in the l oop which may

6. // remove the target view holder.

7. // Step3: 需要做动画时，找出ViewHolder现在的位置，并且处理改变动画，反向遍历列表 ，因为我们可能在循环中调⽤animateChange，这可能会删除⽬标视图持有者。

8. ...

9. // Step 4: Process view info lists and trigger animations

10. // Step3: 处理视图信息列表和触发动画

11. mViewInfoStore.process(mViewInfoProcessCallback);

12. }

13. //成功回调

14. }

到此！！！对于RecyclerView的绘制流程其实我们有了一个大体的了解，总结一下关键点：

1.

2.

3.

4.

5.

RecyclerView 是将绘制流程交给 LayoutManager处理，如果没有设置不会测量子View； 绘制流程是区分正向绘制和倒置绘制； 绘制是先确定锚点，然后向上绘制，向下绘制， fill() 至少会执行两次，如果绘制完还有剩 余空间，则会再执行一次 fill() 方法； LayoutManager获得 View 是从 RecyclerView 中的 Recycler.next() 方法获得，涉及到 RecyclerView的缓存策略，如果缓存没有拿到，则走我们自己重写的 onCreateViewHolder() 方法； 如果RecyclerView宽高没有写死， onMeasure() 就会执行完子View的 measure 和 layout 方 法， onLayout() 仅仅是重置一些参数；如果宽高固定，子View的 measure 和 layout 会延后 到 onLayout() 中执行。

3.3 onDraw()

RecyclerView的 draw 过程可以分为２部分来看：

1.

2.

RecyclerView负责绘制所有decoration； ItemView的绘制由ViewGroup处理，这里的绘制是android常规绘制逻辑。

下面来看看RecyclerView的 draw() 和 onDraw() 方法：

1. @Override

2. public void draw(Canvas c) {

3. super.draw(c);

4.

5. final int count = mItemDecorations.size();

6. for (int i = 0; i < count; i++) {

7. mItemDecorations.get(i).onDrawOver(c, this, mState);

8. }

9. ...

10. }

11.

12. @Override

13. public void onDraw(Canvas c) {

14. super.onDraw(c);

15.

16. final int count = mItemDecorations.size();

17. for (int i = 0; i < count; i++) {

18. mItemDecorations.get(i).onDraw(c, this, mState);

19. }

20. }

测量布局绘制三个步骤只有 onDraw 看起来最简单了， onDraw 方法如果不要需要一些特殊的效果，在 TextView、ImageView这些控件中已经绘制完了，没必要在RecyclerView中去处理子View的绘制了。

4. ViewHolder的生命周期

当我们使用RecyclerView时，打交道最多的就是Adapter中的ViewHolder，我们需要花大量的时间实现 ViewHolder，因此，了解ViewHolder的生命周期非常重要。

4.1 LayoutManager请求RecyclerView提供指定position的View

ViewHolder是和View相绑定的，同时它也是整个复用框架的跟踪单元。在RecyclerView体系中，一般用Cache 和Recycled Pool对ViewHolder进行缓存，当LayoutManager向RecyclerView请求位于某个Position的View时， Recycled View会先去Cache中寻找，如果找到，那么直接返回；如果找不到，那么再去Recycled Pool中寻 找，下面就是整个寻找过程的几种情况：

命中 Cache

这种情况下，不会调用Adapter的 onCreateViewHolder 或者 onBindViewHolder 方法：

命中Cache

Cache 不存在， Recycled Pool 也不存在

这种情况下，会调用 Adapter 的 onCreateViewHolder 方法，让它提供一个对应 viewType 的 ViewHolder ，我们在其中建立 ViewHolder 和 View 之间的关联。

Cache不存在，Recycled Pool也不存在

Cache 不存在， Recycled Pool 存在

这种情况下，会调用 Adapter 的 onBindViewHolder 方法，让它把这个从Pool中存在的 ViewHolder 绑定到 相应的 position ，建立 ViewHolder 和 View 之间的关联。

Cache不存在，Recycled Pool存在

4.2 LayoutManager找到对应位置的View

LayoutManager通过 addView 方法把之前找到的View添加进RecyclerView，RecyclerView通过 onViewAttachToWindow(VH viewHolder) 方法，通知Adapter这个ViewHolder所关联的itemView已经被添 加到了布局当中。

onViewDetachedFromWindow(VH holder)

Called when a view created by this adapter has been detached from its window. 当适配器创建的view（即列表项view）被窗口分离（即滑动离开了当前窗口界面）就会被调用

onViewAttachedToWindow(VH holder)

Called when a view created by this adapter has been attached to a window. 当列表项出现到可视界面的时候调用

AddView

4.3 LayoutManager请求RecyclerView移除某一个位置的View

4.3.1 普通情况

当LayoutManager发现不再需要某一个position的View时，它会通知RecyclerView，RecyclerView通过 onViewDetachFromWindow(VH viewHolder) 通知Adapter和它绑定的itemView被移除了。同时， RecyclerView判断它是否能够被缓存，假设能够被缓存，那么它会先被放到Cache当中，在Cache中又会判断 它内部是否有需要转移到Recycled Pool中的ViewHolder，在放入回收池之后，通过 onViewRecycled(VH viewHolder) 方法通知Adapter它被回收了。

普通情况

4.3.2 特殊情况

在上面的普通的情况中， onViewDetachFromWindow(VH viewHolder) 是立即被回调的。然而在实际当中， 由于我们需要对View的添加、删除做一些过渡动画，这时候，我们需要等待ItemAnimator进行完动画操作之 后，才做detach和recycle的逻辑，这一过程对于LayoutManager是不可见的，LayoutManager并不知道有动画 正在发生，在执行完Layout后，对于LayoutManager来说它们的位置就已经固定了。（即RecyclerView和 LayoutManager看到的View是不一样的，稍后会详细解释）

特殊情况

4.4 ViewHolder的销毁

在一般情况下，我们不会去销毁ViewHolder，而是把它放入到缓存当中，除非出现以下两种情况。

4.4.1 ViewHolder所绑定的itemView当前状态异常

在放入Recycled Pool时，会去检查itemView的状态是否正常。这一操作的目的主要是为了避免出现诸如此类的 情况：想象当前itemView正在执行动画，此时它正在渐渐消失，如果此时把它放入到回收池中，那么当另一个 位置的item需要复用它时，之前的动画还没有结束，会导致它在新的位置出现并伴随着渐渐消失的动画。

当可能会出现上面的情况后，Recycled Pool会先通过Adapter的 onFailedToRecycled(VH viewHolder) 告 诉它出现了异常的情况，由Adapter的实现者通过返回值来决定是否仍然要把它放入到Recycled Pool，默认是 返回false，也就是不放入，那么这个ViewHolder就会被销毁了。

ItemView当前状态异常

当然，以上情况只会出现在没有正确使用动画的时候，如果出现这样的情况，不得不把该ViewHolder销毁时， 就会影响性能。

解决办法是：通过实现ItemAnimator来对item作动画，ItemAnimator知道正确的ViewHolder生命周期， 可以正确的处理动画，动画结束时，可以安全的回收ViewHolder。

4.4.2 Recycled Pool中已经没有足够的空间

Recycled Pool的空间并不是无限大的，因此，如果没有足够的空间存放要被回收的ViewHolder，那么它也会 被销毁。

Recycled Pool中已经没有足够的空间

造成这种情况的一般是动画引起的，例如，我们调用了 notifyItemRangeChanged(0, getItemCount()) 方 法，这时候为了进行淡入淡出的动画，那么我们就需要创建两倍的ViewHolder，出现这种情况时一般有两种解 决方法：

只通知具体发生变化的Item( notifyItemChanged(3) ) 通过 pool.setMaxRecycledViews(type, count) 改变回收池的大小。

5. 其他重要组件

5.1 ChildHelper

对于ChildHelper的作用是：

原文：Provide a virtual children list to layoutmanager. 翻译：为LayoutManager提供虚拟子列表

下面我们就首先看一下为什么需要它。

5.1.1 解决什么问题？

我们看下面这种情况，假如LayoutManager想要移除一个View，而ItemAnimator又希望给这一移除的操作增加 一个动画，那么这时候就会产生冲突，View到底应该怎么办，为此，RecyclerView通过ChildHelper来把它们隔 离开。

冲突产生

形象的表示

5.1.2 解决问题的方法

当RecyclerView收到LayoutManager要求改变布局的请求时，它并不是直接去更改ViewGroup，而是让 ChildHelper和ItemAnimator去协调，并由它来操作ViewGroup。

解决问题的方法

最明显的例子是，假如我们当前列表中状态为0,1,2,3，此时我们移除了position=0的Item，这时候假如删除的 动画还没有完成，那么LayoutManager和RecyclerView的 getChildAt(0) 返回值将会不同，因为在 LayoutManager并不清楚ChildHelper的存在，在它看来，position=0的Item已经被移除了。

1. layoutManager.getChildAt(0); //return 1;

2. recyclerView.getChildAt(0); //return 0;

5.2 AdapterHelper

而AdapterHelper所解决的问题和ChildHelper类似，ChildHelper是处理View的，而AdapterHelper用来跟踪 ViewHolder的，其作用为：

原文

Tracks ViewHolder positions Virtual Adapter for LayoutManager

翻译

跟踪ViewHolder的位置 LayoutManager的虚拟适配器

说起来可能比较抽象，我们用下面这种图理解一下，当我们移动某个Item并且它的onLayout方法还没有完成， 那么Adapter和Layout的position是不相同的：

不同的视角

5.3 ItemTouchHelper

如果需要RecyclerView支持侧滑删除、拖动排序这种操作，RecyclerView已经帮我们提供了实现的接口，通过 重写 ItemTouchHelper 的方法，就可以实现上面提到的那些操作。

Drag & Drop Swipe to dismiss

可以参考：RecyclerView 进阶：使用 ItemTouchHelper 实现拖拽和侧滑删除

5.4 DiﬀUtil

DiﬀUtil是最近版本中推出的一个工具。主要是帮助RecyclerView提升刷新效率的问题。我们举一个例子来说这 个问题。

搜索

这样的搜索功能是很常见的。每次输入不同的文字，都要给出该文字相对应的搜索热词推荐。如果直接使 用 notifyDataSetChanged() ，就会导致整个RecyclerView发生 RequestLayout 。我们都知 道 RequsetLayout 会引起整个View树重新遍历一边Measure和Layout。这样非常消耗性能。而且， RecyclerView重新加载时，只会从RecyclerViewPool中拿缓存的Item。RecyclerViewPool默认只会缓存5个 Item。剩下的Item都需要重新走Create和inﬂate。之后他们还要重写计算宽高，重新计算布局。这个过程非常 耗时。

notifyDataSetChanged

为了提高性能，我们就可以使用DiﬀUtil来对比两组数据，得到数组A切换到数组B的最少移动步骤。

解释Myers算法

这个算法其实属于“动态规划”，感兴趣的同学可以了解一下。

在使用DiﬀUtil得到变化之后，我们可以调用RecyclerView的局部刷新机制。这样不需要 RequestLayout 。刷 新效率非常高。 那DiﬀUtil的对比数据的效率怎么样呢。 这里有一组官方的数据：

100 items and 10 modiﬁcations: avg: 0.39 ms, median: 0.35 ms 100 items and 100 modiﬁcations: 3.82 ms, median: 3.75 ms 100 items and 100 modiﬁcations without moves: 2.09 ms, median: 2.06 ms 1000 items and 50 modiﬁcations: avg: 4.67 ms, median: 4.59 ms 1000 items and 50 modiﬁcations without moves: avg: 3.59 ms, median: 3.50 ms 1000 items and 200 modiﬁcations: 27.07 ms, median: 26.92 ms 1000 items and 200 modiﬁcations without moves: 13.54 ms, median: 13.36 ms

基本都在16ms内，效率还是很高的。

5.5 类图

这样就把RecyclerView的重要组件都介绍完了，最后放一张类图

RecyclerView类图

6. 一些关于RecyclerView的Tips

1.

想要RecyclerView的宽高不随着内容的变化而变化，可以使用如下方法来提高性能

1. recyclerView.setHasFixedSize(true);

2.

如果想提高RecyclerView的滑动流畅性，可以适度增加Cache的大小，默认大小是2。但是也不能太 大，如果太大，会影响初始化的效率。

1. recyclerView. setItemViewCacheSize(5);

3.

如果多个RecyclerView显示的Item一样。比如：

这种情况，就可以使用公共缓存池。

1. int type0 = 0;

2. int type1 = 1;

3. int type2 = 2;

4.

5. RecyclerViewPool mPool = new RecyclerViewPool();

6. mPool.setMaxRecycledViews(type0, 10);

7. mPool.setMaxRecycledViews(type1, 10);

8. mPool.setMaxRecycledViews(type2, 10);

4.

onBind Position != final ，使用 holder.getAdapterPostion()

如果我们像下面这样，在 onBindViewHolder 中绑定了监听：

1. public void onBindViewHolder(final ViewHolder, final int position) {

2. holder.itemView.setOnClickListener(new View.onClickListener) {

3. @Override

4. public void onClick(View view) {

5. removeAtPostion(position);

6. }

7. }

8. }

由于Item会被添加、删除、移动，因此，我们在 onBindViewHolder 中获得位置，并不一定是当前的位置，例 如像下面这样：

1. onBindViewHolder(holder, 5);

2. notifyItemMoved(5, 15);

3. holder.itemView.callOnClick();

那么就会得到错误的位置，这时候应当使用 holder.getAdapterPostion() 来保证能够得到预期的结果。

5.

Payloads

通过 onBindViewHolder 中的List payloads，我们可以指定在bind的时候只更新某一部分的信息，而不是全部 更新。

比如ItemView中包括ImageView、TextView，但是变化的只有TextView，可以指定更新TextView的内 容。

6.

Adapter position and Layout position

就像我们前面在 AdapterHelper 中讨论的那样，在某些时刻，Adapter Position和Layout Position并不相等， 我们应当根据情况选择需要使用哪个，Adapter Position对应数据所处的位置，而Layout Position则对应当前 View的所处的位置。

7.参考文献

1.RecyclerView ins and outs - Google I/O 2016 - https://www.youtube.com/watch?v=LqBlYJTfLP4&t=772s

2.RecyclerView 知识梳理(1) - https://www.jianshu.com/p/21a1384df9a1

3.RecyclerView源码解析 - https://www.jianshu.com/p/c52b947fe064

4.RecyclerView剖析 - https://blog.csdn.net/qq_23012315/article/details/50807224

5.Demo下载地址 - https://github.com/jokermonn/RecyclerViewVisualization

8.课后提问

Q1. RecyclerView的四级缓存都有哪些？它们的区别是什么？

第一级缓存: Scrap 主要用于对于屏幕内的ChildView的缓存，缓存中的ViewHolder不需要重新Bind， 缓存时机是在onLayout的过程中，并且用完即清空。 第二级缓存: Cache 代表当前已经不可见但并没有从布局中移除的View，默认size为2。 第三级缓存: ViewCacheExtension 是留给开发者自定义的缓存池，官方并没有给出默认示例。 第四级缓存: RecycledViewPool 是最终的缓存池，也就是第四级缓存。 RecycledViewPool 提供按 照不同Type缓存不同View的能力，每个type的ViewHolder默认存储5个。

其中 Scrap 和 Cache 中的数据是不需要重新绑定的，而 RecycledViewPool 需要重新绑定。

Q2. 假设有一个无限长度的RecyclerView列表，刚打开时没有缓存任何ViewHolder，那么向下滑动的过程中是 先回收再复用，还是边回收边复用？

如果是LinearLayoutManager布局的RecyclerView，那么向下滑动过程中，其复用是先把滑出屏幕的item 存储到 Cache 里，当Cache存储满后，会把自己缓存的一个ViewHolder存储到 RecycledViewPool ， 如果此时再有同类型的item需要创建时，直接复用 RecycledViewPool 中缓存的那一个。（从这个角 度看，可以认为是先回收再复用，但其实如果RecycledViewPool为空，想复用也没有可复用的 ViewHolder）

但是在向下滑动的过程中，并不会先把 RecycledViewPool 填满再进行复用，一般对于只有一种 ViewType的LayoutManager布局的RecyclerView来说， RecycledViewPool 最多只会存储一个。（从 这个角度看，可以认为是边回收边复用）

Q3. 为什么我们在onBindViewHolder中获得position，并不一定是预期的position？

一般来说有两种情况： 一种是由于在添加、删除item等操作时，LayoutManager和ItemAnimator争抢View，比如 LayoutManager想移除View，而ItemAnimator想对该View作动画，为了解决这种矛盾情况， RecyclerView提供了ChildHelper来解决这个问题，ChildHelper提供了虚拟的Child列表给 LayoutManager，让LayoutManager认为item已经被删除了，而对于RecyclerView来说，这个item还存 在，只不过在作着消失动画。

另外一种情况是当item有添加、移动或删除操作时，LayoutManager还没有完成onLayout()，此时 RecyclerView和LayoutManager看到的child列表也是不同的，而此时如果你想要发生改变后的数据，直 接用position将会无法得到预期的position，而RecyclerView提供了AdapterHelper来解决这个问题，用 viewholder.getAdapterPosition()可以得到预期的position。