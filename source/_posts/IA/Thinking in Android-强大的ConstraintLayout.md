分享的内容大部分是基于以下文章，里面内容比较详细，适合仔细研读，有兴趣的同学可以看看。

-   《Android新特性介绍，ConstraintLayout完全解析》  
      
    <https://blog.csdn.net/guolin_blog/article/details/53122387>

-   《ConstraintLayout终极秘籍（上）》  
      
    <http://blog.chengyunfeng.com/?p=1030>

-   《ConstraintLayout终极秘籍（下）》  
      
    <http://blog.chengyunfeng.com/?p=1031>

\<此处有一个Thinking in Android——强大的ConstraintLayout.key文件，请到Thinking in
Android —— 强大的ConstraintLayout.resources文件中查看\>
