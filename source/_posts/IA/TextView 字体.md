1.  影响字体的属性

-      android:typeface  支持三种    sans  serif  monospace

-      android:fontFamily  支持较多，和api版本相关   \>=api 16

-      android:textStyle     常规，粗体，斜体，粗斜体

          （1）typeface :  支持三种： sans serif  monispace

          （2）fontFamily : api 16 引入的新的属性  支持的字体较多，跟api版本相关

          （3）textStyle:   常规，粗体，斜体，粗斜体   xml
中粗斜体字体尝试不行，可以在java文件中设置 Typeface.BOLD_ITALIC

     2.不同版本字体设置的方式：

           typeface 和 fontFamily 同时设置时：

           api \>= 16 设置了 fontFamily, 设置typeface不生效

TextView.java 相关代码

**api16之前设置字体内部实现方式：**

**private void **setTypefaceByIndex(**int **typefaceIndex, **int **styleIndex) {

    Typeface tf = **null**;

    **switch **(typefaceIndex) {

        **case **SANS:

            tf = Typeface.*SANS_SERIF*;

            **break**;

        **case **SERIF:

            tf = Typeface.*SERIF*;

            **break**;

        **case **MONOSPACE:

            tf = Typeface.*MONOSPACE*;

            **break**;

    }

    setTypeface(tf, styleIndex);

}

Typeface.java

**static **{

    DEFAULT         = create((String) **null**, 0);

    DEFAULT_BOLD    = create((String) **null**, Typeface.*BOLD*);

    SANS_SERIF      = create(**"sans-serif"**, 0);

    SERIF           = create(**"serif"**, 0);

    MONOSPACE       = create(**"monospace"**, 0);

    sDefaults = **new **Typeface[] {

            DEFAULT,

            DEFAULT_BOLD,

            create((String) **null**, Typeface.*ITALIC*),

            create((String) **null**, Typeface.*BOLD_ITALIC*),

    };

}

**api16之后设置字体的方式：**

**private void **setTypefaceFromAttrs(String
familyName, **int **typefaceIndex, **int **styleIndex) {

    Typeface tf = **null**;

    **if **(familyName != **null**) {

        tf = Typeface.*create*(familyName, styleIndex);

        **if **(tf != **null**) {

            setTypeface(tf);

            **return**;

        }

    }

    **switch **(typefaceIndex) {

        **case SANS**:

            tf = Typeface.*SANS_SERIF*;

            **break**;

        **case SERIF**:

            tf = Typeface.*SERIF*;

            **break**;

        **case MONOSPACE**:

            tf = Typeface.*MONOSPACE*;

            **break**;

    }

    setTypeface(tf, styleIndex);

}

1.  **系统支持的字体如何查看**

         Typeface create字体时会解析下面两个xml文件

      sdk/platform/android-XX/data/fonts/system_fonts.xml        4.x

          sdk/platform/android-xx/data/fonts/fonts.xml        5.x

      下面的是 api14
system_fonts.xml，android:fontFamily=“"属性值可以是\<nameset\>标签中的一种,
同一个\<nameset\>里面的name都是同一种字体（有一些例外，和android版本有关）

\<fileset\>整个是一个字体族， 通过android:textStyle= 进行选择          

\<familyset\>

    \<family\>

        \<nameset\>

            \<name\>sans-serif\</name\>

            \<name\>arial\</name\>

            \<name\>helvetica\</name\>

            \<name\>tahoma\</name\>

            \<name\>verdana\</name\>

        \</nameset\>

        \<fileset\>

            \<file\>Roboto-Regular.ttf\</file\>

            \<file\>Roboto-Bold.ttf\</file\>

            \<file\>Roboto-Italic.ttf\</file\>

            \<file\>Roboto-BoldItalic.ttf\</file\>

        \</fileset\>

    \</family\>

    \<family\>

        \<nameset\>

            \<name\>serif\</name\>

            \<name\>times\</name\>

            \<name\>times new roman\</name\>

            \<name\>palatino\</name\>

            \<name\>georgia\</name\>

            \<name\>baskerville\</name\>

            \<name\>goudy\</name\>

            \<name\>fantasy\</name\>

            \<name\>cursive\</name\>

            \<name\>ITC Stone Serif\</name\>

        \</nameset\>

        \<fileset\>

            \<file\>DroidSerif-Regular.ttf\</file\>

            \<file\>DroidSerif-Bold.ttf\</file\>

            \<file\>DroidSerif-Italic.ttf\</file\>

            \<file\>DroidSerif-BoldItalic.ttf\</file\>

        \</fileset\>

    \</family\>

    \<family\>

        \<nameset\>

            \<name\>Droid Sans\</name\>

        \</nameset\>

        \<fileset\>

            \<file\>DroidSans.ttf\</file\>

            \<file\>DroidSans-Bold.ttf\</file\>

        \</fileset\>

    \</family\>

    \<family\>

        \<nameset\>

            \<name\>monospace\</name\>

            \<name\>courier\</name\>

            \<name\>courier new\</name\>

            \<name\>monaco\</name\>

        \</nameset\>

        \<fileset\>

            \<file\>DroidSansMono.ttf\</file\>

        \</fileset\>

    \</family\>

\</familyset\>

 不同api 同一个 name 对应的字体可能不一样， 建议一般用最上面的name

  api 16

    \<family\>

        \<nameset\>

            \<name\>serif\</name\>

            \<name\>times\</name\>

            \<name\>times new roman\</name\>

            \<name\>palatino\</name\>

            \<name\>georgia\</name\>

            \<name\>baskerville\</name\>

            \<name\>goudy\</name\>

            \<name\>fantasy\</name\>

            \<name\>cursive\</name\>

            \<name\>ITC Stone Serif\</name\>

        \</nameset\>

        \<fileset\>

            \<file\>DroidSerif-Regular.ttf\</file\>

            \<file\>DroidSerif-Bold.ttf\</file\>

            \<file\>DroidSerif-Italic.ttf\</file\>

            \<file\>DroidSerif-BoldItalic.ttf\</file\>

        \</fileset\>

    \</family\>

     api 21

    \<family name="cursive"\>

        \<font weight="400" style="normal"\>DancingScript-Regular.ttf\</font\>

        \<font weight="700" style="normal"\>DancingScript-Bold.ttf\</font\>

    \</family\>

4.结论：

     pm 要求 5.0 之前用Roboto-Regular, 5.0
之后用Roboto-Medium，系统默认字体一般是Roboto-Regular

     采用下面的方式做了个测试

     添加fontFamily
系统解析不到对应字体，就成默认字体了，若能解析到就是对应字体。    

\<**TextView   android:layout_width="wrap_content" 
 android:layout_height="wrap_content"   android:text="TextView fontFamily"**

**   ** /\>

\<**TextView   android:layout_width="wrap_content" 
 android:layout_height="wrap_content"   android:text="TextView fontFamily"**

**   android:fontFamily="sans-serif-medium"**/\>

4.2                                                                            
       5.1

![](media/3b1f45073c08ab8b8a6bfbde4211b3b9.jpg)

![](media/c79c45393088f9958a86b81178478efb.png)

为什么要出现这种多对一的设计？api 14 typeface值支持三种，为什么字体会有4种？

附件： 各个api支持的字体 （有遗漏，具体可以对应上面那个xml查看）

api 14    4.0

     Roboto-Resular.ttf

     Roboto-Bold.ttf

     Roboto-Italic.ttf

     Roboto-BoldItalic.ttf

     DroidSerif-Regular.ttf

     DroidSerif-Bold.ttf

     DroidSerif-Italic.ttf

     DroidSerif-BoldItalic.ttf

     DroidSansMono.ttf

api 16  4.1 添加

     Roboto-Light.ttf

     Roboto-LightItalic.ttf

     RobotoCondensend-Regular.ttf

     RobotoCondensed-Bold.ttf

     RobotoCondensed-Italic.ttf

     RobotoCondensed-BoldItalic.ttf 

   DroidSans.ttf

   DroidSans-Bold.ttf

api21 5.0 添加      

   Roboto-Thin.ttf

   Roboto-ThinItalic.ttf

   Roboto-Medium.ttf

   Roboto-MediumItalic.ttf

   Roboto-Black.ttf

   Roboto-BlackItalic.ttf

   CutiveMono.ttf

   ComingSoon.ttf

   DancingScript-Regular.ttf

   DancingScript-Bold.ttf

   CarroisGothicSC-Regular.ttf
