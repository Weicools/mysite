  "RemoteServiceException: Bad notification posted from package \*"
总是位居Crash榜首，经过查找分析并定义为是Notification相关Resource
ID发生变化引起的，后期又通过Keep Resource ID 方式解决了NotificationToggle
的此类Crash。

**解决方案：**

  找到Notification相关Resource ID并将这些ID值固定。Gradle Build Task
本身是支持keep id的，"res/values/public_x.xml"文件就是提供给开发keep ID
的配置文件。出于某些考虑Gradle mergeResourcesTask
把这种文件过滤掉了，但是可以通过脚本使这种文件生效。

**public_x.xml 文件生效脚本：**

mergeResourcesTask
会将资源统一拷贝到一个中间目录下再处理，再处理逻辑中是识别public_x.xml文件的，但是mergeResourceTask向中间目录拷贝时过滤掉了public_x.xml文件，所以要使public_x.xml文件生效，需要将public_x.xml拷贝到中间目录下。脚本如下：

applicationVariants.all { variant -\>

    def buildType = variant.buildType.name

    def flavor = variant.flavorName

    def applicationId = variant.applicationId

    if (flavor == 'oneAppMax') {

        def taskMiddleName = variant.variantData.name.capitalize()

        def scope = variant.variantData.getScope()

        // Pin all resource ids related to notification toolbar to fix "Bad
notification posted from ..." crash

        String mergeResourcesTaskName = scope.getMergeResourcesTask().name

        def mergeResourcesTask = tasks.getByName(mergeResourcesTaskName)

        mergeResourcesTask.doLast {

            println 'mergeResourcesTask, copy(), outDir = ' +
mergeResourcesTask.outputDir

            copy {

                int i = 0

                from('src/main/res/keepids') {

                    include 'values/public.xml'

                    rename 'public.xml', (i == 0 ? "public.xml" :
"public_\${i}.xml")

                    i++

                }

                into(mergeResourcesTask.outputDir)

            }

        }

    }

}

**public_x.xml文件:**

  该文件具体指定了Resource ID 的name、type和ID值，Gradle
Build处理资源时首先查看public_x.xml文件中是否已经定义了ID,要是定义了则采用定义的值，要是没有定义则排除pulic_x.xml中的ID值然后递增赋值。public_x.xml文件名字中的x表示数字。

例，public_x.xml中定义了\<public type="layout"
name="notification_toggle_toolbar_view"
id="0x7f040000”/\>，当处理一个layout资源时首先对比name要是相同则采用文件中配置id=0x7f040000，要是不同则采用layout类型的并排除0x7f040000的累积递增值。

Resource ID 定义举例：

\<resources\>

    \<public type="layout" name="notification_toggle_toolbar_view"
id="0x7f040000"/\>

    \<public type="mipmap" name="ic_launcher" id="0x7f030000"/\>

    \<public type="drawable" name="ic_notification_toggle_home"
id="0x7f02000c"/\>

    \<public type="id" name="notification_toggle_home_text" id="0x7f0c0000"/\>

    \<public type="dimen" name="notification_toggle_icon_width"
id="0x7f0d0000"/\>

    \<public type="string" name="notify_toolbar_memory_used" id="0x7f090001"/\>

    \<public type="color" name="notify_color_bd" id="0x7f0b0000"/\>

    \<public type="style" name="notify_main_text_style" id="0x7f0f0000"/\>

\</resources\>

ID值采用了32位二进制表示，从左到右，前八位为固定值0x7f表示资源,中间8位代表资源类型，如上0x04表示layout、0x03表示mipmap，后16位表示该类资源索引值。

Gradle源码中对资源的索引采用了Hash算法，并将索引值作为了Hash映射值，所以每类资源的索引值采用了递增方式，这样也意味着在定义索引值时优先采用小值。

**声明文件id.xml：**

该文件起到声明的作用，表明Resource 有该资源。如下所示：

\<resources

    xmlns:tools="http://schemas.android.com/tools"

    tools:ignore="all"\>

    \<item type="id" name="notification_toggle_toolbar_view"/\>

    \<item type="id" name="float_window_flashlight"/\>

    \<item type="id" name="ic_launcher"/\>

    \<item type="id" name="ic_notification_toggle_flashlight_open"/\>

    \<item type="id" name="ic_notification_toggle_flashlight_close"/\>

    \<item type="id" name="ic_notification_toggle_recent"/\>

\</resources\>

修改方案：

res/keepids/values/下添加了ids.xml和public.xml，相关的Notification Resource
ID在这个两个文件中声明和定义。
