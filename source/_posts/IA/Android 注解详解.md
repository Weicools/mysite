参考链接：

Android官方文档 <https://developer.android.com/tools/debugging/annotations.html#thread-annotations>  

深入浅出Android Support
Annotation <http://www.cnblogs.com/tianzhijiexian/p/4493158.html>

概述……

最常见的注解：\@Override

**注解的本质：注解是一种类型**

JDK5.0中的类型：1、类（class）2、接口（interface）3、枚举（enum）4、注解（Annotation）

因此，注解与其他3种类型一样，都可以定义、使用，以及包含有自己的属性、方法

注解的总分类：
==============

1.元注解
========

元注解是指注解的注解，只有在自定义注解时，我们才会用到元注解。包括  

\@Retention  定义注解的保留策略（注解的生命周期）

\@Target  定义注解的作用目标

\@Document  说明该注解将被包含在javadoc中

\@Inherited四种  说明子类可以继承父类中的该注解

2.Java & Android 写好的
=======================

接下来详细讲解

3.自定义注解
============

定义注解格式：public \@interface 注解名 {定义体}

稍后有案例。

系统特供：
==========

NULL相关的注解
==============

\@NonNull

用法一：放在参数列表中

        void sayHello(\@NonNull String name) {...}

效果：由于调用方法的参数列表中使用了\@NonNull注解修饰，因此AndroidStudio将会以警告的形式提醒我们这个问题有问题

![](media/1716a129d5f32e339a244bbf7136fd0d.png)

用法二：放在函数声明的前一行

        \@NonNull

        String getName(){

           return null;

        }

效果：

![](media/32fa36b49bbf607d92d12b504e6ffbc3.png)

\@Nullable

用法：放在函数声明的前一行

效果：这可以提醒调用者，如果调用这个方法就必须要做非空判断，否则可能crash

![](media/de459a2de96fac4c0bef44440c1e2150.png)

**资源类型的注解**

以\@StringRes举例

使用效果见下面的图：调用方法时会有参数类型的提示

![](media/2ea237b9bea5f1b7c8f641d9deb94751.png)

必须要传入对应资源类型的参数

![](media/0e9f052d8043c357f856a2637cea550e.png)

如果不按对应资源类型传参，会有警告：

![](media/814bea4757c885986bf5b643913ca910.png)

哪怕是输入数字都不行：

![](media/e9dacd782d471b3e89de06649b7d5155.png)

### 资源类型注解一共有22条，如下：

\@AnimatorRes

\@AnimRes

\@AnyRes  万能牌，但必须是资源类型的，允许传入其余22种资源的任一种。

\@ArrayRes

\@AttrRes

\@BoolRes

\@ColorRes

\@DimenRes

\@DrawableRes

\@FractionRes

\@IdRes

\@IntegerRes

\@InterpolatorRes

\@LayoutRes

\@MenuRes

\@PluralsRes

\@RawRes

\@StringRes

\@StyleableRes

\@StyleRes

\@TransitionRes

\@XmlRes

线程相关的注解
==============

如果你的方法只能在指定的线程类型中被调用，那么你就可以使用以下4个注解来标注它：

\@UiThread

\@MainThread

\@WorkerThread

\@BinderThread

如果一个类中的所有方法都有相同的线程需求，那么你可以注解类本身。比如android.view.View，就被用\@UiThread标注。

关于线程注解使用的一个很好的例子就是AsyncTask，如果你在重写的doInBackground方法里尝试调用onProgressUpdate方法或者View的任何方法，IDE工具就会马上把它标记为一个错误

数值范围约束的注解 \@Size \@IntRange \@FloatRange
=================================================

举例：

如果你的参数是一个float或者double类型，并且一定要在某个范围内，你可以使用\@FloatRange注解：

public void setAlpha(\@FloatRange(from=0.0, to=1.0) float alpha) {...}

对于数据、集合以及字符串，你可以用\@Size注解参数来限定集合的大小（当参数是字符串的时候，可以限定字符串的长度）

集合不能为空: \@Size(min=1)

字符串最大只能有23个字符: \@Size(max=23)

数组只能有2个元素: \@Size(2)

数组的大小必须是2的倍数 (例如图形API中获取位置的x/y坐标数组: \@Size(multiple=2)

权限需求注解 \@RequiresPermission
=================================

如果你的方法的调用需要调用者有特定的权限，你可以使用\@RequiresPermission注解：

\@RequiresPermission(Manifest.permission.SET_WALLPAPER) 

public abstract void setWallpaper(Bitmap bitmap) throws IOException;

如果你至少需要权限集合中的一个，你可以使用anyOf属性：

\@RequiresPermission(anyOf = {    Manifest.permission.ACCESS_COARSE_LOCATION,   
Manifest.permission.ACCESS_FINE_LOCATION}) 

public abstract Location getLastKnownLocation(String provider);

如果你同时需要多个权限，你可以用allOf属性：

\@RequiresPermission(allOf = {    Manifest.permission.READ_HISTORY_BOOKMARKS,   
Manifest.permission.WRITE_HISTORY_BOOKMARKS}) 

public static final void updateVisitedHistory(ContentResolver cr, String url,
boolean real) {

要求调用父类方法的注解 \@CallSuper
==================================

如果你的API允许使用者重写你的方法，但是呢，你又需要你自己的方法(父方法)在重写的时候也被调用，这时候你可以使用\@CallSuper标注：

\@CallSuper protected void onCreate(\@Nullable Bundle savedInstanceState) {

用了这个后，当重写的方法没有调用父方法时，工具就会给予标记提示：

请求检查结果的注解 \@CheckResult
--------------------------------

如果你的方法返回一个值，你期望调用者用这个值做些事情，那么你可以使用\@CheckResult注解标注这个方法。

你并不需要微每个非空方法都进行标注。它主要的目的是帮助哪些容易被混淆，难以被理解的API的使用者。

比如，可能很多开发者都对String.trim()一知半解，认为调用了这个方法，就可以让字符串改变以去掉空白字符。如果这个方法被\@CheckResult标注，工具就会对那些没有使用trim()返回结果的调用者发出警告。

请求编译器无操作 \@Keep
=======================

被这个注解修饰的函数或者类，在代码混淆进行压缩时会被保持住。

当尝试把某个特定的函数或者类从优化操作中排除掉是非常痛苦的事情。使用这个注解将会告诉Proguard不要对指定的函数或者类进行优化操作

用于表示某个程序元素(类,方法等)已过时 \@Deprecated
==================================================

抑制编译器警告 \@SuppressWarnings
=================================

**枚举注解**

有时候，基于性能的考虑，需要用整型常量代替枚举类型，此时枚举注解可以轻松搞掂，这就是大名鼎鼎的\@IntDef
and \@StringDef

**枚举案例：**

![](media/02be8d8d645a593aa5d048a97de52de8.png)

用起来是这样：

![](media/ac4bc0ef48f80a29c02da1f02ad014da.png)

![](media/ddd55b7ddfc9d599b33735496c233aa6.png)

**注解案例：**

![](media/78ea45159dacca2922c7be864d669c85.png)

使用\@Retention(RetentionPolicy.SOURCE)告诉编译器这个新定义的注解不需要被记录在生成的.class文件中（注：源代码级别的，生成class文件的时候这个注解就被编译器自动去掉了）

用起来是这样：

![](media/719e4df777135fbbb5cfdbc64f6fbcfe.png)

![](media/a912fb536c2cbf3dae3c575e7efcc774.png)

**权衡对比：**

枚举最大的好处：类型一定安全。弊端：重量级工具

![](media/73f81383ab8f06449a8f619aefbe2b90.png)

如果直接传值，编译器就会报错

![](media/45297d5b79189d698f6b72d7fa13bf27.png)

![](media/938dddcf18c6c337179e946639d9bd6c.png)

注解的好处：比int更安全，比enum更轻量；弊端：不够安全

注解只能起到提示的作用，编译器并不会报错

![](media/58e6b24346b2b4d9643524948d67daaf.png)

如果直接传递Int值，虽然表示出红线的提醒，但是如果无视红线，依然可以运行。注意，并没有“3”对应的类型，所有运行起来一定会crash。

![](media/ca6dd578f987f37d6398ce1c70cad1c3.png)

此注解还有一个用法，要定义flag

![](media/31ec9f1f1f92961103bd23ddeb7fd2e0.png)

如果有兴趣，可以详细参考:<https://developer.android.com/intl/zh-cn/tools/debugging/annotations.html#call-super>

同类型的还有\@StringDef，使用方法类似。所不同的是它是针对字符串的。该注解一般不常用，但是有的时候非常有用，比如在限定向Activity\#getSystemService方法传递的参数范围的时候
