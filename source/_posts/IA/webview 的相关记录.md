————新的整理分割线——————

 WebView  简介：

Android  SDK  提供了 webView 组件 为了方便开发者在app
内展示网页与网页进行交互的需求。

在android 4.4 之前 ， WebView是基于 WebKit ， 在4.4 以后，开始基于 Chromium
内核， 大大提升了 webView 的性能，也对 HTML5, CSS3, JavaScript做了支持。

其实，这个改动也同时减少了 webview
的坑，在4.4之前的低版本中，你永远不知道它会发生哪些奇怪的问题。

WebView 简单使用

最简单的使用就是：

>   webView.loadUrl(url);

>   webView.loadUrl(“file://android_asset/test/html”);  // 加载 apk 中的 html
>   页面

>   webView.loadUrl(“content://com.android.htmlfileprovider.sdcard/test.html”);
>   //加载本地的HTML页面

>   //加载html的一小段内容， data 为需要展示的内容，mimeType 为展示内容的类型，
>   encoding 为字节码;

>   webView.loadData(String data, String mimeType, String encoding); //

>   // url 为一个网址， 例如 "<https://www.google.com>”

>   //常见使用：

>   webView.reload(); //重新reload，相当于刷新

>   webView.canGoBack(); //是否可以后退

>   webView.goBack(); // 后退

>   webView.canGoForward();

>   …

WebView 的状态和 缓存：

>   //类似于 activity 的生命周期

>   webView.onResume();

>   //当页面失去焦点被切换到后台不可见状态， 执行
>   onPause();,会通知内核暂停所有的动作，比如DOM解析，plugin
>   的执行，javaScript执行，视频暂停...等等

>   webView.onPause();

>   //当应用程序(存在webview)被切换到后台时，这个方法不仅仅针对当前的webview而是全局的全应用程序的webview

>   //它会暂停所有webview的layout，parsing，javascripttimer。降低CPU功耗。

>   webView.pauseTimers();

>   webView.resumeTimers();// 恢复状态

>   webView.destroy(); 

>   // 销毁webView, 此时要注意 在关闭了activity 时， 如果webView
>   的音乐或视频还在播放，必须销毁 webView

>   // 但是当 destroy时，webView 仍然绑定在activity 上， 这是因为在构建 webView
>   的时候传过去了 它context对象，

>   //所以，要先从父容器中移除 webView， 在进行destory,

>   rootLayout.removeView(webView);

>   webView.destroy();

>   webView  的清除缓存 ：

>   webView.clearCache(boolean includeDiskFiles);
>   //清理网页访问留下的缓存，由于内核缓存是全局的，因此这个方法不仅仅是针对webView
>   而是针对整个应用程序的；

>   webView.clearHistory(); // 会清除当前 webView 访问的历史记录（）

>   webView.clearFormData(); // 仅仅清除自动完成填充列表的表单数据，并不会清除
>   webView存储到本地的数据

webView 缓存的位置：

1.  当加载html页面时， webView 会在 /data/data/包名目录下生成database 与 cache
    两个文件夹；

2.  请求的 URL 记录保存在 webViewCache.db 下，
    而URL的内容则是保存在webViewCache文件夹下；

当然这需要网络权限，在 AndroidManifest  中声明.

通常我们可以根据关键字来搜索相对应内容，其实本质上，也是对用户输入的字符进行处理，最终转化为一个可以链接找得到的网址。（这里不讨论这部分）。

既然可以访问网络，我们也需要对当前加载的网页进行一些事件的处理，就需要定制一些监听。官方里提供了一些方法的接口，便于我们做这些事情：

**WebSetting**, **WebViewClient**, **WebChromeClient**,
它们可以让我们去定制一些内容。 

WebSettings 里的设置

WebSettings 是官方提供的 WebView 的管理配置的类。 当WebView
第一次创建时，内部会包含一个默认配置的集合。下面看一下常用的一些配置：

>   Private void initWebSettings () {

>   WebSetting webSettings = webView.getSettings();

>           webSettings.setAllowContentAccess(true);

>           webSettings.setAllowFileAccess(true);

>           if (android.os.Build.VERSION.SDK_INT \>= 16) {

>               webSettings.setAllowFileAccessFromFileURLs(true);

>               webSettings.setAllowUniversalAccessFromFileURLs(true);

>           }

>            // 启用应用缓存

>           webSettings.setAppCacheEnabled(true);

>           //缓存路径

>          
>   webSettings.setAppCachePath(getContext().getCacheDir().getAbsolutePath());

>           // 比较重要，一共有四种模式，在下面说明

>           webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

>           //开启数据库缓存和 DOM 缓存

>           webSettings.setDatabaseEnabled(true);

>           webSettings.setDomStorageEnabled(true);

>          
>   webSettings.setGeolocationDatabasePath(getContext().getFilesDir().toString());

>           //支持缩放,

>           webSettings.setSupportZoom(true);

>           //显示缩放按钮

>           webSettings.setBuiltInZoomControls(true);

>           webSettings.setDisplayZoomControls(false);

>           webSettings.setLoadWithOverviewMode(true);

>           webSettings.setTextZoom(100);

>           webSettings.setUseWideViewPort(true);

>           webSettings.setDefaultTextEncodingName("UTF-8”);

>           // 是否主动加载图片。其实是对4.4
>   已上做兼容，使图片加载占用的内存更少

>           if (android.os.Build.VERSION.SDK_INT \< 19) {

>               webSettings.setLoadsImagesAutomatically(false);

>           } else {

>               webSettings.setLoadsImagesAutomatically(true);

>           }

>           webSettings.setBlockNetworkImage(false);

>           // 支持运行 JS

>           webSettings.setJavaScriptEnabled(true);

>           webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

>           webSettings.setGeolocationEnabled(true);

>           webSettings.setSupportMultipleWindows(false);

>           webSettings.setSaveFormData(false);

>   webSettings.setLayoutAlgorithm(android.webkit.WebSettings.LayoutAlgorithm.NORMAL);

>   }

比较重要的一些配置：

1.      setCacheMode()

    1.  LOAD_DEFAULT:
         默认的缓存模式，在页面进行前进或后退时，如果有缓存可用并未过期，
        就会优先加载缓存，否则，从网络上获取数据;

    2.  LOAD_CACHE_ELSE_NETWORK:  
        只要有缓存便会使用，哪怕它已经过期，如果缓存不可用，会从网络上获取数据;

    3.  LOAD_NO_CACHE:  不加载缓存，只从网络获取数据;

    4.  LOAD_CACHE_ONLY:  只从缓存加载获取数据.

2.     setBlockNetworkImage(boolean flag)

    1.  是否禁止从网络上加载图片，false 表示可以从网络上加载图片。

WebViewClient 的 设置和处理：

WebViewClient 是 帮助 WebView 处理
各种通知和请求事件的，有以下几种方法会经常用到：

1.  onPageStarted(): 在webView
    开始加载页面且仅在整页加载（在某些视频网站上，点击某个视频可能不属于整页加载，故而不会调用
    onPageStarted() ）时回调。

2.  onPageFinished(): 在webView 完成一个界面的加载时会调用一次， 同样也仅在 main
    frame loading 时调用.

3.  onReceivedError() : 在页面加载错误时回调，

代码示例如下：

>   webView.setWebViewClient(new WebViewClient() {

>        \@Override

>        public void onPageStarted(WebView view, String url, Bitmap favicon) {

>              Log.e(TAG, "onPageStarted url = " + url);

>      // 可以做一些逻辑 ，例如:

>      progressBar.setVisibility(VISIBLE);

>      progressBar.setProgress(0);

>      // 判断是否可以

>        }

>        \@Override

>        public void onPageFinished(WebView view, String url) {

>              Log.e(TAG, "onPageFinished”);

>              //
>   恢复图片的主动加载，若不设置，会出现在4.4以下图片加载不出来的情况

>      // 上面的代码，其目的是对API 在19
>   以上的版本做了兼容，因为4.4以上系统在onPageFinished时再恢复图片加载时,

>      //
>   如果存在多张图片引用的是相同的src时，会只有一个image标签得到加载，这样会造成界面不对的情况，

>      // 所以4.4以上要在加载网页的时候就去主动加载图片，

>    
>    //4.4以下可以等到onPageFinished()后再去加载图片（这样会首先去加载网页的界面，省去用户等待的时间）

>              if(!webView.getSettings().getLoadImagesAutomatically()) {

>                  webView.getSettings().setLoadImagesAutomatically(true);

>              }

>      progressBar.setVisibility(GONE);

>        }

>       /\*\*

>       /\* 是否对app 内的链接进行拦截，是否有机型里的内置浏览器打开；

>       /\* 但其实，并不是这样，只要对webView 设置了setWebViewClient()
>   都会在app内 的webview打开链接

>       /\*\*

>        \@Override

>        public boolean shouldOverrideUrlLoading(WebView view,
>   WebResourceRequest request) {

>              return false;

>        }

>        \@Override

>        public void onReceivedHttpError(WebView view, WebResourceRequest
>   request, WebResourceResponse errorResponse) {

>              super.onReceivedHttpError(view, request, errorResponse);

>              Log.e(TAG, "onReceivedHttpError");

>        }

>        \@Override

>        public void onReceivedError(WebView view, WebResourceRequest request,
>   WebResourceError error) {

>              super.onReceivedError(view, request, error);

>              Log.e(TAG, "onReceivedError");         

>        }

>   });

**应注意的问题：**

1.  onPageStarted() 有时会被调用多次，存在一些网址的重定向问题,
     最好加入一些防范机制， 使某些逻辑只执行一次；

2.  在onPageStarted() 去获取 canGoBack()
    操作时，，经过一些测试，发现在某些网站，即使逻辑可以后退，得到的值仍为false，
    后来发现是因为网站加载的进度没有达到要求，在大概进度条到30%左右时，canGoBack()才会返回true;

3.   在 onPageFinished() 里去设置进度条，最好先 postDelay
    一下再做，因为，，你永远不知道在onPageFinished（）被调用时
    网站是否真的加载完成了；

4.  在 onPageFinished() 里去设置进度条时，一定要把visibility 设置为 \`GONE\`,
    若是设置为\`INVISIBILITY\`,则可能仍然会出现进度条加载到100% 后不消失的情况；

WebChromeClient  的设置与处理

WebChromeClient 是辅助 webView 处理 javaScript的对话框，网站图标，网站title，
加载进度等偏向处理外部事件的类。

常用的方法：

1.  onProgressChanged() :  当页面加载进度发生变化时会被调用，
    用来告诉我们当前加载页面的进度；

2.  onReceivedIcon() : 用来接收 web 页面的icon；

3.  onReceivedTitle() : 用来接收 web页面 的title；

4.  onShowCustomView() ：
    当需要加载视频，要求视频可全屏时，需要回调这个方法，在这里进行视频全屏的处理；

5.  onHideCustomView() : 当退出视频全屏时，会调用该回调；

代码示例如下：

>           webView.setWebChromeClient(new WebChromeClient() {

>               \@Override

>               public void onProgressChanged(WebView view, int newProgress) {

>                   super.onProgressChanged(view, newProgress);

>                   progressBar.setProgress(newProgress);

>               }

>               \@Override

>               public void onReceivedTitle(WebView view, String title) {

>                   super.onReceivedTitle(view, title);

>                   //网站标题的处理

>                   webTitle = title.split(" ")[0];

>               }

>               \@Override

>               public void onReceivedIcon(WebView view, Bitmap icon) {

>                       super.onReceivedIcon(view, icon);

>               }

>   \@Override

>               public void onShowCustomView(View view, CustomViewCallback) {

>   //onShowCustomView()方法里面的实现：

>   //customView 为 一个新的全屏的view

>   // 全屏

>   activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

>   //

>   if(customView != null) {

>         callBack.onCustomViewHidden();

>         return;

>   }

>   customView = view;

>   customViewCallback = callback;

>   videoShowLayout.addView(customView);

>   videoShowLayout.setVisibility(View.VISIBLE);

>   //videoShowLayout 是视频全屏的加载的父布局

>   webDetailLayout.setVisibility(View.INVISIBLE);

>       }

>       \@Override

>       Public void onHideCustomView() {

>   if (customView == null) {

>         return;

>   }

>   //界面转回垂直

>   activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

>   webDetailLayout.setVisibility(View.VISIBLE);

>   customView.setVisibility(View.GONE);

>   videoShowLayout.removeView(customView);

>   customView = null;

>   videoShowLayout.setVisibility(View.GONE);

>   customViewCallback.onCustomViewHidden();

>       }

>           });

应注意的问题:

1.  视频加载时，全屏切换时出现的 闪屏的现象，全屏时要注意之前背景色的影响；

2.  视频播放时 ，点击back键退出后，视频仍在播放问题:

>   //点击back 键，此时走的是webView.goBack(); 我们在webView.goBack()
>   前调用以下：

>   webView.reload();

>   webView.stopLoading();

>   webView.goBack();

>   //可以让视频停止

>   //本质是： 因为webView 没有对视频音频
>   的控制接口，所以有些不方便处理视频的播放问题。

WebView 的销毁问题：

当WebView
加载大量的网页时，若不及时销毁，这些占用大量内存的应用，必然给用户带来很多问题，

销毁的代码示例：

>   private void destroyWebView () {

>   if (webView != null) {

>      // 要首先移除

>      removeView(webView);

>      // 清理缓存

>      webView.stopLoading();

>      webView.onPause();

>      webView.clearHistory();

>      webView.clearCache(true);

>      webView.clearFormData();

>      webView.clearSslPreferences();

>      WebStorage.getInstance().deleteAllData();

>      webView.destroyDrawingCache();

>      webView.removeAllViews();

>      // 最后再去webView.destroy();

>      webView.destroy();

>   }

>   }

>   // 上面会在 低版本上（4.4
>   一下）发生crash，问题说是空指针，很多地方都说这是系统bug，仍未找到官方正确的解释；

>   //可做下面处理

>   private void destroyWebView () {

>   ...   

>   if (android.os.Build.VERSION.SDK_INT \<= Build.VERSION_CODES.KITKAT) {

>       //4.4 以下

>        webView = null;

>   } else {

>        webView.destroy();

>   }

>   }

>   //这样可解决

**应注意的问题：**

1.  在销毁webView后，要注意 在程序中的回调（尤其是
    postDelayed()设置了时间的）是不是再次调用了webView对象，如果在销毁后调用，必然发生crash；解决方法，在回调调用出先判断
    webView是否为空，不为空，再继续走；

WebView 的其他相关

webView还有很多需要注意的地方，例如：

1.  JS 与 WebView 的交互；
    有关文章链接：<https://www.jianshu.com/p/345f4d8a5cfa>

2.  WebView  的加载优化；

    1.  资源本地化；

    2.  缓存问题

    3.  延迟加载和执行JS

对于其他的方面，我暂时还没有做足够的了解，以后有了解了，再给大家做分享。

——-——————新的整理分割线————————

这段时间主要做了一些关于webview的事情，总结了一些常见的问题，放在下面了

第一篇是关于webview的常见的定制用法：

\<此处有一个WebView 的反思和记录 （处理很多特殊的情况）.md文件，请到webview
的相关记录.resources文件中查看\>

第二篇是关于在webview实践过程中遇到的一些奇怪的问题：

\<此处有一个WebView 遇到的问题.md文件，请到webview
的相关记录.resources文件中查看\>

没有整理成Evernote 的文档，就放在附件里面了。
