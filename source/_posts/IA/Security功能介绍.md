Security功能旨在检查手机存在的一些危险项，从而保护用户的隐私及安全

**1. Security分类**

security把危险项按照危险程度分为了三类：virus, risk, suggestion

Virus: 已安装的app病毒

Risk: 强烈推荐开启的功能或者是一些对手机安全信息的清理建议，包括：AppLock,
BrowsingHistory, Clipboard, RealTimeProtection, WebProtection，WiFiSpeedMonitor

Suggestion: 建议开启的一些功能，包括：AutoBooster, CallAssistant, ChargingReport

**2. Security扫描**

Risk和Suggestion的扫描，都是通过调用一个返回值为boolean的接口实现的，因此都是同步操作。

Virus扫描是调用了libDevice的接口(HSSecurityManager.getInstance.startScanAllApp(SecurityTaskListener))实现的。当扫描开始，扫描完一个app，扫描完成和扫描失败都会有信息返回。真实的扫描操作在clean进程进行，具体的扫描逻辑是通过安天SDK完成的。

security有扫描页面和详情页面。扫描页面只扫了Virus和部分Risk，详情页面会显示所有Virus，Risk和Suggestion。详情页面显示写成了推荐机制，若是需要添加Risk或者Suggestion实现相关接口即可。

**3. Security清理**

Virus: 卸载病毒app

Risk和Suggestion：开启相应功能或者清除掉相关数据

**4. 清理有效期**

当要进入security功能时，

-   若当前session扫描过并遗留的危险项数目为0，进入done

-   若当前session扫描过但有遗留的危险项，进入security详情页

-   若当前session没扫描过，进入扫描页

**5. Security状态**

Danger: 有virus和risk时

NeedUpdate: 超过三天未扫描或者病毒库更新

Safe: 安全状态
