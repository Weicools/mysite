---
title: 【模拟器】Genymotion的安装使用介绍
date: 2018-01-12 14:16:27
updated: 2019-11-25 16:40:00
---

# 【模拟器】Genymotion的安装使用介绍

之前介绍的【网易MuMu模拟器】并不是很好用，后来又找到了一个很强大的模拟器，大家想调试的时候可以用它，非常强大，适于开发。

1. 首先安装VirtualBox

2. 再安装genymotion

注：需要打开下面的网址注册一个免费账户：

<https://www.genymotion.com/account/login/>

3. 安装完成后打开genymotion

登录刚刚注册的账户，点击setting，看一下VirtualBox和ADB的目录设置是否正确

![](media/d815d62e4619ff5e0cb6dd11b4fd55ef.png)

4. 添加虚拟机

有各种型号和版本的，这里推荐三星S8，因为发现其他虚拟机不能直接通过AS的Run跑程序，只发现三星S8的模组可以，当然其他的也可以通过build
apk生成的包进行拖拽安装apk。

![](media/8db43a750f269772daead0908972f208.png)

不是三星S8也可以，以下是我安装的虚拟机参考。

![](media/b33fc6441dd8be95cebf64de162a595a.png)

想要Android Studio能直接安装app到虚拟机调试，在gradle中按照下图加入x86就好了...

![](media/ebc50ca1567a31e42938afd519bf38d8.png)

当然也可以按照第5步那样来弄。

5. 安装Genymotion-ARM-Translation（不推荐，推荐Gradle中加x86）

因为架构不一致导致，原生的Genymotion模拟器是使用x86架构，因此在安装arm应用肯定会提示该异常，需要安装一个ARM_Translation_Android系列包来增强兼容性。

![](media/9c600071661320f4989b594033dda039.png)

6. 修改模拟器分辨率

可以自定义也可以选择预设好的

![](media/3793d00abcc76a4b08370e0a7ff67b40.png)

7. 设置电池、GPS等

可以通过点击模拟器旁边的按钮进行设置，模拟真机环境，当然有些功能需要交钱才能使用\~一般来说免费的功能就够用啦。

![](media/f4e1ec0ef13025cd96233ff81560b77a.png)

8. 关联Android Studio

搜索Genymotion安装

![](media/a3d495df11426ed60bc0539ad5884d3b.png)

点下面这个就可以直接在AS启动模拟器

![](media/59e539ab26d14a83b5ae7e660e7aeb7d.png)

没有真机的时候，用它来代替也是很好的，而且还能模拟各种屏幕分辨率，尽情调试吧！
