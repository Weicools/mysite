【导航栏——NavigationBar】

我们的导航栏都是透明的，只有一个返回键，返回键的颜色在RootViewController中进行第一次设置，设为了“白色”。

由于返回键只有那一个，是所有界面通用的，所以，所有通过self.navigationController
pushViewController 推出来的Controller都需要在 viewWillAppear
时将返回键的颜色改为这个界面的返回键的颜色，设置为黑色的代码示例：

\- (void)viewWillAppear:(BOOL)animated {

    [superviewWillAppear:animated];

    [self.navigationController.navigationBarsetTintColor:[UIColorblackColor]];

}

设置为白色的代码示例：

\- (void)viewWillAppear:(BOOL)animated {

    [superviewWillAppear:animated];

    [self.navigationController.navigationBarsetTintColor:[UIColorwhiteColor]];

}

【状态栏颜色】

IOS中状态栏有黑白两种颜色，分别对应UIStatusBarStyleDefault 和UIStatusBarStyleLightContent

我把它配置为：

· Splash使用白色（UIStatusBarStyleLightContent）

·
其他界面默认为黑色（UIStatusBarStyleDefault），如果要改为白色，需要重写此方法：

\- (UIStatusBarStyle)preferredStatusBarStyle {

    returnUIStatusBarStyleLightContent;

}

如果要动态改变颜色，可以手动调用
[selfsetNeedsStatusBarAppearanceUpdate] 方法让系统再次读preferredStatusBarStyle的值，该更新方法放在Animation
Block里可以做渐变动画。

【状态栏隐藏】

默认不隐藏，如果要隐藏，需要重写此方法：

\- (BOOL)prefersStatusBarHidden {

    return YES;

}

如果要动态隐藏，可以手动调用
[selfsetNeedsStatusBarAppearanceUpdate] 方法让系统再次读prefersStatusBarHidden的值，该更新方法放在Animation
Block里可以做渐变动画。

【IPhoneX适配】

1.  沉浸式状态栏：在布局时将背景顶部对齐最外层View的顶部即可，不要对齐safe
    area顶部，因为IOS11的safe
    area是在navigationBar正下面，而IOS10是在手机最顶部，而且对于没有NavigationBar的Controller，safe
    area又会变。但是需要注意的是，IPhoneX的底部有一块区域最好不要设置为可点击，所以底部Button最好基于safe
    area布局。

2.  自定义NavigationBar（Toolbar）、或者相对NavigationBar布局：如果需要NavigationBar，我们采用在对应位置添加一个自定义View的方式所以NavigationBar竖屏时高度固定为44pt，IPhoneX以外的手机，NavigationBar顶部距最外层View的顶部20pt，但是IPhoneX上该距离为20+24=44pt，所以，在Xib里写20pt的约束，然后把该约束拖到ViewController里，在viewDidLoad中将该约束的值加上24（kIS_IPHONE_X宏在MaxUtils.h里）；  
    if (kIS_IPHONE_X) {  
        customNavigationBarHeight.constant += 24;  
    }  
    如果NavigationBar也需要沉浸式，那么它在xib里的高度可以直接写成64pt，并且顶部对齐最外层View的顶部，然后在代码里把高度加上24pt。
