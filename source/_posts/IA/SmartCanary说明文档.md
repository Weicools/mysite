SmartCanary是在开源库LeakCanary的基础上进行拓展，结合之前开发的DebugUtils、TMQ的性能评测方法而诞生的属于IA组dev的发现性能问题的工具库。

   
SmartCanary译为“聪明的金丝雀”，聪明是因为它能分析出咱们代码运行时存在的问题和隐患，并能够“叫”到群里，在一些场景下还能叫到对应负责人。

SmartCanary工作原理

   
SmartCanary分为多个模块，每个模块从不同的角度分析代码运行时存在的问题，如果发现问题，则会将问题发送到局域网服务器上，在服务器上备份之后发送到slack群里。App使用时，类似LeakCanary，需要在Application的onCreate中激活各个模块。

   
SmartCanary通过Maven发布，release时使用仅含空方法的no-op包，debug时使用有内容的包。

【Leak模块】

通过
*SmartCanary.Leak.install(Context)* 方法安装，安装之后，SmartCanary会检查Activity的生命周期，在onDestroy执行过了2分钟之后，会dump内存，如果泄漏，分析出引用关系（这些其实是Leak
Canary的内容），然后将信息发送至服务器，由服务器转发到slack。如果发送失败，SmartCanary会自动尝试重传。

   
通过 *SmartCanary.Leak.addCustomField() *或者 *SmartCanary.Leak.addCustomMessage() *方法，可以打印附加的信息到Slack群。

    打印格式：

![](media/3f61a053cc7882284e0c89b3d13d17bb.png)

    最上面为AppName

    之后会有IA dev出品字样

    接下来是模块名，Leak的模块名用的Leak Canary表示

    接下来是正文

    正文结束后是App里自定义的附加信息，这里我们打印了Build Info（也就是Git
Info）以及Owner信息

    点击 Show more…
可以看到完整的内存泄漏简介，点击链接可以跳转服务器，查看类中各变量的值，通常情况下简介一经足够分析原因了

![](media/16a7640f757af4db5307af8dcaf9429d.png)

【Smoothness模块】

    通过 *SmartCanary.Smoothness.install(Context, String
tag)* 方法安装，安装之后，SmartCanary会每秒打印出流畅度相关的sm（TMQ的流畅度概念）、FPS、lost信息。如果发现主线程卡顿超过1s，会采样打印出卡顿时的调用栈帮助分析卡顿原因。同时还能打印出主线程中的会影响流畅度的因素。

    目前Smoothness模块的“叫”功能还在开发中。
