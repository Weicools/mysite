四、 处理广播
-------------

上一节中我们说到，在发送广播过程中会执行scheduleBroadcastsLocked方法来处理相关的广播，我们知道上一节将广播添加到了BroadcastQueue中不同的两个ArrayList中：mParallelBroadcasts，mOrderedBroadcasts

### 4.1 scheduleBroadcastsLocked

[-\> BroadcastQueue.java]

public void scheduleBroadcastsLocked() {

    // 正在处理BROADCAST_INTENT_MSG消息

    if (mBroadcastsScheduled) {

        return;

    }

    //发送BROADCAST_INTENT_MSG消息

    mHandler.sendMessage(mHandler.obtainMessage(BROADCAST_INTENT_MSG, this));

    mBroadcastsScheduled = true;

}

在BroadcastQueue对象创建时，mHandler=new
BroadcastHandler(handler.getLooper());那么此处交由mHandler的handleMessage来处理：

#### 4.1.1 BroadcastHandler

>   来看一下这个BroadcastHandler，是在BroadcastQueue初始化的时候实例化的，然后BroadcastQueue又是在AMS初始化时赋值的。

public ActivityManagerService(Context systemContext) {

    //名为"ActivityManager"的线程

    mHandlerThread = new ServiceThread(TAG,

            android.os.Process.*THREAD_PRIORITY_FOREGROUND*, false);

    mHandlerThread.start();

    mHandler = new MainHandler(mHandlerThread.getLooper());

...

    //创建BroadcastQueue对象，这里就是区分前台广播和后台广播的

    mFgBroadcastQueue = new BroadcastQueue(this, mHandler,

            "foreground", BROADCAST_FG_TIMEOUT, false);

    mBgBroadcastQueue = new BroadcastQueue(this, mHandler,

            "background", BROADCAST_BG_TIMEOUT, true);

...

}

BroadcastQueue(ActivityManagerService service, Handler handler,

               String name, long timeoutPeriod, boolean
allowDelayBehindServices) {

    mService = service;

    //创建BroadcastHandler

    mHandler = new BroadcastHandler(handler.getLooper());

    mQueueName = name;

    mTimeoutPeriod = timeoutPeriod;

    mDelayBehindServices = allowDelayBehindServices;

}

由此可见BroadcastHandler采用的是“ActivityManager”线程的Looper，那具体的BroadcastHandler其实就是一个handler；通过上面send的msg的值来看，就是执行到processNextBroadcast方法中了。

private final class BroadcastHandler extends Handler {

    public void handleMessage(Message msg) {

        switch (msg.what) {

            case BROADCAST_INTENT_MSG: {

                processNextBroadcast(true); //【见小节4.2】

            }

            break;

        ...

        }

    }

}

### 4.2 processNextBroadcast

[-\> BroadcastQueue.java]

final void processNextBroadcast(boolean fromMsg) {

    synchronized(mService) {

        //第一步: 处理并行广播

        while (mParallelBroadcasts.size() \> 0) {

            r = mParallelBroadcasts.remove(0);

            r.dispatchTime = SystemClock.*uptimeMillis*();

            r.dispatchClockTime = System.*currentTimeMillis*();

            final int N = r.receivers.size();

            for (int i=0; i\<N; i++) {

                Object target = r.receivers.get(i);

                //分发广播给已注册的receiver 【见小节4.3】

                deliverToRegisteredReceiverLocked(r, (BroadcastFilter)target,
false);

            }

            addBroadcastToHistoryLocked(r);//将广播添加历史统计

        }

//第二步: 处理Pending广播

if (mPendingBroadcast != null) {

      boolean isDead;

      synchronized (mService.mPidsSelfLocked) {

         ProcessRecord proc =
mService.mPidsSelfLocked.get(mPendingBroadcast.curApp.pid);

         isDead = proc == null \|\| proc.crashing;

    }

    if (!isDead) {

         // It's still alive, so keep waiting

        return;

      } else {

         mPendingBroadcast.state = BroadcastRecord.*IDLE*;

         mPendingBroadcast.nextReceiver = mPendingBroadcastRecvIndex;

         mPendingBroadcast = null;

      }

}

boolean looped = false;

        //第三步: 处理当前有序广播

        do {

            if (mOrderedBroadcasts.size() == 0) {

                mService.scheduleAppGcsLocked(); //没有更多的广播等待处理

                if (looped) {

                    mService.updateOomAdjLocked();

                }

                return;

            }

            r = mOrderedBroadcasts.get(0); //获取串行广播的第一个广播

            boolean forceReceive = false;

            int numReceivers = (r.receivers != null) ? r.receivers.size() : 0;

            if (mService.mProcessesReady && r.dispatchTime \> 0) {

                long now = SystemClock.*uptimeMillis*();

                if ((numReceivers \> 0) && (now \> r.dispatchTime +
(2\*mTimeoutPeriod\*numReceivers))) {

                    broadcastTimeoutLocked(false);
//当广播处理时间超时，则强制结束这条广播

                }

            }

        ...

            if (r.receivers == null \|\| r.nextReceiver \>= numReceivers

                    \|\| r.resultAbort \|\| forceReceive) {

                if (r.resultTo != null) {

                    //处理广播消息消息，调用到onReceive()

                    performReceiveLocked(r.callerApp, r.resultTo,

                            new Intent(r.intent), r.resultCode,

                            r.resultData, r.resultExtras, false, false,
r.userId);

                }

                cancelBroadcastTimeoutLocked(); //取消BROADCAST_TIMEOUT_MSG消息

                addBroadcastToHistoryLocked(r);

                mOrderedBroadcasts.remove(0);

                continue;

            }

        } while (r == null);

        //第四步: 获取下一个receiver

        r.receiverTime = SystemClock.*uptimeMillis*();

        if (recIdx == 0) {

            r.dispatchTime = r.receiverTime;

            r.dispatchClockTime = System.*currentTimeMillis*();

        }

        if (!mPendingBroadcastTimeoutMessage) {

            long timeoutTime = r.receiverTime + mTimeoutPeriod;

            setBroadcastTimeoutLocked(timeoutTime); //设置广播超时延时消息

        }

        //第五步: 处理下条有序广播

        ProcessRecord app = mService.getProcessRecordLocked(targetProcess,

                info.activityInfo.applicationInfo.uid, false);

        if (app != null && app.thread != null) {

            app.addPackage(info.activityInfo.packageName,

                    info.activityInfo.applicationInfo.versionCode,
mService.mProcessStats);

            processCurBroadcastLocked(r, app); //[处理串行广播]

            return;

        ...

        }

        //该receiver所对应的进程尚未启动，则创建该进程

        if ((r.curApp=mService.startProcessLocked(targetProcess,

                info.activityInfo.applicationInfo, true,

                r.intent.getFlags() \| Intent.*FLAG_FROM_BACKGROUND*,

                "broadcast", r.curComponent,

                (r.intent.getFlags()&Intent.FLAG_RECEIVER_BOOT_UPGRADE) != 0,
false, false))

                == null) {

        ...

            return;

        }

    }

}

此处mService为AMS，整个流程还是比较长的，全程持有AMS锁，所以广播效率直接会影响性能。

-   设置广播超时延时消息: setBroadcastTimeoutLocked（文杰说的埋炸弹）
     值得注意的是mParalleBroadcasts中的广播并没有进行超时的设置，所以说当以个广播发出，他的接收者只有动态注册的无序广播时，是不会引起anr的（有待商讨，单从dome中测试结论是这样的）

-   当广播接收者等待时间过长，则调用broadcastTimeoutLocked(false);引爆炸弹

-   当执行完广播,则调用cancelBroadcastTimeoutLocked;拆炸弹

#### 4.2.1 处理mParalleBroadcasts（处理并行广播）

BroadcastRecord r;

mService.updateCpuStats(); 

if (fromMsg) {

    mBroadcastsScheduled = false;

}

while (mParallelBroadcasts.size() \> 0) {

    r = mParallelBroadcasts.remove(0);

    r.dispatchTime = SystemClock.*uptimeMillis*();

    r.dispatchClockTime = System.*currentTimeMillis*();

    final int N = r.receivers.size();

    for (int i = 0; i \< N; i++) {

        Object target = r.receivers.get(i);

        //分发广播给已注册的receiver 【见小节4.3】

        deliverToRegisteredReceiverLocked(r, (BroadcastFilter) target, false);

    }

    addBroadcastToHistoryLocked(r);//将广播添加历史统计

}

通过while循环, 一次性分发完所有的并行广播后,则分发完成后则添加到历史广播队列.

private final void addBroadcastToHistoryLocked(BroadcastRecord r) {

    if (r.callingUid \< 0) {

        return;

    }

    r.finishTime = SystemClock.*uptimeMillis*(); //记录分发完成时间

    mBroadcastHistory[mHistoryNext] = r;

    mHistoryNext = ringAdvance(mHistoryNext, 1, MAX_BROADCAST_HISTORY);

    mBroadcastSummaryHistory[mSummaryHistoryNext] = r.intent;

    mSummaryHistoryEnqueueTime[mSummaryHistoryNext] = r.enqueueClockTime;

    mSummaryHistoryDispatchTime[mSummaryHistoryNext] = r.dispatchClockTime;

    mSummaryHistoryFinishTime[mSummaryHistoryNext] =
System.*currentTimeMillis*();

    mSummaryHistoryNext = ringAdvance(mSummaryHistoryNext, 1,
MAX_BROADCAST_SUMMARY_HISTORY);

}

#### 4.2.2 处理Pending广播

if (mPendingBroadcast != null) {

    boolean isDead;

    synchronized (mService.mPidsSelfLocked) {

        //从mPidsSelfLocked获取正在处理该广播进程，判断该进程是否死亡

        ProcessRecord proc =
mService.mPidsSelfLocked.get(mPendingBroadcast.curApp.pid);

        isDead = proc == null \|\| proc.crashing;

    }

    if (!isDead) {

        return; //正在处理广播的进程保持活跃状态，则继续等待其执行完成

    } else {

        mPendingBroadcast.state = BroadcastRecord.IDLE;

        mPendingBroadcast.nextReceiver = mPendingBroadcastRecvIndex;

        mPendingBroadcast = null;

    }

}

boolean looped = false;

如果一个广播在发送的时候，进程还没有启动起来，那么会将它存在mPendingBroadcast中。由于动态广播是不会保证一定能够收到的，所以mPendingBroadcast是用来描述一个正在等待静态注册的BroadcastReceiver启动的广播。

**4.2.3 处理有序广播**

>   do {

>       //有序广播队列为0，不用处理，返回

>       if (mOrderedBroadcasts.size() == 0) {

>           ...

>           return;

>       }

>       //获取一个有序广播（最顶部的BroadcastRecord）

>       r = mOrderedBroadcasts.get(0);

>       boolean forceReceive = false;

>       //广播超时处理

>       int numReceivers = (r.receivers != null) ? r.receivers.size() : 0;

>       if (mService.mProcessesReady && r.dispatchTime \> 0) {

>           long now = SystemClock.*uptimeMillis*();

>           if ((numReceivers \> 0) &&

>                   (now \> r.dispatchTime + (2\*mTimeoutPeriod\*numReceivers)))
>   {

>               //超时不能接收，就强制结束广播

>               broadcastTimeoutLocked(false); // forcibly finish this broadcast

>               forceReceive = true;

>               //恢复初始状态。

>               r.state = BroadcastRecord.IDLE;

>           }

>       }

>       if (r.state != BroadcastRecord.IDLE) {

>           return;

>       }

>   // 进入下面的条件有以下几种可能，

>   一：广播没有接收者了，二：默认第一次r.nextReceiver =
>   0,但是它每次都会++，等待最后一个处理完了，再++就会得到一个不存在的nextReceiver，此时r.nextReceiver
>   \>= numReceivers，条件成立，

>   三：广播已经被拦截了，四：广播已经被强制结束了

>       if (r.receivers == null \|\| r.nextReceiver \>= numReceivers

>               \|\| r.resultAbort \|\| forceReceive) {

>           // No more receivers for this broadcast!  Send the final

>           // result if requested...

>           if (r.resultTo != null) {

>               try {

>                   //处理广播消息消息，调用到onReceive()

>                   performReceiveLocked(r.callerApp, r.resultTo,

>                           new Intent(r.intent), r.resultCode,

>                           r.resultData, r.resultExtras, false, false,
>   r.userId);

>                   // Set this to null so that the reference

>                   // (local and remote) isn't kept in the mBroadcastHistory.

>                   r.resultTo = null;

>               } catch (RemoteException e) {

>                   r.resultTo = null;

>               }

>           }

>          
>   //r所描述的广播转发任务已经在规定时间内处理完了，需要remove掉前面给mHandler发送的BROADCAST_TIMEOUT_MSG消息。

>           cancelBroadcastTimeoutLocked();

>           // ... and on to the next...

>           addBroadcastToHistoryLocked(r);

>           if (r.intent.getComponent() == null && r.intent.getPackage() == null

>                   &&
>   (r.intent.getFlags()&Intent.*FLAG_RECEIVER_REGISTERED_ONLY*) == 0) {

>               // This was an implicit broadcast... let's record it for
>   posterity.

>               mService.addBroadcastStatLocked(r.intent.getAction(),
>   r.callerPackage,

>                       r.manifestCount, r.manifestSkipCount,
>   r.finishTime-r.dispatchTime);

>           }

>           // 将r所描述的广播转发任务从有序广播队列中删除 

>           mOrderedBroadcasts.remove(0);

>           //赋值为null，进入下一次循环

>           r = null;

>           looped = true;

>           continue;

>       }

>   } while (r == null);

mTimeoutPeriod，对于前台广播则为10s，对于后台广播则为60s。广播超时为2\*mTimeoutPeriod\*numReceivers，接收者个数numReceivers越多则广播超时总时长越大。

#### 4.2.4 获取下条有序广播

>   //通过上面的循环，r就有值了，获取下一条广播

>   int recIdx = r.nextReceiver++;

>   // Keep track of when this receiver started, and make sure there

>   // is a timeout message pending to kill it if need be.

>   r.receiverTime = SystemClock.*uptimeMillis*();

>   if (recIdx == 0) {

>       r.dispatchTime = r.receiverTime;

>       r.dispatchClockTime = System.*currentTimeMillis*();

>   }

>   if (! mPendingBroadcastTimeoutMessage) {

>       long timeoutTime = r.receiverTime + mTimeoutPeriod;

>       setBroadcastTimeoutLocked(timeoutTime);

>   }

>   final BroadcastOptions brOptions = r.options;

>   final Object nextReceiver = r.receivers.get(recIdx);

>   //检查是否是动态广播

>   if (nextReceiver instanceof BroadcastFilter) {

>       BroadcastFilter filter = (BroadcastFilter)nextReceiver;

>      
>   //动态注册的广播给deliverToRegisteredReceiverLocked处理，跟上边处理无序动态广播是一样的方法

>       deliverToRegisteredReceiverLocked(r, filter, r.ordered, recIdx);

>       //判断是否是无序广播

>       if (r.receiver == null \|\| !r.ordered) {

>           //将
>   r.state设置为IDLE，表示不需要等待它的前一个目标广播接收者处理完成一个广播， 

>           // 就可以将该广播继续发送给它的下一个目标广播接收者处理

>           r.state = BroadcastRecord.IDLE;

>           //执行下一个广播，内部也是发送消息

>           scheduleBroadcastsLocked();

>       } else {

>     ....

>       }

>       //有序广播一次只处理一个，直接返回就行

>       return;

>   }

>   //如果上面没有return,那么肯定是静态注册的广播，静态注册注册的广播是ResolveInfo

>   ResolveInfo info = (ResolveInfo)nextReceiver;

>   //这个ComponentName会一直传递到ActivityThread，用来反射new广播接收者对象的

>   ComponentName component = new ComponentName(

>           info.activityInfo.applicationInfo.packageName,

>           info.activityInfo.name);

>   //很多的条件判断，skip不满足就为true

>   boolean skip = false;

>   ...

>   String targetProcess = info.activityInfo.processName;

>   ProcessRecord app = mService.getProcessRecordLocked(targetProcess,

>           info.activityInfo.applicationInfo.uid, false);

>   if (skip) {

>       r.delivery[recIdx] = BroadcastRecord.DELIVERY_SKIPPED;

>       r.receiver = null;

>       r.curFilter = null;

>       //变成初始状态

>       r.state = BroadcastRecord.IDLE;

>       //执行下个广播

>       scheduleBroadcastsLocked();

>       return;

>   }

>   ...

#### 4.2.4 处理下条有序广播

//如果当前进程存在，调用processCurBroadcastLocked处理，静态广播都是走processCurBroadcastLocked处理的

if (app != null && app.thread != null) {

    try {

        app.addPackage(info.activityInfo.packageName,

                info.activityInfo.applicationInfo.versionCode,
mService.mProcessStats);

        processCurBroadcastLocked(r, app);

        //处理完直接返回，一次处理一个

        return;

    } catch (RemoteException e) {

    } catch (RuntimeException e) {

        // If some unexpected exception happened, just skip

        // this broadcast.  At this point we are not in the call

        // from a client, so throwing an exception out from here

        // will crash the entire system instead of just whoever

        // sent the broadcast.

        logBroadcastReceiverDiscardLocked(r);

        finishReceiverLocked(r, r.resultCode, r.resultData,

                r.resultExtras, r.resultAbort, false);

        scheduleBroadcastsLocked();

        // We need to reset the state if we failed to start the receiver.

        r.state = BroadcastRecord.IDLE;

        return;

    }

    // If a dead object exception was thrown -- fall through to

    // restart the application.

}

//程序走到这里，说明进程不存在，那么调用startProcessLocked启动进程，

if ((r.curApp=mService.startProcessLocked(targetProcess,

        info.activityInfo.applicationInfo, true,

        r.intent.getFlags() \| Intent.*FLAG_FROM_BACKGROUND*,

        "broadcast", r.curComponent,

        (r.intent.getFlags()&Intent.FLAG_RECEIVER_BOOT_UPGRADE) != 0, false,
false)) == null) {

    // Ah, this recipient is unavailable.  Finish it if necessary,

    // and mark the broadcast record as ready for the next.

    logBroadcastReceiverDiscardLocked(r);

    finishReceiverLocked(r, r.resultCode, r.resultData,

            r.resultExtras, r.resultAbort, false);

    //启动失败就执行下一个

    scheduleBroadcastsLocked();

    r.state = BroadcastRecord.IDLE;

    return;

}

//把当前的r存下来，方便下一次处理

mPendingBroadcast = r;

//设置当前的receiver的索引,用来表示将要启动的。

mPendingBroadcastRecvIndex = recIdx;

-   如果是动态广播接收者（无序），会调用deliverToRegisteredReceiverLocked一次性处理，即遍历并行列表（mParallelBroadcasts）的每一个BroadcastRecord以及其中的receivers列表，是一个双重循环。

-   如果是静态广播接收者（有序），且对应进程已经创建，会调用processCurBroadcastLocked继续处理；

-   如果是静态广播接收者（有序），且对应进程尚未创建，会调用startProcessLocked创建进程，之后仍然会调用processCurBroadcastLocked继续处理。

接下来,介绍deliverToRegisteredReceiverLocked的处理流程:

### 4.3 deliverToRegisteredReceiverLocked

[-\> BroadcastQueue.java]

private void deliverToRegisteredReceiverLocked(BroadcastRecord r,

                                               BroadcastFilter filter, boolean
ordered, int index) {

    boolean skip = false;

    //权限判断，  检查发送者是否有权限，检查接收者是否有发送者所需的权限等等，

    //此处省略，不符合的skip==true,下面就return。

    ...

    if (skip) {

        r.delivery[index] = BroadcastRecord.DELIVERY_SKIPPED;

        return;

    }

    ....

    //只有有序广播才会进入这个分支

    if (ordered) {

        r.receiver = filter.receiverList.receiver.asBinder();

        r.curFilter = filter;

        filter.receiverList.curBroadcast = r;

        r.state = BroadcastRecord.CALL_IN_RECEIVE;

        if (filter.receiverList.app != null) {

            // Bump hosting application to no longer be in background

            // scheduling class.  Note that we can't do that if there

            // isn't an app...  but we can only be in that case for

            // things that directly call the IActivityManager API, which

            // are already core system stuff so don't matter for this.

            r.curApp = filter.receiverList.app;

            filter.receiverList.app.curReceiver = r;

            mService.updateOomAdjLocked(r.curApp);

        }

    }

    try {

        if (filter.receiverList.app != null &&
filter.receiverList.app.inFullBackup) {

            // Skip delivery if full backup in progress

            // If it's an ordered broadcast, we need to continue to the next
receiver.

            if (ordered) {

                skipReceiverLocked(r);

            }

        } else {

           
//处理广播，filter.receiverList.receiver对应的是客户端ReceiverDispatcher的Binder实体—-InnerReceiver，是在ReceiverList初始化的时候赋值的

            performReceiveLocked(filter.receiverList.app,
filter.receiverList.receiver,

                    new Intent(r.intent), r.resultCode, r.resultData,

                    r.resultExtras, r.ordered, r.initialSticky, r.userId);

        }

        if (ordered) {

            r.state = BroadcastRecord.CALL_DONE_RECEIVE;

        }

    } catch (RemoteException e) {

     ...

    }

}

### 4.4 performReceiveLocked

[-\> BroadcastQueue.java]

void performReceiveLocked(ProcessRecord app, IIntentReceiver receiver,

                          Intent intent, int resultCode, String data, Bundle
extras,

                          boolean ordered, boolean sticky, int sendingUser)
throws RemoteException {

    // Send the intent to the receiver asynchronously using one-way binder
calls.

    if (app != null) {

        if (app.thread != null) {

            try {

                //终于走到ActivityThread里面了

                app.thread.scheduleRegisteredReceiver(receiver, intent,
resultCode,

                        data, extras, ordered, sticky, sendingUser,
app.repProcState);

            } catch (RemoteException ex) {

               .....

            }

        } else {

            // Application has died. Receiver doesn't exist.

            throw new RemoteException("app.thread must not be null");

        }

    } else {

        //调用者进程不存在，则执行该分支

        receiver.performReceive(intent, resultCode, data, extras, ordered,

                sticky, sendingUser);

    }

}

### 4.5 ATP.scheduleRegisteredReceiver

[-\> ApplicationThreadNative.java ::ApplicationThreadProxy]

public void scheduleRegisteredReceiver(IIntentReceiver receiver, Intent intent,
int resultCode, String dataStr, Bundle extras, boolean ordered, boolean sticky,
int sendingUser, int processState) throws RemoteException {

    Parcel data = Parcel.*obtain*();

    data.writeInterfaceToken(IApplicationThread.descriptor);

    data.writeStrongBinder(receiver.asBinder());

    intent.writeToParcel(data, 0);

    data.writeInt(resultCode);

    data.writeString(dataStr);

    data.writeBundle(extras);

    data.writeInt(ordered ? 1 : 0);

    data.writeInt(sticky ? 1 : 0);

    data.writeInt(sendingUser);

    data.writeInt(processState);

    //command=SCHEDULE_REGISTERED_RECEIVER_TRANSACTION

    mRemote.transact(SCHEDULE_REGISTERED_RECEIVER_TRANSACTION, data, null,

            IBinder.*FLAG_ONEWAY*);

    data.recycle();

}

ATP位于system_server进程，是Binder 客户端通过Binder驱动向Binder 服务端发送消息,
ATP所对应的服务端位于发送广播调用端所在进程的ApplicationThread，即进入AT.scheduleRegisteredReceiver，
接下来说明该方法。

### 4.6 AT.scheduleRegisteredReceiver

[-\> ActivityThread.java ::ApplicationThread]

public void scheduleRegisteredReceiver(IIntentReceiver receiver, Intent  ,

                                       int resultCode, String dataStr, Bundle
extras, boolean ordered,

                                       boolean sticky, int sendingUser, int
processState) throws RemoteException {

    updateProcessState(processState, false);

   
//走到ReceiverDispatcher中的performReceive实际是InnerReceiver内部类当中的方法

    receiver.performReceive(intent, resultCode, dataStr, extras, ordered,

            sticky, sendingUser);

}

此处receiver是注册广播时创建的，见小节[2.3]，可知该receiver=LoadedApk.ReceiverDispatcher.InnerReceiver。

### 4.7 InnerReceiver.performReceive

[-\> LoadedApk.java]

public void performReceive(Intent intent, int resultCode, String data, Bundle
extras, boolean ordered, boolean sticky, int sendingUser) {

    LoadedApk.ReceiverDispatcher rd = mDispatcher.get();

    if (rd != null) {

        //【见小节4.8】

        rd.performReceive(intent, resultCode, data, extras, ordered, sticky,
sendingUser);

    } else {

   ...

    }

}

此处方法LoadedApk()属于LoadedApk.ReceiverDispatcher.InnerReceiver,
也就是LoadedApk内部类的内部类InnerReceiver.这里拿到了rd对象，我们知道rd持有InnerReceiver和BroadcastReceiver他们一一对应的关系，由此我们便可以找到我们注册的BroadcastReceiver。

### 4.8 ReceiverDispatcher.performReceive

[-\> LoadedApk.java]

public void performReceive(Intent intent, int resultCode, String data, Bundle
extras, boolean ordered, boolean sticky, int sendingUser) {

    Args args = new Args(intent, resultCode, data, extras, ordered,

            sticky, sendingUser);

    //通过handler消息机制发送args.

    if (!mActivityThread.post(args)) {

        //消息成功post到主线程，则不会走此处。

        if (mRegistered && ordered) {

            IActivityManager mgr = ActivityManagerNative.getDefault();

            args.sendFinished(mgr);

        }

    }

}

其中Args继承于BroadcastReceiver.PendingResult，实现了接口Runnable;
其中mActivityThread是当前进程的主线程的handler,
是在[小节2.3.1]创建ReceiverDispatcher时完成赋值过程的；handler.post（args）,执行到args的run方法

### 4.9 ReceiverDispatcher.Args.run

[-\> LoadedApk.java]

public final class LoadedApk {

    static final class ReceiverDispatcher {

        final class Args extends BroadcastReceiver.PendingResult implements
Runnable {

            public void run() {

    // 这里的mReceiver就是在ReceiverDispatch初始化的时候实现赋值的

                final BroadcastReceiver receiver = mReceiver;

                final boolean ordered = mOrdered;

                final IActivityManager mgr = ActivityManagerNative.getDefault();

                final Intent intent = mCurIntent;

                mCurIntent = null;

                if (receiver == null \|\| mForgotten) {

                    if (mRegistered && ordered) {

                        sendFinished(mgr);

                    }

                    return;

                }

                try {

                    //获取mReceiver的类加载器

                    ClassLoader cl =  mReceiver.getClass().getClassLoader();

                    intent.setExtrasClassLoader(cl);

                    setExtrasClassLoader(cl);

                    receiver.setPendingResult(this);

                    //回调广播onReceive方法

                    receiver.onReceive(mContext, intent);

                } catch (Exception e) {

              ...

                }

                if (receiver.getPendingResult() != null) {

                    finish(); //【见小节4.10】

                }

            }

        }

    }

接下来,便进入主线程（默认的handler是主线程的handler）,最终调用BroadcastReceiver具体实现类的onReceive()方法。

\------\|-BroadcastQueue.performReceiveLocked()

\------\|-------\|-ActivityThread.ApplicationThread.scheduleRegisteredReceiver()

\------\|-------\|-------\|- ReceiverDispatcher.InnerReceiver.performReceive()

\------\|-------\|-------\|-------\|-Handler.post(args)

\------\|-------\|-------\|-------\|-------\|-Args.run()

\------\|-------\|-------\|-------\|-------\|-------\|-BroadcastReceiver.onReceive()

### 4.10 PendingResult.finish

[-\> BroadcastReceiver.java ::PendingResult]

public final void finish() {

    if (mType == TYPE_COMPONENT) { //代表是静态注册的广播

        final IActivityManager mgr = ActivityManagerNative.getDefault();

        if (QueuedWork.hasPendingWork()) {

            QueuedWork.singleThreadExecutor().execute( new Runnable() {

                void run() {

                    sendFinished(mgr); //[见小节4.10.1]

                }

            });

        } else {

            sendFinished(mgr); //[见小节4.10.1]

        }

    } else if (mOrderedHint && mType != TYPE_UNREGISTERED) {
//动态注册的串行广播

        final IActivityManager mgr = ActivityManagerNative.getDefault();

        sendFinished(mgr); //[见小节4.10.1]

    }

}

主要功能:

-   静态注册的广播接收者:

    -   当QueuedWork工作未完成, 即SharedPreferences写入磁盘的操作没有完成,
        则等待完成再执行sendFinished方法;

    -   当QueuedWork工作已完成, 则直接调用sendFinished方法;

-   动态注册的广播接收者:

    -   当发送的是有序广播, 则直接调用sendFinished方法.

另外常量参数说明:

-   TYPE_COMPONENT: 静态注册

-   TYPE_REGISTERED: 动态注册

-   TYPE_UNREGISTERED: 取消注册

#### 4.10.1 sendFinished

[-\> BroadcastReceiver.java ::PendingResult]

public void sendFinished(IActivityManager am) {

    synchronized (this) {

        mFinished = true;

    ...

        // mOrderedHint代表发送是否为串行广播 [见小节4.10.2]

        if (mOrderedHint) {

            am.finishReceiver(mToken, mResultCode, mResultData, mResultExtras,

                    mAbortBroadcast, mFlags);

        } else {

            //静态注册的广播, 仍然需要告知AMS. [见小节4.10.2]

            am.finishReceiver(mToken, 0, null, null, false, mFlags);

        }

    ...

    }

}

此处AMP.finishReceiver，经过binder调用，进入AMS.finishReceiver方法

#### 4.10.2 AMS.finishReceiver

public void finishReceiver(IBinder who, int resultCode, String resultData,
Bundle resultExtras, boolean resultAbort, int flags) {

...

    final long origId = Binder.*clearCallingIdentity*();

    try {

        boolean doNext = false;

        BroadcastRecord r;

        synchronized(this) {

            BroadcastQueue queue = (flags & Intent.*FLAG_RECEIVER_FOREGROUND*)
!= 0

                    ? mFgBroadcastQueue : mBgBroadcastQueue;

            r = queue.getMatchingOrderedReceiver(who);

            if (r != null) {

                doNext = r.queue.finishReceiverLocked(r, resultCode,

                        resultData, resultExtras, resultAbort, true);

            }

        }

        if (doNext) {

            //处理下一条广播

            r.queue.processNextBroadcast(false);

        }

        trimApplications();

    } finally {

        Binder.*restoreCallingIdentity*(origId);

    }

}

五、总结
--------

### 5.1 基础知识

1.BroadcastReceiver分为两类：

-   静态广播接收者：通过AndroidManifest.xml的标签来申明的BroadcastReceiver;

-   动态广播接收者：通过AMS.registerReceiver()方式注册的BroadcastReceiver,
    不需要时记得调用unregisterReceiver();

2.广播发送方式可分为三类:

| 类型       | 方法                 | ordered | sticky |
|------------|----------------------|---------|--------|
| 普通广播   | sendBroadcast        | false   | false  |
| 有序广播   | sendOrderedBroadcast | true    | false  |
| Sticky广播 | sendStickyBroadcast  | false   | true   |

3.广播注册registerReceiver():默认将当前进程的主线程设置为scheuler.
再向AMS注册该广播相应信息,
根据类型选择加入mParallelBroadcasts或mOrderedBroadcasts队列.

4.广播发送processNextBroadcast():根据不同情况调用不同的处理过程:

-   如果是动态广播接收者，则调用deliverToRegisteredReceiverLocked处理；

-   如果是静态广播接收者，且对应进程已经创建，则调用processCurBroadcastLocked处理；

-   如果是静态广播接收者，且对应进程尚未创建，则调用startProcessLocked创建进程。

### 5.2 流程图

最后,通过一幅图来总结整个广播处理过程.
点击查看[大图](http://gityuan.com//images/ams/send_broadcast.jpg)

![](media/54b87712992b25e8f74905e3f87a45d7.jpg)

#### 5.2.1 并行广播

整个过程涉及过程进程间通信, 先来看并行广播处理过程:

1.  广播发送端所在进程: 步骤1\~2;

2.  system_server的binder线程: 步骤3\~5;

3.  system_server的ActivityManager线程: 步骤6\~11;

4.  广播接收端所在进程的binder线程: 步骤12\~13;

5.  广播接收端所在进程的主线程: 步骤14\~15,以及23;

6.  system_server的binder线程: 步骤24\~25.

#### 5.2.2 串行广播

可以看出整个流程中,步骤8\~15是并行广播,
而步骤16\~22则是串行广播.那么再来说说串行广播的处理过程.

1.  广播发送端所在进程: 步骤1\~2;

2.  system_server的binder线程: 步骤3\~5;

3.  system_server的ActivityManager线程:步骤6以及16\~18;

4.  广播接收端所在进程的binder线程: 步骤19;

5.  广播接收端所在进程的主线程: 步骤20\~22;

6.  system_server的binder线程: 步骤24\~25.

再来说说几个关键的时间点:

-   enqueueClockTime: 位于步骤4 scheduleBroadcastsLocked(),
    这是在system_server的binder线程.

-   dispatchClockTime: 位于步骤8
    deliverToRegisteredReceiverLocked(),这是在system_server的ActivityManager线程.

-   finishTime : 位于步骤11 addBroadcastToHistoryLocked()之后,
    这是在并行广播向所有receivers发送完成后的时间点,而串行广播则是一个一个发送完成才会继续.

#### 5.2.3 粘性广播

对于粘性广播，registerReceiver()会有一个返回值，数据类型为Intent。
只有粘性广播在注册过程过程会直接返回Intent，里面带有相关参数。
比如常见的使用场景比如Battery广播的注册。

### 5.3 广播处理机制

1.  当发送串行广播(ordered=true)的情况下：

-   静态注册的广播接收者(receivers)，采用串行处理；

-   动态注册的广播接收者(registeredReceivers)，采用串行处理；

-   当发送并行广播(ordered=false)的情况下：

-   静态注册的广播接收者(receivers)，依然采用串行处理；

-   动态注册的广播接收者(registeredReceivers)，采用并行处理；

简单来说，静态注册的receivers始终采用串行方式来处理（processNextBroadcast）；
动态注册的registeredReceivers处理方式是串行还是并行方式,
取决于广播的发送方式(processNextBroadcast)。

静态注册的广播往往其所在进程还没有创建，而进程创建相对比较耗费系统资源的操作，所以
让静态注册的广播串行化，能防止出现瞬间启动大量进程的喷井效应。

**ANR时机：**只有串行广播才需要考虑超时，因为接收者是串行处理的，前一个receiver处理慢，会影响后一个receiver；并行广播
通过一个循环一次性向所有的receiver分发广播事件，所以不存在彼此影响的问题，则没有广播超时；

-   串行广播超时情况1：某个广播总处理时间 \> 2\* receiver总个数 \*
    mTimeoutPeriod, 其中mTimeoutPeriod，前台队列默认为10s，后台队列默认为60s;

-   串行广播超时情况2：某个receiver的执行时间超过mTimeoutPeriod；

<https://www.jianshu.com/p/efb93e8724cf>

<https://www.jianshu.com/p/190b0e7f9a57>
