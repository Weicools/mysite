**一、Glide简介**

Glide是android平台上的一个高效、开源的多媒体资源管理库，提供多媒体文件的压缩、加载和缓存功能，其目的是实现平滑的图片列表滚动效果。已被广泛的运用在google的开源项目中。

Glide的作者是bumptech，GitHub地址如下：

<https://github.com/bumptech/glide>

**二、Glide的优势**

目前市面上有很多图片加载框架，其中比较知名的有Picasso、UImageLoader、Glide和Fresco。网上有一些这些框架的优劣比较，下面是两个比较典型的意见：

![](media/ee4d8093f5cb8cf8b44d15317c421acb.png)

![](media/e252ac72e2a23837018dea93e3f7c82f.png)

我个人目前的使用感受，Glide的优势是支持gif，使用简便，在图片质量和缓存占用、功能和库大小两方面取得了不错的平衡。

下面是一些网上评价对比的链接：

1、Picasso在大图片上表现差

<http://www.myexception.cn/mobile/1991988.html>

2、StackOverFlow上的讨论帖

<http://stackoverflow.com/questions/29363321/picasso-v-s-imageloader-v-s-fresco-vs-glide/29558169>

3、Picasso和Glide在图片质量和内存开销方面的对比，也同时介绍了很多Glide的使用细节

<http://jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0327/2650.html>

**三、使用注意**

**1、引入依赖**

在build.gradle文件中加入

dependencies {

    compile 'com.github.bumptech.glide:glide:3.7.0'

}

**2、使用方式**

对于一般的情况，Glide使用方式相当简单，只需在加载图片的地方写

>   Glide.with(yourFragment)

>       .load(yourFileDataModel)

>       .into(yourImageView);

其中，load的参数可以是uri，也可以是file path，into的参数是展示图片的ImageView

**3、关于View.setTag的注意事项**

如果你的ImageView使用了setTag(Object
tag)方法存储自己的对象，那么就不能用上面的方法直接使用Glide展示了。因为Glide内部也有用到setTag，会和你的tag产生冲突。解决办法有二：

（1）将自己的所有  setTag(Object tag) 和 getTag()  都改为 setTag(int key, Object
tag) 和 getTag(int key)。这里的key必须以id的形式定义在res中。

（2）用Glide Module对Glide进行统一设置，让所有Glide加载图片的地方都使用特定的tag
key。个人推荐第二种方法，对原有代码的改动很小。

**4、用Glide Module进行统一设置**

统一设置的方法是新建一个类来实现GlideModulej接口。如果要实现上述的tag设置，只需在接口的

**public void **applyOptions(Context context, GlideBuilder builder)

方法中加上这么一行：

ViewTarget.*setTagId*(R.id.*glide_tag_id*);

其中tagid定义在strings.xml中：

\<**item name="glide_tag_id" type="id"**/\>

建好的 Module 类需要定义在 AndroidManifest.xml 的 application 标签下：

\<**meta-data**

**    android:name="YourGlideModule"**

**    android:value="GlideModule" **/\>

 

**5、Glide的预加载**

如果你需要在一个非UI线程中预先加载图片，可以使用downloadOnly(int width, int
height)方法：

>   Glide.with(applicationContext)

>       .load(yourUrl)

>       .downloadOnly(500, 500)

>       .get();

之后在展示的地方用

>   Glide.with(yourFragment)

>       .load(yourUrl)

>       .diskCacheStrategy(DiskCacheStrategy.ALL)

>       .into(yourView);

来将预先缓存的图片显示出来。
