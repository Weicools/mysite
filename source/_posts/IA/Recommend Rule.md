1.  **综述**

   
推荐机制将展示位和推荐内容统一起来，通过展示位定义接口，推荐内容实现接口并以动态注册的方式使其结合达到推荐内容的展示。展示位是展示推荐内容的一个抽象体，可以是一系列触发展示推荐内容的时机点如主界面Back，也可以是某一个具体界面如Done界面，还可以是其他只要满足推荐内容要被展示。展示位和推荐内容的关系如同一张错综复杂的网，而推荐机制的目的正是切断这张网。

1.  **原理分析**

   
从阐述简明起，将展示位定义为Placement，将展示内容定义为Content，将某个具体的展示位定义为XXPlacement，将某个具体的展示内容定义为XXContent。接下来看下这张网并如何切断。

![](media/db5c41b272f96beb29999a7eb4b0123d.png)

   
从左侧看每一个Placement都有关联的Content并且关联的种类还不尽相同，从右侧看每一个Content都有依附的Placement也是不尽相同，但是所有的Placement间无关联，所有的Content间无关联。这样最大的独立个体是XXPlacement、XXContent，而之前的关联可以添加中间者加以隔断。

![](media/5c2d96539f8fc946e192bb33855303cb.png)

   
通过添加中间接口将直接关联降低为间接广联，这样每一个Placement只需关联到自定义的接口，每一个Content若要在某一个展示位展示只需实现展示位定义的接口。Placement和Content的关联去掉后接下来就是具体Content的实现类链到Placement的接口，其中涉及的自然是Content实例化。

![](media/aa0716acf1490ab7888960c30bedeada.png)

   
Content的实例化需要动态的，因为每一个Content针对具体的Placement的并不尽相同，并且存在后续增添Content，所以Content的实例化需要动态。针对这种情况，每一个Placement定义的接口都有一个对应的Factory。为了便于描述将XXPlacment定义的接口定义为IXXContent，将IXXContent对应的Factory定义为IXXContentFactory。经过这样设计后，每一个Placement只需自定义接口然后依赖于接口创建并展示内容，每一个Content只需实现Placement的Content和Factory接口，而实例化操作交于第三方控制并且这个第三方控制的也仅仅是Factory。

1.  ** 实现分析**

   
前一节阐述了总体框架，接下来就是该结构下的运转。推荐机制是针对Placement展示Content而设计，所有的执行都只涉及Placment和Content。推荐机制下，Placement展示Content总体流程如下：Content在Placement下注册Factory实现类，Placement展示时根据RemoteConfig和注册的Factory创建一个有效的Content，展示Content。以Intruder展示Card为例说明如下。

![](media/0dd37cbee8f63783c5921533209c817d.png)

   
如上图Intruder定义了自己的接口，这方面提供了一个共享代码的选择，IIntruderContentFactory继承IContentFactory这样IntruderPlacementMgr可以使用ContentFactoryHolder。ContentFacotoryHodler主要是读取优先级配置然后根据注册的Content返回一个有效的并且优先级最高的一个Content，同时考虑到广告也在优先级配置项中（如下图Priority节点），所以也提供了一个接口判断本次推荐是否优先推荐广告。ContentFactoryHolder的功能在每一个展示位基本都需要，所以建议定义的接口继承自IContent和IContentFactory。

![](media/c078ce3a18921ace00b859cf73b8d2e0.png)

   
Intruder展示推荐内容，从Intruder展示位考虑，被展示的内容只需提供一个View和一个点击回调即可，所以Intruder
Placement定义的接口如下：

![](media/fba0b96cde408389a390022606397e39.png)

同时对应的Factory负责实例化Intruder定义的接口，示例如下

![](media/88dc3585ce0ab11a98d40384ac1cf4e6.png)

   
这样每一个被推荐的内容针对Placement定义的接口，只要实现该接口就可以在该Placement下展示。如下图，NotificationOrganizer
Content在Intruder上展示则实现IIntruderContent，在AppLock上展示则实现IAppLockContent，而OrganizerContent归属于NotificationOrganizer模块，在任何展示位展示涉及的代码和资源均由内部处理，这样就给与了代码和资源的复用。

![](media/57c1b89e34a0a4f3339604bc0f795393.png)

其中动态注册的时机点就放在了Application
onCreate。如下Intruder广告位注册了SmartLock、Organizer和Accessibility。

IntruderPlacementMgr.*getInstance*().registerContentFactory(**new **SmartLockContentFactory());

IntruderPlacementMgr.*getInstance*().registerContentFactory(**new **OrganizerContentFactory());

IntruderPlacementMgr.*getInstance*().registerContentFactory(**new **AccessibilityContentFactory());

   
从以上分析，SmartLock、Organizer和Accessibility在Intruder展示位注册，Intruder触发展示时，IntruderPlacementMgr读取配置若是展示广告则展示广告，若是展示Content则判断三者的优先级和有效性找出符合的一个然后展示。

   
src/main/com/optimizer/test/recommendrule为推荐机制总目录放置公有代码，res/recommendrule放置公有资源，模块下有一个recommendrule放置该模块对应的content如smartlock下的Recommendruld、applock下的recommendruld。

**Recommend System实现的目标：**

1.应用于所有的广告位

2.所有的推荐内容可以融进去

3.Content可以动态增加和删除

5.广告位完全定制Content

6.公有特性代码可以共享

7.使用的是最新的配置文件内容

目标向程序的转化：

Content动态增加和删除决定了广告位不能依赖于Content，广告位只能依赖于接口，Content实现接口，然后通过Factory创建实现类，同时每个广告位完全定制Content，决定每个广告位都有自定义的不受其他广告位限制的接口，同时同一个实现类实现广告位A的接口和广告位B的接口，内部代码可以共享。

**推荐机制的实际应用**

广告位 + 推荐内容 的自由搭配

广告位有特定的展示时机，也有固定的展示视觉样式。

样式1：指的是已有的Done页面效果图。

样式2：FastBoost页面、卸载残留垃圾Dialog

![](media/42ca21d3202e6e0680a8fd609f6753d2.png)

![](media/b900a2d50b83f9a24b284eb66f2e0510.png)

![](media/69fed4a3d436ca04fff5c291e7127e8e.png)

![](media/0bcb63ae5bf43ae586cb887637197923.png)

上面四张图都是样式2的效果图。从左到右推荐的Content内容分别是：Accessibility,
AppLock, SmartLock, NotificationOrganizer

样式3：AppLock锁页面、充电锁屏页面

![](media/9aae92040dbea50b8ad2a790d2f04023.png)

![](media/3cf635be6351100869d53d9064a758b7.png)

![](media/4da4aa5652cb01e66e56485c0e182bb4.png)

![](media/ece0bffc7b3506d3665f9336f91bf593.png)

上面4张是样式3的效果图，从左到右推荐的Content内容分别是AppLock,
NotificationOrganizer, Accessibitliy, SmartLocker

样式4：AppManager页面、入侵者拍照页面、Tab4页面

![](media/6d29a39b43626af537775c0bca00de77.png)

![](media/7151c5bda251d85c997b5e6376555cce.png)

![](media/36be61fe87421ca0e7f722f3c60e528d.png)

![](media/b55f31865ab5dada703a423eb9eb7026.png)

上面四张是样式4的效果图，从左到右推荐的Content内容分别是AppLock, Accessibility,
NotificationOrganizer, SmartLock

样式5：App启动的全屏广告位

![](media/d8acaecca505980846b9262c87a53ed6.png)

![](media/8a0f8106c3b0b791493728798b01d122.png)

![](media/64d3f549f4273ebfc6ee82fca8ea0749.png)

![](media/931429961cbfc1b53a63cedb20e4a999.png)

上面4张是样式5的效果图，从左到右推荐的Content内容分别是Accessibility,
NotificationOrganizer, AppLock, SmartLocker

样式6：LuckyDraw广告位、Done页面上方的Dialog、App启动的Dialog、Done页面返回主页、点击Back退出App

![](media/7b21c36500dade9917c2491e42b4f25a.png)

![](media/5c692add431a68d205acc82c2e727eba.png)

![](media/2941ec93e3a2d830ac7f4bc6538b81d3.png)

![](media/771114982fb54676621f5d1b83ddfb64.png)

上面4张是样式6的效果图，从左到右推荐的Content内容分别是SmartLock, Accessibility,
NotificationOrganizer, AppLocker

**如何添加一个广告位？**

添加一个广告位需要最基本的3个文件：XXPlacementMgr, IXXContentFactory, IXXContent

除此之外，根据需要还可以继续添加显示界面的文件，比如XXHandleView

下面已SmartLock广告位为例。

1.PlacementMgr一般设计成单例模式即可。

2.PlacementMgr中要有一个ContentFactoryHolder类型的对象作为成员变量，其构造方法的参数是config文件中，此广告位的展示优先级配置的路径。

![](media/8fcda69ed8c23823bec88460dcb7f4d4.png)

3.PlacementMgr需要提供registerContentFactory()和unregisterContentFactory()接口，调用这两个接口的需要传递实现了IXXContentFactory的对象。

![](media/e68f6d8884b0596c846191a615fe3a2e.png)

4.PlacementMgr需要提供shouldDisplayAd()接口，返回值是boolean类型。一般是转调ContentFactoryHolder的shouldDisplayAd()接口。

![](media/7b091b77b30979f42de49f8700ecb0fa.png)

5.PlacementMgr需要提供createValidContent()接口，通过ContentFactoryHolder的getValidContentFactory()接口获得已实现了IXXContentFactory的对象，然后再通过这个Factory对象提供的接口获得最终的Content。

![](media/82de1259ee10ab3e417d024f07e22c67.png)

**如何添加一个内容？**

添加一个内容需要最基本的2个文件：XXContentFactory和XXContent

添加推荐机制的规则

1.一个具体的Content需要实现将要展示的广告位提供的接口。比如XXContent implement
IFastBoostContent

2.Content需要在SP文件中记录数据，SP文件的命名方式如下：”optimizer_app_lock_content”（此处以AppLock模块为例）

3.getContentName()方法的返回值需要从CommonConstants中获取，比如：CommonConstants.CONTENT_NAME_APP_LOCK;

4.所有的广告和所有的成员变量，都需要有时机释放。可以在release()接口中实现。

5.一些关键的步骤添加Log，方便自己测试。Log的Tag，使用LogConstants中的内容目前有三种可选：TAG_ALL,
TAG_CONTENT, TAG_PLACEMENT

6.Content的布局文件统一如下：layout_placement_style_2 或者
layout_placement_style_2_for_app_lock

7.布局文件存放的位置，layout文件都放到recommendrule模块的layout下。drawable和string都放到

记录Flurry的规则：

HSAnalytics.logEvent("Content_Viewed", "Placement_Content",
“FastBoost_AppLock”);

HSAnalytics.logEvent("Content_Clicked", "Placement_Content",
“FastBoost_AppLock”);

HSAnalytics.logEvent("Content_Viewed", "Placement_Content", placementName +
"_AppLock");

添加推荐机制的注意事项：易出错的问题

1.所有新添加的Activity，要注意检查是否在AndroidManifest中注册。容易遗漏的有NotificationListenerProcessActivity,
UsageAccessProcessActivity.
