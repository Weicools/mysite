【定义】

ProGuard能够对Java类中的代码进行**压缩（Shrink）、优化（Optimize）、混淆（Obfuscate）、预检（Preveirfy)**，是一个**压缩、优化和混淆Java字节码（.class文件）**文件的免费的工具，它可以
①*删除无用的类、字段、方法和属性*。可以删除没用的注释，最大限度地优化字节码文件。它还可以
②*使用简短的无意义的名称来重命名已经存在的类、字段、方法和属性*。常常用于Android开发用于混淆最终的项目，**增加项目被反编译的难度**。

 

![](media/897926cc2a17e9fbeada0b8d1ef31b47.jpg)

一. ProGuard rules

**※ 什么时候要配置？**

>      当在项目中配置了 minifyEnabled 为 true 时，如果不配置 ProGuard
>   文件，ProGuard
>   会混淆所有代码。而有些情况下，有些代码是不能混淆的，否则就会导致程序出错。常用的不能混淆的代码如下：

>      1. 系统接口（Framework 层下所有的类,系统组件有固定的方法被系统调用）

>      2. Manifest 中声明的所有类及自定义View

>      3. xml 中声明的所有资源名（不是用R.xxxx.xxx找到的所以并没有对应数字）

>     4. 使用反射的地方

>      5.
>   使用第三方开源库或者引用其他第三方的SDK包时，需要在混淆文件中加入对应的混淆规则

>      6. JNI 方法（本身就是根据包名去调用的,如果混淆了就会NotFoundMethod了）

>      7.
>   Parcelable的子类和Creator静态成员变量不混淆，否则会产生android.os.BadParcelableException
>   异常（序列化）

>      8.
>   使用GSON、fastjson等框架时，所写的JSON对象类不混淆，否则无法将JSON解析成对应的对象

>          (e.g. Group group = JSON.parseObject(jsonString, Group.class);)

>      9. 有用到WEBView的JS调用也需要保证写的接口方法不混淆（否则对方无法调用）

>   \* Android Studio 启用 ProGuard 工具后，会默认不混淆系统接口、Manifest
>   中声明的所有类及自定义View和 xml 中声明的所有资源名，
>   因此这些情况下不需要手动配置 ProGuard
>   文件。而其他情况下不应该混淆的代码就需要对 ProGuard 文件进行配置。

**※ 在哪里配置？**

   打开一个 android 项目，在 app 目录下的 build.grade 文件中可以看到如下代码

![](media/9b98d78ac65df0fa3a14cb81f3dd8811.png)

-   minifyEnabled 置为 true 表示在 release 环境下启用 ProGuard
    工具。在打包程序时，这一项必须置为 true, 否则 release
    打出的包将没有压缩、优化、混淆。

-   proguardFiles 命令后面加了两个文件名。启用 ProGuard 后， ProGuard
    对项目的处理将遵循这两个文件的配置。'proguard-android.txt’ 是 Android Studio
    集成 ProGuard 工具时自带的 ProGuard
    配置文件，**proguard-rules.pro是开发项目时由程序员添加的配置文件。**

-   Android Studio 集成 ProGuard
    工具的同时，给出了两个默认配置文件：proguard-android.txt和proguard-android-optimize.txt，两个文件的差别是使用proguard-android.txt将不会对项目代码进行优化(Optimize)，对各个版本兼容性会更好，更不容易出问题。这两个文件可以在电脑
    **android SDK 安装目录下的 /tools/proguard** 的目录下找到。

![](media/0de82c85574188accaadd505f0bc0481.png)

-   proguard-rules.pro 文件可以在当前 android 项目 app
    目录下找到。初始时此文件是空白的，**开发者可以在这个文件中添加自己项目的ProGuard配置，不能在proguard-android.txt和proguard-android-optimize.txt中添加。(不受git控制）**

-   proguardFiles 后面*可以加入多个文件名*，意味着 ProGuard
    工作会遵循多个文件的配置。比如

![](media/d75795cec61834478fa3349daeecaa91.png)

**※ 如何配置？**

![](media/1a21b0499c1f1ed2e4be36cbb6a5f142.png)

（**注意：**-dontobfuscate 不能加在项目中，否则项目将不会混淆。）

**※ ProGuard 配置通配符**

>   ？ 匹配除包分隔符外的任意单个字符

>   \*  匹配除包分隔符外的所有符号

>   \*\* 匹配多个字符(任意)

>   %  匹配所有基本类型(不包含 void)

>   \*\*\*  匹配任何类型

>   !  不匹配

>   \<init\>   匹配所有构造函数

>   \<fields\>  匹配所有字段

>   \<methods\>  匹配所有方法

二. 读取混淆配置

![](media/1ae00e830709f466ffbf7a653152dd2b.png)

**Member = Method + Field**

BaseProguardAction.class  (package
*com.android.build.gradle.internal.transforms*)

>   public void **applyConfigurationFile**(\@NonNull File file) throws
>   IOException, ParseException {

>      if (!file.isFile()) {

>          return;

>      }

>      **ConfigurationParser parser =**

>   **           new ConfigurationParser(file, System.getProperties());**

>      try {

>          **parser.parse(configuration);**

>      } finally {

>          parser.close();

>      }

>   }

ConfigurationParser.class

>   public **ConfigurationParser(**WordReader reader,

>                             Properties properties) throws IOException {

>      this.reader = reader;

>      this.properties = properties;

>      *readNextWord*();

>       // 跳过空格、注释等

>   }

>   public void **parse**(Configuration configuration)

>          throws ParseException, IOException {

>      while (nextWord != null) {

>          lastComments = reader.lastComments();

>           ……

>          // keep相关读取

>          else if (ConfigurationConstants.*KEEP_OPTION*.startsWith(nextWord)) {

>         // 读取-keep后面的内容并将其放入configuration.keep

>          configuration.keep =
>   *parseKeepClassSpecificationArguments*(configuration.keep, false, true,
>   false, false);

>          } else if
>   (ConfigurationConstants.*KEEP_IHANDY_OPTION*.startsWith(nextWord)) {

>              configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, true, true, false,
>   false);

>          } else if
>   (ConfigurationConstants.*KEEP_CLASS_MEMBERS_OPTION*.startsWith(nextWord)) {

>              configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, false, false,
>   false);

>          } else if
>   (ConfigurationConstants.*KEEP_CLASSES_WITH_MEMBERS_OPTION*.startsWith(nextWord))
>   {

>              configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, false, true,
>   false);

>          } else if
>   (ConfigurationConstants.*KEEP_NAMES_OPTION*.startsWith(nextWord)) {

>              configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, true, false, true);

>          } else if
>   (ConfigurationConstants.*KEEP_CLASS_MEMBER_NAMES_OPTION*.startsWith(nextWord))
>   {

>             configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, false, false,
>   true);

>          } else if
>   (ConfigurationConstants.*KEEP_CLASSES_WITH_MEMBER_NAMES_OPTION*.startsWith(nextWord))
>   {

>              configuration.keep =
>   parseKeepClassSpecificationArguments(configuration.keep, false, true, true);

>          }

>      }

>   }

![](media/654eee678b178c6b220664ab93262871.png)

|                                                                                                                                                       |                                                                                                                                                            | 是否保留类名    | 是否根据member决定是否保留类名 | 是否允许shrink     |                                                                        |
|-------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|--------------------------------|--------------------|------------------------------------------------------------------------|
| **Constants**                                                                                                                                         | **格式**                                                                                                                                                   | **markClasses** | **markConditionally**          | **allowShrinking** | **操作**                                                               |
| KEEP_OPTION                                                                                                                                           | \-keep                                                                                                                                                     | True            | False                          | False              | 指定类和类的成员变量是入口节点，保护它们不被移除混淆。                 |
| KEEP_CLASS_MEMBERS_OPTION                                                                                                                             | \-keepclassmembers                                                                                                                                         | False           | False                          | False              | 保护的指定的成员变量不被移除、优化、混淆。                             |
| KEEP_CLASSES_WITH_MEMBERS_OPTION                                                                                                                      | \-keepclasseswithmembers                                                                                                                                   | False           | True                           | False              | 拥有指定成员的类将被保护，根据类成员确定一些将要被保护的类。           |
| KEEP_NAMES_OPTION                                                                                                                                     | \-keepnames                                                                                                                                                | True            | False                          | True               | 指定一些类及其成员名称受到保护，前提是他们在shrink这一阶段没有被去掉。 |
| KEEP_CLASS_MEMBER_NAMES_OPTION                                                                                                                        | \-keepclassmembernames                                                                                                                                     | False           | False                          | True               | 保护指定的类成员，前提是这些成员在shrink阶段没有被删除。               |
| KEEP_CLASSES_WITH_MEMBER_NAMES_OPTION                                                                                                                 | \-keepclasseswithmembernames                                                                                                                               | False           | True                           | True               | 保护指定的类及其成员名，如果它们没有在shrink阶段被删除。               |
| 举个栗子                                                                                                                                              | 解释                                                                                                                                                       |                 |                                |                    |                                                                        |
| **-keep** class com.google.android.gms.ads.identifier.\*\* { \*; }                                                                                    | 保持com.google.android.gms.ads.identifier包下所有类及其内所有member不被混淆和shrink                                                                        |                 |                                |                    |                                                                        |
| \-**keepclassmembers** class \* implements java.io.Serializable{           private static final java.io.ObjectStreamField[] serialPersistentFields; } | 保持实现了Serializable的所有类的成员变量serialPersistentFields不被混淆和shrink                                                                             |                 |                                |                    |                                                                        |
| \-**keepclasseswithmembers** public class \* {           public static void main(java.lang.String[]); }                                               | 保持了包含main方法的类及其内的main方法不被混淆及shrink                                                                                                     |                 |                                |                    |                                                                        |
| **-keepnames** class \* implements android.os.Parcelable {           public static final \*\* CREATOR; }                                              | 保持实现了Parcelable类的所有类及其CREATOR不被混淆，但是可以被shrink                                                                                        |                 |                                |                    |                                                                        |
| **-keepclassmembernames** class \* {           \@com.google.android.gms.common.annotation.KeepName \*; }                                              | 保持被注解了[com.google.android.gms.common.annotation.KeepName](http://com.google.android.gms.common.annotation.keepname/)的member不被混淆，但是可以shrink |                 |                                |                    |                                                                        |
| **-keepclasseswithmembernames** class com.google.android.gms.internal.\*\* { \*; }                                                                    | 保持com.google.android.gms.internal包下所有类及其member不被混淆，但可以被shrink                                                                            |                 |                                |                    |                                                                        |

三. proguard流程

![](media/2342bc22fd32dff7c42c3cd24383915c.png)

————\<\<休息一下\>\>————

四. 混淆过程

（1）访问者模式

![](media/f8e81749c8cfa30167444c812074a7f2.png)

>   **优点：**

>      \* 扩展性好：
>   在不修改对象结构中的元素的情况下，为对象结构中的元素添加新的功能。

>      \* 复用性好： 通过访问者来定义整个对象结构通用的功能，从而提高复用程度。

>      \* 分离无关行为：
>   通过访问者来分离无关的行为，把相关的行为封装在一起，构成一个访问者，这样每一个访问者的功能都比较单一。

>   **缺点：**

>      \* 对象结构变化很困难：
>   不适用于对象结构中的类经常变化的情况，因为对象结构发生了改变，访问者的接口和访问者的实现都要发生相应的改变，代价太高。

>      \* 破坏封装：
>   访问者模式通常需要对象结构开放内部数据给访问者和ObjectStructrue，这破坏了对象的封装性。

（2）混淆中的访问者模式

![](media/c1408700762006002e0d90baff12f4ca.png)

![](media/4fc38afd50df716bc6bcaa66dc8e41c7.png)

    每个Visitor都实现了ClassVisitor，例如ClassCleaner的实现如下

![](media/da4846c6bae84d1223960648d6ca0a55.png)

（3）装饰器模式

![](media/a50ed4dae443b14dbdb1b0a01b7bec25.png)

>   **优点：**功能组合嵌套

>   \* 扩展对象功能，比继承灵活，不会导致类个数急剧增加

>   \* 可以对一个对象进行多次装饰，创造出不同行为的组合，得到功能更加强大的对象

>   \*
>   具体构建类和具体装饰类可以独立变化，用户可以根据需要自己增加新的具体构件子类和具体装饰子类

>   **缺点：**

>   \* 产生很多小对象。大量小对象占据内存，一定程度上影响性能

>   \* 装饰模式易于出错，调试排查比较麻烦

=\>举个栗子：混淆中的装饰器

>   libraryClassPool.classesAccept(new **AllMemberVisitor**(nameMarker));

>   programClassPool.classesAccept(new **AllMemberVisitor**(new
>   MemberObfuscator(this.configuration……)));

>   ……

如果不用AllMemberVisitor包装，每个visitor自己都要写一遍for循环，遍历所有member。

（4）根据configuration.keep将相应内容的visitInfo设为name

*Obfuscator.class*

![](media/1d506fb3b08101bcc4ab858bfa97f5d6.png)

![](media/8cbc793495d199b6cf9246cb8f7f1b83.png)

**※ 设置哪些为keep？**

*NameMarker.class*

将被visit的class的**visitInfo**设为他原本的name

>   class NameMarker extends SimplifiedVisitor implements ClassVisitor,
>   MemberVisitor, AttributeVisitor, InnerClassesInfoVisitor, ConstantVisitor {

>       ……

>       public void visitProgramClass(ProgramClass programClass) {

>           this.keepClassName(programClass);

>           programClass.attributesAccept(this);

>       }

>       public void keepClassName(Clazz clazz) {

>           **ClassObfuscator.setNewClassName(clazz, clazz.getName());**

>       }

>   }

*ClassSpecificationVisitorFactory.class*

生成可以将所有该keep的内容都visit一遍的classVisitor，并用nameMarker去visit

>   public static ClassPoolVisitor createClassPoolVisitor(KeepClassSpecification
>   keepClassSpecification, ClassVisitor classVisitor, MemberVisitor
>   memberVisitor) {

>       ……

>     //
>   如果不设置markClasses及markConditionally则不keep任何class，所以置空classVisitor

>      **if(!keepClassSpecification.markClasses &&
>   !keepClassSpecification.markConditionally) {**

>   **       classVisitor = null;**

>   **   }**

>     // 如果设置markConditionally(根据成员决定是否keep
>   class)，生成class以及member的filter
>   visitor并设置给classVisitor(通过name以及descriptor过滤)

>      if(keepClassSpecification.markConditionally) {

>          ClassVisitor composedClassVisitor =
>   createCombinedClassVisitor(keepClassSpecification, classVisitor,
>   (MemberVisitor)memberVisitor);

>          classVisitor = **createClassMemberTester**(keepClassSpecification,
>   composedClassVisitor);

>          memberVisitor = null;

>      }

>      ……

>      return createClassPoolVisitor((ClassSpecification)keepClassSpecification,
>   classVisitor, (MemberVisitor)memberVisitor);

>   }

※ 根据成员决定是否要保留这个类

生成classVistor可以根据member来过滤，找到有这些member的class

>   private static ClassVisitor createClassMemberTester(List
>   memberSpecifications, boolean isField, ClassVisitor classVisitor) {

>       if(memberSpecifications != null) {

>           for(int index = 0; index \< memberSpecifications.size(); ++index) {

>               MemberSpecification memberSpecification =
>   (MemberSpecification)memberSpecifications.get(index);

>   //每个class仅visit一次

>               **classVisitor = createClassVisitor(memberSpecification,
>   isField, new MemberToClassVisitor(classVisitor))**;

>           }

>       }

>       return classVisitor;

>   }

>   \-------------------------------------------------------------------------------------

>   private static ClassVisitor createClassVisitor(MemberSpecification
>   memberSpecification, boolean isField, MemberVisitor memberVisitor) {

>       String name =
>   [memberSpecification.name](http://memberspecification.name);

>       String descriptor = memberSpecification.descriptor;

>       boolean fullySpecified = name != null && descriptor != null &&
>   !containsWildCards(name) && !containsWildCards(descriptor);

>       if(!fullySpecified) {

>           if(descriptor != null) {

>               memberVisitor = new **MemberDescriptorFilter**(descriptor,
>   (MemberVisitor)memberVisitor);

>           }

>           if(name != null) {

>               memberVisitor = new **MemberNameFilter**(name,
>   (MemberVisitor)memberVisitor);

>           }

>       }

>       ……

>       return (ClassVisitor)(isField?

>   (fullySpecified?

>   new **NamedFieldVisitor**(name, descriptor, (MemberVisitor)memberVisitor):

>   new **AllFieldVisitor**((MemberVisitor)memberVisitor))

>   :(fullySpecified?

>   new **NamedMethodVisitor**(name, descriptor, (MemberVisitor)memberVisitor):

>   new **AllMethodVisitor**((MemberVisitor)memberVisitor)));

>   }

举个栗子：

=\>AllMethodVisitor(MemberNameFilter(**MemberToClassVisitor(上个循环的classVisitor)**))

=\>AllMethodVisitor(MemberNameFilter(**MemberToClassVisitor(AllMethodVisitor(MemberNameFilter(上上个循环的classVisitor)))**))

=\>……

=\>AllMethodVisitor(MemberNameFilter(**MemberToClassVisitor(AllMethodVisitor(MemberNameFilter(…nameMarker…)))**))

（5）Visitor过程

    混淆的过程就是依次调用visitor

![](media/f81052b18a07d40fdbdefa4a54cefa1f.png)

（6）ClassObfuscator生成新

![](media/bef61d7fc4c68b54e94d4da01a7c0c57.png)

名字

>   public void visitProgramClass(ProgramClass programClass)

>   {

>          // 是否仍需要新名字

>          newClassName = newClassName(programClass);

>      ……

>          newClassName = newClassName != null && numericClassName ?

>                  **generateUniqueNumericClassName**(newPackagePrefix) :

>                  **generateUniqueClassName**(newPackagePrefix);

>          setNewClassName(programClass, newClassName);

>      }

>   static String **newClassName**(Clazz clazz) {

>      **Object visitorInfo = clazz.getVisitorInfo();**

>      return visitorInfo instanceof String?(String)visitorInfo:null;

>   }

>   static void **setNewClassName**(Clazz clazz, String name) {

>      **clazz.setVisitorInfo(name);**

>   }

※ 生成数字名字 generateUniqueNumericClassName

>   public class **NumericNameFactory** implements NameFactory {

>      private int index;

>    ……

>      **public String nextName() {**

>   **       return Integer.toString(++this.index);**

>   **   }**

>   }

※ 生成名字 generateUniqueClassName

>   public class **DictionaryNameFactory** implements NameFactory {

>      ……

>      //
>   字典长度内的按顺序取用字典名字，如果在范围外则用nameFactory的nextName()生成，这里是SimpleNameFactory

>      **public String nextName() {**

>   **       String name;**

>   **       if(this.index \< this.names.size()) {**

>   **           name = (String)this.names.get(this.index++);**

>   **       } else {**

>   **           do {**

>   **               name = this.nameFactory.nextName();**

>   **           } while(this.names.contains(name));**

>   **       }**

>   **       return name;**

>      }

>   }

>   \--------------------------------------------------------------------------------

>   public class **SimpleNameFactory** implements NameFactory {

>       ……

>       *// 例如a,b,c,……,aa,ab,ac,……*

>     **private String newName(int index) {**

>   **       int totalCharacterCount = this.generateMixedCaseNames?52:26;**

>   **       int baseIndex = index / totalCharacterCount;**

>   **       int offset = index % totalCharacterCount;**

>   **       char newChar = this.charAt(offset);**

>   **       String newName = baseIndex == 0?new String(new
>   char[]{newChar}):this.name(baseIndex - 1) + newChar;**

>   **       return newName;**

>   **   }**

>   }

**思考题：**

1.  如果想给特定类设置特定的类名，可以通过set哪个属性，在哪一步前set来为它设置特定类名

2.  说明keepclasseswithmembers与keepclasseswithmembernames区别

3.  举个访问者模式的例子，并说明这样做的优点

附录：

1.ihandyKeep (ProGuard.java)

*shrink和optimize的时候保留markClasses =
true的行为，但是在obfuscate时保留markClasses = false（不keep class仅keep
member）的行为。*

>   if (configuration.shrink) {

>      shrink();

>   }

>   if (configuration.optimize) {

>       ……

>   }

>   // 修改 markClasses 值

>   for (Object keepClassSpecification : configuration.keep) {

>      if (keepClassSpecification instanceof KeepClassSpecification) {

>          if (((KeepClassSpecification) keepClassSpecification).isIhandyKeep) {

>              **((KeepClassSpecification) keepClassSpecification).markClasses =
>   false;**

>          }

>      }

>   }

>   if (configuration.obfuscate) {

>      obfuscate();

>   }

2.读取class文件

*Proguard.class*

![](media/36a43a00c400f1bcf0c76e05c607aac3.png)

![](media/d33f91a64f953903f99c263c6d7b1ad0.png)

*ProgramClassReader.class*

>   public void visitProgramClass(ProgramClass programClass) {

>      programClass.u4magic = this.dataInput.readInt();

>      ClassUtil.checkMagicNumber(programClass.u4magic);

>      int u2minorVersion = this.dataInput.readUnsignedShort();

>      int u2majorVersion = this.dataInput.readUnsignedShort();

>      programClass.u4version = ClassUtil.internalClassVersion(u2majorVersion,
>   u2minorVersion);

>      ClassUtil.checkVersionNumbers(programClass.u4version);

>      programClass.u2constantPoolCount = this.dataInput.readUnsignedShort();

>      programClass.constantPool = new
>   Constant[programClass.u2constantPoolCount];

>   ……

>   }

![](media/2f145360ec75c0a40b59995cfab93783.png)

3.proguard过程详情

1.压缩（Shrink）在压缩处理这一步中，用于检测和删除没有使用的类，字段，方法和属性。

2.优化（Optimize）优化代码，非入口节点类会加上private/static/final,
没有用到的参数会被删除，一些方法可能会变成内联代码。

3.混淆（Obfuscate）在混淆处理这一步中，使用a,b,c等无意义的名称，对类，字段和方法进行重命名。

4.预检（Preveirfy）预校验代码是否符合Java1.6或者更高的规范(唯一一个与入口类不相关的步骤)。android的dex中不需要，去掉这一步可以加快混淆速度

4.*NameXXXVisitor 以及 AllXXXVisitor*

>   *ClassPool.class*

>   public void **classAccept**(String className, ClassVisitor classVisitor) {

>      Clazz clazz = this.getClass(className);

>      if(clazz != null) {

>          clazz.accept(classVisitor);

>      }

>   }

>   public void **classesAccept**(ClassVisitor classVisitor) {

>       Iterator iterator = this.classes.values().iterator();

>       while(iterator.hasNext()) {

>           Clazz clazz = (Clazz)iterator.next();

>           clazz.accept(classVisitor);

>       }

>   }

>   *------------------------------------------------------------------------------------------------------NamedClassVisitor.class*

>   *// 如果classname不为空，则将所有classname的class增加visitor*

>   public class NamedClassVisitor implements ClassPoolVisitor {

>      private final ClassVisitor classVisitor;

>      private final String name;

>      public NamedClassVisitor(ClassVisitor classVisitor, String name) {

>          this.classVisitor = classVisitor;

>          this.name = name;

>      }

>      public void **visitClassPool**(ClassPool classPool) {

>          **classPool.classAccept(this.name, this.classVisitor);**

>      }

>   }

>   \------------------------------------------------------------------------------------------------------

>   *AllClassVisitor.class*

>   *// 如果classname为空，则将所有的class增加visitor*

>   public class AllClassVisitor implements ClassPoolVisitor {

>      private final ClassVisitor classVisitor;

>      public AllClassVisitor(ClassVisitor classVisitor) {

>          this.classVisitor = classVisitor;

>      }

>      public void visitClassPool(ClassPool classPool) {

>          classPool.classesAccept(this.classVisitor);

>      }

>   }

*Useful Links:*

<https://www.jianshu.com/p/60e82aafcfd0> 混淆详细用法

<https://www.jianshu.com/p/3d89e3c2a081> 混淆概要用法

<https://blog.csdn.net/ouxie/article/details/53320029> 混淆问题refer

<https://www.cnblogs.com/yxwkf/p/5222589.html> class文件解析

<https://blog.csdn.net/zhangjg_blog/article/details/21486985> class文件格式

<https://www.guardsquare.com/en/products/proguard/manual/usage/attributes>
支持的attribute

<https://www.jianshu.com/p/2f8a2c1e2a11> 混淆各种参数详解

<https://blog.csdn.net/ahaitongxue/article/details/80402710> 通配符

回答：

1. setVisitInfo

2. keepclasseswithmembers 根据类成员确定一些将要被保护的类，不允许shrink

   keepclasseswithmembernames = keepclasseswithmembers + allowShrinking

3.
