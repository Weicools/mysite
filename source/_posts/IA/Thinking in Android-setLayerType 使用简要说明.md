神秘的setLayerType方法可以传入三种参数：

LAYER_TYPE_NONE：默认情况下，为此种LayerType

![](media/bd508b84e0edd5b54133a8d4c7211ba7.png)

LAYER_TYPE_SOFTWARE：缓存一张bitmap，如果不发生重绘，CPU则直接使用这张bitmap上交到GPU进行绘制，当应用的硬件加速启动时，设置该种参数，会导致在该View上的硬件加速失效

LAYER_TYPE_HARDWARE：缓存一张硬件纹理，如果不发生重绘，GPU直接使用这张纹理进行绘制，当硬件加速未启动时，则效果同LAYER_TYPE_SOFTWARE相同

调用setLayerType方法并不能启动硬件加速，调用该方法并传入LAYER_TYPE_SOFTWARE仅能使调用该方法的View硬件加速失效，在整体启用硬件加速的情况下，调用LAYER_TYPE_SOFTWARE再调用LAYER_TYPE_HARDWARE会使View的硬件加速再起启用，但调用LAYER_TYPE_NONE并不能关闭硬件加速和启动硬件加速

关于View的isHardwareAccelerated()方法和Canvas的isHardwareAccelerated()方法，View的方法返回值在一开始就是确定的，即使该View未启用硬件加速还是可能返回值为true，Canvas的isHardwareAccelerated()方法返回值是相对准确的

通过一张图来说明LAYER_TYPE_HARDWARE的一个典型用法

![](media/6327099467a1526976cbf4660138f46b.png)

再谈硬件加速：

   
Android的UI系统是非常复杂的，图形界面的绘制细节，留到以后我们在讨论，对于开启与关闭硬件加速的区别，我们可以简单的理解为：

    关闭：假设某一个View调用invalidate()，整个View树都会重新绘制，提交给GPU

   
开启：我们把View树分层存在一个DisplayList中，调用某一个View的invalidate()，我们只重新绘制这个View并重新把这个DisplayList提交给GPU就可以了
