1.为什么静态广播都是串行处理的？

2.查看一下AMS的代码，看一下AMS中存在几个BroadcastQueue对象，分别代表着什么？

3.存在下面注册的三个广播，当分别发送有序广播和无序广播时，执行顺序分别是什么（不确定的话写代码试一下）？

![](media/7df226fb555bfc4795fe9e15dbef6210.png)

![](media/d5b2c681ead7ab07340ab49e4c381425.png)

\<此处有一个BroadcastReceiver.mdj文件，请到BroadcastReceiver思考题以及文档.resources文件中查看\>

\<此处有一个BroadcastReceiver.key文件，请到BroadcastReceiver思考题以及文档.resources文件中查看\>

\@hao.su

\@jian.zhang

\@di.yao

\@long.liu

\@baoqi.guo

\@zhenliang.zhou

\@zhixiang.xiao

\@zhengwei.zhang

\@guolong.peng

\@meng.wang

\@xiaojian.li

\@chenhao.sun

\@guang.liu

\@hui.zhu

\@wenjie.li

关于todo

1.粘性广播废弃原因

![](media/fc650505d40c2b08a3b0182164edc94b.png)

可以在BroadcastReceiver中的OnReceiver方法中使用isInitialStickyBroadcast()来确定粘性广播是注册的时候收到的还是sendBroadcast的时候收到的，如果是true，则表示是注册的时候立刻执行了onReceiver方法。

2.关于广播权限的使用方法

<https://juejin.im/post/5a15586a51882578da0d84ac>

比较易懂
