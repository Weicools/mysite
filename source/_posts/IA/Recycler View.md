A flexible view for providing a limited window into a large data set.

Android是一个不断进化的平台，Android
5.0的v7版本支持包中引入了新的RecyclerView控件，正如官方文档所言，RecyclerView是ListView的豪华增强版。

它主要包含以下几处新的特性，如ViewHolder，ItemDecorator，LayoutManager，SmothScroller以及增加或删除item时item动画等。官方推荐我们采用RecyclerView来取代ListView。

![](media/0982a61a7e585d2163fd2c533396913c.png)

![](media/d320f5511303e8d36b3b5307e4783ad9.png)

**ViewHolder**

 
ViewHolder是用来保存视图引用的类，无论是ListView亦或是RecyclerView。只不过在ListView中，ViewHolder需要自己来定义，且这只是一种推荐的使用方式，不使用当然也可以，这不是必须的。只不过不使用ViewHolder的话，ListView每次getView的时候都会调用findViewById(int)，这将导致ListView性能展示迟缓。而在RecyclerView中使用RecyclerView.ViewHolder则变成了必须，尽管实现起来稍显复杂，但它却解决了ListView面临的上述不使用自定义ViewHolder时所面临的问题。

viewHolder中主要的参数：

![](media/da177cb356a88f7b2cd448bba892e66d.png)

mFlags :

 

FLAG_BOUND:
 ViewHolder已经绑定到某个位置，mPosition、mItemId、mItemViewType都有效 

FLAG_UPDATE:
ViewHolder绑定的View对应的数据过时需要重新绑定，mPosition、mItemId还是一致的

FLAG_INVALID:  ViewHolder绑定的View对应的数据无效，需要完全重新绑定不同的数据 

FLAG_REMOVED:  ViewHolder对应的数据已经从数据集移除 

FLAG_NOT_RECYCLABLE:  ViewHolder不能复用 

FLAG_RETURNED_FROM_SCRAP:  这个状态的ViewHolder会加到scrap list被复用。 

FLAG_CHANGED:  ViewHolder内容发生变化，通常用于表明有ItemAnimator动画 

FLAG_IGNORE:  ViewHolder完全由LayoutManager管理，不能复用 

FLAG_TMP_DETACHED——ViewHolder从父RecyclerView临时分离的标志，便于后续移除或添加回来 

FLAG_ADAPTER_POSITION_UNKNOWN——ViewHolder不知道对应的Adapter的位置，直到绑定到一个新位置 

FLAG_ADAPTER_FULLUPDATE——方法addChangePayload(null)调用时设置

**Adapter**

RecyclerView

![](media/5a677f2ea3be170de6a132a276d4341f.png)

ListView

![](media/03feaf4b1ec20e560f3230ccc2c35d1b.png)

**RecycledViewPool**

RecyclerViewPool用于多个RecyclerView之间共享View。

只需要创建一个RecyclerViewPool实例，然后调用RecyclerView的setRecycledViewPool(RecycledViewPool)方法即可。

RecyclerView默认会创建一个RecyclerViewPool实例。

关键数据：

![](media/15afe0442210ffd919e0cea18ff2d7eb.png)

关键方法：

![](media/1778b9b453b7cc9a41cf2b6905768944.png)

![](media/22f3b77968382f16aab75b09c58ac649.png)

**ViewCacheExtension**

由开发者控制的可以作为View缓存的帮助类（默认Recycler不会做缓存View的操作）。

调用Recycler.getViewForPosition(int)方法获取View时

Recycler先检查attached scrap和一级缓存

如果没有则检查ViewCacheExtension.getViewForPositionAndType(Recycler, int, int)

如果没有则检查RecyclerViewPool

![](media/3d1241669797b041ba91acdc7b1060fd.png)

**Recycler**

![](media/4f5a87d21956ae7719d992a54f66f191.png)

获取某个位置需要展示的View，先检查是否有可复用的View，没有则创建新View并返回。具体过程为：
 

（1）检查mChangedScrap，若匹配到则返回相应holder  

（2）检查mAttachedScrap，若匹配到且holder有效则返回相应holder  

（3）检查mViewCacheExtension，若匹配到则返回相应holder  

（4）检查mRecyclerPool，若匹配到则返回相应holder  

（5）否则执行Adapter.createViewHolder()，新建holder实例  

（6）返回holder.itemView 

**ItemDecoration**

一个抽象类，顾名思义，就是用来装饰RecyclerView的子item的，通过名字就可以知道，功能并不仅仅是添加间距绘制分割线，是用来装饰item的。

![](media/adc49601365e8d6e0259554ad9355bc2.png)

**ItemAnimator / SimpleAnimator**

![](media/6133fa1dfd6c801ca8b34a7ab16b8bc0.png)

animateRemove（Add、Move和Change）：这些方法会在动画发生的时候回调，一般会在这个方法中用列表记录每个Item的动画以及属性

endAnimation、endAnimations：分别是在一个Item或是多个Item需要立即停止的时候回调

isRunning：如果需要顺畅滑动的时候，必须要重写这个方法，很多时候比如在网络加载的时候滑动卡顿就是这个方法逻辑不对

 runPendingAnimations：这是最重要的一个方法。因为animateDisappearence等方法返回的是animateRemove等方法返回的值，而这个方法则是根据这些值来确定是否有准备好的动画需要播放，如果有，就会回调这个方法。在这个方法我们需要处理每一个动作（Remove、Add、Move以及Change）的动画

一般步骤就是：

1.  创建一个SimpleItemAnimator的子类

2.  创建每个动作的动作列表

3.  重写animateRemove等方法，当界面中有动作发生，这些函数会被回调，这里进行记录并返回true使得runPendingAnimations开始执行

4.  重写runPendingAnimations，当③的方法返回true的时候，就认为需要执行动画，我们需要把动画执行的逻辑写在这里面

5.  重写isRunning，提供动画播放状态，一般是返回动作列表是否为空

6.  如果有需要，重写endAnimation、endAnimations、onRemoveFinish等方法

**LayoutManager**

我们知道ListView只能在垂直方向上滚动，Android
API没有提供ListView在水平方向上面滚动的支持。或许有多种方式实现水平滑动，但是ListView并不是设计来做这件事情的。

RecyclerView相较于ListView，在滚动上面的功能扩展了许多。它可以支持多种类型列表的展示要求，主要如下：

LinearLayoutManager，可以支持水平和竖直方向上滚动的列表。

StaggeredGridLayoutManager，可以支持交叉网格风格的列表，类似于瀑布流或者Pinterest。

GridLayoutManager，支持网格展示，可以水平或者竖直滚动，如展示图片的画廊。

主要作用是，测量和摆放RecyclerView中ItemView，以及当ItemView对用户不可见时循环复用处理。

通过设置LayoutManager的属性，可以实现水平滚动、垂直滚动、方块表格等列表形式。

其内部类Properties包含了所需要的大部分属性。

**触摸事件监听**（OnItemTouchListener， SimpleOnItemTouchListener）

关于Item项的手势监听事件，如单击和双击没有像其他AdapterView一样提供具体接口但是RcyclerView提供OnItemTouchListener，
SimpleOnItemTouchListener，可以通过继承去实现自定义的事件监听。
