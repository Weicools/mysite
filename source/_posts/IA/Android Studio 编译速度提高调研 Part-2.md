**打包流程**

![](media/9d152dc4fb99d030b3f8b119c680ed18.jpg)

-   aapt执行2次

-   每次构建都是一个全新的过程，所以每次构建时间恒定，代码量越多，构建时间越慢

-   全量编译

**增量编译**

-   增量编译工具

    -   Android Studio Instant Run  - 官方

    -   LayoutCast

    -   Buck  -  Facebook

    -   Freeline - 阿里开源

    -   JRebel -  Zeroround

-   增量编译思想

    -   拆解连接设备与工程间扫描以及构建增量包任务     

    -   将应用的资源打成资源包，推送到设备上

    -   运行期间，手机端反射R文件生成ids.xml和public.xml，确保增量包的资源ID和全量包ID一致

    -   代码增量使用dex插入系统dexlist最前位置方式

-   存在问题

    -   资源打成资源包推送，如果资源文件过大，还是会很慢 

    -   反射R文件，效率低下

    -   存在枚举常量时，无法对其排除，导致数组越界，致命缺陷

    -   dex插入系统方式，由于安全校验问题，4.X系统无法运行

-   进化方案

    -   任务拆解为可并发执行的子任务，发挥多核优势  macbook 默认16线程

    -   增加缓存机制，缩减编译时间

**横向对比**

-   通病： full-build -\> 奇慢

-   **Android Studio**

    -   **优点**

        -   官方支持

        -   稳定

        -   0配置

        -   持续优化

    -   **缺点**

        -   只支持资源修改，JAVA文件修改需要重启整个应用

        -   5.0以下不支持

-   **Freeline & Buck**

    -   **优势**

        -   构建过程只编译本次参与修改的部分

        -   增量范围最小化

        -   懒加载

        -   多级缓存

        -   依赖管理

        -   长链接无安装式运行期动态替换

        -   不支持删除带ID的资源  freeline

    -   **劣势**

        -   clear data后，只能全量编译

-   **JRebel**

    -   **优势：**

        -   配置简单，安装插件即可

        -   比InstantRun更早，热部署经验丰富

    -   **劣势**

        -   收费，价格不菲   \$475/Y/P

        -   Crash后需要重新全量编译

**Instant Run**

-   启动方式

    -   Hot Swap 10.7X

        -   已存在函数内部修改

        -   只支持主进程, 其他进程按Cold Swap走

    -   Warm Swap

        -   修改或移除已存在的资源

    -   Cold Swap  4.7X

        -   大多数其他类修改

    -   Full Build

        -   基于AndroidManifest的修改

        -   改变系统UI元素，widget, notification

-   其他：

    -   不能用pro guard

**官方推荐加快编译速度方式**

-   <https://developer.android.com/studio/build/optimize-your-build.html>

-   确保SDK TOOLS和GRADLE插件版本最新

-   为debug环境创建flavorf, minsdk为21

-   resConfigs, 避免编译不需要的资源

-   ext.enableCrashlytics = false, 关闭Crashlytics功能

-   Fabric初始化时，对Crashlytics进行debug模式过滤

    ![](media/a1112d53a2c37805980bbd79da9ed12b.png)

-   不要动态设置versionCode, versionName

-   依赖不要用”+”模式，使用确切版本

-   Configure on demand 打开

-   preDexLibraries true

-   maxProcessCount 8

-   javaMaxHeapSize 2048m

-   png -\> webP

**技巧**

-   **./gradlew --profile --recompile-scripts --offline --rerun-tasks
    assembleOneAppMaxDebug 生成报告**

    ![](media/89a61a06939f6357dceae468e5f80f48.png)

-   <https://plus.google.com/u/0/+RicardoAmaral/posts/e9PG6vSN5w3> 
     **命令行与AndroidStudio对比**

    -   **Command Line:** gradlew clean assembleDebug **2min 0sec**

    -   **Command Line:** gradlew assembleDebug **1min 5sec**

    -   **Android Studio:** Build » Rebuild Project **4min 0sec**

    -   **Android Studio:** Build » Make Project **2min 40sec**

    -   **Android Studio:** Run / Debug **2min 0sec**

**趋势**

![](media/347fce468af1e40d530acec421aaa8b8.jpg)

**References**

<http://www.shixunwang.net/article/289323968069/>  编译工具对比
