一 简介

Animation(动画),简单点说就是在一段时间内,显示的内容发生了变化。

Core
Animation(核心动画)是一组功能强大、效果华丽的动画API，无论在iOS系统或者在开发的App中，都有大量应用。

Core Animation所在的位置如下图所示：

![](media/a8b4acd3793c53b3de2315907b286306.png)

iOS动画相关框架

从上图可以发现，UIKit是常用的框架，显示、动画都通过Core Animation, Core
Animation是核心动画，位于UIKit的下一层，依赖于OpenGl ES做GPU渲染，依赖于Core
Graphics做CPU渲染。最底层的Graphics HardWare是图形硬件。

iOS动画主要是指Core Animation框架，你可能认为它只是用来做动画的，但是Core
Animation并不仅仅是用来做动画的，动画只是其特性的冰山一角。Core
Animation是iOS和Mac
OS平台上负责图形渲染与动画的基础框架，但是我们本次重点是介绍其中的做动画的部分。

Core Animation是直接作用在CALayer上的，并非UIView，从CALayer(CA = Core
Animation)类名来看就可以看出iOS的Layer就是为动画而生的。Core
Animation的核心是CALayer对象。

二 UIView(视图)和CALayer(图层)的关系

![](media/58579b06fd461013ec1a640a2a2c2a24.png)

UIView与CALayer关系图

-   UIView属于iOS中的类，而CALayer属于iOS和Max OS共用的类

-   UIView继承自UIResponder，可以接收并响应事件,CALayer继承自NSObject不处理用户的交互。

-   每一个UIView都有一个CALayer实例的图层属性。实际上这些背后关联的图层才是真正用来在屏幕上显示和做动画，UIView仅仅是对它的一个封装。

-   UIView的职责就是创建并管理这个CALayer，以确保当子视图在层级关系中添加或者被移除的时候，他们关联的图层也同样对应在层级关系树当中有相同的操作。

-   UIView的属性只是简单地返回了CALayer对象的属性，对于CALayer对象，改变其属性，都会触发一个简单动画。

但是为什么iOS要基于UIView和CALayer提供两个平行的层级关系？为什么不用一个简单的层级来处理所有事情？

原因在于要做职责分离，这样也能避免很多重复代码。在iOS和Mac
OS两个平台上，事件和用户交互有很多地方的不同，基于多点触控的用户界面和基于鼠标键盘有着本质的区别，这就是为什么iOS有UIKit和UIView，但是Mac
OS有AppKit和NSView的原因。他们功能上很相似，但是在实现上有着显著的区别。绘图，布局和动画，把这种功能的逻辑分开并应用到独立的Core
Animation框架，苹果就能够在iOS和Mac
OS之间共享代码，使得对苹果自己的OS开发团队和第三方开发者去开发两个平台的应用更加便捷。

三 Core Animation动画家谱

![](media/e76410d69f6295ff063e3e8ca689feec.png)

注：UML图中'+'表示属性或操作具有公共可见性，并非iOS中的+号修饰

从上图的协议以及类的属性入手，分析一下上图结构：

**CAMediaTiming**协议中定义了时间，重复次数等。属性定义如下：

\+duration: 动画的持续时间

\+beginTime: 动画的开始时间 从开始延迟几秒的话，设置为【CACurrentMediaTime() +
秒数】 的方式

\+repeatCount: 动画的重复次数

\+autoreverses: 执行的动画按照原动画返回执行 动画结束时是否执行逆动画

\+fillMode:决定当前对象在非active时间段的行为，比如动画开始之前或结束之后(想要fillMode有效，最好设置removedOnCompletion=NO)

-   kCAFillModeRemoved
    这个是默认值，当动画开始前和结束后，动画对layer没有影响，动画结束后，layer恢复到之前的状态

-   kCAFillModeForwards 当动画结束后，layer会一直保持动画最后的状态

-   kCAFillModeBackwards
    当动画开始之前，只需要将动画加入了一个layer，layer便立即进入动画的初始状态并等待动画开始

-   kCAFillModeBoth 这个其实是上面两个的合成

**CAAnimation**核心动画基础类，不能直接使用。除了CAMediaTiming协议中的方法，增加了CAAnimationDelegate的代理属性等。具体属性定义如下：

\+timingFunction:
CAMediaTimingFunction类，控制动画的显示节奏。关于CAMediaTimingFunction，系统提供五种选择以及自定义贝塞尔曲线。分别是：

-   kCAMediaTimingFunctionLinear 线性动画，控制点为：(0.0,0.0) and (1.0,1.0)

-   kCAMediaTimingFunctionEaseIn 先慢后快（慢进快出）控制点为：(0.42,0.0) and
    (1.0,1.0)

-   kCAMediaTimingFunctionEaseOut 先快后慢（快进慢出）控制点为： (0.0,0.0) and
    (0.58,1.0)

-   kCAMediaTimingFunctionEaseInEaseOut 先慢后快再慢 控制点为：(0.42,0.0) and
    (0.58,1.0)

-   kCAMediaTimingFunctionDefault 默认，也属于中间比较快 控制点为：(0.25,0.1)
    and (0.25,1.0)

-   functionWithControlPoints::::
    自定义贝塞尔曲线，第一个和最后一个点代表了曲线的起点和终点，剩下的中间两个点叫做控制点，它们控制了曲线的形状，贝塞尔曲线的控制点其实位于曲线之外，也就是说曲线并不一定要穿过它们，你可以把它们想象成吸引经过它们曲线的磁铁。这其实就是AE文件曲线上对应的控制点。(注意这里奇怪的语法，并没有包含具体每个参数的名称，这在objective-C中是合法的，但是却违反了苹果对方法命名的指导方针)

标准贝塞尔曲线函数：

![](media/670cab907dbbf3ac89986283342a97cd.png)

**+delegate**: 动画代理。能够检测动画的执行和结束。代码

>   \@interface NSObject (CAAnimationDelegate)

>   \- (void)animationDidStart:(CAAnimation \*)anim;

>   \- (void)animationDidStop:(CAAnimation \*)anim finished:(BOOL)flag;

>   \@end

**removedOnCompletion **默认为YES,代表动画结束后就从图层上移除，图形会恢复到动画执行前的状态。如果想让图层保持显示动画执行后的状态，就设置为NO，同时还需要设置fillMode为kCAFillModeForwards。

**CAPropertyAnimation**属性动画，针对对象的可动画属性进行效果的设置，不可直接使用。属性定义如下：

\+keyPath: CALayer的某个属性名，并通过这个属性的值进行修改，达到相应的动画效果。

**CABasicAnimation** 基础动画，通过keyPath对应属性进行控制，只能从一个数值（fromValue）变换成另一个数值（toValue）

**CAKeyframeAnimation**关键帧动画，同样通过keyPath对应属性进行控制，但它可以通过values或者path进行多个阶段的控制。属性定义如下：

\+values: 关键帧组成的数组,里面的元素称为”关键帧”(keyframe)，动画对象会在指定的时间(duration)内，依次显示values数组中的每一个关键帧

\+path:
可以设置一个CGPathRef\\CGMutablePathRef,让层跟着路径移动。优先级比values高，但是只对CALayer的anchorPoint和position起作用。如果你设置了path，那么values将被忽略。

\+keyTimes:
可以为对应的关键帧指定对应的时间点,其取值范围为0到1.0,keyTimes中的每一个时间值都对应values中的每一帧.当keyTimes没有设置的时候,各个关键帧的时间是平分的。

**CABasicAnimation** 可看做是最多只有2个关键帧的CAKeyframeAnimation。

**CATransition** 过渡动画,用于做过渡动画或者转场动画，能够为图层提供移出屏幕和移入屏幕的动画效果。UINavigationController就是通过CATransition实现了将控制器的视图推入屏幕的动画效果

\+type: 动画过渡类型,Apple 官方的SDK其实只提供了四种过渡效果。

-   kCATransitionFade 渐变效果

-   kCATransitionMoveIn 进入覆盖效果

-   kCATransitionPush 推出效果

-   kCATransitionReveal 揭露离开效果

私有API提供了其他很多非常炫的过渡动画，比如\@"cube"、\@"suckEffect"、\@"oglFlip"、\@"rippleEffect"、\@"pageCurl"、\@"pageUnCurl"、\@"cameraIrisHollowOpen"、\@"cameraIrisHollowClose"等。**注意点** 私有api，不建议开发者们使用。因为苹果公司不提供维护，并且有可能造成你的app审核不通过。

\+subType: 动画过渡方向

-   kCATransitionFromRight 从右侧进入

-   kCATransitionFromLeft 从左侧进入

-   kCATransitionFromTop 从顶部进入

-   kCATransitionFromBottom 从底部进入

**CAAnimationGroup** 组动画,
CAAnimation的子类，可以保存一组动画对象，将CAAnimationGroup对象加入层后，组中所有动画对象可以同时并发运行。

\+animations: 用来保存一组动画对象的NSArray 

**Core Animation使用步骤**

1.  首先得有CALayer

2.  初始化一个CAAnimation对象，并设置一些动画相关属性

3.  通过调用CALayer的addAnimation:forKey:
    方法，增加CAAnimation对象到CALayer中，这样能开始执行动画了

4.  通过调用CALayer的removeAnimationForKey:方法，可以停止CALayer中的动画

四 Core Animation动画原理篇

Core Animation基于一个假设：屏幕上的任何东西都可以做动画。动画并不需要你在Core
Animation中手动打开，相反，需要明确地关闭，否则它会一直存在。

当你改变CALayer的一个可做动画的属性，它并不能立刻在屏幕上体现出来。相反，它是从先前的值平滑过渡到新的值。

现在思考一下几个问题：

1.  当改变一个属性的时候，Core Animation是如何判断动画的类型和持续时间的？

2.  当改变一个属性的时候，Core
    Animation并没有立刻生效，而是通过一段时间的渐变更新，这是怎么做到的？

3.  当改变UIView属性的时候，为什么没有动画效果？

接下来会从一下几个方面解释这几个问题：

1.  **动画的事务**

在Core
Animation里面存在事务(CATransaction)这样一个概念，它负责协调多个动画更新显示操作。事务是核心动画里面的一个基本单元，动画的产生必然伴随着layer的Animatable属性的变化，而layer属性的变化必须属于某一个事务。因此核心动画依赖事务，任何改变可以做动画的图层属性都不会立刻发生变化，而是当事务一旦提交的时候开始用一个动画过渡到新值。事务的作用是保证一个或多个layer的一个或多个属性变化同时进行。

事务是通过CATransaction类来做管理，这个类的设计有些奇怪，并不像它的命名预期的那样去管理一个简单的事务，而是管理了一组你不能访问的事务。CATransaction没有属性或者实例方法，并且也不能用+alloc和-init方法创建它。但是可以用+begin和+commit分别来入栈或者出栈。

事务分为隐式和显式：

隐式：没有明显调用事务的方法，由系统自动生成事务。比如直接设置一个layer的position属性，则会在当前线程自动生成一个事务，并在下一个runloop中自动commit事务。

显式：明显调用事务的方法 ([CATransaction begin])和([CATransaction commit])

Core Animation在每个run
loop周期中自动开始一次新的事务，即使你不显式的用[CATransaction
begin]开始一次事务，任何在一次run
loop循环中属性的改变都会被集中起来，然后做一次0.25秒的动画。

事务的可设置属性：

-   animationDuration：动画时间， 默认0.25秒

-   animationTimingFunction：动画时间曲线

-   disableActions：是否关闭动画， 默认打开

-   completionBlock：动画执行完毕的回调

使用CATransaction控制动画：

>   \- (IBAction)changeColor

>   {

>       //begin a new transaction

>       [CATransaction begin];

>       //set the animation duration to 1 second

>       [CATransaction setAnimationDuration:1.0];

>       //randomize the layer background color

>       CGFloat red = arc4random() / (CGFloat)INT_MAX;

>       CGFloat green = arc4random() / (CGFloat)INT_MAX;

>       CGFloat blue = arc4random() / (CGFloat)INT_MAX;

>       self.colorLayer.backgroundColor = [UIColor colorWithRed:red green:green
>   blue:blue alpha:1.0].CGColor;

>   //add the spin animation on completion

>       [CATransaction setCompletionBlock:\^{

>           //rotate the layer 90 degrees

>           CGAffineTransform transform = self.colorLayer.affineTransform;

>           transform = CGAffineTransformRotate(transform, M_PI_2);

>           self.colorLayer.affineTransform = transform;

>       }];

>       //commit the transaction

>       [CATransaction commit];

>   }

>   我们都用过UIView的动画方法做过一些动画效果，UIView有两个方法，+beginAnimations:context:和+commitAnimations，和CATransaction的+begin和+commit方法类似。实际上在+beginAnimations:context:和+commitAnimations之间所有视图或者图层属性的改变而做的动画都是由于设置了CATransaction的原因。

>   在iOS4中，苹果对UIView添加了一种基于block的动画方法：+animateWithDuration:animations:。这样写对做一堆的属性动画在语法上会更加简单，但实质上它们都是在做同样的事情。

>   CATransaction的+begin和+commit方法在+animateWithDuration:animations:内部自动调用，这样block中所有属性的改变都会被事务所包含。这样也可以避免开发者由于对+begin和+commit匹配的失误造成的风险。

1.  **图层行为**

行为：当改变属性时CALayer自动应用的动画称作行为。

为什么当我们仅改变某个属性的值时，它竟然会自动的作出相应的动画来。当我们改变颜色时，它会有颜色渐变的过渡动画，当我们修改尺寸时，它会有尺寸的渐变动画等等。

当属性被修改时，CALayer会查找改属性对应的行为，即相应改作出的动画类型是什么。具体步骤是这样的：

1.  首先CALayer检查自己是否有CALayerDelegate协议的代理，并且检查是否实现了-actionForLayer:forKey:方法。如果有，则直接调用，并返回结果，即改属性对应动画的行为。

2.  若没有代理，或代理类未实现上面的代理方法，CALayer接着检查自己的actions属性，它是一个以属性名为key,
    以图层行为为value的字典，可以使用它来存储图层的自定义行为。若在字典中查得改属性对应的动画行为，则返回。

3.  若在actions字典中未查得改属性的行为，则CALayer继续在其属性style字典中查找。这是一个字典，用于存储未明确由CALayer定义的属性值。

4.  若在style字典中仍未查得改属性对应的行为，那么CALayer将会直接调用定义了每个属性的标准行为的defaultActionForKey:方法。

经过以上这么一轮检索，要么返回一个图层的行为，要么返回nil，此时就不会有动画发生。

1.  **呈现图层**

改变一个CALayer的属性并没有立刻生效，而是通过一段时间渐变更新。这是怎么做到的呢？

当你改变一个图层的属性，属性值的确是立刻更新的，但是屏幕上并没有马上发生改变。这是因为你设置的属性并没有直接调整图层的外观，相反，他只是定义了图层动画结束之后将要变化的外观。

当设置CALayer的属性，实际上是在定义当前事务结束之后图层如何显示的模型。Core
Animation扮演了一个控制器的角色，并且负责根据图层行为和事务设置去不断更新视图的这些属性在屏幕上的状态。这意味着CALayer除了“真实”值之外，必须要知道当前显示在屏幕上的属性值的记录。每个图层属性的显示值都被存储在一个叫做呈现图层的独立图层当中，他可以通过-presentationLayer方法来访问。这个呈现图层实际上是目标图层的复制，但是它的属性值代表了在任何指定时刻当前外观效果。换句话说，你可以通过呈现图层的值来获取当前屏幕上真正显示出来的值。

动画渲染流程

Core
Animation处在iOS的核心地位：应用内和应用间都会用到它。一个简单的动画可能同步显示多个app的内容，例如当在iPhone上多个程序之间使用手势切换，会使得多个程序同时显示在屏幕上。在一个特定的应用中用代码实现它是没有意义的，因为在iOS中不可能实现这种效果（App都是被沙箱管理，不能访问别的视图）。动画和屏幕上组合的图层实际上被一个单独的进程管理，而不是你的应用程序。这个进程就是所谓的渲染服务。

![](media/6a1f45601cf7f35ca617ea83eb5523ea.png)

当运行一段动画时候，这个过程会被四个分离的阶段：

1.  布局 -
    这是准备你的视图/图层的层级关系，以及设置图层属性（位置，背景色，边框等等）的阶段。

2.  显示 -
    这是图层被绘制的阶段。查询是否有重写-drawRect:和-drawLayer:inContext:方法，如果有重写的话，这里的渲染是会占用CPU资源进行处理的。

3.  准备 - 这是Core Animation准备发送动画数据到渲染服务的阶段。这同时也是Core
    Animation将要执行一些别的事务例如解码动画过程中将要显示的图片的时间点。

4.  提交 - 这是最后的阶段，Core
    Animation打包所有图层和动画属性，然后通过IPC（内部处理通信）发送到渲染服务进行显示。

但是这些仅仅阶段仅仅发生在应用程序之内，在动画在屏幕上显示之前仍然有更多的工作。一旦打包的图层和动画到达渲染服务进程，渲染服务进程会解析提交的图层和动画属性，然后对动画的每一帧做出如下工作：

-   对所有的图层属性计算中间值，设置OpenGL几何形状（纹理化的三角形）来执行渲染

-   在屏幕上渲染可见的三角形

所以一共有六个阶段；最后两个阶段在动画过程中不停地重复。前五个阶段都在软件层面处理（通过CPU），只有最后一个被GPU执行。而且，你真正只能控制前两个阶段：布局和显示。Core
Animation框架在内部处理剩下的事务，你也控制不了它。

五 核心动画案例

基础动画

![](media/e72c26cb20fb188aa1b496eb63415a99.gif)

>   // 使用CABasicAnimation 创建基础动画

>   CABasicAnimation \*animation = [CABasicAnimation
>   animationWithKeyPath:\@"position"];

>   animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(0,
>   SCREEN_HEIGHT/2)];

>   animation.toValue = [NSValue valueWithCGPoint:CGPointMake(SCREEN_WIDTH,
>   SCREEN_HEIGHT/2)];

>   animation.duration = 3;

>   [self.animateView.layer addAnimation:animation
>   forKey:\@"positionAnimation”];

>   // UIView 代码块调用

>   self.animateView.center = CGPointMake(0, SCREEN_HEIGHT/2);

>   [UIViewanimateWithDuration:3animations:\^{

>       self.animateView.center = CGPointMake(SCREEN_WIDTH, SCREEN_HEIGHT/2);

>   }];

关键帧动画

![](media/35233a9366548d099bc6c3f97fd694e8.gif)

>   UIBezierPath \*bezierPath = [[UIBezierPath alloc] init];

>   [bezierPath moveToPoint:CGPointMake(0, 300)];

>   [bezierPath addCurveToPoint:CGPointMake(400, 400)
>   controlPoint1:CGPointMake(100, 0) controlPoint2:CGPointMake(150, 700)];

>   CAShapeLayer \*pathLayer = [CAShapeLayer layer];

>   pathLayer.path = bezierPath.CGPath;

>   pathLayer.fillColor = [UIColor clearColor].CGColor;

>   pathLayer.strokeColor = [UIColor redColor].CGColor;

>   pathLayer.lineWidth = 3;

>   [self.view.layer addSublayer:pathLayer];

>   CAKeyframeAnimation \*animation = [CAKeyframeAnimation animation];

>   animation.keyPath = \@"position";

>   animation.duration = 5;

>   animation.path = bezierPath.CGPath;

>   animation.rotationMode = kCAAnimationRotateAuto;

>   [self.animationView.layer addAnimation:animation
>   forKey:\@"keyframeAnimation"];

过渡动画

![](media/ef8026918fa668294da3c397047c1f61.gif)

>   CATransition \*anima = [CATransition animation];

>   anima.type = kCATransitionMoveIn;//设置动画的类型

>   anima.subtype = kCATransitionFromRight; //设置动画的方向

>   anima.duration = 0.25f;

>   [self.animationView.layer addAnimation:anima forKey:\@"moveInAnimation"];

组动画

![](media/d58b40ead6810e3655286e687e805453.gif)

>   //位移动画

>   CABasicAnimation \*anima1 = [CABasicAnimation
>   animationWithKeyPath:\@"position"];

>   anima1.fromValue = [NSValue valueWithCGPoint:CGPointMake(0,
>   SCREEN_HEIGHT/2-75)];

>   anima1.toValue = [NSValue valueWithCGPoint:CGPointMake(SCREEN_WIDTH/2,
>   SCREEN_HEIGHT/2-75)];

>   anima1.fillMode = kCAFillModeForwards;

>   anima1.removedOnCompletion = NO;

>       //缩放动画

>   CABasicAnimation \*anima2 = [CABasicAnimation
>   animationWithKeyPath:\@"transform.scale"];

>   anima2.fromValue = [NSNumber numberWithFloat:0.8f];

>   anima2.toValue = [NSNumber numberWithFloat:2.0f];

>   anima2.fillMode = kCAFillModeForwards;

>   anima2.removedOnCompletion = NO;

>       //旋转动画

>   CABasicAnimation \*anima3 = [CABasicAnimation
>   animationWithKeyPath:\@"transform.rotation"];

>   anima3.toValue = [NSNumber numberWithFloat:M_PI\*4];

>   anima3.fillMode = kCAFillModeForwards;

>   anima3.removedOnCompletion = NO;

>       //组动画

>   CAAnimationGroup \*groupAnimation = [CAAnimationGroup animation];

>   groupAnimation.animations = [NSArray arrayWithObjects:anima1,anima2,anima3,
>   nil];

>   groupAnimation.duration = 4.0f;

>   [self.animationView.layer addAnimation:groupAnimation
>   forKey:\@"groupAnimation"];
