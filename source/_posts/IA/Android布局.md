此文档面向PM，设计师以及DEV初学者。目标是让PM和设计师了解一点android布局知识，在一定程度上确保设计图在移交给DEV前就能确定是可实现的，并且不会出适配问题。同时让DEV初学者知道有哪些布局方式，在布局时选择一种最为合理的方式。

**一、基本知识**

![](media/b42c532aaf6e86b9451e4b38d1fa310f.png)

                                                                  图1

android布局原理类似于ps的图层，是通过一层一层嵌套来实现的。比如上图，第一眼会认为C在B里面，B又在A里面，所以用android布局时，实现方式是A嵌套B，B再嵌套C。A，B，C，在android里称之为View。

**1. 父控件，子控件与兄弟控件**

如上图，采用A嵌套B，B再嵌套C的实现方式，那么A就是B的父控件，B也就是A的子控件。一个父控件可以有多个子控件，但每个子控件只能有一个父控件。当一个父控件包含多个子控件时，各个子控件互为兄弟控件。布局结构如下图

![](media/c48182d7dadea639501e5f17a503a6d9.png)

                                                                               
     图2

**2. view的几个概念（公用属性）**

layout_width：设置view的宽度，可以设置具体dp值，也可以设置为wrap_content（显示内容有多大就有多大）和match_parent（父控件多大我就是多大），view的宽度可以超出父控件的宽度，但只能显示出在父控件的那部分视图。

layout_height：设置view的高度，同layout_width

margin（外边距）：用于子控件设置相对于父控件的边距。可以设置距左（marginLeft）、距右(marginRight)、距上(marginTop)、距下(marginBottom)

padding（内边距）：用于设置控件自身的留白，父控件设置此属性，则子控件在父控件的留白区域不会显示。可以设置距左（paddingLeft）、距右(paddingRight)、距上(paddingTop)、距下(paddingBottom)

layout_gravity：设置控件相对于其父控件的位置。常用的属性值有center（居中），center_vertical（垂直方向居中），center_horizontal（水平方向居中），left（居左），right（居右）,top（居上），bottom（居下）。可以同时取多个值，不冲突即可

gravity：设置显示内容或其子控件相对于自身的位置。常用属性值同layout_gravity

**3. view和viewGroup**

viewGroup本质上也是view，只是它是一种更特殊的view。它两的区别是view只能作为子控件，而viewGroup不仅能作为子控件还能作为父控件（看图2）。

**4. 常用的子控件**

TextView：显示文字的view

ImageView：显示图片的view

Button：按钮

…...

这些子控件还有一些自身特定的属性，这里不再介绍。

**二、常用的父控件**

常用的父控件有帧布局（FrameLayout），线性布局（LinearLayout），相对布局（RelativeLayout）以及百分比布局（PercentRelativeLayout，PercentLinearLayout）等

**1. 帧布局（FrameLayout）**

一个父控件包含多个子控件时，子控件按照层次堆叠的方式进行布局，后面的子控件会覆盖前面的子控件。各个子控件默认是从父控件的左上角开始布局。

再看图1布局，另一种理解方式时A是父控件，B和C都作为A的子控件。A作为帧布局，B和C设置相应内边距和外边距，即可实现此效果。

**2. 线性布局（LinearLayout）**

线性布局按指定方向（水平/垂直）对子控件进行布局，子控件按照指定的方向依次排布（从左往右/从上往下）。在子控件之间有比例上的相关关联时，可以利用线性布局的weight属性实现。
  

![](media/9891a802ae82cd4d290260ec5cd4e011.png)

                                                                               
     图3

如图3:

第一行指定的方向是水平方向；

第二行指定的方向是垂直方向； 

第三行指定的方向是水平方向，指定了每一个子控件的权重（weight）分别为1：1.5：0.8：1，所以各个子元素的宽是不相等的。

**3. 相对布局（RelativeLayout）**

相对布局可以让子控件相对于兄弟控件或父控件进行布局，可以设置子控件相对于兄弟控件或父控件进行上下左右对齐，所以其能实现的效果更多。但是相对布局绘制过程更复杂，如果可以用帧布局或者线性布局实现，尽量不用相对布局。

 

![](media/704bd60dc03bee16bedd45f6e9e3c919.png)

都是相对于父控件的布局方式

图4 

**用于子控件设置其跟父控件关系的一些属性**

layout_alignParentBottom   当前控件底端与父控件的底端对齐  如图D

layout_alignParentLeft         当前控件左端与父控件的左端对齐
 默认控件会放在左上角，所以图上没有事例

layout_alignParentRight      当前控件右端与父控件的右端对齐   如图F

layout_alignParentTop      当前控件上端与父控件的上端对齐   
默认控件会放在左上角，所以图上没有事例

layout_centerHorizontal    
 当前控件位于父控件的横向中间位置（水平方向上的中间）如图A

layout_centerVertical    当前控件位于父控件的纵向中间位置（平面上的正中间）   
如图B

layout_centerInParent  当前控件位于父控件的纵横向中间位置（垂直方向上的中间
 如图C

![](media/193ed433bdf47b388056181979b8eb37.png)

都是相对于A来布局

图5

**用于子控件设置其跟兄弟控件的一些属性**

layout_above                  使当前控件位于给出id控件的上方，如图B

layout_below       使当前控件位于给出id控件的下方，如图C

layout_toLeftOf              使当前控件位于给出id控件的左侧, 如图 D

layout_toRightOf       使当前控件位于给出id控件的右侧， 如图F

layout_alignLeft              使当前控件与给出id控件的左边重合，
如图H(被HH压住了)

layout_alignRight            使当前控件与给出id控件的右边重合，如图HH

layout_alignBottom        使当前控件与给出id控件的底部部重合, 如图F (被FF压住了)

layout_alignTop       使当前控件与给出id控件的顶部重合，如图FF

**4. 百分比布局（PercentRelativeLayout，PercentLinearLayout）**

在pm测试时，经常会发现在不同的手机上会出现适配问题，原因在于使用上面三种父控件很难实现子控件占父控件百分比的效果。为了更好的适配，就可以使用百分比布局。

PercentRelativeLayout常用属性

layout_widthPercent                      宽度相对于父控件宽度百分比

layout_heightPercent                     高度相对于父控件高度百分比

layout_marginPercent                  
 距上距下相对于父控件高度百分比，距左距右相对于父控件宽度百分比

layout_marginLeftPercent              距左相对于父控件宽度百分比

layout_marginTopPercent              距上相对于父控件高度百分比

layout_marginRightPercent.          距右相对于父控件宽度百分比

layout_marginBottomPercent       距下相对于父控件高度百分比

layout_marginStartPercent           距左相对于父控件高度百分比

layout_marginEndPercent            距右相对于父控件高度百分比

layout_aspectRatio                      宽高比

 

**三、布局举例**

1. 一个用简单布局（帧布局、线性布局和相对布局）的例子

![](media/d153878107baf73186bedfea835060f7.png)

2. 一个用百分比布局的例子

![](media/5d329a9e614267c4d6383d11737a6d0d.png)

3. 一个项目中会引起折返跑的例子

设计图

![](media/285eed661198a0416623390bcb78bf07.png)

错误示例1

![](media/7260dd6c20f1a5811a89d7d28fa834a6.png)

错误原因：布局方式不对

错误示例2

![](media/c33fe55e70c688b341fc06a66fda4a04.png)

错误原因：主文案没设边距且没居中

 **四、建议**

1.
布局时层次结构尽可能少，层级越多view的绘制耗时也越多，容易引起掉帧。对于设计师，在设计时可以想想开发应该如何布局，在能保证设计效果的前提下尽可能确保设计图层次少。对于开发，拿到设计图时不要着急写代码，先理解设计师对各个元素的设计意图，选择一个最合理的方式进行布局。在能满足设计效果的前提下，尽量使用简单的布局。优先使用帧布局和线性布局，其次是相对布局，万不得已（比如涉及到百分比）时用百分比布局。

2.
在布局层次已达到最优的情况下，对一个区域的绘制次数要尽可能少，最好不要多于4次。在Android里，绘制超过一次就称为多一次过度绘制，过度绘制也会影响app的性能。所以设计师在设计时也应该随手想想设计图会不会引起严重的过度绘制。对于开发，应该要保证能不设置背景的地方就不去设置背景。

3. 在需要开发完成时，开发可以使用android studio自带的android device
monitor看布局层次结构，打开手机系统setting的过度绘制选项可以查看布局的过度绘制情况。

**五、总结**

船要往前行，需要各个人员的互相协作。项目组要发展，也需要各个不同职位的同学互相配合。设计师随手想想开发应该怎么实现，开发在写代码前先了解设计师意图，同时想想在不同频幕分辨率的手机上和多语言环境下是否会出适配问题，不确定的地方多沟通，尽量在早期发现问题，避免折返跑。
