1.

Map\<String, String\> testParams = new HashMap\<\>();

testParams.put("advertising_id", "b6e0a265-4ea9-475b-aba6-8f6d7d3783af");

testParams.put("android_id", "3bdab6c1ffad559d");

//testParams这个是为了测试用的，正式上线的时候一定要删掉，里面的advertising_id、android_id值在添加设备白名单的时候会有的

CrossMarketUtil.CrossMarketData crossMarketData = new
CrossMarketUtil.CrossMarketData("com.hyperspeed.rocketclean.pro", "space",
"OverseasBasic", "Space", “DoneFullscreen");

//测试的时候用到

crossMarketData.addParameters(testParams);

CrossMarketUtil.recordCrossMarketImpression(crossMarketData);

CrossMarketUtil.openCrossMarket(activity, crossMarketData);

2.

命名规则：

Media Source ：010（组内的所有app，这个值必须是010）

Channel ：OverseasBasic/CNBasic/Health/VPN（四选一）

Site ID ：App内部名（问自己的项目负责人或者PM，PM更懂一些）

Campaign ：各个app根据具体推荐位置命名，例如DoneFullscreen（这个PM决定）

3.

添加设备白名单，参照appsflyer官网介绍。

4.

让被推广的app的PM在AppsFlyer后台添加一个链接（water、appbox、vpn这边的PM做过类似的事了），这个link
name就是在appsflyer的一个名字，包含media source就行，因为Channel、Site
ID、Campaign都可以在代码中替换。

5.

PM要在yaml文件里添加

Application:

CrossPromote:

AppsflyerAppBaseUrl:"https://app.appsflyer.com"

AppsflyerImpressionBaseUrl:"https://impression.appsflyer.com"
