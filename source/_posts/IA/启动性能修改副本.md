1.OptimizerApplication中main、work进程初始化使用idlehandler延迟。

rocket clean pro为例：

main进程中

![](media/987f4412321979ab5453dfc920aad53b.png)

work进程中

![](media/bda36ed3e125a5232193c3176cd1be45.png)

2.OptimizerApplication -onCreate

![](media/207f997e9c40f6e739dfb37c2f8324a0.png)

Fabric初始化限制线程池中线程的个数。

![](media/d8a5f87b555901880940184175182fc4.png)

main跟work进程才执行LanguageUtils相关代码。

3.OptimizerApplication
-runOnMainProcess中registerReceiver的broadcastReceiver放入异步线程中执行

![](media/9adfa62a6b468bbc7df0030d0bf9fcd8.png)

其中 HSAlertMgr.delayRateAlert();这个方法需要抛到UI线程中执行，否则crash。

![](media/9adfa62a6b468bbc7df0030d0bf9fcd8.png)

4.OptimizerApplication -runOnWorkProcess中

![](media/016a4842b396cb74aed746ec25a5694e.png)

保活中需要执行NotificationToggleManager.getInstance().getNotification()；该代码会启动clean进程，从而严重影响启动性能。改动为第一次getNotification不执行真正的扫描逻辑。

但是线上测试影响toggle点击率，所以需要加入以上代码执行notificationToggleManager扫描逻辑延迟。注意:启动toggle的代码需要放到GDPR里面。

另决定：保活相关不放到idlehandler里面。

5.主页耗时逻辑使用idlehandler延迟；不是再一开始就初始化的界面使用AsyncLayoutInflater异步加载。

![](media/987f4412321979ab5453dfc920aad53b.png)
