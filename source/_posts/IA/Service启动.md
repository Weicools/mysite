源码取自android-26

首先我们看看**Activity**中的**startService**如何实现的。

先看继承关系：

![](media/31eea6d3dd1404278bd3d8dec41f9b1b.png)

   
从图中可以看出，Activity继承了ContextWrapper类，在ContextWrapper类中，有一个成员变量mBase，它是一个ContextImpl实例，而ContextImpl类和ContextWrapper类一样继承于Context类，ContextWrapper类的startService函数最终过调用ContextImpl类的startService函数来实现。这种类设计方法在设计模式里面，就称之为装饰模式（Decorator），或者包装模式（Wrapper）。

    
在ContextImpl类的startService类，最终又调用了ActivityManageService类的startService来实现启动服务的操作。下面从源码看startService到AMS的启动。

在代码中调用**startService**会先调用到**ContextWrapper**的**startService**方法

[**ContextWrapper.java**]

>   \@Override

>   public ComponentName startService(Intent service) {

>       return mBase.startService(service);

>   }

**mBase**是**ContextImpl**的实例，所以看**ContextImpl**中的**startService**实现

[**ContextImpl.java**]

>   \@Override

>   public ComponentName startService(Intent service) {

>       warnIfCallingFromSystemProcess();    // 如果是系统进程调用会打印一个log

>       return startServiceCommon(service, false, mUser);

>   }

>   private ComponentName startServiceCommon(Intent service, boolean
>   requireForeground,

>           UserHandle user) {

>       try {

>           validateServiceIntent(service); //
>   若启动service的intent为空，5.0以上会抛出异常

>           service.prepareToLeaveProcess(this);

>           // 26版本

>          ComponentName cn = ActivityManager.*getService*().startService(

>               mMainThread.getApplicationThread(), service,
>   service.resolveTypeIfNeeded(

>                           getContentResolver()), requireForeground,

>                           getOpPackageName(), user.getIdentifier()); 

>           //
>   26以下版本，在26以上ActivityManagerNative类被弃用，代理类ActivityManagerProxy被删除

>     ComponentName cn = ActivityManagerNative.getDefault().startService(

>   mMainThread.getApplicationThread(), service, service.resolveTypeIfNeeded(

>   getContentResolver()), getOpPackageName(), user.getIdentifier());

>      //
>   不论是ActivityManager.*getService*()还是ActivityManagerNative.getDefault()，返回的都是AMS对象，接下来的代码就在AMS的startService中执行

>           if (cn != null) {

>               if (cn.getPackageName().equals("!")) {

>                   throw new SecurityException(

>                           "Not allowed to start service " + service

>                           + " without permission " + cn.getClassName());

>               } else if (cn.getPackageName().equals("!!")) {

>                   throw new SecurityException(

>                           "Unable to start service " + service

>                           + ": " + cn.getClassName());

>               } else if (cn.getPackageName().equals("?")) {

>                   throw new IllegalStateException(

>                           "Not allowed to start service " + service + ": " +
>   cn.getClassName());

>               }

>           }

>           return cn;

>       } catch (RemoteException e) {

>           throw e.rethrowFromSystemServer();

>       }

>   }

先看看AMS的启动：在SystemServer中启动的

[**SystemServer.java**]

>   public static void main(String[] args) {

>       new SystemServer().run();

>   }

>   private void run() {

>       …

>       // Start services.

>       try {

>           *traceBeginAndSlog*("StartServices");

>           startBootstrapServices();

>           startCoreServices();

>           startOtherServices();

>           SystemServerInitThreadPool.*shutdown*();

>       } catch (Throwable ex) {

>           Slog.e("System",
>   "\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*");

>           Slog.e("System", "\*\*\*\*\*\*\*\*\*\*\*\* Failure starting system
>   services", ex);

>           throw ex;

>       } finally {

>           *traceEnd*();

>       }

>       …

>   }

>   private void startBootstrapServices() {

>       …

>       // Activity manager runs the show.

>       *traceBeginAndSlog*("StartActivityManager");

>       mActivityManagerService = mSystemServiceManager.startService(

>               ActivityManagerService.Lifecycle.class).getService();

>       mActivityManagerService.setSystemServiceManager(mSystemServiceManager);

>       mActivityManagerService.setInstaller(installer);

>       *traceEnd*();

>       …

>   // Set up the Application instance for the system process and get started.

>   *traceBeginAndSlog*("SetSystemProcess");

>   mActivityManagerService.setSystemProcess();

>   *traceEnd*();

>       …

>   }

[**SystemServiceManager.java**]

>   public \<T extends SystemService\> T startService(Class\<T\> serviceClass) {

>       try {

>       …

>           final T service;

>           try {

>               Constructor\<T\> constructor =
>   serviceClass.getConstructor(Context.class);    //
>   通过反射创建LifeCycle实例，得到LifeCycle的构造器

>               service = constructor.newInstance(mContext); //
>   创建LifeCycle类型的service对象

>           } …

>           startService(service);

>           return service;

>       } finally {

>           Trace.traceEnd(Trace.TRACE_TAG_SYSTEM_SERVER);

>       }

>   }

>   public void startService(\@NonNull final SystemService service) {

>       // Register it.

>       mServices.add(service);  // 注册 ArrayList\<SystemService\> mServices =
>   new ArrayList\<SystemService\>();

>       // Start it.

>       long time = System.*currentTimeMillis*();

>       try {

>           service.onStart();

>       } catch (RuntimeException ex) {

>           throw new RuntimeException("Failed to start service " +
>   service.getClass().getName()

>                   + ": onStart threw an exception", ex);

>       }

>       warnIfTooLong(System.*currentTimeMillis*() - time, service, "onStart");

>   }

[AMS内部类**Lifecycle.java**]

>   public static final class Lifecycle extends SystemService {

>       private final ActivityManagerService mService;

>       public Lifecycle(Context context) {

>           super(context);

>           mService = new ActivityManagerService(context);    //
>   LifeCycle实例实际是个AMS对象

>       }

>       \@Override

>       public void onStart() {

>           mService.start();

>       }

>       public ActivityManagerService getService() {

>           return mService;

>       }

>   }

[AMS]

>   public void setSystemProcess() {

>       try {

>           ServiceManager.addService(Context.*ACTIVITY_SERVICE*, this, true);
>   //
>   把这个AMS这个Binder实例添加到Binder进程间通信的守护进程ServiceManager中去，AMS启动

>       …

>       } catch (PackageManager.NameNotFoundException e) {

>           throw new RuntimeException(

>                   "Unable to find android system package", e);

>       }

>   }

AMS的启动到此结束

接下来看**ActivityManager.getService**()怎么返回**AMS**

[**ActivityManager.java**]

>   */\*\**

>   *\* \@hide*

>   *\*/*

>   public static IActivityManager getService() {

>       return *IActivityManagerSingleton*.get();

>   }

>   private static final Singleton\<IActivityManager\>
>   *IActivityManagerSingleton* =

>           new Singleton\<IActivityManager\>() {

>               \@Override

>               protected IActivityManager create() {

>   // public static final String *ACTIVITY_SERVICE* = "activity”;

>       // 获取名为“activity”的服务，服务都注册到ServiceManager中统一管理

>                   final IBinder b =
>   ServiceManager.getService(Context.*ACTIVITY_SERVICE*);

>                   final IActivityManager am =
>   IActivityManager.Stub.asInterface(b);

>                   return am;

>               }

>           };

[**ServiceManager.java**]

>   **publicstatic**[IBinder](http://androidxref.com/8.1.0_r33/s?defs=IBinder&project=frameworks)[getService](http://androidxref.com/8.1.0_r33/s?refs=getService&project=frameworks)([String](http://androidxref.com/8.1.0_r33/s?defs=String&project=frameworks)[name](http://androidxref.com/8.1.0_r33/s?refs=name&project=frameworks))
>   {

>           **try** {

>              
>   [IBinder](http://androidxref.com/8.1.0_r33/s?defs=IBinder&project=frameworks)[service](http://androidxref.com/8.1.0_r33/s?defs=service&project=frameworks)
>   =
>   [sCache](http://androidxref.com/8.1.0_r33/xref/frameworks/base/core/java/android/os/ServiceManager.java#sCache).[get](http://androidxref.com/8.1.0_r33/s?defs=get&project=frameworks)([name](http://androidxref.com/8.1.0_r33/s?defs=name&project=frameworks));

>               **if**
>   ([service](http://androidxref.com/8.1.0_r33/s?defs=service&project=frameworks)
>   != [null](http://androidxref.com/8.1.0_r33/s?defs=null&project=frameworks))
>   {

>                  
>   **return**[service](http://androidxref.com/8.1.0_r33/s?defs=service&project=frameworks);

>               } **else** {

>                  
>   **return**[Binder](http://androidxref.com/8.1.0_r33/s?defs=Binder&project=frameworks).[allowBlocking](http://androidxref.com/8.1.0_r33/s?defs=allowBlocking&project=frameworks)([getIServiceManager](http://androidxref.com/8.1.0_r33/xref/frameworks/base/core/java/android/os/ServiceManager.java#getIServiceManager)().[getService](http://androidxref.com/8.1.0_r33/xref/frameworks/base/core/java/android/os/ServiceManager.java#getService)([name](http://androidxref.com/8.1.0_r33/s?defs=name&project=frameworks)));

>               }

>           } **catch**
>   ([RemoteException](http://androidxref.com/8.1.0_r33/s?defs=RemoteException&project=frameworks)
>   e) {

>              
>   [Log](http://androidxref.com/8.1.0_r33/s?defs=Log&project=frameworks).e([TAG](http://androidxref.com/8.1.0_r33/xref/frameworks/base/core/java/android/os/ServiceManager.java#TAG),
>   "error in getService", e);

>           }

>          
>   **return**[null](http://androidxref.com/8.1.0_r33/s?defs=null&project=frameworks);

>       }

**Singleton**为**android.util**中的抽象类

[**Singleton.java**]

>   **publicabstractclass**[Singleton](http://androidxref.com/8.0.0_r4/s?refs=Singleton&project=frameworks)\<T\>
>   {

>       **private** T
>   [mInstance](http://androidxref.com/8.0.0_r4/s?refs=mInstance&project=frameworks);

>       **protectedabstract** T
>   [create](http://androidxref.com/8.0.0_r4/s?refs=create&project=frameworks)();

>       **publicfinal** T
>   [get](http://androidxref.com/8.0.0_r4/s?refs=get&project=frameworks)() {

>           **synchronized** (**this**) {

>               **if**
>   ([mInstance](http://androidxref.com/8.0.0_r4/xref/frameworks/base/core/java/android/util/Singleton.java#mInstance)
>   == [null](http://androidxref.com/8.0.0_r4/s?defs=null&project=frameworks)) {

>                  
>   [mInstance](http://androidxref.com/8.0.0_r4/xref/frameworks/base/core/java/android/util/Singleton.java#mInstance)
>   =
>   [create](http://androidxref.com/8.0.0_r4/xref/frameworks/base/core/java/android/util/Singleton.java#create)();

>               }

>              
>   **return**[mInstance](http://androidxref.com/8.0.0_r4/xref/frameworks/base/core/java/android/util/Singleton.java#mInstance);

>           }

>       }

>   }

**接下来看ActivityManagerService** 中的**startService**怎么实现的

[**ActivityManagerService.java**]

>   \@Override

>   public ComponentName startService(IApplicationThread caller, Intent service,

>           String resolvedType, boolean requireForeground, String
>   callingPackage, int userId)

>           throws TransactionTooLargeException {

>       enforceNotIsolatedCaller("startService”);    //
>   当调用者是孤立进程，则抛出异常

>       …

>       synchronized(this) {

>           final int callingPid = Binder.*getCallingPid*(); // 调用者pid

>           final int callingUid = Binder.*getCallingUid*(); // 调用者uid

>           final long origId = Binder.*clearCallingIdentity*();

>           ComponentName res;

>           try {

>               res = mServices.startServiceLocked(caller, service,

>                       resolvedType, callingPid, callingUid,

>                       requireForeground, callingPackage, userId);

>           } finally {

>               Binder.*restoreCallingIdentity*(origId);

>           }

>           return res;

>       }

>   }

**mServices**为**ActiveServices**实例，**ActiveServices**是一个辅助**AMS**进行**Service**管理的类，包含**Service**的启动、绑定和停止等

[**ActiveServices.java**]

>   ComponentName startServiceLocked(IApplicationThread caller, Intent service,
>   String resolvedType,

>           int callingPid, int callingUid, boolean fgRequired, String
>   callingPackage, final int userId)

>           throws TransactionTooLargeException {

>    …

>       //
>   首先通过retrieveServiceLocked来解析service这个intent，将解析结果放在res.record中，接着调用bringUpServiceLocked进一步处理

>       ServiceLookupResult res =

>           retrieveServiceLocked(service, resolvedType, callingPackage,

>                   callingPid, callingUid, userId, true, callerFg, false);

>       ...

>       ServiceRecord r = res.record;

>       ...

>       ComponentName cmp = startServiceInnerLocked(smap, service, r, callerFg,
>   addToStarting);

>       return cmp;

>   }

>   ComponentName startServiceInnerLocked(ServiceMap smap, Intent service,
>   ServiceRecord r,

>           boolean callerFg, boolean addToStarting) throws
>   TransactionTooLargeException {

>       ...

>       String error = bringUpServiceLocked(r, service.getFlags(), callerFg,
>   false, false);

>       if (error != null) {

>           return new ComponentName("!!", error);

>       }

>       ...

>       return r.name;

>   }

>   private String bringUpServiceLocked(ServiceRecord r, int intentFlags,
>   boolean execInFg,

>           boolean whileRestarting, boolean permissionsReviewRequired)

>           throws TransactionTooLargeException {

>       if (r.app != null && r.app.thread != null) {

>       //
>   该service已创建，再次调用startService,会调用service.onStartCommand()过程

>           sendServiceArgsLocked(r, execInFg, false);

>           return null;

>       }

>       if (!whileRestarting && mRestartingServices.contains(r)) {

>           // 若等待重启则直接返回

>           return null;

>       }

>       // 启动service前，把service从重启服务队列中移除

>       if (mRestartingServices.remove(r)) {

>           clearRestartingIfNeededLocked(r);

>       }

>       // Make sure this service is no longer considered delayed, we are
>   starting it now.

>       if (r.delayed) {

>           if (*DEBUG_DELAYED_STARTS*) Slog.v(*TAG_SERVICE*, "REM FR DELAY LIST
>   (bring up): " + r);

>           getServiceMapLocked(r.userId).mDelayedStartList.remove(r);

>           r.delayed = false;

>       }

>      // 确保拥有该服务的user已经启动，否则停止

>       if (!mAm.mUserController.hasStartedUserState(r.userId)) {

>           String msg = "Unable to launch app "

>                   + r.appInfo.packageName + "/"

>                   + r.appInfo.uid + " for service "

>                   + r.intent.getIntent() + ": user " + r.userId + " is
>   stopped";

>           Slog.w(*TAG*, msg);

>           bringDownServiceLocked(r);

>           return msg;

>       }

>       // Service is now being launched, its package can't be stopped.

>       try {

>           AppGlobals.getPackageManager().setPackageStoppedState(

>                   r.packageName, false, r.userId);

>       } catch (RemoteException e) {

>       } catch (IllegalArgumentException e) {

>           Slog.w(*TAG*, "Failed trying to unstop package "

>                   + r.packageName + ": " + e);

>       }

>       final boolean isolated =
>   (r.serviceInfo.flags&ServiceInfo.*FLAG_ISOLATED_PROCESS*) != 0;    //
>   判断Service是否在独立进程中启动

>       //
>   得到AndroidManifest.xml文件定义service标签时指定的android:process属性值

>   final String procName = r.processName;

>       String hostingType = "service";

>       ProcessRecord app;

>       if (!isolated) {

>           app = mAm.getProcessRecordLocked(procName, r.appInfo.uid, false);

>           if (DEBUG_MU) Slog.v(*TAG_MU*, "bringUpServiceLocked: appInfo.uid="
>   + r.appInfo.uid

>                       + " app=" + app);

>           if (app != null && app.thread != null) {

>               try {

>                   app.addPackage(r.appInfo.packageName, r.appInfo.versionCode,
>   mAm.mProcessStats);

>                   realStartServiceLocked(r, app, execInFg);    //
>   目标进程已存在，直接执行realStartServiceLocked

>                   return null;

>               } catch (TransactionTooLargeException e) {

>                   throw e;

>               } catch (RemoteException e) {

>                   Slog.w(*TAG*, "Exception when starting service " +
>   r.shortName, e);

>               }

>               // If a dead object exception was thrown -- fall through to

>               // restart the application.

>           }

>       } else {

>           // If this service runs in an isolated process, then each time

>           // we call startProcessLocked() we will get a new isolated

>           // process, starting another process if we are currently waiting

>           // for a previous process to come up.  To deal with this, we store

>           // in the service any current isolated process it is running in or

>           // waiting to have come up.

>           app = r.isolatedProc;

>           if (WebViewZygote.isMultiprocessEnabled()

>                   &&
>   r.serviceInfo.packageName.equals(WebViewZygote.getPackageName())) {

>               hostingType = "webview_service";

>           }

>       }

>       // Not running -- get it started, and enqueue this service record

>       // to be executed when the app comes up.

>       if (app == null && !permissionsReviewRequired) {

>           if ((app=mAm.startProcessLocked(procName, r.appInfo, true,
>   intentFlags,    // 目标进程不存在，先执行startProcessLocked创建进程

>                   hostingType, r.name, false, isolated, false)) == null) {

>               String msg = "Unable to launch app "

>                       + r.appInfo.packageName + "/"

>                       + r.appInfo.uid + " for service "

>                       + r.intent.getIntent() + ": process is bad";

>               Slog.w(*TAG*, msg);

>               bringDownServiceLocked(r);    // 进程启动失败

>               return msg;

>           }

>           if (isolated) {

>               r.isolatedProc = app;

>           }

>       }

>       if (!mPendingServices.contains(r)) {

>           mPendingServices.add(r);    //
>   将这个ServiceRecord保存在成员变量mPendingServices中

>       }

>       if (r.delayedStop) {

>           // Oh and hey we've already been asked to stop!

>           r.delayedStop = false;

>           if (r.startRequested) {

>               if (*DEBUG_DELAYED_STARTS*) Slog.v(*TAG_SERVICE*,

>                       "Applying delayed stop (in bring up): " + r);

>               stopServiceLocked(r);

>           }

>       }

>       return null;

>   }

>   private final void startProcessLocked(ProcessRecord app, String hostingType,

>           String hostingNameStr, String abiOverride, String entryPoint,
>   String[] entryPointArgs) {

>       ...

>       try {

>           ...

>           if (entryPoint == null) entryPoint = "android.app.ActivityThread";

>           Trace.traceBegin(Trace.TRACE_TAG_ACTIVITY_MANAGER, "Start proc: " +

>                   app.processName);

>           checkTime(startTime, "startProcess: asking zygote to start proc");

>           ProcessStartResult startResult;

>           if (hostingType.equals("webview_service")) {

>               startResult = startWebView(entryPoint,

>                       app.processName, uid, uid, gids, debugFlags,
>   mountExternal,

>                       app.info.targetSdkVersion, seInfo, requiredAbi,
>   instructionSet,

>                       app.info.dataDir, null, entryPointArgs);

>           } else {

>               startResult = Process.start(entryPoint,

>                       app.processName, uid, uid, gids, debugFlags,
>   mountExternal,

>                       app.info.targetSdkVersion, seInfo, requiredAbi,
>   instructionSet,

>                       app.info.dataDir, invokeWith, entryPointArgs);    //
>   调用Process.start()创建一个新进程，指定新的进程执行android.app.ActivityThread类

>           }

>           ...

>           ProcessRecord oldApp;

>   synchronized (mPidsSelfLocked) {

>         this.mPidsSelfLocked.put(startResult.pid, app);    //
>   将新进程pid保存在mPidSelfLocked列表中

>       ...

>   }

>          ...

>       } catch (RuntimeException e) {

>           ...

>       }

>   }

[**ActiveServices.java**]

>   private final void realStartServiceLocked(ServiceRecord r,

>           ProcessRecord app, boolean execInFg) throws RemoteException {

>       ...

>       r.app = app;

>       r.restartTime = r.lastActivity = SystemClock.*uptimeMillis*();

>       final boolean newService = app.services.add(r);

>       bumpServiceExecutingLocked(r, execInFg, "create");    //
>   发送延迟处理的消息SERVICE_TIME_OUT_MSG

>       mAm.updateLruProcessLocked(app, false, null);

>       updateServiceForegroundLocked(r.app, /\* oomAdj= \*/ false);

>       mAm.updateOomAdjLocked(); 

>       boolean created = false;

>       try {

>           ...

>           app.thread.scheduleCreateService(r, r.serviceInfo,

>                  
>   mAm.compatibilityInfoForPackageLocked(r.serviceInfo.applicationInfo),

>                   app.repProcState);   
>   // scheduleCreateService创建service对象并调用onCreate

>       //
>   app.thread是IApplicationThread类型，实质上是一个Binder，具体实现是ApplicationThraed

>           r.postNotification();

>           created = true;

>       } ...

>       sendServiceArgsLocked(r, execInFg, true);    // 调用service的其他方法

>       ...

>   }

看**ApplicationThread**中的**scheduleCreateService**

[**ApplicationThread.java]**

>   public final void scheduleCreateService(IBinder token,

>           ServiceInfo info, CompatibilityInfo compatInfo, int processState) {

>       updateProcessState(processState, false);

>       CreateServiceData s = new CreateServiceData();

>       s.token = token;

>       s.info = info;

>       s.compatInfo = compatInfo;

>       sendMessage(H.*CREATE_SERVICE*, s);    // 发送消息给Handler
>   H来完成，H接收消息并通过ActivityThread的handleCreateService来完成service的启动

>   }

[**ActivityThread.java**]

>   private void handleCreateService(CreateServiceData data) {

>       // If we are getting ready to gc after going to the background, well

>       // we are back active so skip it.

>       unscheduleGcIdler();

>       LoadedApk packageInfo = getPackageInfoNoCheck(

>               data.info.applicationInfo, data.compatInfo);

>       Service service = null;

>       try {

>           java.lang.ClassLoader cl = packageInfo.getClassLoader();

>           service = (Service) cl.loadClass(data.info.name).newInstance();   
>   // 通过类加载器创建service实例

>       } catch (Exception e) {

>          ...

>       }

>       try {

>           if (*localLOGV*) Slog.v(*TAG*, "Creating service " +
>   data.info.name);

>           ContextImpl context = ContextImpl.*createAppContext*(this,
>   packageInfo);    // 创建ContextImpl对象

>           context.setOuterContext(service);

>           Application app = packageInfo.makeApplication(false,
>   mInstrumentation);    // 创建Application对象

>           service.attach(context, this, data.info.name, data.token, app,

>                   ActivityManager.*getService*());    //
>   通过attach方法建立二者之间的关系

>           service.onCreate();    // 调用service的onCreate方法

>           mServices.put(data.token, service);    // 将service存储到列表

>                  // 列表定义 final ArrayMap\<IBinder, Service\> mServices =
>   new ArrayMap\<\>();

>           try {

>               ActivityManager.*getService*().serviceDoneExecuting(

>                       data.token, *SERVICE_DONE_EXECUTING_ANON*, 0, 0);     
>    // 最终回调到ActiveServices中的serviceDoneExecutingLocked

>           } catch (RemoteException e) {

>               throw e.rethrowFromSystemServer(); 

>           }

>       } catch (Exception e) {

>           if (!mInstrumentation.onException(service, e)) {

>               throw new RuntimeException(

>                   "Unable to create service " + data.info.name

>                   + ": " + e.toString(), e);

>           }

>       }

>   }

[**Service.java**]

>   public void onCreate() {

>       // 继承service实现onCreate方法

>   }

[**ActiveServices.java**]

>   void serviceDoneExecutingLocked(ServiceRecord r, int type, int startId, int
>   res) {

>       boolean inDestroying = mDestroyingServices.contains(r);

>       if (r != null) {

>           ...

>           final long origId = Binder.*clearCallingIdentity*();

>           serviceDoneExecutingLocked(r, inDestroying, inDestroying);    //
>   最终会回调一个重载serviceDoneExecutingLocked

>           Binder.*restoreCallingIdentity*(origId);

>       } else {

>           Slog.w(*TAG*, "Done executing unknown service from pid "

>                   + Binder.*getCallingPid*());

>       }

>   }

>   private void serviceDoneExecutingLocked(ServiceRecord r, boolean
>   inDestroying,

>           boolean finishing) {

>       r.executeNesting--;

>       if (r.executeNesting \<= 0) {

>           if (r.app != null) {

>               r.app.execServicesFg = false;

>               r.app.executingServices.remove(r);

>               if (r.app.executingServices.size() == 0) {

>                  // 移除服务启动超时的消息

>                  
>   mAm.mHandler.removeMessages(ActivityManagerService.*SERVICE_TIMEOUT_MSG*,
>   r.app);

>               } else if (r.executeFg) {

>                   ...

>               }

>               if (inDestroying) {

>                   mDestroyingServices.remove(r);

>                   r.bindings.clear();

>               }

>               mAm.updateOomAdjLocked(r.app, true);

>           }

>           r.executeFg = false;

>           ...

>       }

>   }

handleCreateService()执行后会移除服务启动超时的消息*SERVICE_TIMEOUT_MSG。 *

**[ActiveServices.java]**

>   private final void bumpServiceExecutingLocked(ServiceRecord r, boolean fg,
>   String why) {

>       if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "\>\>\> EXECUTING "

>               + why + " of " + r + " in app " + r.app);

>       else if (DEBUG_SERVICE_EXECUTING) Slog.v(*TAG_SERVICE_EXECUTING*,
>   "\>\>\> EXECUTING "

>               + why + " of " + r.shortName);

>       long now = SystemClock.*uptimeMillis*();

>       if (r.executeNesting == 0) {

>           r.executeFg = fg;

>           ServiceState stracker = r.getTracker();

>           if (stracker != null) {

>               stracker.setExecuting(true,
>   mAm.mProcessStats.getMemFactorLocked(), now);

>           }

>           if (r.app != null) {

>               r.app.executingServices.add(r);

>               r.app.execServicesFg \|= fg;

>               if (r.app.executingServices.size() == 1) {

>                   scheduleServiceTimeoutLocked(r.app);

>               }

>           }

>       } else if (r.app != null && fg && !r.app.execServicesFg) {

>           r.app.execServicesFg = true;

>           scheduleServiceTimeoutLocked(r.app);

>       }

>       r.executeFg \|= fg;

>       r.executeNesting++;

>       r.executingStart = now;

>   }

>   void scheduleServiceTimeoutLocked(ProcessRecord proc) {

>       if (proc.executingServices.size() == 0 \|\| proc.thread == null) {

>           return;

>       }

>       Message msg = mAm.mHandler.obtainMessage(

>               ActivityManagerService.*SERVICE_TIMEOUT_MSG*);

>       msg.obj = proc;

>   // 当超时后仍没有remove该SERVICE_TIMEOUT_MSG消息，则执行service timeout流程

>       mAm.mHandler.sendMessageDelayed(msg,

>               proc.execServicesFg ? *SERVICE_TIMEOUT* :
>   *SERVICE_BACKGROUND_TIMEOUT*);

>       // static final int *SERVICE_TIMEOUT* = 20\*1000; 前台服务为20秒

>   // static final int *SERVICE_BACKGROUND_TIMEOUT* = *SERVICE_TIMEOUT* \* 10;
>   后台服务为200秒

>   }

[**ActiveServices.java**]

>   private final void sendServiceArgsLocked(ServiceRecord r, boolean execInFg,

>           boolean oomAdjusted) throws TransactionTooLargeException {

>       final int N = r.pendingStarts.size();

>       if (N == 0) {

>           return;

>       }

>       ArrayList\<ServiceStartArgs\> args = new ArrayList\<\>();

>       while (r.pendingStarts.size() \> 0) {

>           ServiceRecord.StartItem si = r.pendingStarts.remove(0);

>           if (DEBUG_SERVICE) {

>               Slog.v(*TAG_SERVICE*, "Sending arguments to: "

>                       + r + " " + r.intent + " args=" + si.intent);

>           }

>           if (si.intent == null && N \> 1) {

>               // If somehow we got a dummy null intent in the middle,

>               // then skip it.  DO NOT skip a null intent when it is

>               // the only one in the list -- this is to support the

>               // onStartCommand(null) case.

>               continue;

>           }

>           si.deliveredTime = SystemClock.*uptimeMillis*();

>           r.deliveredStarts.add(si);

>           si.deliveryCount++;

>           if (si.neededGrants != null) {

>               mAm.grantUriPermissionUncheckedFromIntentLocked(si.neededGrants,

>                       si.getUriPermissionsLocked());

>           }

>           mAm.grantEphemeralAccessLocked(r.userId, si.intent,

>                   r.appInfo.uid, UserHandle.getAppId(si.callingId));

>           bumpServiceExecutingLocked(r, execInFg, "start");

>           if (!oomAdjusted) {

>               oomAdjusted = true;

>               mAm.updateOomAdjLocked(r.app, true);

>           }

>           if (r.fgRequired && !r.fgWaiting) {

>               if (!r.isForeground) {

>                   if (DEBUG_BACKGROUND_CHECK) {

>                       Slog.i(*TAG*, "Launched service must call
>   startForeground() within timeout: " + r);

>                   }

>                   scheduleServiceForegroundTransitionTimeoutLocked(r);

>               } else {

>                   if (DEBUG_BACKGROUND_CHECK) {

>                       Slog.i(*TAG*, "Service already foreground; no new
>   timeout: " + r);

>                   }

>                   r.fgRequired = false;

>               }

>           }

>           int flags = 0;

>           if (si.deliveryCount \> 1) {

>               flags \|= Service.*START_FLAG_RETRY*;

>           }

>           if (si.doneExecutingCount \> 0) {

>               flags \|= Service.*START_FLAG_REDELIVERY*;

>           }

>           args.add(new ServiceStartArgs(si.taskRemoved, si.id, flags,
>   si.intent));

>       }

>       ParceledListSlice\<ServiceStartArgs\> slice = new
>   ParceledListSlice\<\>(args);

>       slice.setInlineCountLimit(4);

>       Exception caughtException = null;

>       try {

>           r.app.thread.scheduleServiceArgs(r, slice);    // 回调onStartCommand

>       } catch (TransactionTooLargeException e) {

>           if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "Transaction too large for
>   " + args.size()

>                   + " args, first: " + args.get(0).args);

>           Slog.w(*TAG*, "Failed delivering service starts", e);

>           caughtException = e;

>       } catch (RemoteException e) {

>           // Remote process gone...  we'll let the normal cleanup take care of
>   this.

>           if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "Crashed while sending
>   args: " + r);

>           Slog.w(*TAG*, "Failed delivering service starts", e);

>           caughtException = e;

>       } catch (Exception e) {

>           Slog.w(*TAG*, "Unexpected exception", e);

>           caughtException = e;

>       }

>       if (caughtException != null) {

>           // Keep nesting count correct

>           final boolean inDestroying = mDestroyingServices.contains(r);

>           for (int i = 0; i \< args.size(); i++) {

>               serviceDoneExecutingLocked(r, inDestroying, inDestroying);

>           }

>           if (caughtException instanceof TransactionTooLargeException) {

>               throw (TransactionTooLargeException)caughtException;

>           }

>       }

>   }

[**ApplicationThread.java**]

>   public final void scheduleServiceArgs(IBinder token, ParceledListSlice args)
>   {

>       List\<ServiceStartArgs\> list = args.getList();

>       for (int i = 0; i \< list.size(); i++) {

>           ServiceStartArgs ssa = list.get(i);

>           ServiceArgsData s = new ServiceArgsData();

>           s.token = token;

>           s.taskRemoved = ssa.taskRemoved;

>           s.startId = ssa.startId;

>           s.flags = ssa.flags;

>           s.args = ssa.args;

>           sendMessage(H.*SERVICE_ARGS*, s);

>       }

>   }

[**ActivityThread.java**]

>   private void handleServiceArgs(ServiceArgsData data) {

>       Service s = mServices.get(data.token);

>       if (s != null) {

>           try {

>               if (data.args != null) {

>                   data.args.setExtrasClassLoader(s.getClassLoader());

>                   data.args.prepareToEnterProcess();

>               }

>               int res;

>               if (!data.taskRemoved) {

>                   res = s.onStartCommand(data.args, data.flags, data.startId);
>   // 调用service的s.onStartCommand

>               } else {

>                   s.onTaskRemoved(data.args);

>                   res = Service.*START_TASK_REMOVED_COMPLETE*;

>               }

>               QueuedWork.*waitToFinish*();

>               try {

>                   ActivityManager.*getService*().serviceDoneExecuting(

>                           data.token, *SERVICE_DONE_EXECUTING_START*,
>   data.startId, res);

>               } catch (RemoteException e) {

>                   throw e.rethrowFromSystemServer();

>               }

>               ensureJitEnabled();

>           } catch (Exception e) {

>               if (!mInstrumentation.onException(s, e)) {

>                   throw new RuntimeException(

>                           "Unable to start service " + s

>                           + " with " + data.args + ": " + e.toString(), e);

>               }

>           }

>       }

>   }

[**Service.java**]

>   public \@StartResult int onStartCommand(Intent intent, \@StartArgFlags int
>   flags, int startId) {

>       onStart(intent, startId);

>       return mStartCompatibility ? *START_STICKY_COMPATIBILITY* :
>   *START_STICKY*;

>   }

**bindService**

[**ContextWrapper.java**]

>   \@Override

>   public boolean bindService(Intent service, ServiceConnection conn,

>           int flags) {

>       return mBase.bindService(service, conn, flags);

>   }

[**ContextImpl.java**]

>   \@Override

>   public boolean bindService(Intent service, ServiceConnection conn,

>           int flags) {

>       warnIfCallingFromSystemProcess();

>       return bindServiceCommon(service, conn, flags, mMainThread.getHandler(),

>               Process.*myUserHandle*());

>   }

>   private boolean bindServiceCommon(Intent service, ServiceConnection conn,
>   int flags, Handler

>           handler, UserHandle user) {

>       // Keep this in sync with
>   DevicePolicyManager.bindDeviceAdminServiceAsUser.

>       IServiceConnection sd;

>       if (conn == null) {

>           throw new IllegalArgumentException("connection is null");

>       }

>       if (mPackageInfo != null) {

>           sd = mPackageInfo.getServiceDispatcher(conn, getOuterContext(),
>   handler, flags);

>       } else {

>           throw new RuntimeException("Not supported in system context");

>       }

>       validateServiceIntent(service);

>       try {

>           IBinder token = getActivityToken();

>           if (token == null && (flags&*BIND_AUTO_CREATE*) == 0 && mPackageInfo
>   != null

>                   && mPackageInfo.getApplicationInfo().targetSdkVersion

>                   \< android.os.Build.VERSION_CODES.*ICE_CREAM_SANDWICH*) {

>               flags \|= *BIND_WAIVE_PRIORITY*;

>           }

>           service.prepareToLeaveProcess(this);

>           int res = ActivityManager.*getService*().bindService(

>               mMainThread.getApplicationThread(), getActivityToken(), service,

>               service.resolveTypeIfNeeded(getContentResolver()),

>               sd, flags, getOpPackageName(), user.getIdentifier());

>           if (res \< 0) {

>               throw new SecurityException(

>                       "Not allowed to bind to service " + service);

>           }

>           return res != 0;

>       } catch (RemoteException e) {

>           throw e.rethrowFromSystemServer();

>       }

>   }

[**ActivityManagerService.java**]

>   public int bindService(IApplicationThread caller, IBinder token, Intent
>   service,

>           String resolvedType, IServiceConnection connection, int flags,
>   String callingPackage,

>           int userId) throws TransactionTooLargeException {

>       enforceNotIsolatedCaller("bindService");

>       // Refuse possible leaked file descriptors

>       if (service != null && service.hasFileDescriptors() == true) {

>           throw new IllegalArgumentException("File descriptors passed in
>   Intent");

>       }

>       if (callingPackage == null) {

>           throw new IllegalArgumentException("callingPackage cannot be null");

>       }

>       synchronized(this) {

>           return mServices.bindServiceLocked(caller, token, service,

>                   resolvedType, connection, flags, callingPackage, userId);

>       }

>   }

**AMS**会调用**ActiveServices**的**bindServiceLocked**方法，**bindServiceLocked**再调用**bringUpServiceLocked**方法，**bringUpServiceLocked**会调用**realStartServiceLocked**方法，最终回调**onCreate **

在**bringUpServiceLocked**中还会回调**requestServiceBindingLocked**

[**ActiveServices.java**]

>   private final boolean requestServiceBindingLocked(ServiceRecord r,
>   IntentBindRecord i,

>           boolean execInFg, boolean rebind) throws
>   TransactionTooLargeException {

>       if (r.app == null \|\| r.app.thread == null) {

>           // If service is not currently running, can't yet bind.

>           return false;

>       }

>       ...

>       if ((!i.requested \|\| rebind) && i.apps.size() \> 0) {

>           try {

>               bumpServiceExecutingLocked(r, execInFg, "bind");

>              
>   r.app.forceProcessStateUpTo(ActivityManager.PROCESS_STATE_SERVICE);

>               r.app.thread.scheduleBindService(r, i.intent.getIntent(),
>   rebind,

>                       r.app.repProcState);

>               if (!rebind) {

>                   i.requested = true;

>               }

>               i.hasBound = true;

>               i.doRebind = false;

>           } catch (TransactionTooLargeException e) {

>               // Keep the executeNesting count accurate.

>               if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "Crashed while binding
>   " + r, e);

>               final boolean inDestroying = mDestroyingServices.contains(r);

>               serviceDoneExecutingLocked(r, inDestroying, inDestroying);

>               throw e;

>           } catch (RemoteException e) {

>               if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "Crashed while binding
>   " + r);

>               // Keep the executeNesting count accurate.

>               final boolean inDestroying = mDestroyingServices.contains(r);

>               serviceDoneExecutingLocked(r, inDestroying, inDestroying);

>               return false;

>           }

>       }

>       return true;

>   }

[**ApplicationThread.java**]

>   public final void scheduleBindService(IBinder token, Intent intent,

>           boolean rebind, int processState) {

>       updateProcessState(processState, false);

>       BindServiceData s = new BindServiceData();

>       s.token = token;

>       s.intent = intent;

>       s.rebind = rebind;

>       if (*DEBUG_SERVICE*)

>           Slog.v(*TAG*, "scheduleBindService token=" + token + " intent=" +
>   intent + " uid="

>                   + Binder.*getCallingUid*() + " pid=" +
>   Binder.*getCallingPid*());

>       sendMessage(H.*BIND_SERVICE*, s);

>   }

[**ActivityThread.java**]

>   private void handleBindService(BindServiceData data) {

>       Service s = mServices.get(data.token);

>       if (*DEBUG_SERVICE*)

>           Slog.v(*TAG*, "handleBindService s=" + s + " rebind=" +
>   data.rebind);

>       if (s != null) {

>           try {

>               data.intent.setExtrasClassLoader(s.getClassLoader());

>               data.intent.prepareToEnterProcess();

>               try {

>                   if (!data.rebind) {

>                       IBinder binder = s.onBind(data.intent);    //
>   回调Service的onBind方法，返回一个Binder对象，此时service处于绑定状态，但onBind是service的方法，客户端不知道已经成功连接了service

>                       ActivityManager.*getService*().publishService(

>                               data.token, data.intent, binder);

>                   } else {

>                       s.onRebind(data.intent);

>                       ActivityManager.*getService*().serviceDoneExecuting(

>                               data.token, *SERVICE_DONE_EXECUTING_ANON*, 0,
>   0);

>                   }

>                   ensureJitEnabled();

>               } catch (RemoteException ex) {

>                   throw ex.rethrowFromSystemServer();

>               }

>           } catch (Exception e) {

>               if (!mInstrumentation.onException(s, e)) {

>                   throw new RuntimeException(

>                           "Unable to bind to service " + s

>                           + " with " + data.intent + ": " + e.toString(), e);

>               }

>           }

>       }

>   }

[**ActivityManagerService.java**]

>   public void publishService(IBinder token, Intent intent, IBinder service) {

>       // Refuse possible leaked file descriptors

>       if (intent != null && intent.hasFileDescriptors() == true) {

>           throw new IllegalArgumentException("File descriptors passed in
>   Intent");

>       }

>       synchronized(this) {

>           if (!(token instanceof ServiceRecord)) {

>               throw new IllegalArgumentException("Invalid service token");

>           }

>           mServices.publishServiceLocked((ServiceRecord)token, intent,
>   service);

>       }

>   }

[**ActiveServices.java**]

>   void publishServiceLocked(ServiceRecord r, Intent intent, IBinder service) {

>       final long origId = Binder.*clearCallingIdentity*();

>       try {

>           if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "PUBLISHING " + r

>                   + " " + intent + ": " + service);

>           if (r != null) {

>               Intent.FilterComparison filter

>                       = new Intent.FilterComparison(intent);

>               IntentBindRecord b = r.bindings.get(filter);

>               if (b != null && !b.received) {

>                   b.binder = service;

>                   b.requested = true;

>                   b.received = true;

>                   for (int conni=r.connections.size()-1; conni\>=0; conni--) {

>                       ArrayList\<ConnectionRecord\> clist =
>   r.connections.valueAt(conni);

>                       for (int i=0; i\<clist.size(); i++) {

>                           ConnectionRecord c = clist.get(i);

>                           if (!filter.equals(c.binding.intent.intent)) {

>                               if (DEBUG_SERVICE) Slog.v(

>                                       *TAG_SERVICE*, "Not publishing to: " +
>   c);

>                               if (DEBUG_SERVICE) Slog.v(

>                                       *TAG_SERVICE*, "Bound intent: " +
>   c.binding.intent.intent);

>                               if (DEBUG_SERVICE) Slog.v(

>                                       *TAG_SERVICE*, "Published intent: " +
>   intent);

>                               continue;

>                           }

>                           if (DEBUG_SERVICE) Slog.v(*TAG_SERVICE*, "Publishing
>   to: " + c);

>                           try {

>                               c.conn.connected(r.name, service, false);

>                           } catch (Exception e) {

>                               Slog.w(*TAG*, "Failure sending service " +
>   r.name +

>                                     " to connection " + c.conn.asBinder() +

>                                     " (in " + c.binding.client.processName +
>   ")", e);

>                           }

>                       }

>                   }

>               }

>               serviceDoneExecutingLocked(r, mDestroyingServices.contains(r),
>   false);

>           }

>       } finally {

>           Binder.*restoreCallingIdentity*(origId);

>       }

>   }
