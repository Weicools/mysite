Boost 分两种：NormalBoost 和 PowerBoost

过程基本分两个阶段： Scan 和 Clean

Scan和Clean的真实实现都是在:clean进程，

     对外提供的接口都是HSAppMemoryManager这个类,  里面封装了具体的跨进程的实现
(跨进程的具体实现可以参见文杰讲的AILD)

1. NormalBoost

    (1) Scan （扫描所有运行的app，填充对应app的数据）

    a. LibDevice 对应的接口：

HSAppMemoryManager.getInstance().startScanXXX(Listener );         

    接口有多个，分两种:  1. 扫描过程中回调扫描开始、回传进度、回调扫描成功\\失败
  2. 只回调扫描开始，成功\\失败  对应不同的Listener

    扫描完成 回传的结果是 List\<HSAppMemory\>

    b. 具体实现（分两部分：扫描正在运行的app  && 填充内存占用数据）

 （1）扫描正在运行的app, 填充一些基础属性
（RecentApp，音乐软件，app对应所有进程的pid等）

    1.    系统版本小于5.0

    a. 通过ActivityManager的getRunningAppProcess() 获取到正在运行的进程,
 该方法返回的是List\<RunAppProcessInfo\>

    b. 通过RunAppProcessInfo的对象可以知道进程名和pid,
通过进程名分包（分一下哪个进程属于哪个app）

    c. 每一个app对应一个HSAppMemory对象， 填充app对应所有进程的的pid,
填充是不是recentApp、是不是音乐软件

    2.   系统版本大于等于5.0 小于8.0

    a.通过ActivityManager的getRunningServices()获取正在运行的Service,
改方法返回值是List\<RunningServiceInfo\>

   
b.通过RunningServiceInfo的对象可以知道Service对应app的包名及Service所在进程的pid，分包（分一下哪个Service属于哪个

app）

    c. 每一个app对应一个HSAppMemory对象， 填充app对应所有进程的的pid,
填充是不是recentApp、是不是音乐软件

    3.大于8.0

    a. 通过 PackageManager的getInstalledPackages() 获取安装的应用信息,
改方法返回值是List\<PackageInfo\>

    b. 通过 packInfo.applicationInfo.flags & ApplicaitonInfo.FLAG_STOPPED
判断是不是在运行

    c.每一个app对应一个HSAppMemory对象，这种方式获取不到pid,
只填充了是不是音乐软件，是不是recentApp

       （2）填充内存占用：

上面扫出来的正在运行的app，对于能拿到进程pid的情况下，可计算内存占用大小

每个进程的内存占用是通过 ActivityManager 的 getProcessMemoryInfo()
 方法获取到一个Debug.MemoryInfo的数组，MemoryInfo
有一个getTotalPss()的方法，获取到的就是占用内存的大小，一个app占用的内存总大小，就是把所有进程的相加即可

（2）Clean

    a. LibDevice 对应接口  

HSAppMemoryManager.getInstance().startClean(List\<HSAppMemory\>,  Listener );

    第一个参数是需要清理的app列表，第二个参数是个Listener,回传清理结果

    b. 具体实现：

1.  通过ActivityManager.killBackgroundProcess(String packageName);

2.  小于5.0，通过反射的方式调用 forceStopPackage 方法  
      
    Method declaredMethod =
    *activityManager*.getClass().getDeclaredMethod("forceStopPackage", new
    Class[]{String.class});  
      
    if (declaredMethod != null) {  
      
         declaredMethod.setAccessible(true);  
      
         declaredMethod.invoke(*activityManager*, new
    Object[]{appMemory.getPackageName()});  
      
    }

                         3. android.os.Process.killprocess(pid);

2.PowerBoost

  (1)  Scan  同上

  (2)  Clean

    通过Accessibility的方式进行强杀，就是在Setting
app详情页里面，模拟点击ForceStop按钮

     a. LibDevice 对应接口  

      HSAccTaskManage.getInstance().startForceStop(List\<String\>
packageNameList,  Listener);

 

 b.具体实现：

    在Manifest注册一个Service继承自AccessibilityService,
用户授权后，系统会回传给我们手机上发生的所有事件，我们也可以模拟点击

1.  跳到对应App setting的详情页

2.  在详情页通过view的id找到对应ForceStop的按钮，拿到的是一个AccessibilityNodeInfo对象

3.  通过AccessibilityNodeInfo 触发点击事件，从而实现模拟点击

 

 3.有效期：

 
 1.清理有效期：在当前session内，清理是有效的，就是说点击功能模块，不会触发扫描和清理,
直接到Done页面

一般有这么几个标记：

    NormalBoostProvider.setCleanValidInCurrentSessionFlag()

    NormalBoostProvider.setRealCleanInCurrentSessionFlag()  

                    NormalBoostProvider.setRealCleanSessionEndTime()

    清理完成之后会把 RealCleanInCurrentSession 和 CleanValidInCurrentSession
置为true

    Session End 的时候，会通过RealCleanInCurrentSession 这个标记是否为true,
记RealCleanSessionEndTime的时间

    Session End的时候也会把RealCleanInCurrentSession置位false

    什么时候过期：

    Session Start 的时间 - RealCleanSessionEndTime \> 2分钟   有效期过期
 CleanValidCurrentSession 会置为false

    2.扫描有效期：

    当前时间 - 上次扫描的时间 \< 30s，
点击功能模块就不会触发扫描，数据用上次扫描的数据
