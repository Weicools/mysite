JobScheduler 及 android-job 简介

  JobScheduler:

    JobScheduler的API是android
5.0才引入的。它允许你通过为系统定义要在以后的某个时间或在指定的条件下运行某些作业来优化电池寿命。

    例如：

   
你需要充电并且有网络的时候执行某个任务，那你就需要自己注册两个广播，充电的时候收到充电的广播判断一下是不是有网，

网络发生变化的时候收到广播判断下是否在充电，然后去确定自己的任务是否需要执行，这样做应用会唤醒多次。JobScheduler会在这两个

    条件都满足的时候才唤醒你的应用。

   1. JobService:

        JobScheduler是系统服务，通过启动App的JobService来实现功能

**       public abstract class JobService extend Service**

是个抽象类，需复写 onStartJob 和 onStopJob 两个方法

           public abstract boolean onStartJob(JobParameters params);

      public abstract boolean onStopJob(JobParameters params);

    JobService继承自Service, 需在Manifest中声明, 需要添加
android:permission=“android.permission.BIND_JOB_SERVICE” 这个权限

\<service android:name=“.MyJobService”

         android:permission=“android.permission.BIND_JOB_SERVICE”/\>

   2.JobInfo

JobInfo.Builder 支持的设置条件：充电， 网络， 时间，系统是否空闲

         setMinimumLatency(long minLatencyMillis):     

  这个函数能让你设置任务的延迟运行时间(单位是毫秒),这个函数与setPeriodic(long
time)方法不兼容，假设这两个方法同一时候调用了会crash；

  setOverrideDeadline(long maxExecutionDelayMillis): 

 这种方法让你能够设置任务最晚的执行时间。假设到了规定的时间时其它条件还未满足。你的任务也会被启动。与setMinimumLatency(long
time)一样，这种方法也不能与setPeriodic(long
time)同时调用，同一时候调用这两个方法会crash。

setPersisted(boolean isPersisted): 

       这种方法告诉系统当你的设备重新启动之后你的任务是否还要继续运行。

setRequiredNetworkType(int networkType): 

    
这种方法让你这个任务仅仅有在满足指定的网络条件时才会被运行。默认条件是JobInfo.NETWORK_TYPE_NONE。这意味着无论是否有网络这个任务都会被运行。

   
 另外两个可选类型。一种是JobInfo.NETWORK_TYPE_ANY，它表明须要随意一种网络才使得任务能够运行。

     还有一种是JobInfo.NETWORK_TYPE_UNMETERED，它表示设备不是蜂窝网络(
比方在WIFI连接时 )时任务才会被运行。

setRequiresCharging(boolean requiresCharging): 

   这种方法告诉你的应用，仅仅有当设备在充电时这个任务才会被运行。  设置成false,
直接就忽略了充电这个条件。

setRequiresDeviceIdle(boolean requiresDeviceIdle): 

 
  这种方法告诉你的任务仅仅有当用户没有在使用该设备且有一段时间没有使用时才会启动该任务。

 setPeriodic(long intervalMills) 

 设置任务循环的间隔时间

    有两点需要注意：

    1.setPeriodic方法   

 

      7.0及以上 最小的时间间隔是15min， 小于15min是不生效的 

    5.1及以上 最小5秒。

    5.0没限制

       如果调用了 setPeriodic() 方法，设置了间隔时间， 就不会考虑其他条件，
每隔时间间隔执行一次

        2.如果设置了 setRequiredNetworkType  setRequiresCharging
，setRequiresDeviceIdle  都会去判断其他条件是否满足,才决定Job是否执行

    3. JobScheduler用法：

   ** **

**创建JobService**

publicclass MyJobService extends JobService {

    privatestatic final String TAG = "MyJobService";

    \@Override

          public boolean onStartJob(JobParameters params) {

          Log.e(TAG, "onStartJob”);

         Toast.makeText(getBaseContext(), "onStartJob",
Toast.LENGTH_SHORT).show();

            returnfalse;   //return false, 任务已经执行完毕， return true,
系统假定这个任务正要被执行，当执行完任务是需要调用jobFinished()来通知系统

    }

      \@Override

    public boolean onStopJob(JobParameters params) {      
 //任务在执行过程中，系统需要停止此任务时会执行

         Log.e(TAG, "onStopJob");

          Toast.makeText(getBaseContext(), "onStopJob",
Toast.LENGTH_SHORT).show();

          returnfalse;

      }

}

    **schedule job**

JobScheduler jobScheduler = (JobScheduler)
getSystemService(JON_SCHEDULER_SERVICE);

JobInfo.Builder builder = new JobInfo.Builder(int jobId, new
ComponetName(MainActivity.this, MyJobService.class));

    //builder.setPeriodic(1000);

builder.setRequiresCharging(true);

builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

builder.setRequiresDeviceIdle(true);

jobScheduler.schedule(builder.build());

    **JobScheduler还支持cancel：**

          jobScheduler.cancel(int jobId)  //cancel 掉对应ID的Job

        jobScheduler.cancelAll()  //cancel 应用对应的所有Job

JobScheduler 实现方式

   
  JobService是一个Service,JobScheduler是系统服务，一定存在跨进程通信，可以猜想它是通过BindService来启动的。，

JobService.java

    public final IBinder onBind(Intent intent) {

      if (mBinder == null) {

         mBinder = new JobInterface(this);

      }

      return mBinder.asBinder();

       }

    **onBind 返回的是一个JobInterface对象，通过AIDL实现的。**

staticfinalclass JobInterface extends IJobService.Stub {

      final WeakReference\<JobService\> mService;

      JobInterface(JobService service) {

        mService = new WeakReference\<\>(service);

    }

      \@Override

      public void startJob(JobParameters jobParams) throws RemoteException {

        JobService service = mService.get();

          if (service != null) {

              service.ensureHandler();

              Message m = Message.obtain(service.mHandler, MSG_EXECUTE_JOB,
jobParams);

            m.sendToTarget();

        }

      }

      \@Override

      public void stopJob(JobParameters jobParams) throws RemoteException {

          JobService service = mService.get();

          if (service != null) {

              service.ensureHandler();

              Message m = Message.obtain(service.mHandler, MSG_STOP_JOB,
jobParams);

              m.sendToTarget();

          }

        }

   }

    **JobService中的Handler对象的初始化，可以看到是用的main looper,
回调是在主线程**

void ensureHandler() {

      synchronized (mHandlerLock) {

          if (mHandler == null) {

              mHandler = new JobHandler(getMainLooper());

          }

      }

}

   **JobHandler的实现**

class JobHandler extends Handler {

      JobHandler(Looper looper) {

          super(looper);

      }

    \@Override

      public void handleMessage(Message msg) {

final JobParameters params = (JobParameters) msg.obj;

          switch (msg.what) {

          case MSG_EXECUTE_JOB:

              try {

                  boolean workOngoing = JobService.this.onStartJob(params);

                  ackStartMessage(params, workOngoing);

              } catch (Exception e) {

                  Log.e(TAG, "Error while executing job: " + params.getJobId());

                 throw new RuntimeException(e);

             }

             break;

          case MSG_STOP_JOB:

              try {

                  boolean ret = JobService.this.onStopJob(params);

                  ackStopMessage(params, ret);

              } catch (Exception e) {

                  Log.e(TAG, "Application unable to handle onStopJob.", e);

                  throw new RuntimeException(e);

             }

             break;

   }

JobSchedulerService 的实现方式： 

>   public JobSchedulerService(Context context) {

>        super(context);      *// 创建控制器*      mControllers = new
>   ArrayList\<StateController\>();     *// 添加网络控制器*    
>    mControllers.add(ConnectivityController.get(this));      *//
>   添加时间控制器*     mControllers.add(TimeController.get(this));      *//
>   添加空闲控制器*      mControllers.add(IdleController.get(this));      *//
>   添加电池控制器*      mControllers.add(BatteryController.get(this));      *//
>   添加应用空闲控制器*      mControllers.add(AppIdleController.get(this));     
>    *// 初始化JobHandler，主要处理任务到期和检查任务的消息*      mHandler = new
>   JobHandler(context.getMainLooper());      *// 初始化mJobSchedulerStub*    
>    mJobSchedulerStub = new JobSchedulerStub(); }

     在schedule的时候，
会把job扔给这几个Controller，每个Controller会根据Job设置的条件，判断是否跟踪这个Job。

     当条件满足时，会Bind  Job对应的JobService, 调用startJob方法

 具体参见这个链接：<http://www.voidcn.com/blog/zhangyongfeiyong/article/p-6143011.html>

android-job

   android-job 是JobScheduler向下兼容的第三方库，由evernote 出品。

android-job 根据不同版本有多个不同的Proxy 

        V21 V24 通过JobScheduler实现，  V14 V19通过AlarmManager实现，
还有一种是依赖于google play service.

    如果不强制设置使用某个api的版本，默认是按如下的优先级，
根据版本和是否支持GCM来确定

publicstatic JobApi getDefault(Context context, boolean gcmEnabled) {

    if (V_24.isSupported(context)) {

        return V_24;

    } elseif (V_21.isSupported(context)) {

        return V_21;

    } elseif (gcmEnabled && GCM.isSupported(context)) {

        return GCM;

    } elseif (V_19.isSupported(context)) {

        return V_19;

      } else {

          return V_14;

    }

}

使用方法：

    1.Gradle 里添加如下依赖

dependencies {

     compile 'com.evernote:android-job:1.1.10'

}

    2.Application 的onCreate里面添加
 JobManager.create(this).addJobCreator(new DemoJobCreator());  

    当设置的条件满足后会调用JobCreator的create方法new一个对应Tag的Job,
然后调用Job的onRunJob方法执行任务

>   public class App extends Application {  
>     
>       \@Override  
>       public void onCreate() {  
>           super.onCreate();  
>           JobManager.create(this).addJobCreator(new DemoJobCreator());  
>       }  
>   }

>   public class DemoJobCreator implements JobCreator {  
>     
>       \@Override  
>       public Job create(String tag) {  
>           switch (tag) {  
>               case DemoSyncJob.TAG:  
>                   return new DemoSyncJob();  
>               default:  
>                   return null;  
>           }  
>       }  
>   }

>   publicclassDemoSyncJobextendsJob {

>       publicstaticfinalStringTAG="job_demo_tag";

>       \@Override

>       \@NonNull

>       protectedResultonRunJob(Paramsparams) {

>           // run your job here

>           returnResult.SUCCESS;

>   }

>   }

用法：

  new JobRequest.Builder(DemoSyncJob.TAG)

          .setExecutionWindow(30_000L, 40_000L)

          .build()

          .schedule();

 要让条件同时满足，需setRequirementsEnforced(true)

注意事项：

onRunJob 方法的回调不在主线程

我们对android-job的封装：

   用法：

class SDCardMonitorJob extends Job implements HSJobInterface {  

public static final String JobTAG = "SDCardMonitorJob";

\@Override

protected Result onRunJob(Params params) {

    L.*l*("SDCardMonitorJob  onRunJob -------\>");

    tryToRunJob();

    return Result.SUCCESS;

}

\@Override

public void tryToRunJob() {

    L.*l*("SDCardMonitorJob  tryToRunJob -------\>");

    HSSDCardMonitorMgr.*getInstance*().tryToScanExternalStorage();

}

}

JobRequest jobRequest = new JobRequest.Builder(SDCardMonitorJob.*JobTAG*)

     .setPeriodic(JobRequest.MIN_INTERVAL)

      .build();

HSJobSchedulerManager.getInstance().scheduleJob(jobRequest,
SDCardMonitorJob.class);

    实现方式：  

      android-job 需要的一个JobCreator 

public class DeviceMonitorJobCreator implements JobCreator {

    \@Override

    public Job create(String tag) {

        if (tag == null \|\| tag.length() \<= 0) {

            return null;

        }

        String className =
HSJobSchedulerManager.getInstance().getExecuteJobClass(tag);

//记录job每次执行的时间，为了使进程死后重启起来判断时间是否长时间没执行，主动执行一次

        HSPreferenceHelper.create(HSApplication.getContext(),

            HSJobSchedulerManager.PREF_JOB_SCHEDULER).putLongInterProcess(tag +
"_run_time", System.currentTimeMillis());

        try {

            return (Job) Class.forName(className).newInstance();

        }  catch (){…..}

}

scheduler Job的封装：HSJobSchedulerManager.java

public void scheduleJob(final JobRequest jobRequest, final Class jobClass) {

      // 对于同一个tag的job，只会schedule一次，任务队列中只会有一个

// 从android-job的JobManager里取出已经sheduler的job

    Set\<JobRequest\> jobSet =
JobManager.create(HSApplication.getContext()).getAllJobRequestsForTag(jobRequest.getTag());

//如果是空的， shedule job， 并且小于5.0的执行一次Job, \>
5.0的系统的JobScheduler会主动执行一次

    if (jobSet.isEmpty()) {

          L.l("HSJobSchedulerManager  schedule job IntervalMs = " +
jobRequest.getIntervalMs());

        if (Build.VERSION.SDK_INT \< Build.VERSION_CODES.LOLLIPOP &&
jobRequest.isPeriodic()) {

             tryToRunJob(jobClass);

        }

          setExecutorJobClass(jobRequest.getTag(), jobClass);

        jobRequest.schedule()；

return；

    }

      L.l("HSJobSchedulerManager  job has scheduled -------\>");

    Iterator\<JobRequest\> iterator = jobSet.iterator();

    if (iterator.hasNext()) {

          JobRequest request = iterator.next();

        if (Build.VERSION.SDK_INT \< Build.VERSION_CODES.LOLLIPOP &&
request.isPeriodic()) {

              HSPreferenceHelper hsPreferenceHelper =
HSPreferenceHelper.create(HSApplication.getContext(),

                  HSJobSchedulerManager.PREF_JOB_SCHEDULER);

//这里是为了确保进程死了又重启之后确保任务长时间没执行主动指定一次；

             if (System.currentTimeMillis() -
hsPreferenceHelper.getLongInterProcess(request.getTag() + "_run_time", 0)

                 \> jobRequest.getIntervalMs() \* 2) {

                   hsPreferenceHelper.putLongInterProcess(request.getTag() +
"_run_time", System.currentTimeMillis());

                  tryToRunJob(jobClass);

              }

          }

     }

   }

private void tryToRunJob(Class jobClass) {

    try {

          ((HSJobInterface) jobClass.newInstance()).tryToRunJob();

    } catch (InstantiationException e) {

          e.printStackTrace();

    } catch (IllegalAccessException e) {

          e.printStackTrace();

    }

}  
