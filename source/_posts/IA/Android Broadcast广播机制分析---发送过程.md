发送广播是在Activity或Service中调用sendBroadcast()方法，而Activity或Service都间接继承于Context抽象类，真正干活是交给ContextImpl类。

### 3.1 sendBroadcast

[ContextImpl.java]

>   public void sendBroadcast(Intent intent) {

>       warnIfCallingFromSystemProcess();

>       String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());

>       try {

>           intent.prepareToLeaveProcess();

>           // 调用AMP.broadcastIntent 【见3.2】

>           ActivityManagerNative.getDefault().broadcastIntent(

>                   mMainThread.getApplicationThread(), intent, resolvedType,
>   null,

>                   Activity.*RESULT_OK*, null, null, null,
>   AppOpsManager.OP_NONE, null, false, false,

>                   getUserId());

>       } catch (RemoteException e) {

>       ...

>       }

>   }

### 跟注册的过程一样，ActivityManagerNative.getDefault()最终调用的其实就是AMS中的方法。

### 3.2 AMS.broadcastIntent

[-\> ActivityManagerService.java]

>   public final int broadcastIntent(IApplicationThread caller, Intent intent,
>   String resolvedType, IIntentReceiver resultTo, int resultCode, String
>   resultData, Bundle resultExtras, String[] requiredPermissions, int appOp,
>   Bundle options, boolean serialized, boolean sticky, int userId) {

>       enforceNotIsolatedCaller("broadcastIntent");

>       synchronized(this) {

>           //验证广播intent是否有效

>           intent = verifyBroadcastLocked(intent);

>           //获取调用者进程记录对象

>           final ProcessRecord callerApp = getRecordForAppLocked(caller);

>           final int callingPid = Binder.*getCallingPid*();

>           final int callingUid = Binder.*getCallingUid*();

>           final long origId = Binder.*clearCallingIdentity*();

>           //【见小节3.3】

>           int res = broadcastIntentLocked(callerApp,

>                   callerApp != null ? callerApp.info.packageName : null,

>                   intent, resolvedType, resultTo, resultCode, resultData,
>   resultExtras,

>                   requiredPermissions, appOp, null, serialized, sticky,

>                   callingPid, callingUid, userId);

>           Binder.*restoreCallingIdentity*(origId);

>           return res;

>       }

>   }

broadcastIntent()方法有两个布尔参数serialized和sticky来共同决定是普通广播，有序广播，还是Sticky广播，参数如下：

这个了解一下就行了

| 类型                 | serialized | sticky |
|----------------------|------------|--------|
| sendBroadcast        | false      | false  |
| sendOrderedBroadcast | true       | false  |
| sendStickyBroadcast  | false      | true   |

### 3.3 AMS.broadcastIntentLocked

>       private final int broadcastIntentLocked() {

>   *       * //step5: 查询receivers和registeredReceivers

>           //step6: 处理并行广播

>           //step7: 合并registeredReceivers到receivers

>           //step8: 处理串行广播

>           return ActivityManager.BROADCAST_SUCCESS;

>       }

>   }

broadcastIntentLocked这个方法比较长，这里我们先将其划分为8个部分来分别说明。

#### 3.3.1 设置广播flag

>           intent = new Intent(intent);

>   //增加该flag，则广播不会发送给已停止的进程

>           intent.addFlags(Intent.*FLAG_EXCLUDE_STOPPED_PACKAGES*);

>   //当没有启动完成时，不允许启动新进程

>           if (!mProcessesReady &&
>   (intent.getFlags()&Intent.FLAG_RECEIVER_BOOT_UPGRADE) == 0) {

>               intent.addFlags(Intent.*FLAG_RECEIVER_REGISTERED_ONLY*);

>           }

>           userId = handleIncomingUser(callingPid, callingUid, userId,

>                   true, ALLOW_NON_FULL, "broadcast", callerPackage);

>   //检查发送广播时用户状态

>           if (userId != UserHandle.USER_ALL && !isUserRunningLocked(userId,
>   false)) {

>               if ((callingUid != Process.SYSTEM_UID

>                       \|\| (intent.getFlags() &
>   Intent.FLAG_RECEIVER_BOOT_UPGRADE) == 0)

>                       && !Intent.*ACTION_SHUTDOWN*.equals(intent.getAction()))
>   {

>                   return ActivityManager.BROADCAST_FAILED_USER_STOPPED;

>               }

>           }

这个过程最所做的工作是：

-   添加flag=FLAG_EXCLUDE_STOPPED_PACKAGES，保证已停止app不会收到该广播；

-   当系统还没有启动完成，则不允许启动新进程（静态注册的Receiver收到广播时如果该进程没活着则会拉起该进程），即只有动态注册receiver才能接受广播（动态注册的广播Receiver其进程肯定是活着的）

-   当非USER_ALL广播且当前用户并没有处于Running的情况下，除非是系统升级广播或者关机广播，否则直接返回。

**3.3.2 广播权限验证**

>   int callingAppId = UserHandle.getAppId(callingUid);

>   if (callingAppId == Process.SYSTEM_UID \|\| callingAppId ==
>   Process.PHONE_UID

>           \|\| callingAppId == Process.SHELL_UID \|\| callingAppId ==
>   Process.BLUETOOTH_UID

>           \|\| callingAppId == Process.NFC_UID \|\| callingUid == 0) {

>       //直接通过

>   } else if (callerApp == null \|\| !callerApp.persistent) {

>       try {

>           if (AppGlobals.getPackageManager().isProtectedBroadcast(

>                   intent.getAction())) {

>               //不允许发送给受保护的广播

>               throw new SecurityException(msg);

>           } else if
>   (AppWidgetManager.*ACTION_APPWIDGET_CONFIGURE*.equals(intent.getAction())) {

>       ...

>           }

>       } catch (RemoteException e) {

>           return ActivityManager.BROADCAST_SUCCESS;

>       }

>   }

主要功能：

-   对于callingAppId为SYSTEM_UID，PHONE_UID，SHELL_UID，BLUETOOTH_UID，NFC_UID之一或者callingUid
    == 0时都畅通无阻；

-   否则当调用者进程为空 或者非persistent进程（可持续进程？？？）的情况下：

    -   当发送的是受保护广播mProtectedBroadcasts(只允许系统使用)，则抛出异常；

    -   当action为ACTION_APPWIDGET_CONFIGURE时，虽然不希望该应用发送这种广播，处于兼容性考虑，限制该广播只允许发送给自己，否则抛出异常。

#### 3.3.3 处理系统相关广播

>   final String action = intent.getAction();

>   if (action != null) {

>       switch (action) {

>           case Intent.*ACTION_UID_REMOVED*: //uid移除

>           case Intent.*ACTION_PACKAGE_REMOVED*: //package移除

>           case Intent.*ACTION_PACKAGE_ADDED*: //增加package

>           case Intent.*ACTION_PACKAGE_CHANGED*: //package改变

>           case Intent.*ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE*:
>   //外部设备不可用

>           case Intent.*ACTION_EXTERNAL_APPLICATIONS_AVAILABLE*: //外部设备可用

>           case Intent.*ACTION_TIMEZONE_CHANGED*:
>   //时区改变，通知所有运行中的进程

>           case Intent.*ACTION_TIME_CHANGED*: //时间改变，通知所有运行中的进程

>           case Intent.ACTION_CLEAR_DNS_CACHE: //DNS缓存清空

>           case Proxy.*PROXY_CHANGE_ACTION*: //网络代理改变

>       }

>   }

这个过主要处于系统相关的10类广播

#### 3.3.4 增加sticky广播

>   if (sticky) {

>       if (checkPermission(android.Manifest.permission.*BROADCAST_STICKY*,

>               callingPid, callingUid)

>               != PackageManager.*PERMISSION_GRANTED*) {

>           throw new SecurityException("");

>       }

>       if (requiredPermissions != null && requiredPermissions.length \> 0) {

>           return ActivityManager.BROADCAST_STICKY_CANT_HAVE_PERMISSION;

>       }

>       if (intent.getComponent() != null) {

>           //当sticky广播发送给指定组件，则throw Exception

>       }

>       if (userId != UserHandle.USER_ALL) {

>           //当非USER_ALL广播跟USER_ALL广播出现冲突,则throw Exception

>       }

>       ArrayMap\<String, ArrayList\<Intent\>\> stickies =
>   mStickyBroadcasts.get(userId);

>       if (stickies == null) {

>           stickies = new ArrayMap\<\>();

>           mStickyBroadcasts.put(userId, stickies);

>       }

>       ArrayList\<Intent\> list = stickies.get(intent.getAction());

>       if (list == null) {

>           list = new ArrayList\<\>();

>           stickies.put(intent.getAction(), list);

>       }

>       final int stickiesCount = list.size();

>       int i;

>       for (i = 0; i \< stickiesCount; i++) {

>           if (intent.filterEquals(list.get(i))) {

>               //替换已存在的sticky intent

>               list.set(i, new Intent(intent));

>               break;

>           }

>       }

>       //新的intent追加到list

>       if (i \>= stickiesCount) {

>           list.add(new Intent(intent));

>       }

>   }

这个过程主要是将sticky广播增加到list，并放入mStickyBroadcasts里面。

#### 3.3.5 查询receivers和registeredReceivers

>   List receivers = null;//

>   List\<BroadcastFilter\> registeredReceivers = null;

>   //当允许静态接收者处理该广播，则通过PMS根据Intent查询相应的静态receivers

>   if ((intent.getFlags() & Intent.*FLAG_RECEIVER_REGISTERED_ONLY*) == 0) {

>       receivers = collectReceiverComponents(intent, resolvedType, callingUid,
>   users);

>   }

>   if (intent.getComponent() == null) {

>       if (userId == UserHandle.USER_ALL && callingUid == Process.SHELL_UID) {

>   ...

>       } else {

>           // 查询相应的动态注册的广播

>           registeredReceivers = mReceiverResolver.queryIntent(intent,

>                   resolvedType, false, userId);

>       }

>   }

-   receivers：记录着匹配当前intent的所有静态注册广播接收者，

-   registeredReceivers：记录着匹配当前的所有动态注册的广播接收者。

注意：

-   mReceiverResolver是AMS的成员变量，记录着已注册的广播接收者的resolver.在广播注册的过程中可以看到最后就是将BroadcastFilter添加到这个成员变量中

**AMS.collectReceiverComponents**：

>   private List\<ResolveInfo\> collectReceiverComponents(Intent intent, String
>   resolvedType, int callingUid, int[] users) {

>       List\<ResolveInfo\> receivers = null;

>       for (int user : users) {

>          
>   //调用PMS.queryIntentReceivers，可获取AndroidManifest.xml声明的接收者信息

>           List\<ResolveInfo\> newReceivers = AppGlobals.getPackageManager()

>                   .queryIntentReceivers(intent, resolvedType, STOCK_PM_FLAGS,
>   user);

>           if (receivers == null) {

>               receivers = newReceivers;

>           } else if (newReceivers != null) {

>           ...

>               //将所有用户的receiver整合到receivers

>           }

>       }

>       return receivers;

>   }

-   ResolveInfo是通过收集AndroidManifest文件中的\<intent-filter\>标签下的信息以及对应的四大组件，我们通过IntentFilter下的信息就可以匹配到相应的组件，具体怎么生成的，其实是PMS的事情。
       <https://blog.csdn.net/wang_yubin/article/details/8564335>

-   这里的ResolveInfo可以理解为静态广播在PMS中最终注册的对象类型，类似动态广播在AMS中注册的BroadcastFilter；

#### 3.4.6 处理并行广播

>   //用于标识是否需要用新intent替换旧的intent。

>   final boolean replacePending = (intent.getFlags() &
>   Intent.*FLAG_RECEIVER_REPLACE_PENDING*) != 0;

>   //处理并行广播

>   int NR = registeredReceivers != null ? registeredReceivers.size() : 0;

>   if (!ordered && NR \> 0) {

>       final BroadcastQueue queue = broadcastQueueForIntent(intent);

>       //创建BroadcastRecord对象

>       BroadcastRecord r = new BroadcastRecord(queue, intent, callerApp,

>               callerPackage, callingPid, callingUid, resolvedType,
>   requiredPermissions,

>               appOp, brOptions, registeredReceivers, resultTo, resultCode,
>   resultData,

>               resultExtras, ordered, sticky, false, userId);

>       final boolean replaced = replacePending &&
>   queue.replaceParallelBroadcastLocked(r);

>       if (!replaced) {

>           //将BroadcastRecord加入到并行广播队列[见下文]

>           queue.enqueueParallelBroadcastLocked(r);

>           //处理广播【见下一节】

>           queue.scheduleBroadcastsLocked();

>       }

>       //动态注册的广播接收者处理完成，则会置空该变量；

>       registeredReceivers = null;

>       NR = 0;

>   }

广播队列BroadcastQueue中有一个成员变量mParallelBroadcasts，类型为ArrayList，记录着所有的并行广播。

>   // 并行广播,加入mParallelBroadcasts队列

>   public void enqueueParallelBroadcastLocked(BroadcastRecord r) {

>       mParallelBroadcasts.add(r);

>       r.enqueueClockTime = System.*currentTimeMillis*();

>   }

#### 3.4.7 合并registeredReceivers到receivers

>   int ir = 0;

>   if (receivers != null) {

>       //防止应用监听该广播，在安装时直接运行。

>       String skipPackages[] = null;

>       if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())

>               \|\| Intent.ACTION_PACKAGE_RESTARTED.equals(intent.getAction())

>               \|\|
>   Intent.ACTION_PACKAGE_DATA_CLEARED.equals(intent.getAction())) {

>           Uri data = intent.getData();

>           if (data != null) {

>               String pkgName = data.getSchemeSpecificPart();

>               if (pkgName != null) {

>                   skipPackages = new String[] { pkgName };

>               }

>           }

>       } else if
>   (Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE.equals(intent.getAction())) {

>           skipPackages =
>   intent.getStringArrayExtra(Intent.EXTRA_CHANGED_PACKAGE_LIST);

>       }

>       //将skipPackages相关的广播接收者从receivers列表中移除

>       if (skipPackages != null && (skipPackages.length \> 0)) {

>   ...

>       }

>      
>   //[3.4.6]有一个处理动态广播的过程，处理完后再执行将动态注册的registeredReceivers合并到receivers

>       int NT = receivers != null ? receivers.size() : 0;

>       int it = 0;

>       ResolveInfo curt = null;

>       BroadcastFilter curr = null;

>       while (it \< NT && ir \< NR) {

>           if (curt == null) {

>               curt = (ResolveInfo)receivers.get(it);

>           }

>           if (curr == null) {

>               curr = registeredReceivers.get(ir);

>           }

>           if (curr.getPriority() \>= curt.priority) {

>               receivers.add(it, curr);

>               ir++;

>               curr = null;

>               it++;

>               NT++;

>           } else {

>               it++;

>               curt = null;

>           }

>       }

>   }

>   while (ir \< NR) {

>       if (receivers == null) {

>           receivers = new ArrayList();

>       }

>       receivers.add(registeredReceivers.get(ir));

>       ir++;

>   }

动态注册的registeredReceivers，全部合并都receivers，再统一按串行方式处理。这个过程跟文杰讲过的归并排序类似。

#### 3.4.8 处理串行广播

>   if ((receivers != null && receivers.size() \> 0)

>           \|\| resultTo != null) {

>       BroadcastQueue queue = broadcastQueueForIntent(intent);

>       //创建BroadcastRecord

>       BroadcastRecord r = new BroadcastRecord(queue, intent, callerApp,

>               callerPackage, callingPid, callingUid, resolvedType,

>               requiredPermissions, appOp, brOptions, receivers, resultTo,
>   resultCode,

>               resultData, resultExtras, ordered, sticky, false, userId);

>       boolean replaced = replacePending &&
>   queue.replaceOrderedBroadcastLocked(r);

>       if (!replaced) {

>           //将BroadcastRecord加入到有序广播队列

>           queue.enqueueOrderedBroadcastLocked(r);

>           //处理广播【见小节4.1】

>           queue.scheduleBroadcastsLocked();

>       }

>   }

广播队列中有一个成员变量mOrderedBroadcasts，类型为ArrayList，记录着所有的有序广播。

>   // 串行广播 加入mOrderedBroadcasts队列

>   public void enqueueOrderedBroadcastLocked(BroadcastRecord r) {

>       mOrderedBroadcasts.add(r);

>       r.enqueueClockTime = System.*currentTimeMillis*();

>   }

### 3.5 小结

发送广播过程:

1.  默认不发送给已停止（Intent.FLAG_EXCLUDE_STOPPED_PACKAGES）的应用包；

2.  处理各种PACKAGE,TIMEZONE等相关的系统广播；

3.  当广播的Intent没有设置FLAG_RECEIVER_REGISTERED_ONLY，则允许静态广播接收者来处理该广播；
    创建BroadcastRecord对象,并将该对象加入到相应的广播队列,
    然后调用BroadcastQueue的scheduleBroadcastsLocked()方法来完成的不同广播处理:

处理方式：

1.  无序广播： 广播发送过程处理

-   先把动态注册的Receiver加入到BroadcastQueue中，然后再把静态注册的Receiver加入到BroadcastQueue中；

-   会先根据动态注册的Receiver创建BroadcastRecord对象;

-   并添加到mParallelBroadcasts队列；

-   然后执行queue.scheduleBroadcastsLocked;

-   有序广播： 广播发送广播处理

-   把静态注册的Receivers以及动态注册mRegisteredReceivers合并；

    -   合并时，排序关键字为IntentFilter的priority

    -   当priority一样时，先注册的动态广播\>后注册的动态广播\>静态广播（静态广播的顺序找小智调研吧）

-   创建BroadcastRecord对象；

-   并添加到mOrderedBroadcasts队列；

-   然后执行queue.scheduleBroadcastsLocked；

可见不管哪种广播方式，接下来都会执行scheduleBroadcastsLocked方法来处理广播；
