**Android开机动画**

在详细分析SurfaceFlinger模块之前要先看看Android的开机动画，因为Android开机动画是一个C++应用程序，需要使用SurfaceFlinger服务来创建和渲染自己的Surface，并且不涉及与用户进行交互，所以能以最简洁的方式体现SurfaceFlinger。

当一台Android设备打开，首先映入眼帘的是一个黑屏，然后一个静态图，然后就是如下：

![](media/654229ca4fbb8523465ca0149e9a9a39.gif)

第三个开机画面（Android 6.0）

1.  第一个黑屏是在内核启动的过程中出现的，它是一个静态的画面，这个过程还完全属于linux。

2.  第二个是init进程启动的过程中出现的，它也是一个静态的画面。

3.  第三个是在Android系统服务启动的过程中出现的，它是一个动态的画面。

第三个开机动画是应用程序BootAnimation来负责显示的，它在启动脚本init.rc中被配置成了一个服务，位于system/core/rootdir/init.rc：

**init.rc是什么？**

首先一张**Android总体启动**框架图描述一下init所处的位置，如下：

![](media/a054882349c6676f1762c6ceebd90840.png)

简单的说Init.rc就是上图中init执行的脚本，Android
init.rc文件由系统第一个启动的init程序解析。

此文件由语句组成，主要包括了四种类型的语句：**Action**，**Commands**，**Services**，**Options**。在init.rc文件里一条语句一般是占领一行，单词之间是通过空格符来相隔的，注释是以\#号开头。

**服务启动机制**

system/core/init/init.cpp中main函数解析并执行init.rc，其中主要逻辑有以下两点：

1.  执行Parser::ParseConfig读取并解析init.rc文件内容，将service信息放置到ServiceManager

2.  执行restart_processes()建立service进程

这个不是重点，在这里简单略过，接下来主要分析一下BootAnimation。

第三个开机动画是应用程序BootAnimation来负责显示的，它在启动脚本init.rc中被配置成了一个服务，如下

>   service bootanim /system/bin/bootanimation

>       class core animation

>       user graphics  \#用户

>       group graphics audio  \#用户组

>       disabled  \#init进程启动时，不会自动启动bootanimation

>       oneshot

>       writepid /dev/stune/top-app/tasks

用来启动应用程序bootanimation的服务是disable的，即init进程在启动的时候，不会主动将应用程序bootanimation启动起来。那什么时候启动呢，这里剧透一下：其实是当SurfaceFlinger服务启动的时候，它通过修改系统属性ctl.start的值来通知init进程启动应用程序bootanimation，以便显示第三个开机画面。

看一下Surfaceflinger服务，在rc中该服务的描述如下：

>   service surfaceflinger /system/bin/surfaceflinger

>       class core

>       user system

>       group graphics drmrpc readproc

>       onrestart restart zygote

>       writepid /dev/stune/foreground/tasks

>       disabled

从rc文件中可以看到init会启动Surfaceflinger服务，那么就分析一下Surfaceflinger的源码。

>   **int** main(**int**, **char**\*\*) {

>       ……

>   // binder thread

>   *   * ProcessState::self()-\>setThreadPoolMaxThreadCount(4);

>   *   * sp\<ProcessState\> ps(ProcessState::self());

>       ps-\>startThreadPool();

>       *// instantiate surfaceflinger*

>   *   * sp\<SurfaceFlinger\> flinger = **new** SurfaceFlinger();

>       ……

>       *// initialize before clients can connect*

>   *   * flinger-\>init();

>       *// publish surface flinger*

>   *   * sp\<IServiceManager\> sm(defaultServiceManager());

>       sm-\>addService(String16(SurfaceFlinger::getServiceName()), flinger,
>   **false**);

>       ……

>       *// run surface flinger in this thread*

>   *   * flinger-\>run();

>       **return** 0;

>   }

其中主要的工作就是打开Binder线程池，新建一个SurfaceFlinger对象并初始化，然后记录到ServiceManager，最后调用run方法。

我们的重点在init方法，接下来着重看一下SurfaceFlinger::init()方法。

>   **void** SurfaceFlinger::init() {

>       ……

>   *   * **if** (getHwComposer().hasCapability(

>               HWC2::Capability::PresentFenceIsNotReliable)) {

>           mStartPropertySetThread = **new** StartPropertySetThread(**false**);

>       } **else** {

>           mStartPropertySetThread = **new** StartPropertySetThread(**true**);

>       }

>       **if** (mStartPropertySetThread-\>Start() != NO_ERROR) {

>           ALOGE(**"Run StartPropertySetThread failed!"**);

>       }

>       ALOGV(**"Done initializing"**);

>   }

init方法中创建了StartPropertySetThread然后调用了其Start方法，从名字大概能猜到这里改了init.rc中对应属性。

>   status_t StartPropertySetThread::Start() {

>       **return** run(**"SurfaceFlinger::StartPropertySetThread"**,
>   PRIORITY_NORMAL);

>   }

>   **bool** StartPropertySetThread::threadLoop() {

>   *    *property_set(kTimestampProperty, mTimestampPropertyValue ? **"1"** :
>   **"0"**);

>   *    *property_set(**"service.bootanim.exit"**, **"0"**);

>   *    *property_set(**"ctl.start"**, **"bootanim”**);

>   *    ***return false**;

>   }

当系统属性发生改变时，init进程就会接收到一个系统属性变化通知，这个通知最终在init进程中的函数handle_property_set_fd处理。

从上面可以看到SurfaceFlinger服务在初始化后修改了系统属性，其中修改系统属性是以start结尾的，因此，接下来就会调用函数msg_start来启动一个名称为“bootanim”的服务。

我们看一下init(android-8.1.0_r1\\system\\core\\init\\init.cpp)是如何处理的：

>   **void** handle_control_message(**const** std::string& msg, **const**
>   std::string& name) {

>       Service\* svc = ServiceManager::GetInstance().FindServiceByName(name);

>       **if** (svc == **nullptr**) {

>           LOG(ERROR) \<\< **"no such service '"** \<\< name \<\< **"'"**;

>           **return**;

>       }

>       **if** (msg == **"start"**) {

>           svc-\>Start();

>       } **else if** (msg == **"stop"**) {

>           svc-\>Stop();

>       } **else if** (msg == **"restart"**) {

>           svc-\>Restart();

>       } **else** {

>           LOG(ERROR) \<\< **"unknown control msg '"** \<\< msg \<\< **"'"**;

>       }

>   }

handle_control_message中：首先调用ServiceManager::FindServiceByName方法查询到要启动的服务，然后根据msg执行相应操作。

因为init.rc中有bootanimation的定义，因此在init进程执行Parser::ParseConfig时，会将该服务添加到ServiceManager中，所以bootanimation应用是存在的。

到此，bootanimation应用就启动了。

**阶段小结：**

init.rc配置了BootAnimation和SurfaceFlinger服务，其中BootAnimation是disable的。

init处理init.rc脚本，当SurfaceFlinger服务启动起来后调用init方法时，会开一个线程去修改BootAnimation相关的系统属性。

init监听到系统属性改变时会做对应处理，这里是start bootanimation服务。

BootAnimation服务启动后就开始了工作，我们看到的第三个开机动画就开始显示了。
