-   Glide

         
 如果传入activity的context，那么当这个Activity或Fragment被销毁的时候，图片加载也会停止。如果传入的是ApplicationContext，那么只有当应用程序被杀掉的时候，图片加载才会停止。

         
如果是Application的context，就没有onDestroy这个时机。但是用Activity或者Fragment的Context的话，在发起请求的时刻，如果它们已经走完onDestroy了，Glide自己会抛一条crash出来，需要写的时候注意。当前这一屏的ImageView，如果是使用Activity或者Fragment的context的话，在onDestroy时，这一屏加载好的图片会进入内存缓存（并且正在加载的操作会被立即cancel）；如果用的Application的context，当Activity退出时，这一屏的加载好的图片会在下一次gc的时候被废弃掉，并且后台的加载操作无法被cancel。我们还是建议使用近的context，在Activity里尽量使用isFinishing来判断是否发起加载请求

-   sendNotification

-   AcbNativeAdContainerView & adContentView

-   AcbImageLoader

-   通过Context创建URI

-   getContentResolver()

-   Toast：用activity的context会使toast持有activity，如果仍在显示会导致内存泄漏。方案：使用applicationContext()或者ToastUtil

-   Inflate View

-   startActivity & Intent

-   HSPreferenceHelper.create

-   Dialog中的getString

—————————————————————————不可以分界线————————————————————————

-   Static的变量不可以用activity的

—————————————————————————注意点分界线————————————————————————

-   Activity成员变量如果直接应用this，会有NPE，因为成员变量是在构造函数之前初始化的，这时候还没有this，可以使用局部变量替代成员变量，如果某些地方只能采用成员变量的话，就需要使用applicationContext了——Max

>   例如:

>       XXXActivity{

>   // 成员变量初始化不能用this

>   private final int screenHeight =
>   DisplayUtils.getScreenWithOutNavigationBarHeight() -
>   StatusBarUtils.getStatusBarHeight(this);

>       ……

>       onCreate() {……}

>   }

HSApplication.getContext() =\> getApplicationContext()
