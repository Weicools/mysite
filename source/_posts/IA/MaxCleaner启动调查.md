测试目的：测试 ClearData 与 网络 对app冷启动速度的影响

测试设备：Samsung S6 CTC， SM-G9209

测试环境：remove all recent apps

测试路径：各测试5次，取平均值 (case 2有两次数据无效，实际结果为三次实验平均值)

                  1. Network ON -\> Force stop MaxCleaner + Clear data -\>
remove all recent apps -\> start MaxCleaner

  2. Network OFF -\> Force stop MaxCleaner + Clear data -\> remove all recent
apps -\> start MaxCleaner 

                  3. Network ON -\> Force stop MaxCleaner -\> remove all recent
apps -\> start MaxCleaner

  2. Network OFF -\> Force stop MaxCleaner -\> remove all recent apps -\> start
MaxCleaner 

测试数据：

| 测试项目                        | EnterActivity 加载时间(ms) | MainActivity 加载时间(ms) | 从点击图标到MainActivity加载完成时间(ms) |
|---------------------------------|----------------------------|---------------------------|------------------------------------------|
| ForceStop+ClearData Network ON  | 1510                       | 1028                      | 3320                                     |
| ForceStop Network ON            | 1525                       | 1158                      | 3327                                     |
| ForceStop+ClearData Network OFF | 1559                       | 705                       | 2756                                     |
| ForceStop Network OFF           | 1325                       | 551                       | 2494                                     |

测试结果

| 测试项目                                     | EnterActivity 加载时间(ms) | MainActivity 加载时间(ms) | 从点击图标到MainActivity加载完成时间(ms) |
|----------------------------------------------|----------------------------|---------------------------|------------------------------------------|
| ForceStop VS ForceStop+ClearData Network ON  | \+ 1% (15ms)               | \+ 12% (130ms)            | \+ 0.21% (7ms)                           |
| ForceStop VS ForceStop+ClearData Network OFF | \- 15% (230ms)             | \- 21% (150ms)            |  9.5% (260ms)                            |

    时间轴

| Network | create task | create EnterAppActivity | focuse stack | focus EnterAppActivity | proc_start main | main bound | restart EnterAppActivity | proc_start work | work bound | resumed EnterAppActivity | proc_start clean | clean bound | launch done | create MainActivity | pause EnterAppActivity | pause done | restart MainActivity | resumed MainActivity | launch done |
|---------|-------------|-------------------------|--------------|------------------------|-----------------|------------|--------------------------|-----------------|------------|--------------------------|------------------|-------------|-------------|---------------------|------------------------|------------|----------------------|----------------------|-------------|
| OFF     | 0           | 0                       | 1            | 2                      | 20              | 38         | 41                       | 129             | 146        | 853                      | 876              | 916         | 1295        | 1859                | 1862                   | 1869       | 1875                 | 2040                 | 2359        |
| ON      | 0           | 0                       | 1            | 2                      | 20              | 39         | 46                       | 137             | 151        | 882                      | 902              | 931         | 1468        | 1996                | 2000                   | 2184       | 2196                 | 2594                 | 3225        |

main

TimaKeyStoreProvider

MultiDex

FirebaseApp 10ms

com.google.android.gms.phenotype

GooglePlayServicesUtil

HSGDPR

AcbRemoteConfigUpdater

AcdAd

initRecommendRuleOnMainProcess

IAPManager

LibIAP

SmartLockerUtils

work

TimaKeyStoreProvider

MultiDex

SyncContentProvider

UserPresentPlacementProvider

HSGDPR

RemoteConfigProvider

NetworkSecurityConfig

LibAutopilot

GoldenEyeConfig

AcbAdsInit

CrashlyticsCore

PermanentUtils

NotificationToggleManager

AppUsageScan

AppLockThemeManager

MopubCommonUtils

FrameworkFlurryRecorder

CommonSessionUtils

FlurryAgent

chromium

clean

TimaKeyStoreProvider

MultiDex

HSGDPR

AcbHSFrameworkAdapter

AppsFlyerLib

Autopilot

Crashlytics

SecurityService

GooglePlayServicesUtil

UsageMonitorService

JunkService
