 Registered alarms are retained while the device is asleep (and can optionally
wake the device up if they go off during that time), but will be cleared if it
is turned off and rebooted.

睡眠时可以选择性地唤醒，关机之后不保存。

亲自测试：开启AlarmManager后再杀死进程(Force Stop)，到了时间也不会触发(Alarm
manager会被注销)；如果刚刚过了时间再重新唤起进程，也不会触发；唤起进程后又一次到了时间，会触发；如果是系统自己杀死进程的话到时间会自动触发

使用：

Intent intent =new Intent();

intent.setAction("xxxxx");

PendingIntentpendingIntent = PendingIntent.getBroadcast(this, 0,intent, 0);

AlarmManageram = (AlarmManager) getSystemService(ALARM_SERVICE); 

am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5 \* 1000,
pendingIntent);

首先Dalvik虚拟机会在SystemServer进程中创建一个叫做ServerThread的线程并调用它的initAndLoop方法，在initAndLoop方法中会创建主线程Looper和初始化各种Manager所对应的Binder服务，我们所常见的Binder服务如WindowManagerService、AlarmManagerService、PowerManagerService等均在这里创建并加入到ServiceManager中进行统一管理。而我们通过getSystemService方式来得到各种Manager的工作主要是在ContextImpl中完成的，不过LayoutInflater、WindowManager以及SearchManager除外。通过ContextImpl我们可以知道各种Manager和Binder服务的一一对应关系，比如AlarmManager对应AlarmManagerService、WindowManager对应WindowManagerService。

![](media/eeabf683c19c18fd84efcbe3535ed37b.png)

从Android4.4开始，Alarm默认为非精准模式，除非显示指定采用精准模式。在非精准模式下，Alarm是批量提醒的，每个alarm根据其触发时间和最大触发时间（1秒钟？1个月？）的不同会被加入到不同的batch中，同一个batch的不同alarm是同时发生的，这样就无法实现精准闹钟，官方的解释是批量处理可以减少设备被唤醒次数以及节约电量。同时所有的batch是按开始时间升序排列的，在一个batch内部，不同的闹钟也是按触发时间升序排列的，所以闹钟的唤醒顺序是按照batch的排序依次触发的，而同一个batch中的alarm是同时触发的，可以用下面这个示意图来描述：

![](media/ef1476fdf208addb441c3264f08d4923.jpg)

系统中可以有多个batch，每个batch中可以有多个alarm

当我们创建一个alarm的时候，仅仅是将这个alarm加入到某个batch中，系统中有一个batch列表，专门用于存储所有的alarm。可是仅仅把alarm加入到batch中还不行，系统还必须提供一个类似于Looper的东西一直去遍历这个列表，一旦它发现有些alarm的时间已经到达就要把它取出来去执行。事实上，AlarmManagerService中的确有一个类似于Looper的东西去干这个事情，只不过它是个线程，叫做AlarmThread。

AlarmThread会一直循环的跑着，一旦有新的alarm触发，它就会取出一个batch然后逐个发送PendingIntent，具体alarm的触发是由底层来完成的

Alarm和Timer以及Handler在定时任务上的区别
-----------------------------------------

**相同点**：

三者都可以完成定时任务，都支持一次性定时和循环定时（注：Handler可以间接支持循环定时任务）

**不同点**：

Handler和Timer在定时上是类似的，二者在系统休眠的情况下无法正常工作，定时任务不会按时触发。Alarm在系统休眠的情况下可以正常工作，并且还可以决定是否唤醒系统，同时Alarm在自身不启动的情况下仍能正常收到定时任务提醒，但是当系统重启或者应用被杀死的情况下，Alarm定时任务会被取消。另外，从Android4.4开始，Alarm事件默认采用非精准方式，即定时任务可能会有小范围的提前或延后，当然我们可以强制采用精准方式，而在此之前，Alarm事件都是精准方式。

Time对象调用一次schedule方法就是创建了一个线程，使用成本高，

Handler不需要新开启线程，占资源少。

<http://www.jianshu.com/p/4aaf6dda9b83?nomobile=yes>

<http://blog.csdn.net/singwhatiwanna/article/details/18448997>
