**一. Autopilot  概述**

-   互联网产品智能优化平台

-   用 AI 代替人去做互联网产品，比人做的更快更好

-   互联网产品的 AlphaGo

**二. Autopilot  原理**

    **2.1 系统架构**

![](media/5cb7edc06fc2da68a4d1ee2311ca4bf2.png)

** 2.2 Server **

![](media/df76d3517e9541ec6211df48b1d5f2b4.png)

    2.2.1. Audience 匹配

    2.2.2. 获取 Case 算法

    2.2.3. 各种报表算法

    2.2.4. 择优推送算法 

    ... 权限 / 日志 / 报警 ...

    **2.3 SDK **           

       **2.3.0     Prepare**

        Topic:              测试主题, 比如 测试一个按钮颜色 红色好还是绿色好?

        Topic.X:           待测因子, 比如 按钮颜色;  而 红色和绿色
是这个因子的两个 case

        Topic.Y:           测试结果, 比如 此按钮的点击率

        Topic Event:    测试主题中定义的一组 Event, 比如 btn_clicked /
btn_showed

        Topic Set:        主要用来分组 Audience

        AppEvent:        App 级别的事件, 比如 main_app_open  等, SDK
内部会自行触发;

                         PM 也可以自定义 AppEvent ,此时需要 dev 手动触发

    Topic 目前分以下三种,第三种 developing

     **1.  Life Time Topic**

         生命周期:  install && init Completed  -\>  uninstall   (Clear Data)

             在 init Completed 之前, life time topic 的 case 是不确定且无意义的

             在 init Completed 之后, case
就不会再发生变化,除非服务器找到最优解,并推送给 app

             life time topic 适合测试 留存 等一些全局指标

     **2.  One Time Topic**

         生命周期:  Session Start -\> Session End

             app 可认为 one time topic 的 case 每个 session 都是不同的,且 SDK
保证同一个 session 不会发生变化

             one time topic 适合测试 点击率/展示率 等局部指标

     **3.  One Day Topic(开发中 ...)**

         生命周期:  Day Start -\> Day End

   **2.3.1   两大模块**

      **1    AutopilotConfig**

                主要接口:  getX( );   

类似 HSConfig,提供 Autopilot Config 下载 / 查询 功能

注意: 对于 one Time topic  的 case:  App 可认为每次 Get 出来的值都不同

       ** 2    AutopilotEvent**

**   ** 主要接口: logX( xxx )

    类似 Flurry (HSAnalytics) ,  提供 Event 记录/上传 功能

**2.3.2 全局广播**

    1 \<action android:name="com.acb.autopilot.USER_INIT_COMPLETE" /\>

  代表用户初始化完成(即渠道信息准备完毕,能够比较准确识别用户群体)

                2 \<action
android:name="com.acb.autopilot.CONFIG_FETCH_FINISHED" /\>

      代表当前 Session 跟服务器沟通完毕, 沟通 成功/失败 都会发此广播

   **2.3.2   运行机制**

A   首次通信时机

-   等待 Audience 渠道信息返回后，再向服务器申请 Test Case 和上报 event

    -   第一个 Session 最大等待时间 9 s，若 9 s
        内渠道信息仍未返回，可以向服务器申请 Test Case 

    -   最多等待 3 个 session 

        B   上报策略

-   每个 event 都会触发上传, 但如果相邻的几个 event 在 200 ms 内,会合并 event
    统一上传

        C   下载策略

-    每个 session start 都会尝试去服务器下载

-    session 内下载成功，使用服务器返回的 Topic.x

    -    session 内下载失败，使用 Default 配置或 Last Topic.x

                    D   存储策略

-   event 发生后先存硬盘，只在以下情况清除：

    -   event 上报成功

    -   event 存储上限 (3000  条)

    -   event 条数超过上限，抛弃旧 event

    -   硬盘满，不会记入新event

![](media/78827845023f53377ed7a82b615b8cb6.png)

  **2.3.4 内置 Android 组件:**

**  **  默认所有组件在主进程, app 可以依据自己情况自定义进程

   1. AutopilotService

支撑 Autopilot 所在进程,保证 Autopilot 逻辑能够正确运行

   2. AutopilotProvider

封装 config / event 所有接口

   3. AutopilotReceiver

监控 session_start / session_end

**三. Autopilot 接口**

<http://git.ihandysoft.com/bingroup/bin_autopilot_android/wikis/home>

**四. Autopilot 集成**

   **4.1  Maven 引用示例：**

     // beta:

     compile "com.acb.autopilot:autopilot.beta:4.1.4.+"

     // gm:

compile "com.acb.autopilot:autopilot.gm:4.1.3.gm”

   **4.2 配置**

           ** 1. Json 配置文件 (跟 PM 要) 放在 Asset 目录**

               e.g.  Autopilot_1001_Config_20170526_204459.json

           ** 2. yaml 配置:**

               **debug:**

                    libAutoPilot:

                      ConfigFile: "Autopilot_1001_Config_20170526_204459.json"

                      Connection:

                        SigKey: "1"

                        HashKey: "x5UJ\~fb}3_Dma\>l B]YB/?'1As[\\"E\<I!"

               **release:**

                    libAutoPilot:

                      ConfigFile: "Autopilot_1001_Config_20170526_204459.json"

                      Connection:

                        SigKey: "1"

                        HashKey: "%LSz=X+W=?NLS!2z{\$]jlyXNdNqX(Ki5”

 **4.3     demo**

<http://git.ihandysoft.com/all-tech-server/service_appcloudbox_server/wikis/android_sdk_sample_code_page>

**附件: **

**{**

**    "autopilot_id": 1001,**

**    "autopilot_name": "PilotAndroidSDK",**

**    "get_url":
"https://dev-console.appcloudbox.net/autopilot/api/test/get";,**

**    "submit_url":
"https://dev-console.appcloudbox.net/autopilot/api/test/submit";,**

**    "topics": {**

**        "topic-1495697867776": {**

**            "topic_type": "life_time",**

**            "topic_name": "lifetime_topic_1",**

**            "variations": {**

**                "lifetime_topic_1.x1": {**

**                    "type": "string",**

**                    "value": "绿"**

**                },**

**                "lifetime_topic_1.x2": {**

**                    "type": "boolean",**

**                    "value": true**

**                },**

**                "lifetime_topic_1.x3": {**

**                    "checksum": "9540a05e76fa266d67c4725fd3979ffa",**

**                    "type": "pkg",**

**                    "value":
"http://cdn.crazz.cn/services/autopilot/static/1001/topic-1495697867776/admobad_alert_bg.png";**

**                },**

**                "lifetime_topic_1.x4": {**

**                    "type": "array",**

**                    "value": "0"**

**                }**

**            },**

**            "events": [**

**                {**

**                    "name": "test"**

**                },**

**                {**

**                    "name": "lifetime_topic_1.event1"**

**                },**

**                {**

**                    "name": "lifetime_topic_1.event2"**

**                },**

**                {**

**                    "name": "lifetime_topic_1.event3"**

**                }**

**            ],**

**            "topic_id": "topic-1495697867776"**

**        },**

**        "topic-1495698921194": {**

**            "topic_type": "one_time",**

**            "topic_name": "onetime_topic_2",**

**            "variations": {**

**                "onetime_topic_2.x1": {**

**                    "type": "string",**

**                    "value": "鱼"**

**                },**

**                "onetime_topic_2.x2": {**

**                    "type": "boolean",**

**                    "value": true**

**                }**

**            },**

**            "events": [**

**                {**

**                    "name": "test"**

**                },**

**                {**

**                    "name": "onetime_topic_2.event2"**

**                },**

**                {**

**                    "name": "onetime_topic_2.event1"**

**                }**

**            ],**

**            "topic_id": "topic-1495698921194"**

**        }**

**    },**

**    "app_events": [**

**        {**

**            "name": "main_app_open"**

**        },**

**        {**

**            "name": "main_app_close"**

**        },**

**        {**

**            "name": "extended_active"**

**        },**

**        {**

**            "name": "ad_show"**

**        },**

**        {**

**            "name": "ad_click"**

**        },**

**        {**

**            "name": "iap"**

**        },**

**        {**

**            "name": "test_customize_app_event1"**

**        },**

**        {**

**            "name": "test_customize_app_event2"**

**        },**

**        {**

**            "name": "test_customize_app_event3"**

**        }**

**    ]**

**}**

Demo:

*/\*\**

*\* SampleCode*

*\* Created by sharp on 2017年7月7日.*

*\*/*

public class SampleCode {

//

//        请按照下面指示粘贴对应的注释和代码到程序合适位置

        */\*\**

*         \*  使用 \@{topic_id} - \@{variation_name} 远程配置*

*         \*  ---------------------------------------------*

*         \*  Topic 名称:           \@{topic_name}*

*         \*  Topic 描述:           \@{topic_description}*

*         \*  Topic.x 可能值:       \@{variation_value}*

*         \*  Topic.x 描述:         \@{variation_description}*

*         \*/*

*       * String \@{variation_name}String =
AutopilotConfig.getStringToTestNow("\@{topic_id}", "\@{variation_name}",
"\@{variation_name_default_value}");

        // *TODO 请即刻使用
\@{variation_name}String，延迟使用可能会导致数据不准确*

*        /\*\**

*         \*  使用 \@{topic_id} - \@{variation_name} 进行测试*

*         \**

*         \*  Topic 名称:           \@{topic_name}*

*         \*  Topic ID:            \@{topic_id}*

*         \*  Topic 类型:          \@{topic_type}*

*         \*  Topic 描述:          \@{topic_description}*

*         \* -------------------------------------------------------*

*         \*  Topic.x:                      \@{variation_name}*

*         \*  Topic.x 类型:                 \@{variation_type}*

*         \*  Topic.x 可能值:               \@{variation_value}*

*         \*  Topic.x 描述:                 \@{variation_description}*

*         \*/*

*       * boolean valueBoolean =
AutopilotConfig.*getBooleanToTestNow*("\@{topic_id}", "\@{variation_name}",
\@{variation_name_default_value});

        // *TODO 请即刻使用 valueString，延迟使用可能会导致数据不准确*

*        /\*\**

*         \*  使用 \@{topic_id} - \@{variation_name} 进行测试*

*         \**

*         \*  Topic 名称:           \@{topic_name}*

*         \*  Topic ID:            \@{topic_id}*

*         \*  Topic 类型:          \@{topic_type}*

*         \*  Topic 描述:          \@{topic_description}*

*         \* -------------------------------------------------------*

*         \*  Topic.x:                      \@{variation_name}*

*         \*  Topic.x 类型:                 \@{variation_type}*

*         \*  Topic.x 可能值:               \@{variation_value}*

*         \*  Topic.x 描述:                 \@{variation_description}*

*         \*/*

*       * double valueDouble =
AutopilotConfig.*getDoubleToTestNow*("\@{topic_id}", "\@{variation_name}",
\@{variation_name_default_value});

        // *TODO 请即刻使用 valueString，延迟使用可能会导致数据不准确*

*        /\*\**

*         \*  上传 Topic 日志: \@{topic_id} - \@{event_name}*

*         \**

*         \*  Topic.Event 名称:           \@{event_name}*

*         \*  Topic.Event 描述:    \@{event_description}*

*         \*/*

*       * AutopilotEvent.logTopicEvent("\@{topic_id}", "\@{event_name}");

        */\*\**

*         \*  任意广告每展示一次,需要调用此接口上传日志*

*         \*/*

*       * AutopilotEvent.onAdShow();

        */\*\**

*         \*  任意广告每点击一次,需要调用此接口上传日志*

*         \*/*

*       * AutopilotEvent.onAdClick();

        */\*\**

*         \*  用户购买成功后,需要调用此接口,参数为美元价格*

*         \*/*

*       * AutopilotEvent.onIAP(3.99);

        // *TODO 3.99 只是个示例, dev 需要确保用产品的美元价格替代*

}
