Glide是一个快速高效的Android图片加载库，主要目的是让任何形式的图片列表的滚动能够更快更平滑，顺便支持了对图片的获取、缩放、显示的几乎所有需求。 

Glide is a fast and efficient image loading library for Android focused on
smooth scrolling.

    Glide的使用方法也十分smooth：

>   Glide.with(this).load(string).into(imageView);

其中：

-   *with()* 支持Context、Activity、FragmentActivity和Fragment。

-   *load()* 支持String、Uri、File、Integer、byte[]、T
    model类型，其中String类型可以是fila path、uri和url。

-   *into()* 支持ImageView、Y target、int, int三种类型。

Glide加载的原理：

![](media/daa5821d4702436754690014f3ad2aca.png)

    本文会对通过with、load、into三步来对 *Glide v3.7.0 *进行全面的分析。

第一步——with

>   Glide.with(this)

【*Glide对象*】

Glide有一个全局的*Glide对象*，通过*Glide.get(context)*可以获取，是咱们常见的单例模式：

![](media/2ca2c4166b4265afb3412ddd5fb76a59.png)

在第一次调用 *with *时也会隐式调用*Glide.get(context)*，由此触发*Glide对象*的创建。下面是第一次 *with *时会执行到的一条语句：

![](media/4301d3a3fdfcbeb174579b0b663b0df7.png)

【*GlideModule*】

 在创建*Glide对象*时，Glide会先查找Manifest的application标签下，meta-data中value=GlideModule的name值：

![](media/0d8afe96d59478338bb7d98892de7740.png)

通过反射找到这个实现了 *GlideModule *接口的类，调用 *applyOptions *方法和 *registerComponents *方法

![](media/70d4d4bd3a02e2049eba83a1197b61dd.png)

*(这里并没有用到builder，所以在第一次into(imageView)之前setTagId都是可以的)*

   
*applyOptions*方法中，*GlideBuilder*提供了*setBitmapPool*、*setMemoryCache*、*setDiskCache*、*setResizeService*、*setDiskCacheService*、*setDecodeFormat*
6个方法

-   *setBitmapPool*：设置Bitmap池。为了避免重复申请、回收Bitmap带来的开销，Glide采用BitmapPool来缓存创建过的Bitmap。使用过的Bitmap并不是直接回收，而是丢进池子里，下次接着用，当池子满、内存不足、手动清理时再回收Bitmap。

    -   Android Api11及之后默认是*LruBitmapPool*，采用Lru回收策略。

    -   复用策略：Api19(Android
        4.4)之前必须得是相同的宽、高、Config才能重复利用；Api19及以后，只要新的Bitmap中mbuffer大小不超过之前就可以复用。

    -   默认大小为“4个铺满屏幕的ARGB_8888的Bitmap”的大小，高内存手机不得超过进程最大内存的26.67%，低内存手机不得超过22%。具体参考MemorySizeCalculatar。如果你想改变池子的大小，可以使用以下设置：

![](media/5d5d8b682c356a1630c3e59bbc090be4.png)

-   可以通过下面的方式禁用：

![](media/0ae02fe436df2a35d8caec5daba7df9a.png)

-   *setMemoryCache*：设置内存缓存。内存缓存做为第一级缓存。第二级缓存为activeResource缓存，它握着资源的弱引用，如果之前加载过的资源还在其他地方引用着(active)，将直接拿过来使用。第一级第二级缓存直接在主线程加载。

    -   默认是*LruResourceCache*，采用Lru回收策略。

    -   默认大小为“2个铺满屏幕的ARGB_8888的Bitmap”的大小，高内存手机不得超过进程最大内存的13.33%，低内存手机不得超过11%。具体参考*MemorySizeCalculatar*。如果你想改变内存缓存的大小，可以使用以下设置：

![](media/d054f4cdfabb28178f09d54aab179a0c.png)

-   *setDiskCache*：设置磁盘缓存。磁盘缓存属于第三级缓存，优先级比新获取资源的优先级高，从磁盘缓存中获取资源以及新获取资源在子线程中执行。

    -   默认是*InternalCacheDiskCacheFactory*，缓存目录为App的内部缓存目录下的*image_manager_disk_cache*文件夹，策略为Lru。

    -   默认大小为250MB。如果想重载目录或者大小，可以参考*InternalCacheDiskCacheFactory*，创建一个类似的类，使用*setDiskCache*方法。

![](media/baa1f2c6ee5946641296f9384abc24c7.png)

-   *setResizeService* (估计是 setSourceService
    手抖打错了)：设置获取资源用的线程池（*sourceService*），通俗地来说Glide就是在
    *sourceService* 线程池中获取图片的。

    -   默认为先进先出策略(*FifoPriorityThreadPoolExecutor*)。

    -   默认线程池大小为
        Runtime.getRuntime().availableProcessors()。可以通过下面的方法更改池子的大小：

![](media/f6f9ab2a9f8dd1fd1eee542a280d7c1e.png)

-   *setDiskCacheService*：设置读取磁盘缓存用的线程池。所有请求都会走这个线程池中过一遍，也就是说，如果对磁盘缓存的依赖很高的话，可以尝试将这个池子扩大。

    -   默认为先进先出策略(*FifoPriorityThreadPoolExecutor*)。

    -   默认线程池大小为1。可以通过下面的方法更改池子的大小：

![](media/b3e271d90c199c972ff84414b3bda06d.png)

-   *setDecodeFormat*： *PREFER_ARGB_8888*和*PREFER_RGB_565*两种，Glide
    3.x默认是*PREFER_ARGB_565*——如果图片没有透明度，将采用*Bitmap.Config.RGB_565*进行解码，有透明度的图片采用*Bitmap.Config.RGB_8888*。由于*Bitmap.Config.RGB_565*解码出来的图片画质会低一些，如果整个App对画质都比较敏感，可以使用以下设置：

![](media/e81ab7eb13b7da8fa0c25ca490332a43.png)

*注：为了解除PREFER_ARGB_8888的歧义，下面是Glide在解码Bitmap时使用的Config的代码，也可以作为咱们App的参考：*

![](media/4ef7bf6a8a84d1bbd0917820049e4985.png)

以上便是*applyOptions*中可以设置的参数，这些参数都是App全局的，具体细节将在*load*章节进行更详细的介绍。

   
*registerComponents*方法中，可以调用glide对象的*register*、*setMemoryCategory*等方法。

-   *register*：由于Glide V3 版本对这部分的支持不是很完善，这里不做介绍。

-   *setMemoryCategory*：对*BitmapPool*、*MemoryCache*的大小的进行修改，有*HIGH*(1.5倍)、*NORMAL*(不变)、*LOW*(0.5倍)三种。下面这个例子会将*BitmapPool*和*MemoryCache*的大小两个都乘以1.5倍。

![](media/18f00166bea49efea8346da5855d6a31.png)

   
对于*Glide对象*，可以通过*clearMemory()*清理*BitmapPool*和*MemoryCache*，可以通过*clearDiskCache()*清理*DiskCache*。

【*RequestManager*】

上面提到过：*with()*操作支持Context、Activity、FragmentActivity、Fragment参数。如果参数类型为Activity、FragmentActivity或者Fragment，Glide会通过添加空白fragment的方式监听它的的生命周期(onStart、onStop、onDestroy)，以此来控制加载的继续、暂停和中止。如果参数给的是Context类型，Glide会递归调用getBaseContext，如果找到Activity、FragmentActivity或Fragment，则监听它的生命周期，否则Glide将不监听其生命周期。

也就是说，你的Glide请求

>   Glide.with(xxx).load(filePath).into(imageView);

会跟随 *xxx* 的生命周期，无论你传入的是Application还是Activity还是Fragment。

*注：主线程调用with时，如果Api大于19并且所属Activity的isDestroyed返回true，Glide会抛出crash。而且，在子线程调用with时，不会监听生命周期。*

   *with()*获取*RequestManager*的流程图如下：

![](media/7335e8ff7dd2d677b3cfed40eaed5125.png)

第二步——load

>   Glide.with(this).load(xxx)

【*GenericRequestBuilder*】

 
 从上面我们可以知道 *with() *的结果是*RequestManager*，*RequestManager*会根据不同的接口生成各种不同的 *GenericRequestBuilder*，*GenericRequestBuilder*保存了一次请求需要的所有信息。*RequestManager*中最常用的接口就是*load*，生成*GenericRequestBuilder的*子类*DrawableTypeRequest*。

为了支持各式各样的请求类型，Glide定义了4个*类型*（*ClassType*）：

![](media/468ec32c76eb84e0e4efa77582309227.png)

-   *ModelType*：表示资源的模型的类型。*load*
    传入的参数类型就是 *ModelType*，例如 *Glide.with(this).load(filePath)*
    中，*ModelType* 为 *String*。

-   *DataType*：将模型进行处理得到的数据，例如 *Glide.with(this).load(filePath).asGif()*
    中，*DataType *为 *InputStream*，对模型进行处理 = 对文件进行读取。

-   *ResourceType*：对数据进行解码得到的类型，例如 *Glide.with(this).load(filePath).asGif()*
    中，*ResourceType* 为 *GifDrawable*，对数据进行解码 =
    将输入流解码成 *GifDrawable* 类型。

-   TranscodeType：对解码后的数据进行转码得到的类型，例如 *Glide.with(this).load(filePath).asGif()*
    中，*TranscodeType* 为 *GifDrawable*，这里的转码是从 *GifDrawable*
    转码到 *GifDrawable*。

上面4个*类型*之间的关系可以用下面这张图来描述：

![](media/9382ea31a38c99fa8c1bfa2b769ae939.png)

    实际上，执行 *Glide.with(this).load(String)*
之后*Glide*已经帮我们确定了*ModelType*、*DataType*、*ResourceType*、*TranscodeType*，并且还帮我们实现了默认的*ModelLoader*、*ResourceDecoder *和
*ResourceTranscoder*，还包括与磁盘缓存相关的*Encoder*和*Decoder*。如果不加
*asGif*、*asBitmap*
等限制，Glide将使用兼容模式，根据内置的优先级尝试不同的方案，整体流程如下：

![](media/21c86b207f0ffe084dd1f734f7605be6.png)

    其中：

-   *ImageViewModelLoader\<String,
    ImageViewWrapper\>*：模型加载器，包装了*StreamStringLoader*和*FileDescriptorStringLoader*，会通过输入的*String*来判断是文件名、文件的Uri、CP的Uri、资源的Uri或者是网络的Url，然后同时尝试通过*InputStream *(支持普通的图片和Gif)
    和 *ParcelFileDescriptor *(支持视频的某一帧) 来打开文件或者链接。

注：Glide中大量使用了*包装*(*wrapper*)设计模式。

-   *ImageVideoWrapper*：为了兼容普通的图片、Gif和视频，包装了 *InputStream *和 *ParcelFileDescriptor *的结构，二者可以同时不为*null*。

-   *GifBitmapWrapperResourceDecoder\<ImageVideoWrapper,
    GifBitmapWrapper\>*：解码器，先判断是不是Gif类型，如果是Gif类型，尝试解码Gif，如果解码成功，生成*GifDrawable*（继承自GlideDrawable）；如果失败则尝试对 *InputStream *通过下采样的方式进行解码，解码依赖 *BitmapFactory *类，如果解码成功，生成Bitmap；否则使用*ParcelFileDescriptor *依赖系统的 *MediaMetadataRetriever*
    获取视频第一帧。

*如果你想改帧数改为第12帧，你可以这样写，这样写可能会导致大量的新对象，不过目的只是为了节目效果。*

![](media/8292c3fad29335c1dd2326c75a703963.png)

*如果将图中的12改成0，就等同于 Glide 默认的
Decoder，可以作为参考。由此可见Glide的兼容性带来了代码极差的可读性。*

-   GifBitmapWrapper：封装了GifDrawable和Bitmap的类，二者有且仅有一个不为null。

-   Transformation\<GifBitmapWrapper\>：转换器，对decoder解码出来的结果进行转换（与transcode不同），比如CenterCrop、FitCenter。

-   GifBitmapWrapperDrawableTranscoder\<GifBitmapWrapper,
    GlideDrawable\>：如果GifBitmapWrapper中不为null的是GifDrawable，直接返回，如果是Bitmap，生成一个GlideBitmapDrawable（继承自GlideDrawable）并返回。

-   GlideDrawableImageViewTarget：将Transcode后的GlideDrawable通过setImageDrawable()的加载到ImageView上，并执行动画。

-   DrawableTypeRequest\<String\>：把虚线框内逻辑封装起来的一个*GenericRequestBuilder*子类。下面这两句话是等价的：

![](media/fab033b97422131edbc849dbbdd35122.png)

*=*

![](media/26725311cdd0cc949227baa06c42a609.png)

*DrawableTypeRequest只提供ModelType的自定义，如果要自定义其他的类型，只能乖乖地向上面那样写。（Glide
v4版本简化了这种使用方式，不过核心思想还是一致的）*

可以看出，Glide的 *load*
操作并没有发起请求，而只是同步创建了一个 *GenericRequestBuilder*，描述了本次请求是怎么构成的，真正的请求是在 *into*
操作中发起的。

第三步——into

>   Glide.with(this).load(filePath).into(imageView);

【*Target*和*Request*】

    上面说到了Glide图片请求由
*ModelType-\>DataType\>ResourceType-\>TranscodeType*
4个阶段构建而成，但最终并没有将转码过后的 *GlideDrawable* 类型应用到 *ImageView*
上。因为真正应用的语句为 into(imageView)。

    在执行*GenericRequestBuilder的into*操作时，如果参数类型为
ImageView，Glide会根据*TranscodeType*帮我们生成*GlideDrawableImageViewTarget*、*BitmapImageViewTarget*或*DrawableImageViewTarget*：

![](media/8f8bce8ad62210e8c1cf8668aac827be.png)

![](media/ecfa9b9a0371fb7dcc7bb4add7ed5704.png)

    而最终是生成一个*Request*类型传给*Target*进行绑定：

![](media/b09dc553ff4dd36fb8961d5362913653.png)

    从代码中可以发现，Glide会先取出 *Target *之前的 *Request *并
*cancel* 它，这里可以转到*ViewTarget *中的对应方法（View相关的Target的抽象类）：

![](media/879c9c0f9255c11fd0cb7932334f8523.png)

*这便是我们使用Glide时为什么需要设置 tagId
的原因，如果不设置，View默认的tag将被Glide使用*

    在绑定完成后，执行
*requestTracker.runRequest(request)*，*requestTracker*是创建*GenericRequestBuilder*时由*RequestManager*传入的，它的作用主要是控制请求的声明周期：

![](media/7578653611cb723ebf6761cd566092d2.png)

   
上面代码中，如果不是*pause*状态，将调用*request.begin()*（这种情况下是同步的），否则加入*pendingRequests*，在下次*resume*时调用*begin*方法。*Request*是一个接口，Glide中有两个*Request*的实现类，一个是一般的请求*GenericRequest*，一个是为了避免缩略图请求和正常请求冲突问题的*ThumbnailRequestCoordinator*（缩略图协调器，这里不详细介绍了，简单来说：它的作用就是如果原图已经加载好了，那么缩略图的请求就会被直接*clear*）。

*GenericRequest*的*begin*方法实现是个抽象代码在这里：

![](media/aa1d4846b5dde6607a03888233534d4d.png)

   
可以发现：在构建*Request*时如果*ImageView*的已经有宽、高，那么执行到*begin*的时候，直接自己回调自己的*onSizeReady*方法，然后判断是不是应该显示*placeholderDrawable*，否则调用*target.getSize(this)*等待*target*回调*onSizeReady*，然后判断是不是应该显示*placeholderDrawable。*对于*target*为*ViewTarget*的情况，是当View布局结束后、准备绘制前由*ViewTarget*回调一次*onSizeReady*。

下面我们进入*onSizeReady*方法：

![](media/ce26143618b38b437400803097f4a117.png)

    其实没有什么逻辑，取了些参数，打了些log，核心只有一句
*engine.load*。*engine*我们等一会儿再解释，咱们回到begin方法

【*engine*】

这里又多了一个概念——*engine*，*engine*作为Glide加载的核心驱动，前面提到的ModelLoader、Decoder、Encoder、Transformation、Transcoder、内存缓存、磁盘缓存全在*engine*中实现的，*engine.load*方法：

![](media/ea476975bf6b53a54c9ee34009ca076d.png)

    *load*方法分为下面几步：

1.  首先计算*EngineKey*，其实就是对上面代码中*buildKey*方法传入的参数的封装。这个key会用于内存缓存和磁盘缓存，由于内存缓存和磁盘缓存还是有区别的，所以它们对key各中参数的使用也有一定的区别。

2.  然后获取一级缓存——内存缓存，也就是我们设置的*MemoryCache*，文章后面我们会知道*MemoryCache*缓存的是*Transcode*之后准备载入*Target*时的最终结果，如果命中，直接回调*Target*的*onResourceReady*
    (*GlideDrawableImageViewTarget*将调用*ImageView.setImageDrawable*）。内存缓存的key值由*ModelLoader*返回的*DataFetcher*的*id*（对应代码中的*”id”*）、*签名*、*Target的宽*、*Target的高*、*CacheDecoder*、*SourceDecoder*、*Transformation*、*Transcoder*，*sourceEncoder*的*getId*返回的String拼接而成。

3.  如果一级缓存未命中，将使用二级缓存——活跃资源缓存，也就是代码中的*loadFromActiveResources(key,
    isMemoryCacheable)*。在加载时，如果*MemoryCache*命中，会直接从*MemoryCache*中取出作为活跃资源，使用弱引用将这部分活跃资源缓存下来，因此可以放心地清理*MemoryCache*，不用考虑因正在使用*MemoryCache*而产生的问题。

4.  如果都没有命中，那么会查找，如果存在和当前*key*一样的*engineJob*，挂载到它上面，否则将新起一个*engineJob。*

5.  engineJob.start(runnable)的代码如下：  
    

    ![](media/1226662cb38fceac36daf741079595c0.png)

    >     
    >   这里需要注意的是——无论你有没有使用磁盘缓存，Glide都会走*diskCacheService*线程池里过一遍。

6.  *engineRunnable*的*run*方法代码如下：  
    

    ![](media/fdaa89676e9d92b532407df2f7281726.png)

    >     
    >   核心代码是*decode()*  
    >   

    ![](media/dcc03776ec123e51e258b15e3fc556f8.png)

    >     
    >   其中：*isDecodingFromCache()*就是*engineRunnable*的默认状态，也就是说，*engineRunnable*无论如何都会去尝试磁盘缓存。

7.  我们接着来看decodeFromCache方法：  
    

    ![](media/02837e5fe038bafc3dce2197248fbed0.png)

    >     
    >   磁盘缓存分*Result*（对应*ResourceType* *Transform*之后的结果，例如将*Bitmap*进行*centerCrop*之后的结果）和*Source*（对应*DataType，*例如*InputStream*）两类，默认是*Result*，在读缓存时它的优先级也会高一些。如果缓存关闭了，那么将直接返回空，但是仍然要从*diskCacheService*线程池里跑一遍（不知道后来有没有优化）。

8.  磁盘缓存如果命中，则*Transcode*之后执行*onLoadComplete，*最终会通过*engineJob*的static变量*MAIN_THREAD_HANDLER*回调的*onResourceReady*，把结果传给*Target；*如果未命中，会回调*onLoadFailed*，此时的*onLoadFailed*只是磁盘缓存命中失败，它会把状态*isDecodingFromCache()*改为*false*，然后再来一次...  
    

    ![](media/43c43d7aa6e56f8fa52175f952ea4f75.png)

9.  再来一次还是走上面的*run*方法，只不过这时候*isDecodingFromCache()*为*false*。接下来走*decodeFromSource()*

10. 然后就是漫长的*ModelType*-\>*DataType*-\>*ResourceType*-\>*TranscodeType*的过程。期间伴随写入磁盘缓存（*DataType*写入*Source*缓存，*ResourceType* *Transform*之后写入*Result*缓存），结束之后回调*onResourceReady*，然后保存内存缓存、弱引用资源缓存。

11. *Target*拿着*onResourceReady*回调回来的*TranscodeType*(*GlideDrawable*)、还有*Animate*对象(*GlideAnimate，*跟着*builder*一路传了过来)，该干嘛干嘛。（比如*GlideDrawableImageViewTarget*就对“长宽比在0.95～1.05之间”的*ImageView*做了优化，避免了由加载缩略图引起的重新布局问题）

【*Glide动画*】

   
Glide动画在*Request*回调*onResourceReady*时执行，此时Glide会判断是不是从内存缓存或者资源缓存获得到了结果：

-   如果是：

    -   不执行动画

-   如果不是：

    -   在使用*DrawableTypeRequest*，也就是在调用 *Glide.with(this).load(filaPath).into(imageView)* 的情况下，Glide会判断当前的请求中有没有加载完成过缩略图：

        -   如果有加载完成过缩略图：通过TransitionDrawable实现前后两张Drawable的过渡动画

        -   如果没有加载完成过缩略图：执行默认的300ms淡出动画

    -   如果是自定义的*GenericRequestBuilder*，Glide也会判断当前的请求中有没有加载完成过缩略图：

        -   如果有加载完成过缩略图：不执行动画

        -   如果没有加载完成过缩略图：将执行设置的Animator，默认为空。（如果是这种情况，需要在Animator中自行cancel掉上一次的动画）

对于咱们的需求，设置一个300ms的淡出Animator已经足够了。

以上就是Glide加载图片的全部过程。

我们的需求是用Glide实现对App的Icon、对Apk文件的Icon进行加载，我们希望输入一个String再输入一个ImageView就能实现，于是应该自定义：

-   ModelType：String；

-   DataType：String；

-   ResourceType：Drawable；

-   TranscodeType：Drawable；

-   ModelType-\>DataType：不需要做任何处理，只是*getId*需要返回String的内容，不然*EngineKey*全都一样了。

-   DataType-\>ResourceType：调接口去拿Drawable，如果拿到是BitmapDrawable并且很大，那么需要将它进行裁剪以节省内存，其他的就算了，很少见（Android
    8.0 才会有除BitmapDrawable以外需要内存大小的Drawable）；

-   Transformation：默认没有；

-   ResourceType-\>TranscodeType：什么都不干就行，Glide默认有一个*UnitTranscoder*会将*ResourceType*直接变成*TranscodeType*；

-   MemoryCache：用默认的；

-   Animate：使用300ms淡出的AlphaAnimator()；

-   DiskCache：不用；

所以我封装了一个方法：

![](media/1fa3f2435ba9c4662c9fedea7b751de9.png)

和Glide一个用法：

>   AppIconGlide.with(this).load(packageName\|ApkFilePath).into(imageView);

如果需要去掉动画，使用：

>   AppIconGlide.with(this).load(packageName\|ApkFilePath).dontAnimate().into(imageView);

获取不到Drawable时默认使用mipmap中的app_icon_default，如果加载失败时需要显示咱们App的Icon，使用：（注意：低版本的error传入SVG的id会crash，可以传入SVG的drawable）

AppIconGlide.with(this).load(packageName\|ApkFilePath).error(R.mipmap.ic_launcher).into(imageView);
