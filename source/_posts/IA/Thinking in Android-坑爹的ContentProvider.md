一、什么是ContentProvider？

![](media/64c8618d09ad44f5023640ebd5c92ddf.png)

二、为什么要使用ContentProvider？

![](media/f917ad5c6aee967ff4b4c7bc119fceff.png)

三、ContentProvider原理

[http://gityuan.com/2016/07/30/content-provider](http://gityuan.com/2016/07/30/content-provider/)

<http://blog.csdn.net/Luoshengyang/article/details/6946067>

四、我们所遇到的问题

ContentProvider重复创建，导致内存泄漏！

\<此处有一个ActivityManagerService.java文件，请到Thinking in Android ——
坑爹的ContentProvider.resources文件中查看\>

\<此处有一个ActivityThread 5.0.java文件，请到Thinking in Android ——
坑爹的ContentProvider.resources文件中查看\>

\<此处有一个ActivityThread.java文件，请到Thinking in Android ——
坑爹的ContentProvider.resources文件中查看\>

![](media/6a9e54957120071a93d128a7f2fed3d5.jpg)

五、结论：

在Android4.0.4及之前的版本，存在着这样的缺陷，在一个CP的onCreate中对其他CP进行依赖，且在同一个进程中会造成CP重复创建，且会破坏CP在一个应用中原有的生命周期，再次请求会重复创建CP，又由于引用不能去除，导致了内存泄漏，在高版本中尽管不会破坏CP在应用中原有生命周期，但是还是会创建两次CP。
