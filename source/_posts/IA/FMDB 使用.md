目前情况：使用DB —\> 选用Sqlite —\> 选用FMDB封装库

为什么不直接用sqlite？

Sqlite直接操作DB文件能执行sql语句，但是用一个DB不仅仅能执行即可，还要准确安全可恢复的执行sql语句。在这方面FMDB支持这些特性。

FMDB支持哪些特性？

1.  对多线程的并发操作进行了处理，线程安全，核心是FMDatabaseQueue

2.  OC方式封装SQLite API，使用方便

3.  轻量级框架

几个重要类：

1.  FMDatabase，代表一个单独的SQLite数据库，用来执行sql语句，线程不安全

2.  FMResultSet，使用FMDatabase执行查询后的结果集

3.  FMDatabaseQueue，用于在多线程中执行多个查询或更新，线程安全

提供上面这些方法后还是有点懵，因为不知道怎么搭建

然后龙哥写了一个FMDatabaseHelper，负责搭建，供我们更方便使用，如下

![](media/07da27ae2a5245becba500305ba4d346.png)

createTable和upgrade需要被复写

前期有几个点我们没有考虑，所以上面这个需要补充和修改，如下几点：

1.  FMDatabaseQueue要集成进来，因为要线程安全

2.  这种模式限定了继承，一般情况都是DBMgr用，一个Mgr继承一个Helper有点怪，建议改成包含方式

3.  VersionKey感觉怪，自己维护的DB需要其他地方告诉我的DB
    version是什么，建议helper内部处理

提议如下：

![](media/8ad950117e6cae31ecb3e63ccd64335e.png)

其他，MessageFilterExtension中FMDB库的导入方式可以改了
