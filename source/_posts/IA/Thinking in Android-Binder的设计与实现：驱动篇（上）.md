![](media/e3690fc85ba6b58234f182e89578cc00.jpg)

本次分享将围绕以下几个话题进行：

1、驱动程序再探

2、Binder驱动程序在Binder架构中的位置

3、Binder驱动程序中的数据结构（本讲会用到的）

4、Binder驱动程序的初始化过程

5、Binder驱动程序的打开过程

6、Binder驱动程序的内存映射与管理过程

1、驱动程序再探

前文Binder背景篇中我们已经知道进程之间是相互隔离的，进程之间不能够直接访问彼此的内存，如果进程之间想要相互访问数据就需要借助一些跨进程通讯手段了，我们知道的有Socket、共享内存以及在Android操作系统上的广泛应用的Binder。跨进程通讯手段的共性在于都是需要通过各个进程所共享的内核地址空间的内存来进行的。

![](media/42ef25c3f5dd32b245777a4fc05ac397.png)

(32位操作系统)
每个进程都有4G的虚拟地址空间，其中3G用户空间，1G内核空间，每个进程共享内核空间，拥有独立的用户空间

但是当进程运行在用户态时内核地址空间的内存是不能够被访问的，进程需要陷入到内核态后，才可以对内核资源进行访问。进程对内核空间的资源进行访问我们在上文也说过了，唯一合法的方式就是系统调用，每个系统调用对应了一个方法，例如open、mmap和ioctl等。我们需要通过传递给系统调用处理程序一个系统调用号来找到想要执行的系统调用。系统调用处理程序是需要陷入到内核态才能够执行，而陷入到内核态的方式就是触发一个异常（软中断，可以认为是特定的指令），然后再去执行具体的系统调用。

![](media/93d6b6462c4a51cc2b4e106a00326305.png)

系统调用过程

   
通过上次的分享我们也已经知道了驱动程序主要是用来操作硬件设备的，例如我们想要使用键盘，那么在内核中就一定对应了一个键盘的驱动程序来处理我们敲击键盘这件事。驱动程序是可动态加载的，这样无论我们想要使用什么样的硬件外设，只要厂商提供了驱动程序，我们就都可以去轻松的使用了。但是驱动程序同样运行在内核空间，硬件设备出于安全考虑，也只有在CPU处在内核态时才能够访问。因此用户程序想要使用驱动程序，同样要使用系统调用。我们知道在Linux的世界中“一切皆文件”，这一切都是通过虚拟文件系统来实现的（同内存管理模块，调度程序处在同一层级，是内核的重要组成），通过虚拟文件系统，用户程序访问驱动程序同操作普通的文件并没有什么区别，想要访问驱动程序，只需要同操作一个文件一样调用open函数，拿到文件描述符，然后通过各种系统调用函数，就可以同驱动程序进行通讯了。下面是一段系统调用函数，最终会调用到驱动程序所对应的函数。

>   asmlinkage ssize_t sys_write(unsigned int fd, const char \* buf, size_t
>   count)  

>   {  

>       ssize_t ret;  

>       struct file \* file;  

>       struct inode \* inode;  

>       ssize_t (\*write)(struct file \*, const char \*, size_t, loff_t \*); /\*
>   指向驱动程序中的wirte函数的指针\*/

>       lock_kernel();

>       ret = -EBADF;  

>       file = fget(fd); /\* 通过文件描述符得到文件指针 \*/

>       if (!file)  

>           goto bad_file;

>       if (!(file-\>f_mode & FMODE_WRITE))  

>           goto out;

>       inode = file-\>f_dentry-\>d_inode; /\* 得到inode信息 \*/  

>       ret = locks_verify_area(FLOCK_VERIFY_WRITE, inode, file, file-\>f_pos,
>   count);  

>       if (ret)  

>           goto out;  

>       ret = -EINVAL;  

>       if (!file-\>f_op \|\| !(write = file-\>f_op-\>write)) /\*
>   f_op为结构体file_operations，在上一讲我们介绍过，这是一个包含很多函数指针的结构体，驱动程序在初始化时候会对这个结构体进行初始化，假设这是一个屏幕的驱动，那么write函数定义的就可能是在屏幕上显示文字，如果是一个音响的驱动，那么write函数定义的就可能是在发出特定的声音，后面binder驱动程序初始化我们会再详细介绍 \*/

>           goto out;  

>       down(\&inode-\>i_sem);  

>       ret = write(file, buf, count, \&file-\>f_pos); /\*
>   使用驱动程序中的write函数将数据输入设备 \*/

>       up(\&inode-\>i_sem);  

>   out:  

>       fput(file);

>       bad_file:

>       unlock_kernel();

>       return ret;

>   }

![](media/f4242a81cb5f7264ded25de1252221e9.png)

驱动程序访问流程

2、Binder驱动程序在Binder架构中的位置

设备驱动通常分为字符设备（如键盘驱动）和块设备（如硬盘驱动）和网络设备三种，misc设备驱动程序属于字符设备驱动的一种，正如其名，misc设备驱动比较特殊的一类字符设备驱动，这一类设备驱动程序有着统一的主设备号（驱动程序的唯一性由主从设备号来决定）。Binder驱动程序是比较特殊的驱动程序，它并没有对应任何的硬件设备，它利用了自身处在内核空间可以访问内核资源的特性，来为用户进程提供跨进程通讯服务。从之前的内容我们已经可以了解到，跨进程通讯最重要的就是如何突破进程间隔离的屏障，同时又要确保数据传输的准确性、稳定性以及传输效率，在Binder进程间通讯架构中，这些核心任务最终都是通过Binder驱动程序来实现的，所以Binder驱动程序在在整个架构中至关重要，Binder驱动程序建立起了最终的通讯桥梁。为了用户程序方便、透明的使用Binder驱动程序，Android的操作系统开发者提供了在Native层和Java层的各种Binder概念，以及ServiceManager，但最终实现跨进程的还是要依靠Binder驱动程序，所以理解Binder驱动程序对于我们理解Binder是至关重要的。下面这张图可以反映出Binder架构的各个层次：

![](media/2067f3f3081b3136b08ed29ca1c759d8.jpg)

Binder架构的各个层次

3、Binder驱动程序中的主要数据结构

    Binder驱动程序实现的主要文件：

\<此处有一个binder.c文件，请到Thinking in Android ——
Binder的设计与实现：Binder驱动篇（上）.resources文件中查看\>

\<此处有一个binder.h文件，请到Thinking in Android ——
Binder的设计与实现：Binder驱动篇（上）.resources文件中查看\>

\<此处有一个binder_1.h文件，请到Thinking in Android ——
Binder的设计与实现：Binder驱动篇（上）.resources文件中查看\>

    本讲涉及到的Binder驱动程序中主要结构体列表如下：

| 序号 | 结构体           | 名称           | 解释                                                                        |
|------|------------------|----------------|-----------------------------------------------------------------------------|
| 1    | binder_proc      | binder进程     | 每个进程调用open()打开binder驱动都会创建该结构体，用于管理IPC所需的各种信息 |
| 2    | binder_node      | binder实体     | 对应于BBinder对象，记录BBinder的进程、指针、引用计数等                      |
| 3    | binder_ref       | binder引用     | 对应于BpBinder对象，记录BpBinder的引用计数、死亡通知、BBinder指针等         |
| 4    | binder_ref_death | binder死亡引用 | 记录binder死亡的引用信息                                                    |
| 5    | binder_buffer    | binder内存     | 调用mmap()创建用于Binder传输数据的缓存区                                    |

Binder驱动程序中主要用到的结构体说明如下：

(1)、binder_proc

结构体binder_proc用来描述一个正在使用Binder进程间通信机制的进程。当一个进程调用函数open来打开设备文件/dev/binder时，Binder驱动程序就会为它创建一个binder_proc结构体，并且将它保存在一个全局的hash列表中，而成员变量proc_node就正好是该hash列表中的一个节点。

>   struct binder_proc {

>       struct hlist_node proc_node;

>       struct rb_root threads;

>       struct rb_root nodes;

>       struct rb_root refs_by_desc;

>       struct rb_root refs_by_node;

>       int pid;

>       struct vm_area_struct \*vma;

>       struct mm_struct \*vma_vm_mm;

>       struct task_struct \*tsk;

>       struct files_struct \*files;

>       struct hlist_node deferred_work_node;

>       int deferred_work;

>       void \*buffer;

>       ptrdiff_t user_buffer_offset;

>       struct list_head buffers;

>       struct rb_root free_buffers;

>       struct rb_root allocated_buffers;

>       size_t free_async_space;

>       struct page \*\*pages;

>       size_t buffer_size;

>       uint32_t buffer_free;

>       struct list_head todo;

>       wait_queue_head_t wait;

>       struct binder_stats stats;

>       struct list_head delivered_death;

>       int max_threads;

>       int requested_threads;

>       int requested_threads_started;

>       int ready_threads;

>       long default_priority;

>       struct dentry \*debugfs_entry;

>   };

![](media/05c6c15da6d87ff068c8770f8bcfaa37.png)

内核中的全局hash表binder_procs描述了所有使用Binder驱动程序的进程

(2)、binder_node

   
结构体binder_node用来描述一个Binder实体对象。每一个Service组件在Binder驱动程序中都对应有一个Binder实体对象，用来描述它在内核中的状态。

>   struct binder_node {

>       int debug_id;

>       struct binder_work work;

>       union {

>           struct rb_node rb_node;

>           struct hlist_node dead_node;

>       };

>       struct binder_proc \*proc;

>       struct hlist_head refs;

>       int internal_strong_refs;

>       int local_weak_refs;

>       int local_strong_refs;

>       binder_uintptr_t ptr;

>       binder_uintptr_t cookie;

>       unsigned has_strong_ref:1;

>       unsigned pending_strong_ref:1;

>       unsigned has_weak_ref:1;

>       unsigned pending_weak_ref:1;

>       unsigned has_async_transaction:1;

>       unsigned accept_fds:1;

>       unsigned min_priority:8;

>       struct list_head async_todo;

>   };

(3)、binder_ref

结构体binder_ref用来描述一个Binder引用对象。每一个Client组件在Binder驱动程序中都对应有一个Binder引用对象，用来描述它在内核中的状态。

>   struct binder_ref {

>       /\* Lookups needed: \*/

>       /\*   node + proc =\> ref (transaction) \*/

>       /\*   desc + proc =\> ref (transaction, inc/dec ref) \*/

>       /\*   node =\> refs + procs (proc exit) \*/

>       int debug_id;

>       struct rb_node rb_node_desc;

>       struct rb_node rb_node_node;

>       struct hlist_node node_entry;

>       struct binder_proc \*proc;

>       struct binder_node \*node;

>       uint32_t desc;

>       int strong;

>       int weak;

>       struct binder_ref_death \*death;

>   };

介绍完上面的概念我们能得到下面这样一张关系图：

![](media/0c8ec5583b5e24ea530f9fbbd295176e.png)

binder_proc结构体是进程使用Binder驱动程序在内核中的一个描述

binder_node和binder_ref我们可以在如下这张图中找到其位置，大家能找到他们吗？

![](media/53d830ba682809ca13f6a4b52202f4aa.jpg)

Binder中的Binder引用和Binder实体

(4)、binder_ref_death

结构体binder_ref_death用来描述一个Service组件的死亡接收通知。在正常情况下，一个Service组件被其他Client进程引用时，它是不可以销毁的。然而，Client进程是无法控制它所引用的Service组件的生命周期的，因为Service组件所在的进程可能会意外地崩溃，从而导致它意外地死亡。一个折中的处理办法是，Client进程能够在它所引用的Service组件死亡时获得通知，以便可以做出相应的处理。这时候Client进程就需要将一个用来接收死亡通知的对象的地址注册到Binder驱动程序中。

成员变量cookie用来保存负责接收死亡通知的对象的地址，成员变量work的取值为BINDER_WORK_DEAD_BINDER、BINDER_WORK_CLEAR_DEATH_NOTIFICATION或者BINDER_WORK_DEAD_BINDER_AND_CLEAR，用来标志一个具体的死亡通知类型。

>   struct binder_ref_death {

>       struct binder_work work;

>       binder_uintptr_t cookie; //指向用来接收死亡通知的对象的地址

>   };

(5)、binder_buffer

结构体binder_buffer用来描述一个内核缓冲区，它是用来在进程间传输数据的。每一个使用Binder进程间通信机制的进程在Binder驱动程序中都有一个内核缓冲区列表，用来保存Binder驱动程序为它所分配的内核缓冲区，而成员变量entry正好是这个内核缓冲区列表的一个节点。同时，进程又使用了两个红黑树来分别保存那些正在使用的内核缓冲区，以及空闲的内核缓冲区。如果一个内核缓冲区是空闲的，即它的成员变量free的值等于1，那么成员变量rb_node就是空闲内核缓冲区红黑树中的一个节点；否则，成员变量rb_node就是正在使用内核缓冲区红黑树中的一个节点。这个结构体是Binder驱动程序实现进程间通信一次copy的核心所在。

>   struct binder_buffer {

>       struct list_head entry; /\* free and allocated entries by address \*/

>       struct rb_node rb_node; /\* free entry by size or allocated entry \*/

>                   /\* by address \*/

>       unsigned free:1;

>       unsigned allow_user_free:1;

>       unsigned async_transaction:1;

>       unsigned debug_id:29;

>       struct binder_transaction \*transaction;

>       struct binder_node \*target_node;

>       size_t data_size; //数据大小

>       size_t offsets_size; //binder对象的偏移数组大小

>       uint8_t data[0]; //保存通讯数据

>   };

![](media/41c07ab81fd016abf5a6f7a68d1b6b2f.jpg)

binder_buffer的内存布局

   
以上就是在本讲Binder驱动程序中所要用到的主要数据结构，接下来的Binder驱动使用流程中我们将会用到这些概念，大家可以先简单的记住这些概念。

4、Binder驱动程序的初始化过程

Binder设备的初始化过程是在Binder驱动程序的初始化函数binder_init中进行的，它的实现如下所示。

>   static int \__init binder_init(void)

>   {

>       int ret;

>       binder_deferred_workqueue = create_singlethread_workqueue("binder");

>       if (!binder_deferred_workqueue)

>           return -ENOMEM;

>       binder_debugfs_dir_entry_root = debugfs_create_dir("binder", NULL);

>       if (binder_debugfs_dir_entry_root)

>           binder_debugfs_dir_entry_proc = debugfs_create_dir("proc",

>                            binder_debugfs_dir_entry_root);

>       ret = misc_register(\&binder_miscdev); //注册Binder驱动程序

>       if (binder_debugfs_dir_entry_root) {

>           debugfs_create_file("state",

>                       S_IRUGO,

>                       binder_debugfs_dir_entry_root,

>                       NULL,

>                       \&binder_state_fops);

>           debugfs_create_file("stats",

>                       S_IRUGO,

>                       binder_debugfs_dir_entry_root,

>                       NULL,

>                       \&binder_stats_fops);

>           debugfs_create_file("transactions",

>                       S_IRUGO,

>                       binder_debugfs_dir_entry_root,

>                       NULL,

>                       \&binder_transactions_fops);

>           debugfs_create_file("transaction_log",

>                       S_IRUGO,

>                       binder_debugfs_dir_entry_root,

>                       &binder_transaction_log,

>                       \&binder_transaction_log_fops);

>           debugfs_create_file("failed_transaction_log",

>                       S_IRUGO,

>                       binder_debugfs_dir_entry_root,

>                       &binder_transaction_log_failed,

>                       \&binder_transaction_log_fops);

>       }

>       return ret;

>   }

   
这个过程发生在我们手机启动的过程中，也就是说在启动Binder服务，注册Binder设备驱动。这段代码首先在目标设备上创建了一个/proc/binder/proc目录，每一个使用了Binder进程间通信机制的进程在该目录下都对应有一个文件，这些文件是以进程ID来命名的，通过它们就可以读取到各个进程的Binder线程池、Binder实体对象、Binder引用对象以及内核缓冲区等信息。接下来又在/proc/binder目录下创建了五个文件state、stats、transactions、transaction_log和failed_transaction_log，通过这五个文件就可以读取到Binder驱动程序的运行状况。

    其中最重要的代码是ret =
misc_register(\&binder_miscdev);，misc_register函数就是用来创建一个misc类型的字符设备的，通过这一行代码会将Binder设备注册上，之后用户的各个进程就可以正常的使用Binder服务了（这个过程可以类比我们为新买的鼠标安装驱动这个过程）。binder_miscdev是Binder设备驱动的描述，其定义如下：

>   static struct miscdevice binder_miscdev = {

>       .minor = MISC_DYNAMIC_MINOR,
>   //次设备号，由内核动态分配，misc类型的驱动程序主设备号是确定的

>       .name = "binder", //通过这个名字，我们可以像打开文件一样使用驱动程序

>       .fops = &binder_fops //还记得上面我们讲过的write系统调用过程吗？

>   };

   
minor是具体的混杂设备次驱动号，name是驱动名，而我们使用驱动的各种服务就是通过fops来实现的。Binder驱动对外所提供的接口函数就是通过binder_fops来定义的。

>   static const struct file_operations binder_fops = {

>       .owner = THIS_MODULE,

>       .poll = binder_poll,

>       .unlocked_ioctl = binder_ioctl,

>       .compat_ioctl = binder_ioctl,

>       .mmap = binder_mmap,

>       .open = binder_open,

>       .flush = binder_flush,

>       .release = binder_release,

>   };

    最重要的就是binder_open、binder_mmap和binder_ioctl三个函数。

    这三个函数调用的方式同我们上面介绍过的系统调用流程是一致的。

![](media/6bb10c6b98f35c2f36ba27cf9d6e90ca.png)

使用Binder驱动程序的系统调用

5、Binder驱动程序的打开过程

一个进程在使用Binder进程间通信机制之前，首先要调用函数open打开设备文件/dev/binder来获得一个文件描述符，然后才能通过这个文件描述符来和Binder驱动程序交互，继而和其他进程执行Binder进程间通信。

   
当进程调用函数open打开设备文件/dev/binder，经过我们上面描述过的系统调用流程，最终Binder驱动程序的binder_open会被调用。

   
调用open函数打开设备文件的过程我们可以在下面地文件中找到。（ProcessState类是一个单例，为应用程序提供了Binder进程间通讯服务）

\<此处有一个ProcessState.cpp文件，请到Thinking in Android ——
Binder的设计与实现：Binder驱动篇（上）.resources文件中查看\>

binder_open的实现如下：

>   static int binder_open(struct inode \*nodp, struct file \*filp)

>   {

>       struct binder_proc \*proc;

>       binder_debug(BINDER_DEBUG_OPEN_CLOSE, "binder_open: %d:%d\\n",

>                current-\>group_leader-\>pid, current-\>pid);

>       proc = kzalloc(sizeof(\*proc), GFP_KERNEL);
>   //分配一块内存存储binder_proc结构体实例

>       if (proc == NULL)

>           return -ENOMEM;

>       get_task_struct(current);

>       proc-\>tsk = current; //current就是进程在内核中描述，task_struct

>       INIT_LIST_HEAD(\&proc-\>todo);

>       init_waitqueue_head(\&proc-\>wait);

>       proc-\>default_priority =
>   task_nice(current); //大家还记得上节讲的nice值么？

>       binder_lock(__func__);

>       binder_stats_created(BINDER_STAT_PROC);

>       hlist_add_head(&proc-\>proc_node,
>   \&binder_procs); //把binder_proc加到全局列表binder_procs中

>       proc-\>pid = current-\>group_leader-\>pid;

>       INIT_LIST_HEAD(\&proc-\>delivered_death);

>       filp-\>private_data = proc;
>   //把binder_proc存到文件的private_data中，这样以后拿这个proc就很容易了

>       binder_unlock(__func__);

>       if (binder_debugfs_dir_entry_proc) {

>           char strbuf[11];

>           snprintf(strbuf, sizeof(strbuf), "%u", proc-\>pid);

>           proc-\>debugfs_entry = debugfs_create_file(strbuf, S_IRUGO,

>               binder_debugfs_dir_entry_proc, proc, \&binder_proc_fops);

>       }

>       return 0;

>   }

   
这个过程最主要的其实就是为想要使用Binder驱动服务的进程创建一个binder_proc结构体，并且对这个binder_proc结构体进行初始化。这个过程有设置proc的tsk为当前的控制块current（进程在内核中的描述task_struct）,设置proc的tsk为当前进程的优先级等。binder_proc同时会被加入到全局hash列表binder_procs中。

 
  初始化完成之后的binder_proc结构体proc保存在参数filp的成员变量private_data中。参数filp指向一个打开文件结构体，当进程调用函数open打开设备文件/dev/binder之后，内核就会返回一个文件描述符给进程，而这个文件描述符与参数filp所指向的打开文件结构体是关联在一起的。因此，当进程后面以这个文件描述符为参数调用函数mmap或者ioctl来与Binder驱动程序交互时，内核就会将与该文件描述符相关联的打开文件结构体传递给Binder驱动程序，这时候Binder驱动程序就可以通过它的成员变量private_data来获得前面在函数binder_open中为进程创建的binder_proc结构体proc。

   
最后，会在/proc/binder/proc目录下创建一个以进程ID为名称的只读文件，并且以函数binder_read_proc_proc作为它的文件内容读取函数。通过读取文件/proc/binder/proc/\<PID\>的内容，我们就可以获得进程\<PID\>的Binder线程池、Binder实体对象、Binder引用对象，以及内核缓冲区等信息。

   
介绍完binder_open之后，结合我们上面介绍过的各种Binder驱动中的数据结构，我们可以通过下图对这些概念有一个更完整的认识。

![](media/bb7c0ce9c3353da28fba27f9e7591c6f.png)

围绕着binder_proc结构体的一些概念

6、Binder驱动程序的内存映射与管理过程

进程打开了设备文件/dev/binder之后，还必须要调用函数mmap把这个设备文件映射到进程的地址空间，然后才可以使用Binder进程间通信机制。设备文件/dev/binder对应的是一个虚拟的设备，将它映射到进程的地址空间的目的并不是对它的内容感兴趣，而是为了为进程分配内核缓冲区，以便它可以用来传输进程间通信数据。

当进程调用函数mmap将设备文件/dev/binder映射到自已的地址空间时，Binder驱动程序中的函数binder_mmap就会被调用。

   
调用mmap函数进行内存映射的过程我们可以在下面地文件中找到。（ProcessState类是一个单例，为应用程序提供了Binder进程间通讯服务）

\<此处有一个ProcessState.cpp文件，请到Thinking in Android ——
Binder的设计与实现：Binder驱动篇（上）.resources文件中查看\>

binder_mmap函数如下：

>   static int binder_mmap(struct file \*filp, struct vm_area_struct \*vma)
>   //用户地址空间的描述

>   {

>       int ret;

>       struct vm_struct \*area; //内核地址空间的描述

>       struct binder_proc \*proc = filp-\>private_data;

>       const char \*failure_string;

>       struct binder_buffer \*buffer;

>       if (proc-\>tsk != current)

>           return -EINVAL;

>       if ((vma-\>vm_end - vma-\>vm_start) \> SZ_4M)

>           vma-\>vm_end = vma-\>vm_start + SZ_4M; //最多分配4M。

>       binder_debug(BINDER_DEBUG_OPEN_CLOSE,

>                "binder_mmap: %d %lx-%lx (%ld K) vma %lx pagep %lx\\n",

>                proc-\>pid, vma-\>vm_start, vma-\>vm_end,

>                (vma-\>vm_end - vma-\>vm_start) / SZ_1K, vma-\>vm_flags,

>                (unsigned long)pgprot_val(vma-\>vm_page_prot));

>       if (vma-\>vm_flags & FORBIDDEN_MMAP_FLAGS) {

>           ret = -EPERM;

>           failure_string = "bad vm_flags";

>           goto err_bad_arg;

>       }

>       vma-\>vm_flags = (vma-\>vm_flags \| VM_DONTCOPY) & \~VM_MAYWRITE;
>   //标记用户地址空间只读，不可copy

>       mutex_lock(\&binder_mmap_lock);

>       if (proc-\>buffer) {

>           ret = -EBUSY;

>           failure_string = "already mapped";

>           goto err_already_mapped;

>       }

>       area = get_vm_area(vma-\>vm_end - vma-\>vm_start, VM_IOREMAP);
>   //获取内核地址空间描述

>       if (area == NULL) {

>           ret = -ENOMEM;

>           failure_string = "get_vm_area";

>           goto err_get_vm_area_failed;

>       }

>       proc-\>buffer = area-\>addr; //地址赋值

>       proc-\>user_buffer_offset = vma-\>vm_start - (uintptr_t)proc-\>buffer;
>   //通过这个值，我们可以方便的换算内核、用户地址的开始位置

>       mutex_unlock(\&binder_mmap_lock);

>   \#ifdef CONFIG_CPU_CACHE_VIPT

>       if (cache_is_vipt_aliasing()) {

>           while (CACHE_COLOUR((vma-\>vm_start \^ (uint32_t)proc-\>buffer))) {

>               pr_info("binder_mmap: %d %lx-%lx maps %p bad alignment\\n",
>   proc-\>pid, vma-\>vm_start, vma-\>vm_end, proc-\>buffer);

>               vma-\>vm_start += PAGE_SIZE;

>           }

>       }

>   \#endif

>       proc-\>pages = kzalloc(sizeof(proc-\>pages[0]) \* ((vma-\>vm_end -
>   vma-\>vm_start) / PAGE_SIZE), GFP_KERNEL);
>   //page是在底层真正描述内存的东西，这里分配了一个page的数组，为了以后存page用

>       if (proc-\>pages == NULL) {

>           ret = -ENOMEM;

>           failure_string = "alloc page array";

>           goto err_alloc_pages_failed;

>       }

>       proc-\>buffer_size = vma-\>vm_end - vma-\>vm_start; //存储缓冲区大小

>       vma-\>vm_ops = \&binder_vm_ops;

>       vma-\>vm_private_data = proc;

>       if (binder_update_page_range(proc, 1, proc-\>buffer, proc-\>buffer +
>   PAGE_SIZE, vma)) { //真正进行内存映射的函数，这里只分配一个page

>           ret = -ENOMEM;

>           failure_string = "alloc small buf";

>           goto err_alloc_small_buf_failed;

>       }

>       buffer = proc-\>buffer;
>   //做一些分配后的操作，这里的proc-\>buffer是一个void\*指针，驱动程序用这个指针可以很方便的指来指去

>       INIT_LIST_HEAD(\&proc-\>buffers);

>       list_add(&buffer-\>entry, \&proc-\>buffers);

>       buffer-\>free = 1;

>       binder_insert_free_buffer(proc, buffer);

>       proc-\>free_async_space = proc-\>buffer_size / 2;

>       barrier();

>       proc-\>files = get_files_struct(current);

>       proc-\>vma = vma;

>       proc-\>vma_vm_mm = vma-\>vm_mm;

>       /\*pr_info("binder_mmap: %d %lx-%lx maps %p\\n",

>            proc-\>pid, vma-\>vm_start, vma-\>vm_end, proc-\>buffer);\*/

>       return 0;

>   //下面是释放过程

>   err_alloc_small_buf_failed:

>       kfree(proc-\>pages);

>       proc-\>pages = NULL;

>   err_alloc_pages_failed:

>       mutex_lock(\&binder_mmap_lock);

>       vfree(proc-\>buffer);

>       proc-\>buffer = NULL;

>   err_get_vm_area_failed:

>   err_already_mapped:

>       mutex_unlock(\&binder_mmap_lock);

>   err_bad_arg:

>       pr_err("binder_mmap: %d %lx-%lx %s failed %d\\n",

>              proc-\>pid, vma-\>vm_start, vma-\>vm_end, failure_string, ret);

>       return ret;

>   }

这其中vm_area_struct结构体用于描述一段用户地址空间，而vm_struct则用来描述一段内核地址空间。从代码
vma-\>vm_end = vma-\>vm_start + SZ_4M;
中我们也可以看出，Binder驱动程序最多可以为进程分配4M内核缓冲区来传输进程间通信数据。

    从代码proc-\>buffer =
area-\>addr;可以看出函数get_vm_area在进程的内核地址空间中分配一段大小为（vma-\>vm_end-vma-\>vm_start）的空间。如果分配成功，就把它的起始地址以及大小保存在proc-\>buffer和proc-\>buffer_size中。

proc-\>user_buffer_offset = vma-\>vm_start - (uintptr_t)proc-\>buffer;
这行代码描述了内核地址空间和用户地址空间起始值的一个差值，通过这个差值，只要知道其中的一个地址，就可以方便地计算出另外一个地址。

   
真正的内存映射过程发生在binder_update_page_range()函数中，从参数我们可以看出在binder_mmap函数调用过程中只映射了一个PAGE_SIZE的物理内存大小，在进程使用Binder进程间通讯的过程中，这通常是不够用的，在通讯过程中还会再调用binder_update_page_range()函数来分配物理内存。

binder_update_page_range()函数如下：

>   static int binder_update_page_range(struct binder_proc \*proc, int allocate,

>                       void \*start, void \*end,

>                       struct vm_area_struct \*vma)

>   {

>       ...

>       if (allocate == 0)
>   //当allocate值为0时会执行用户虚拟地址和内核虚拟地址的释放工作，最后执行物理内存的释放

>           goto free_range;

>       if (vma == NULL) {

>           pr_err("%d: binder_alloc_buf failed to map pages in userspace, no
>   vma\\n",

>               proc-\>pid);

>           goto err_no_vma;

>       }

>       for (page_addr = start; page_addr \< end; page_addr += PAGE_SIZE) {

>           int ret;

>           page = &proc-\>pages[(page_addr - proc-\>buffer) / PAGE_SIZE];

>           BUG_ON(\*page);

>           \*page = alloc_page(GFP_KERNEL \| \__GFP_HIGHMEM \| \__GFP_ZERO);

>           if (\*page == NULL) {

>               pr_err("%d: binder_alloc_buf failed for page at %p\\n",

>                   proc-\>pid, page_addr);

>               goto err_alloc_page_failed;

>           }

>           tmp_area.addr = page_addr;

>           tmp_area.size = PAGE_SIZE + PAGE_SIZE /\* guard page? \*/;

>           ret = map_vm_area(&tmp_area, PAGE_KERNEL, page);
>   //物理内存映射进虚拟内核空间

>           if (ret) {

>               pr_err("%d: binder_alloc_buf failed to map page at %p in
>   kernel\\n",

>                      proc-\>pid, page_addr);

>               goto err_map_kernel_failed;

>           }

>           user_page_addr =

>               (uintptr_t)page_addr + proc-\>user_buffer_offset;

>           ret = vm_insert_page(vma, user_page_addr,
>   page[0]); //物理内存映射进虚拟用户空间

>           if (ret) {

>               pr_err("%d: binder_alloc_buf failed to map page at %lx in
>   userspace\\n",

>                      proc-\>pid, user_page_addr);

>               goto err_vm_insert_page_failed;

>           }

>           /\* vm_insert_page does not seem to increment the refcount \*/

>       }

>       if (mm) {

>           up_write(\&mm-\>mmap_sem);

>           mmput(mm);

>       }

>       return 0;

>   free_range:

>       for (page_addr = end - PAGE_SIZE; page_addr \>= start;

>            page_addr -= PAGE_SIZE) {

>           page = &proc-\>pages[(page_addr - proc-\>buffer) / PAGE_SIZE];

>           if (vma)

>               zap_page_range(vma, (uintptr_t)page_addr +

>                   proc-\>user_buffer_offset, PAGE_SIZE, NULL);

>   err_vm_insert_page_failed:

>           unmap_kernel_range((unsigned long)page_addr, PAGE_SIZE);

>   err_map_kernel_failed:

>           __free_page(\*page);

>           \*page = NULL;

>   err_alloc_page_failed:

>           ;

>       }

>   err_no_vma:

>       if (mm) {

>           up_write(\&mm-\>mmap_sem);

>           mmput(mm);

>       }

>       return -ENOMEM;

>   }

当allocate值为1时，会将用户虚拟地址和内核虚拟地址空间映射到物理内存上，内核虚拟地址映射发生在函数map_vm_area(&tmp_area,
PAGE_KERNEL, page)函数的执行过程中，用户虚拟地址映射发生在vm_insert_page(vma,
user_page_addr, page[0])函数的执行过程中。

![](media/d3bad49682414c263835588454f916c8.png)

内存映射所要用到的函数

   
这样做的好处是当Binder驱动程序需要将一块数据传输给一个进程时，它就可以先把这块数据保存在为该进程所分配的一块内核缓冲区中，然后再把这块内核缓冲区的用户空间地址告诉进程，最后进程就可以访问到里面的数据了。这样做的好处便是不需要将数据从内核空间拷贝到用户空间，从而提高了数据的传输效率。

函数binder_update_page_range()就是Binder进程间通讯，只需要一次copy的核心所在。  

![](media/5768d4b57ff70612d3ca2b782ef292d5.jpg)

内存映射

   
接下来我们分析一下binder_mmap过程中另外一个函数binder_insert_free_buffer()，其代码如下：

>   static void binder_insert_free_buffer(struct binder_proc \*proc,

>                         struct binder_buffer \*new_buffer)

>   {

>       struct rb_node \*\*p = \&proc-\>free_buffers.rb_node;

>       struct rb_node \*parent = NULL;

>       struct binder_buffer \*buffer;

>       size_t buffer_size;

>       size_t new_buffer_size;

>       BUG_ON(!new_buffer-\>free);

>       new_buffer_size = binder_buffer_size(proc, new_buffer);

>       binder_debug(BINDER_DEBUG_BUFFER_ALLOC,

>                "%d: add free buffer, size %zd, at %p\\n",

>                 proc-\>pid, new_buffer_size, new_buffer);

>       while (\*p) {

>           parent = \*p;

>           buffer = rb_entry(parent, struct binder_buffer, rb_node);

>           BUG_ON(!buffer-\>free);

>           buffer_size = binder_buffer_size(proc, buffer);

>           if (new_buffer_size \< buffer_size)

>               p = \&parent-\>rb_left;

>           else

>               p = \&parent-\>rb_right;

>       }

>       rb_link_node(&new_buffer-\>rb_node, parent, p);

>       rb_insert_color(&new_buffer-\>rb_node, \&proc-\>free_buffers);

>   }

   
这个函数的主要工作其实就是计算binder_buffer大小同时插入到binder_proc的成员变量free_buffers中去。最后binder_proc的内存缓冲区的布局如下：

![](media/6cb1ff37db45d7d4928a8132b540ce90.jpg)

内存缓冲区的布局

   
我们上面提到过binder_mmap只会分配一个PAGE_SIZE大小的物理内存，之后在进程间通讯过程中客户端进程首先会将数据从用户空间拷贝到内核空间，然后再将数据传递到服务端进程在内核中的内存缓冲区，这就需要Binder驱动程序从服务端进程的内存缓冲池中分配一块合适的内存缓冲区来保存这些数据，以便服务端进程能够拿到这些数据，完成一次进程间通讯。这个的内存分配的过程是由函数binder_alloc_buf来执行的，函数如下：

>   static struct binder_buffer \*binder_alloc_buf(struct binder_proc \*proc,

>                             size_t data_size,

>                             size_t offsets_size, int is_async)

>   {

>       struct rb_node \*n = proc-\>free_buffers.rb_node;

>       struct binder_buffer \*buffer;

>       size_t buffer_size;

>       struct rb_node \*best_fit = NULL;

>       void \*has_page_addr;

>       void \*end_page_addr;

>       size_t size;

>       if (proc-\>vma == NULL) {

>           pr_err("%d: binder_alloc_buf, no vma\\n",

>                  proc-\>pid);

>           return NULL;

>       }

>       size = ALIGN(data_size, sizeof(void \*)) +

>           ALIGN(offsets_size, sizeof(void \*));

>       if (size \< data_size \|\| size \< offsets_size) {

>           binder_user_error("%d: got transaction with invalid size
>   %zd-%zd\\n",

>                   proc-\>pid, data_size, offsets_size);

>           return NULL;

>       }

>       if (is_async &&

>           proc-\>free_async_space \< size + sizeof(struct binder_buffer)) {

>           binder_debug(BINDER_DEBUG_BUFFER_ALLOC,

>                    "%d: binder_alloc_buf size %zd failed, no async space
>   left\\n",

>                     proc-\>pid, size);

>           return NULL;

>       }

>       while (n) {

>           buffer = rb_entry(n, struct binder_buffer, rb_node);

>           BUG_ON(!buffer-\>free);

>           buffer_size = binder_buffer_size(proc, buffer);

>           if (size \< buffer_size) {

>               best_fit = n;

>               n = n-\>rb_left;

>           } else if (size \> buffer_size)

>               n = n-\>rb_right;

>           else {

>               best_fit = n;

>               break;

>           }

>       }

>       if (best_fit == NULL) {

>           pr_err("%d: binder_alloc_buf size %zd failed, no address space\\n",

>               proc-\>pid, size);

>           return NULL;

>       }

>       if (n == NULL) {

>           buffer = rb_entry(best_fit, struct binder_buffer, rb_node);

>           buffer_size = binder_buffer_size(proc, buffer);

>       }

>       binder_debug(BINDER_DEBUG_BUFFER_ALLOC,

>                "%d: binder_alloc_buf size %zd got buffer %p size %zd\\n",

>                 proc-\>pid, size, buffer, buffer_size);

>       has_page_addr =

>           (void \*)(((uintptr_t)buffer-\>data + buffer_size) & PAGE_MASK);

>       if (n == NULL) {

>           if (size + sizeof(struct binder_buffer) + 4 \>= buffer_size)

>               buffer_size = size; /\* no room for other buffers \*/

>           else

>               buffer_size = size + sizeof(struct binder_buffer);

>       }

>       end_page_addr =

>           (void \*)PAGE_ALIGN((uintptr_t)buffer-\>data + buffer_size);

>       if (end_page_addr \> has_page_addr)

>           end_page_addr = has_page_addr;

>       if (binder_update_page_range(proc, 1,

>           (void \*)PAGE_ALIGN((uintptr_t)buffer-\>data), end_page_addr, NULL))

>           return NULL;

>       rb_erase(best_fit, \&proc-\>free_buffers);

>       buffer-\>free = 0;

>       binder_insert_allocated_buffer(proc, buffer);

>       if (buffer_size != size) {

>           struct binder_buffer \*new_buffer = (void \*)buffer-\>data + size;

>           list_add(&new_buffer-\>entry, \&buffer-\>entry);

>           new_buffer-\>free = 1;

>           binder_insert_free_buffer(proc, new_buffer);

>       }

>       binder_debug(BINDER_DEBUG_BUFFER_ALLOC,

>                "%d: binder_alloc_buf size %zd got %p\\n",

>                 proc-\>pid, size, buffer);

>       buffer-\>data_size = data_size;

>       buffer-\>offsets_size = offsets_size;

>       buffer-\>async_transaction = is_async;

>       if (is_async) {

>           proc-\>free_async_space -= size + sizeof(struct binder_buffer);

>           binder_debug(BINDER_DEBUG_BUFFER_ALLOC_ASYNC,

>                    "%d: binder_alloc_buf size %zd async free %zd\\n",

>                     proc-\>pid, size, proc-\>free_async_space);

>       }

>       return buffer;

>   }

binder_alloc_buf函数会根据一次进程通讯所需要传输的数据量来分配缓存。通过遍历binder_proc的成员free_buffers红黑树，试图来找到最合适的binder_buffer来作为传递数据的载体。有些情况下free_buffers中的binder_buffer会大于传输所需的数据大小，这种情况下，就会将binder_buffer拆成两块，一块用于传输数据使用，另一块会视情况放回到free_buffers中。除此之外还有缓存释放的相关方法，binder_free_buf、binder_delete_free_buffer和binder_transaction_buffer_release，原理是相类似的就不在介绍了。

 
内存映射过程是binder实现进程间通讯只需一次copy的关键，也为正式进行进程间通讯活动进行了铺垫，以上就是binder驱动程序进行内存映射的主要过程。

![](media/e4b09e740fce1eaf0d5f89df538a5802.jpg)
