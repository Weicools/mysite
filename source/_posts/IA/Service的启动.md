**Service的启动**

![](media/9425cb34bf37f82f34cce8575b38b165.png)

#### ContextWrapper.java

\@Override  
public ComponentName startService(Intent service) {  
return mBase.startService(service);  
}

>   Servie的启动从ContextWrapper的startService开始

>   mBase的类型是ContextImpl

#### ContextImpl.java

\@Override  
public ComponentName startService(Intent service) {  
warnIfCallingFromSystemProcess();  
return startServiceCommon(service, false, mUser);  
}  
  
private ComponentName startServiceCommon(Intent service, boolean
requireForeground,  
UserHandle user) {  
try {  
validateServiceIntent(service);  
service.prepareToLeaveProcess(this);  
ComponentName cn = ActivityManager.getService().startService(  
mMainThread.getApplicationThread(), service, service.resolveTypeIfNeeded(  
getContentResolver()), requireForeground,  
getOpPackageName(), user.getIdentifier());  
...  
return cn;  
} ...  
}

>   ActivityManager.getService()返回的是一个AMS对象，从这里开始进入系统进程

#### AMS

\@Override  
public ComponentName startService(IApplicationThread caller, Intent service,  
String resolvedType, boolean requireForeground, String callingPackage, int
userId)  
throws TransactionTooLargeException {  
enforceNotIsolatedCaller("startService");  
...  
  
synchronized(this) {  
final int callingPid = Binder.getCallingPid();  
final int callingUid = Binder.getCallingUid();  
final long origId = Binder.clearCallingIdentity();  
ComponentName res;  
try {  
res = mServices.startServiceLocked(caller, service,  
resolvedType, callingPid, callingUid,  
requireForeground, callingPackage, userId);  
} finally {  
Binder.restoreCallingIdentity(origId);  
}  
return res;  
}  
}

>   AMS通过mServices对象来完成service后续的启动,
>   mServices对象是ActiveServices类型，这是一个辅助AMS进行Service管理的类，包括Service的启动、绑定和停止等。

#### ActiveServices.java

ComponentName startServiceLocked(IApplicationThread caller, Intent service,
String resolvedType,  
int callingPid, int callingUid, boolean fgRequired, String callingPackage, final
int userId)  
throws TransactionTooLargeException {  
...  
  
ComponentName cmp = startServiceInnerLocked(smap, service, r, callerFg,
addToStarting);  
return cmp;  
}  
  
ComponentName startServiceInnerLocked(ServiceMap smap, Intent service,
ServiceRecord r,  
boolean callerFg, boolean addToStarting) throws TransactionTooLargeException {  
ServiceState stracker = r.getTracker();  
if (stracker != null) {  
stracker.setStarted(true, mAm.mProcessStats.getMemFactorLocked(),
r.lastActivity);  
}  
r.callStart = false;  
synchronized (r.stats.getBatteryStats()) {  
r.stats.startRunningLocked();  
}  
String error = bringUpServiceLocked(r, service.getFlags(), callerFg, false,
false);  
if (error != null) {  
return new ComponentName("!!", error);  
}  
  
if (r.startRequested && addToStarting) {  
boolean first = smap.mStartingBackground.size() == 0;  
smap.mStartingBackground.add(r);  
r.startingBgTimeout = SystemClock.uptimeMillis() +
mAm.mConstants.BG_START_TIMEOUT;  
  
if (first) {  
smap.rescheduleDelayedStartsLocked();  
}  
} else if (callerFg \|\| r.fgRequired) {  
smap.ensureNotStartingBackgroundLocked(r);  
}  
  
return r.name;  
}

startServiceLocked没有完成具体的启动工作，而是把后续工作交给bringUpServiceLocked方法来处理

private String bringUpServiceLocked(ServiceRecord r, int intentFlags, boolean
execInFg,  
boolean whileRestarting, boolean permissionsReviewRequired)  
throws TransactionTooLargeException {  
*//
如果该service已创建，再次调用startService()时，只调用Service的onStartCommand()来运行该service*  
if (r.app != null && r.app.thread != null) {  
sendServiceArgsLocked(r, execInFg, false);  
return null;  
}  
...  
  
*// 判断service是否在独立进程中*  
final boolean isolated = (r.serviceInfo.flags&ServiceInfo.FLAG_ISOLATED_PROCESS)
!= 0;  
final String procName = r.processName;  
String hostingType = "service";  
ProcessRecord app;  
  
if (!isolated) {  
app = mAm.getProcessRecordLocked(procName, r.appInfo.uid, false);  
if (app != null && app.thread != null) {  
try {  
app.addPackage(r.appInfo.packageName, r.appInfo.versionCode, mAm.mProcessStats);  
realStartServiceLocked(r, app, execInFg);  
return null;  
} catch (TransactionTooLargeException e) {  
throw e;  
}  
}  
} ...  
  
if (app == null && !permissionsReviewRequired) {  
if ((app=mAm.startProcessLocked(procName, r.appInfo, true, intentFlags,  
hostingType, r.name, false, isolated, false)) == null) {  
bringDownServiceLocked(r);  
return msg;  
}  
if (isolated) {  
r.isolatedProc = app;  
}  
}  
  
return null;  
}

>   首先判断service是否已创建，已创建则调用sendServiceArgsLocked()。然后判断service是否在独立进程中，不在独立进程则调用realStartServiceLocked()，在独立进程则创建一个新进程startProcessLocked()。

private final void realStartServiceLocked(ServiceRecord r,  
ProcessRecord app, boolean execInFg) throws RemoteException {  
...  
r.app = app;  
r.restartTime = r.lastActivity = SystemClock.uptimeMillis();  
  
final boolean newService = app.services.add(r);  
*// 发送create超时的延迟消息*  
bumpServiceExecutingLocked(r, execInFg, "create");  
mAm.updateLruProcessLocked(app, false, null);  
updateServiceForegroundLocked(r.app, false);  
mAm.updateOomAdjLocked();  
  
boolean created = false;  
try {  
synchronized (r.stats.getBatteryStats()) {  
r.stats.startLaunchedLocked();  
}  
mAm.notifyPackageUse(r.serviceInfo.packageName,  
PackageManager.NOTIFY_PACKAGE_USE_SERVICE);  
app.forceProcessStateUpTo(ActivityManager.PROCESS_STATE_SERVICE);  
*// 服务进入onCreate()*  
app.thread.scheduleCreateService(r, r.serviceInfo,  
mAm.compatibilityInfoForPackageLocked(r.serviceInfo.applicationInfo),  
app.repProcState);  
r.postNotification();  
created = true;  
} catch (DeadObjectException e) {  
mAm.appDiedLocked(app);  
throw e;  
} ...  
  
if (r.whitelistManager) {  
app.whitelistManager = true;  
}  
  
requestServiceBindingsLocked(r, execInFg);  
  
updateServiceClientActivitiesLocked(app, null, true);  
  
if (r.startRequested && r.callStart && r.pendingStarts.size() == 0) {  
r.pendingStarts.add(new ServiceRecord.StartItem(r, false, r.makeNextStartId(),  
null, null, 0));  
}  
*// 服务进入onStartCommand()*  
sendServiceArgsLocked(r, execInFg, true);  
  
if (r.delayed) {  
getServiceMapLocked(r.userId).mDelayedStartList.remove(r);  
r.delayed = false;  
}  
  
if (r.delayedStop) {  
r.delayedStop = false;  
if (r.startRequested) {  
stopServiceLocked(r);  
}  
}  
}

>   bumpServiceExecutingLocked()发送一个延迟处理的消息SERVICE_TIMEOUT_MSG

>   在realStartServiceLocked中先通过app.thread的scheduleCreateService方法来创建Service对象并调用其onCreate，再通过sendServiceArgsLocked来调用onStartCommand()

private final void bumpServiceExecutingLocked(ServiceRecord r, boolean fg,
String why) {  
long now = SystemClock.uptimeMillis();  
if (r.executeNesting == 0) {  
...  
if (r.app != null) {  
r.app.executingServices.add(r);  
r.app.execServicesFg \|= fg;  
if (r.app.executingServices.size() == 1) {  
scheduleServiceTimeoutLocked(r.app);  
}  
}  
} else if (r.app != null && fg && !r.app.execServicesFg) {  
r.app.execServicesFg = true;  
scheduleServiceTimeoutLocked(r.app);  
}  
...  
}  
  
void scheduleServiceTimeoutLocked(ProcessRecord proc) {  
if (proc.executingServices.size() == 0 \|\| proc.thread == null) {  
return;  
}  
Message msg = mAm.mHandler.obtainMessage(  
ActivityManagerService.SERVICE_TIMEOUT_MSG);  
msg.obj = proc;  
mAm.mHandler.sendMessageDelayed(msg,  
proc.execServicesFg ? SERVICE_TIMEOUT : SERVICE_BACKGROUND_TIMEOUT);  
}

>   在方法scheduleCreateService()执行完成，也就是onCreate()回调执行完成之后便会remove掉该消息。

*// How long we wait for a service to finish executing.*  
static final int SERVICE_TIMEOUT = 20\*1000; *// 前台服务为20秒*  
  
*// How long we wait for a service to finish executing.*  
static final int SERVICE_BACKGROUND_TIMEOUT = SERVICE_TIMEOUT \* 10; *//
后台服务为200秒*

接下来我们看scheduleCreateService()方法的执行，从这儿进入用户进程

#### ApplicationThread.java

public final void scheduleCreateService(IBinder token,  
ServiceInfo info, CompatibilityInfo compatInfo, int processState) {  
updateProcessState(processState, false);  
CreateServiceData s = new CreateServiceData();  
s.token = token;  
s.info = info;  
s.compatInfo = compatInfo;  
  
sendMessage(H.CREATE_SERVICE, s);  
}

#### ActivityThread.java

private void handleCreateService(CreateServiceData data) {  
unscheduleGcIdler();  
  
LoadedApk packageInfo = getPackageInfoNoCheck(  
data.info.applicationInfo, data.compatInfo);  
Service service = null;  
try {  
java.lang.ClassLoader cl = packageInfo.getClassLoader();  
*// 通过反射创建目标服务对象*  
service = (Service) cl.loadClass(data.info.name).newInstance();  
} ...  
try {  
ContextImpl context = ContextImpl.createAppContext(this, packageInfo);  
context.setOuterContext(service);  
  
Application app = packageInfo.makeApplication(false, mInstrumentation);  
service.attach(context, this, data.info.name, data.token, app,  
ActivityManager.getService());  
*// 调用service的onCreate()*  
service.onCreate();  
mServices.put(data.token, service);  
try {  
*// 通知AMS当前service创建完成*  
ActivityManager.getService().serviceDoneExecuting(  
data.token, SERVICE_DONE_EXECUTING_ANON, 0, 0);  
} ...  
} catch (Exception e) {  
...  
}  
}

>   先通过类加载器创建Service的实例，接着创建ContextImpl对象并通过Service的attach方法建立二者之间的关系，最后调用Service的onCreate方法将Service对象存储到ActivityThread的一个列表中

final ArrayMap\<IBinder, Service\> mServices = new ArrayMap\<\>();

接下来我们看onCreate()执行完后如何remove掉SERVICE_TIMEOUT_MSG消息的，执行完service.onCreate()后AMS调用了serviceDoneExecuting()方法，这方法最终回调到ActiveServices中的serviceDoneExecutingLocked()方法

#### ActiveServices.java

void serviceDoneExecutingLocked(ServiceRecord r, int type, int startId, int res)
{  
boolean inDestroying = mDestroyingServices.contains(r);  
if (r != null) {  
if (type == ActivityThread.SERVICE_DONE_EXECUTING_START) {  
...  
serviceDoneExecutingLocked(r, inDestroying, inDestroying);  
Binder.restoreCallingIdentity(origId);  
} ...  
}  
  
private void serviceDoneExecutingLocked(ServiceRecord r, boolean inDestroying,  
boolean finishing) {  
r.executeNesting--;  
if (r.executeNesting \<= 0) {  
if (r.app != null) {  
r.app.execServicesFg = false;  
r.app.executingServices.remove(r);  
if (r.app.executingServices.size() == 0) {  
*// 移除服务启动超时的消息*  
mAm.mHandler.removeMessages(ActivityManagerService.SERVICE_TIMEOUT_MSG, r.app);  
} ...  
}  
r.executeFg = false;  
...  
}  
}

#### ActiveServices.java

private final void sendServiceArgsLocked(ServiceRecord r, boolean execInFg,  
boolean oomAdjusted) throws TransactionTooLargeException {  
...  
while (r.pendingStarts.size() \> 0) {  
*// 发送start超时的延迟消息*  
bumpServiceExecutingLocked(r, execInFg, "start");  
...  
}  
  
ParceledListSlice\<ServiceStartArgs\> slice = new ParceledListSlice\<\>(args);  
slice.setInlineCountLimit(4);  
Exception caughtException = null;  
try {  
r.app.thread.scheduleServiceArgs(r, slice);  
} ...  
}

>   之前提到过sendServiceArgsLocked用来调用Service的onStartCommand()，最终调用到ApplicationThread的scheduleServiceArgs方法，再经过一系列调用到ActivityThread的handleServiceArgs()方法

#### ActivityThread.java

private void handleServiceArgs(ServiceArgsData data) {  
Service s = mServices.get(data.token);  
if (s != null) {  
try {  
if (data.args != null) {  
data.args.setExtrasClassLoader(s.getClassLoader());  
data.args.prepareToEnterProcess();  
}  
int res;  
if (!data.taskRemoved) {  
*// 调用Service的onStartCommand()*  
res = s.onStartCommand(data.args, data.flags, data.startId);  
} else {  
s.onTaskRemoved(data.args);  
res = Service.START_TASK_REMOVED_COMPLETE;  
}  
  
QueuedWork.waitToFinish();  
  
try {  
*// 通知AMS，service启动完成*  
ActivityManager.getService().serviceDoneExecuting(  
data.token, SERVICE_DONE_EXECUTING_START, data.startId, res);  
} catch (RemoteException e) {  
throw e.rethrowFromSystemServer();  
}  
ensureJitEnabled();  
} catch (Exception e) {  
...  
}  
}  
}

>   s.onStartCommand()执行完后又调用了AMS的serviceDoneExecuting()方法，remove掉start的延时消息与remove掉create延时消息过程一致

%23%20Service%E7%9A%84%E5%90%AF%E5%8A%A8%0A!%5B%5D(http%3A%2F%2Fassets.processon.com%2Fchart_image%2F5bc6ef31e4b08faf8c802e06.png%3F_%3D1542972916072)%0A%0A%23%23%23%23%20ContextWrapper.java%0A%60%60%60%20java%0A%40Override%0A%20%20%20%20public%20ComponentName%20startService(Intent%20service)%20%7B%0A%20%20%20%20%20%20%20%20return%20mBase.startService(service)%3B%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20Servie%E7%9A%84%E5%90%AF%E5%8A%A8%E4%BB%8EContextWrapper%E7%9A%84startService%E5%BC%80%E5%A7%8B%0A%3E%0A%3E%20mBase%E7%9A%84%E7%B1%BB%E5%9E%8B%E6%98%AFContextImpl%0A%0A%23%23%23%23%20ContextImpl.java%0A%60%60%60java%0A%20%40Override%0A%20%20%20%20public%20ComponentName%20startService(Intent%20service)%20%7B%0A%20%20%20%20%20%20%20%20warnIfCallingFromSystemProcess()%3B%0A%20%20%20%20%20%20%20%20return%20startServiceCommon(service%2C%20false%2C%20mUser)%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20private%20ComponentName%20startServiceCommon(Intent%20service%2C%20boolean%20requireForeground%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20UserHandle%20user)%20%7B%0A%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20validateServiceIntent(service)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20service.prepareToLeaveProcess(this)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20ComponentName%20cn%20%3D%20ActivityManager.getService().startService(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20mMainThread.getApplicationThread()%2C%20service%2C%20service.resolveTypeIfNeeded(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20getContentResolver())%2C%20requireForeground%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20getOpPackageName()%2C%20user.getIdentifier())%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20cn%3B%0A%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20ActivityManager.getService()%E8%BF%94%E5%9B%9E%E7%9A%84%E6%98%AF%E4%B8%80%E4%B8%AAAMS%E5%AF%B9%E8%B1%A1%EF%BC%8C%E4%BB%8E%E8%BF%99%E9%87%8C%E5%BC%80%E5%A7%8B%E8%BF%9B%E5%85%A5%E7%B3%BB%E7%BB%9F%E8%BF%9B%E7%A8%8B%0A%0A%23%23%23%23%20AMS%0A%60%60%60java%0A%20%40Override%0A%20%20%20%20public%20ComponentName%20startService(IApplicationThread%20caller%2C%20Intent%20service%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20String%20resolvedType%2C%20boolean%20requireForeground%2C%20String%20callingPackage%2C%20int%20userId)%0A%20%20%20%20%20%20%20%20%20%20%20%20throws%20TransactionTooLargeException%20%7B%0A%20%20%20%20%20%20%20%20enforceNotIsolatedCaller(%22startService%22)%3B%0A%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20synchronized(this)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20final%20int%20callingPid%20%3D%20Binder.getCallingPid()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20final%20int%20callingUid%20%3D%20Binder.getCallingUid()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20final%20long%20origId%20%3D%20Binder.clearCallingIdentity()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20ComponentName%20res%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20res%20%3D%20mServices.startServiceLocked(caller%2C%20service%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20resolvedType%2C%20callingPid%2C%20callingUid%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20requireForeground%2C%20callingPackage%2C%20userId)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%20finally%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20Binder.restoreCallingIdentity(origId)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20res%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20AMS%E9%80%9A%E8%BF%87mServices%E5%AF%B9%E8%B1%A1%E6%9D%A5%E5%AE%8C%E6%88%90service%E5%90%8E%E7%BB%AD%E7%9A%84%E5%90%AF%E5%8A%A8%2C%20mServices%E5%AF%B9%E8%B1%A1%E6%98%AFActiveServices%E7%B1%BB%E5%9E%8B%EF%BC%8C%E8%BF%99%E6%98%AF%E4%B8%80%E4%B8%AA%E8%BE%85%E5%8A%A9AMS%E8%BF%9B%E8%A1%8CService%E7%AE%A1%E7%90%86%E7%9A%84%E7%B1%BB%EF%BC%8C%E5%8C%85%E6%8B%ACService%E7%9A%84%E5%90%AF%E5%8A%A8%E3%80%81%E7%BB%91%E5%AE%9A%E5%92%8C%E5%81%9C%E6%AD%A2%E7%AD%89%E3%80%82%0A%0A%23%23%23%23%20ActiveServices.java%0A%60%60%60java%0AComponentName%20startServiceLocked(IApplicationThread%20caller%2C%20Intent%20service%2C%20String%20resolvedType%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20int%20callingPid%2C%20int%20callingUid%2C%20boolean%20fgRequired%2C%20String%20callingPackage%2C%20final%20int%20userId)%0A%20%20%20%20%20%20%20%20%20%20%20%20throws%20TransactionTooLargeException%20%7B%0A%20%20%20%20%20%20%20%20...%0A%0A%20%20%20%20%20%20%20%20ComponentName%20cmp%20%3D%20startServiceInnerLocked(smap%2C%20service%2C%20r%2C%20callerFg%2C%20addToStarting)%3B%0A%20%20%20%20%20%20%20%20return%20cmp%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20ComponentName%20startServiceInnerLocked(ServiceMap%20smap%2C%20Intent%20service%2C%20ServiceRecord%20r%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20boolean%20callerFg%2C%20boolean%20addToStarting)%20throws%20TransactionTooLargeException%20%7B%0A%20%20%20%20%20%20%20%20ServiceState%20stracker%20%3D%20r.getTracker()%3B%0A%20%20%20%20%20%20%20%20if%20(stracker%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20stracker.setStarted(true%2C%20mAm.mProcessStats.getMemFactorLocked()%2C%20r.lastActivity)%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20r.callStart%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20synchronized%20(r.stats.getBatteryStats())%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.stats.startRunningLocked()%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20String%20error%20%3D%20bringUpServiceLocked(r%2C%20service.getFlags()%2C%20callerFg%2C%20false%2C%20false)%3B%0A%20%20%20%20%20%20%20%20if%20(error%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20new%20ComponentName(%22!!%22%2C%20error)%3B%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20if%20(r.startRequested%20%26%26%20addToStarting)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20boolean%20first%20%3D%20smap.mStartingBackground.size()%20%3D%3D%200%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20smap.mStartingBackground.add(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.startingBgTimeout%20%3D%20SystemClock.uptimeMillis()%20%2B%20mAm.mConstants.BG_START_TIMEOUT%3B%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(first)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20smap.rescheduleDelayedStartsLocked()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%20else%20if%20(callerFg%20%7C%7C%20r.fgRequired)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20smap.ensureNotStartingBackgroundLocked(r)%3B%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20return%20r.name%3B%0A%20%20%20%20%7D%0A%60%60%60%0AstartServiceLocked%E6%B2%A1%E6%9C%89%E5%AE%8C%E6%88%90%E5%85%B7%E4%BD%93%E7%9A%84%E5%90%AF%E5%8A%A8%E5%B7%A5%E4%BD%9C%EF%BC%8C%E8%80%8C%E6%98%AF%E6%8A%8A%E5%90%8E%E7%BB%AD%E5%B7%A5%E4%BD%9C%E4%BA%A4%E7%BB%99bringUpServiceLocked%E6%96%B9%E6%B3%95%E6%9D%A5%E5%A4%84%E7%90%86%0A%0A%60%60%60%20java%0A%20%20%20%20%20private%20String%20bringUpServiceLocked(ServiceRecord%20r%2C%20int%20intentFlags%2C%20boolean%20execInFg%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20boolean%20whileRestarting%2C%20boolean%20permissionsReviewRequired)%0A%20%20%20%20%20%20%20%20%20%20%20%20throws%20TransactionTooLargeException%20%7B%0A%20%20%20%20%20%20%20%20%20%2F%2F%20%E5%A6%82%E6%9E%9C%E8%AF%A5service%E5%B7%B2%E5%88%9B%E5%BB%BA%EF%BC%8C%E5%86%8D%E6%AC%A1%E8%B0%83%E7%94%A8startService()%E6%97%B6%EF%BC%8C%E5%8F%AA%E8%B0%83%E7%94%A8Service%E7%9A%84onStartCommand()%E6%9D%A5%E8%BF%90%E8%A1%8C%E8%AF%A5service%0A%20%20%20%20%20%20%20%20%20if%20(r.app%20!%3D%20null%20%26%26%20r.app.thread%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20sendServiceArgsLocked(r%2C%20execInFg%2C%20false)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20null%3B%0A%20%20%20%20%20%20%20%20%20%7D%0A%09...%0A%0A%20%20%20%20%20%20%20%20%2F%2F%20%E5%88%A4%E6%96%ADservice%E6%98%AF%E5%90%A6%E5%9C%A8%E7%8B%AC%E7%AB%8B%E8%BF%9B%E7%A8%8B%E4%B8%AD%0A%20%20%20%20%20%20%20%20final%20boolean%20isolated%20%3D%20(r.serviceInfo.flags%26ServiceInfo.FLAG_ISOLATED_PROCESS)%20!%3D%200%3B%0A%20%20%20%20%20%20%20%20final%20String%20procName%20%3D%20r.processName%3B%0A%20%20%20%20%20%20%20%20String%20hostingType%20%3D%20%22service%22%3B%0A%20%20%20%20%20%20%20%20ProcessRecord%20app%3B%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20if%20(!isolated)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20app%20%3D%20mAm.getProcessRecordLocked(procName%2C%20r.appInfo.uid%2C%20false)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(app%20!%3D%20null%20%26%26%20app.thread%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20app.addPackage(r.appInfo.packageName%2C%20r.appInfo.versionCode%2C%20mAm.mProcessStats)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20realStartServiceLocked(r%2C%20app%2C%20execInFg)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20null%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%20catch%20(TransactionTooLargeException%20e)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20throw%20e%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20if%20(app%20%3D%3D%20null%20%26%26%20!permissionsReviewRequired)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20((app%3DmAm.startProcessLocked(procName%2C%20r.appInfo%2C%20true%2C%20intentFlags%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20hostingType%2C%20r.name%2C%20false%2C%20isolated%2C%20false))%20%3D%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20bringDownServiceLocked(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20msg%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(isolated)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.isolatedProc%20%3D%20app%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20return%20null%3B%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20%E9%A6%96%E5%85%88%E5%88%A4%E6%96%ADservice%E6%98%AF%E5%90%A6%E5%B7%B2%E5%88%9B%E5%BB%BA%EF%BC%8C%E5%B7%B2%E5%88%9B%E5%BB%BA%E5%88%99%E8%B0%83%E7%94%A8sendServiceArgsLocked()%E3%80%82%E7%84%B6%E5%90%8E%E5%88%A4%E6%96%ADservice%E6%98%AF%E5%90%A6%E5%9C%A8%E7%8B%AC%E7%AB%8B%E8%BF%9B%E7%A8%8B%E4%B8%AD%EF%BC%8C%E4%B8%8D%E5%9C%A8%E7%8B%AC%E7%AB%8B%E8%BF%9B%E7%A8%8B%E5%88%99%E8%B0%83%E7%94%A8realStartServiceLocked()%EF%BC%8C%E5%9C%A8%E7%8B%AC%E7%AB%8B%E8%BF%9B%E7%A8%8B%E5%88%99%E5%88%9B%E5%BB%BA%E4%B8%80%E4%B8%AA%E6%96%B0%E8%BF%9B%E7%A8%8BstartProcessLocked()%E3%80%82%0A%0A%60%60%60java%0A%20%20%20%20%20private%20final%20void%20realStartServiceLocked(ServiceRecord%20r%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20ProcessRecord%20app%2C%20boolean%20execInFg)%20throws%20RemoteException%20%7B%0A%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20r.app%20%3D%20app%3B%0A%20%20%20%20%20%20%20%20r.restartTime%20%3D%20r.lastActivity%20%3D%20SystemClock.uptimeMillis()%3B%0A%0A%20%20%20%20%20%20%20%20final%20boolean%20newService%20%3D%20app.services.add(r)%3B%0A%20%20%20%20%20%20%20%20%2F%2F%20%E5%8F%91%E9%80%81create%E8%B6%85%E6%97%B6%E7%9A%84%E5%BB%B6%E8%BF%9F%E6%B6%88%E6%81%AF%0A%20%20%20%20%20%20%20%20bumpServiceExecutingLocked(r%2C%20execInFg%2C%20%22create%22)%3B%0A%20%20%20%20%20%20%20%20mAm.updateLruProcessLocked(app%2C%20false%2C%20null)%3B%0A%20%20%20%20%20%20%20%20updateServiceForegroundLocked(r.app%2C%20false)%3B%0A%20%20%20%20%20%20%20%20mAm.updateOomAdjLocked()%3B%0A%0A%20%20%20%20%20%20%20%20boolean%20created%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20synchronized%20(r.stats.getBatteryStats())%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.stats.startLaunchedLocked()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20mAm.notifyPackageUse(r.serviceInfo.packageName%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20PackageManager.NOTIFY_PACKAGE_USE_SERVICE)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20app.forceProcessStateUpTo(ActivityManager.PROCESS_STATE_SERVICE)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E6%9C%8D%E5%8A%A1%E8%BF%9B%E5%85%A5onCreate()%0A%20%20%20%20%20%20%20%20%20%20%20%20app.thread.scheduleCreateService(r%2C%20r.serviceInfo%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20mAm.compatibilityInfoForPackageLocked(r.serviceInfo.applicationInfo)%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20app.repProcState)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.postNotification()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20created%20%3D%20true%3B%0A%20%20%20%20%20%20%20%20%7D%20catch%20(DeadObjectException%20e)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20mAm.appDiedLocked(app)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20throw%20e%3B%0A%20%20%20%20%20%20%20%20%7D%20...%0A%0A%20%20%20%20%20%20%20%20if%20(r.whitelistManager)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20app.whitelistManager%20%3D%20true%3B%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20requestServiceBindingsLocked(r%2C%20execInFg)%3B%0A%0A%20%20%20%20%20%20%20%20updateServiceClientActivitiesLocked(app%2C%20null%2C%20true)%3B%0A%0A%20%20%20%20%20%20%20%20if%20(r.startRequested%20%26%26%20r.callStart%20%26%26%20r.pendingStarts.size()%20%3D%3D%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.pendingStarts.add(new%20ServiceRecord.StartItem(r%2C%20false%2C%20r.makeNextStartId()%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20null%2C%20null%2C%200))%3B%0A%20%20%20%20%20%20%20%20%7D%0A%09%2F%2F%20%E6%9C%8D%E5%8A%A1%E8%BF%9B%E5%85%A5onStartCommand()%0A%20%20%20%20%20%20%20%20sendServiceArgsLocked(r%2C%20execInFg%2C%20true)%3B%0A%0A%20%20%20%20%20%20%20%20if%20(r.delayed)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20getServiceMapLocked(r.userId).mDelayedStartList.remove(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.delayed%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20if%20(r.delayedStop)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.delayedStop%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(r.startRequested)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20stopServiceLocked(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20bumpServiceExecutingLocked()%E5%8F%91%E9%80%81%E4%B8%80%E4%B8%AA%E5%BB%B6%E8%BF%9F%E5%A4%84%E7%90%86%E7%9A%84%E6%B6%88%E6%81%AFSERVICE%5C_TIMEOUT%5C_MSG%0A%3E%20%E5%9C%A8realStartServiceLocked%E4%B8%AD%E5%85%88%E9%80%9A%E8%BF%87app.thread%E7%9A%84scheduleCreateService%E6%96%B9%E6%B3%95%E6%9D%A5%E5%88%9B%E5%BB%BAService%E5%AF%B9%E8%B1%A1%E5%B9%B6%E8%B0%83%E7%94%A8%E5%85%B6onCreate%EF%BC%8C%E5%86%8D%E9%80%9A%E8%BF%87sendServiceArgsLocked%E6%9D%A5%E8%B0%83%E7%94%A8onStartCommand()%0A%0A%60%60%60java%0Aprivate%20final%20void%20bumpServiceExecutingLocked(ServiceRecord%20r%2C%20boolean%20fg%2C%20String%20why)%20%7B%0A%20%20%20%20%20%20%20%20long%20now%20%3D%20SystemClock.uptimeMillis()%3B%0A%20%20%20%20%20%20%20%20if%20(r.executeNesting%20%3D%3D%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(r.app%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.app.executingServices.add(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.app.execServicesFg%20%7C%3D%20fg%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20(r.app.executingServices.size()%20%3D%3D%201)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20scheduleServiceTimeoutLocked(r.app)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%20else%20if%20(r.app%20!%3D%20null%20%26%26%20fg%20%26%26%20!r.app.execServicesFg)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.app.execServicesFg%20%3D%20true%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20scheduleServiceTimeoutLocked(r.app)%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20...%0A%20%20%20%20%7D%0A%0A%20void%20scheduleServiceTimeoutLocked(ProcessRecord%20proc)%20%7B%0A%20%20%20%20%20%20%20%20if%20(proc.executingServices.size()%20%3D%3D%200%20%7C%7C%20proc.thread%20%3D%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20Message%20msg%20%3D%20mAm.mHandler.obtainMessage(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ActivityManagerService.SERVICE_TIMEOUT_MSG)%3B%0A%20%20%20%20%20%20%20%20msg.obj%20%3D%20proc%3B%0A%20%20%20%20%20%20%20%20mAm.mHandler.sendMessageDelayed(msg%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20proc.execServicesFg%20%3F%20SERVICE_TIMEOUT%20%3A%20SERVICE_BACKGROUND_TIMEOUT)%3B%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20%20%E5%9C%A8%E6%96%B9%E6%B3%95scheduleCreateService()%E6%89%A7%E8%A1%8C%E5%AE%8C%E6%88%90%EF%BC%8C%E4%B9%9F%E5%B0%B1%E6%98%AFonCreate()%E5%9B%9E%E8%B0%83%E6%89%A7%E8%A1%8C%E5%AE%8C%E6%88%90%E4%B9%8B%E5%90%8E%E4%BE%BF%E4%BC%9Aremove%E6%8E%89%E8%AF%A5%E6%B6%88%E6%81%AF%E3%80%82%0A%0A%60%60%60java%0A%2F%2F%20How%20long%20we%20wait%20for%20a%20service%20to%20finish%20executing.%0A%20%20%20%20static%20final%20int%20SERVICE_TIMEOUT%20%3D%2020\*1000%3B%20%2F%2F%20%E5%89%8D%E5%8F%B0%E6%9C%8D%E5%8A%A1%E4%B8%BA20%E7%A7%92%0A%0A%2F%2F%20How%20long%20we%20wait%20for%20a%20service%20to%20finish%20executing.%0A%20%20%20%20static%20final%20int%20SERVICE_BACKGROUND_TIMEOUT%20%3D%20SERVICE_TIMEOUT%20\*%2010%3B%20%2F%2F%20%E5%90%8E%E5%8F%B0%E6%9C%8D%E5%8A%A1%E4%B8%BA200%E7%A7%92%0A%60%60%60%20%0A%E6%8E%A5%E4%B8%8B%E6%9D%A5%E6%88%91%E4%BB%AC%E7%9C%8BscheduleCreateService()%E6%96%B9%E6%B3%95%E7%9A%84%E6%89%A7%E8%A1%8C%EF%BC%8C%E4%BB%8E%E8%BF%99%E5%84%BF%E8%BF%9B%E5%85%A5%E7%94%A8%E6%88%B7%E8%BF%9B%E7%A8%8B%0A%0A%23%23%23%23%20ApplicationThread.java%0A%60%60%60java%0Apublic%20final%20void%20scheduleCreateService(IBinder%20token%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ServiceInfo%20info%2C%20CompatibilityInfo%20compatInfo%2C%20int%20processState)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20updateProcessState(processState%2C%20false)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20CreateServiceData%20s%20%3D%20new%20CreateServiceData()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20s.token%20%3D%20token%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20s.info%20%3D%20info%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20s.compatInfo%20%3D%20compatInfo%3B%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20sendMessage(H.CREATE_SERVICE%2C%20s)%3B%0A%20%20%20%20%20%20%20%20%7D%0A%60%60%60%0A%0A%0A%23%23%23%23%20ActivityThread.java%0A%60%60%60java%0Aprivate%20void%20handleCreateService(CreateServiceData%20data)%20%7B%0A%20%20%20%20%20%20%20%20unscheduleGcIdler()%3B%0A%0A%20%20%20%20%20%20%20%20LoadedApk%20packageInfo%20%3D%20getPackageInfoNoCheck(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20data.info.applicationInfo%2C%20data.compatInfo)%3B%0A%20%20%20%20%20%20%20%20Service%20service%20%3D%20null%3B%0A%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20java.lang.ClassLoader%20cl%20%3D%20packageInfo.getClassLoader()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E9%80%9A%E8%BF%87%E5%8F%8D%E5%B0%84%E5%88%9B%E5%BB%BA%E7%9B%AE%E6%A0%87%E6%9C%8D%E5%8A%A1%E5%AF%B9%E8%B1%A1%0A%20%20%20%20%20%20%20%20%20%20%20%20service%20%3D%20(Service)%20cl.loadClass(data.info.name).newInstance()%3B%0A%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20ContextImpl%20context%20%3D%20ContextImpl.createAppContext(this%2C%20packageInfo)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20context.setOuterContext(service)%3B%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20Application%20app%20%3D%20packageInfo.makeApplication(false%2C%20mInstrumentation)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20service.attach(context%2C%20this%2C%20data.info.name%2C%20data.token%2C%20app%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ActivityManager.getService())%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E8%B0%83%E7%94%A8service%E7%9A%84onCreate()%0A%20%20%20%20%20%20%20%20%20%20%20%20service.onCreate()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20mServices.put(data.token%2C%20service)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E9%80%9A%E7%9F%A5AMS%E5%BD%93%E5%89%8Dservice%E5%88%9B%E5%BB%BA%E5%AE%8C%E6%88%90%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ActivityManager.getService().serviceDoneExecuting(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20data.token%2C%20SERVICE_DONE_EXECUTING_ANON%2C%200%2C%200)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%20%20%20%20%7D%20catch%20(Exception%20e)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20%E5%85%88%E9%80%9A%E8%BF%87%E7%B1%BB%E5%8A%A0%E8%BD%BD%E5%99%A8%E5%88%9B%E5%BB%BAService%E7%9A%84%E5%AE%9E%E4%BE%8B%EF%BC%8C%E6%8E%A5%E7%9D%80%E5%88%9B%E5%BB%BAContextImpl%E5%AF%B9%E8%B1%A1%E5%B9%B6%E9%80%9A%E8%BF%87Service%E7%9A%84attach%E6%96%B9%E6%B3%95%E5%BB%BA%E7%AB%8B%E4%BA%8C%E8%80%85%E4%B9%8B%E9%97%B4%E7%9A%84%E5%85%B3%E7%B3%BB%EF%BC%8C%E6%9C%80%E5%90%8E%E8%B0%83%E7%94%A8Service%E7%9A%84onCreate%E6%96%B9%E6%B3%95%E5%B0%86Service%E5%AF%B9%E8%B1%A1%E5%AD%98%E5%82%A8%E5%88%B0ActivityThread%E7%9A%84%E4%B8%80%E4%B8%AA%E5%88%97%E8%A1%A8%E4%B8%AD%0A%0A%60%60%60java%0Afinal%20ArrayMap%3CIBinder%2C%20Service%3E%20mServices%20%3D%20new%20ArrayMap%3C%3E()%3B%0A%60%60%60%0A%0A%E6%8E%A5%E4%B8%8B%E6%9D%A5%E6%88%91%E4%BB%AC%E7%9C%8BonCreate()%E6%89%A7%E8%A1%8C%E5%AE%8C%E5%90%8E%E5%A6%82%E4%BD%95remove%E6%8E%89SERVICE%5C_TIMEOUT%5C_MSG%E6%B6%88%E6%81%AF%E7%9A%84%EF%BC%8C%E6%89%A7%E8%A1%8C%E5%AE%8Cservice.onCreate()%E5%90%8EAMS%E8%B0%83%E7%94%A8%E4%BA%86serviceDoneExecuting()%E6%96%B9%E6%B3%95%EF%BC%8C%E8%BF%99%E6%96%B9%E6%B3%95%E6%9C%80%E7%BB%88%E5%9B%9E%E8%B0%83%E5%88%B0ActiveServices%E4%B8%AD%E7%9A%84serviceDoneExecutingLocked()%E6%96%B9%E6%B3%95%0A%0A%23%23%23%23%20ActiveServices.java%0A%60%60%60java%0Avoid%20serviceDoneExecutingLocked(ServiceRecord%20r%2C%20int%20type%2C%20int%20startId%2C%20int%20res)%20%7B%0A%20%20%20%20%20%20%20%20boolean%20inDestroying%20%3D%20mDestroyingServices.contains(r)%3B%0A%20%20%20%20%20%20%20%20if%20(r%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(type%20%3D%3D%20ActivityThread.SERVICE_DONE_EXECUTING_START)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%20%20%20%20serviceDoneExecutingLocked(r%2C%20inDestroying%2C%20inDestroying)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20Binder.restoreCallingIdentity(origId)%3B%0A%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%7D%0A%20%20%20%20%0Aprivate%20void%20serviceDoneExecutingLocked(ServiceRecord%20r%2C%20boolean%20inDestroying%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20boolean%20finishing)%20%7B%0A%20%20%20%20%20%20%20%20r.executeNesting--%3B%0A%20%20%20%20%20%20%20%20if%20(r.executeNesting%20%3C%3D%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20(r.app%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.app.execServicesFg%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20r.app.executingServices.remove(r)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20(r.app.executingServices.size()%20%3D%3D%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E7%A7%BB%E9%99%A4%E6%9C%8D%E5%8A%A1%E5%90%AF%E5%8A%A8%E8%B6%85%E6%97%B6%E7%9A%84%E6%B6%88%E6%81%AF%20%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20mAm.mHandler.removeMessages(ActivityManagerService.SERVICE_TIMEOUT_MSG%2C%20r.app)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20r.executeFg%20%3D%20false%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%60%60%60%0A%0A%23%23%23%23%20ActiveServices.java%0A%60%60%60java%0A%20%20%20%20private%20final%20void%20sendServiceArgsLocked(ServiceRecord%20r%2C%20boolean%20execInFg%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20boolean%20oomAdjusted)%20throws%20TransactionTooLargeException%20%7B%0A%09%09%20...%0A%20%20%20%20%20%20%20%20while%20(r.pendingStarts.size()%20%3E%200)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E5%8F%91%E9%80%81start%E8%B6%85%E6%97%B6%E7%9A%84%E5%BB%B6%E8%BF%9F%E6%B6%88%E6%81%AF%0A%20%20%20%20%20%20%20%20%20%20%20%20bumpServiceExecutingLocked(r%2C%20execInFg%2C%20%22start%22)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20ParceledListSlice%3CServiceStartArgs%3E%20slice%20%3D%20new%20ParceledListSlice%3C%3E(args)%3B%0A%20%20%20%20%20%20%20%20slice.setInlineCountLimit(4)%3B%0A%20%20%20%20%20%20%20%20Exception%20caughtException%20%3D%20null%3B%0A%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20r.app.thread.scheduleServiceArgs(r%2C%20slice)%3B%0A%20%20%20%20%20%20%20%20%7D%20...%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20%E4%B9%8B%E5%89%8D%E6%8F%90%E5%88%B0%E8%BF%87sendServiceArgsLocked%E7%94%A8%E6%9D%A5%E8%B0%83%E7%94%A8Service%E7%9A%84onStartCommand()%EF%BC%8C%E6%9C%80%E7%BB%88%E8%B0%83%E7%94%A8%E5%88%B0ApplicationThread%E7%9A%84scheduleServiceArgs%E6%96%B9%E6%B3%95%EF%BC%8C%E5%86%8D%E7%BB%8F%E8%BF%87%E4%B8%80%E7%B3%BB%E5%88%97%E8%B0%83%E7%94%A8%E5%88%B0ActivityThread%E7%9A%84handleServiceArgs()%E6%96%B9%E6%B3%95%0A%0A%23%23%23%23%20ActivityThread.java%0A%60%60%60java%0Aprivate%20void%20handleServiceArgs(ServiceArgsData%20data)%20%7B%0A%20%20%20%20%20%20%20%20Service%20s%20%3D%20mServices.get(data.token)%3B%0A%20%20%20%20%20%20%20%20if%20(s%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20(data.args%20!%3D%20null)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20data.args.setExtrasClassLoader(s.getClassLoader())%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20data.args.prepareToEnterProcess()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20int%20res%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20(!data.taskRemoved)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E8%B0%83%E7%94%A8Service%E7%9A%84onStartCommand()%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20res%20%3D%20s.onStartCommand(data.args%2C%20data.flags%2C%20data.startId)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20s.onTaskRemoved(data.args)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20res%20%3D%20Service.START_TASK_REMOVED_COMPLETE%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20QueuedWork.waitToFinish()%3B%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20try%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20%E9%80%9A%E7%9F%A5AMS%EF%BC%8Cservice%E5%90%AF%E5%8A%A8%E5%AE%8C%E6%88%90%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ActivityManager.getService().serviceDoneExecuting(%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20data.token%2C%20SERVICE_DONE_EXECUTING_START%2C%20data.startId%2C%20res)%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%20catch%20(RemoteException%20e)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20throw%20e.rethrowFromSystemServer()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20ensureJitEnabled()%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%20catch%20(Exception%20e)%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20...%0A%20%20%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%60%60%60%0A%3E%20s.onStartCommand()%E6%89%A7%E8%A1%8C%E5%AE%8C%E5%90%8E%E5%8F%88%E8%B0%83%E7%94%A8%E4%BA%86AMS%E7%9A%84serviceDoneExecuting()%E6%96%B9%E6%B3%95%EF%BC%8Cremove%E6%8E%89start%E7%9A%84%E5%BB%B6%E6%97%B6%E6%B6%88%E6%81%AF%E4%B8%8Eremove%E6%8E%89create%E5%BB%B6%E6%97%B6%E6%B6%88%E6%81%AF%E8%BF%87%E7%A8%8B%E4%B8%80%E8%87%B4
