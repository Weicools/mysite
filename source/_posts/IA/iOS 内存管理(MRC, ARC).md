**一.什么是内存管理**

程序在运行的过程中通常通过以下行为，来增加程序的的内存占用：

(1)创建一个OC对象

(2)定义一个变量

(3)调用一个函数或者方法

一个移动设备的内存是有限的，每个软件所能占用的内存也是有限的。

当程序所占用的内存较多时，系统就会发出内存警告，这时就得回收一些不需要再使用的内存空间。比如回收一些不需要使用的对象、变量等。

如果程序占用内存过大，系统可能会强制关闭程序，造成程序崩溃、闪退现象，影响用户体验.

所以，我们需要对内存进行合理的分配内存、清除内存，回收那些不需要再使用的对象。从而保证程序的稳定性。

那么，我们进行内存管理的对象是什么呢，那些需要关注那些不需要关注呢？

**(1)任何继承了NSObject的对象需要进行内存管理**

**(2)其他非对象类型(int、char、float、double、struct、enum等)
不需要进行内存管理**

这是因为

继承了NSObject的对象的存储在操作系统的堆里边。

**操作系统的堆：**一般由程序员分配释放，若程序员不释放，直到程序结束时由OS回收，分配方式类似于链表。

非OC对象一般放在操作系统的栈里面

**操作系统的栈：**由操作系统自动分配释放，存放函数的参数值，局部变量的值等。其操作方式类似于数据结构中的栈(先进后出)

**二.内存管理模型**

提供给Objective-C程序员的基本内存管理模型有以下3种：

**(1)自动垃圾收集（iOS运行环境不支持）**

**(2)手工引用计数和自动释放池(MRC)**

**(3)自动引用计数(ARC)**

**1.MRC 手动管理内存(Manual Reference Counting)**

**(1)引用计数器**

引用计数器是一个整数，每个OC对象都有自己的引用计数器。

任何一个对象，刚创建的时候，初始的引用计数为1；

当使用**alloc**、**new**或者**copy**创建一个对象时，对象的引用计数器默认就是1；

当没有任何人使用这个对象时，系统才会回收这个对象。

当对象的引用计数器为0时，对象占用的内存就会被系统回收。

如果对象的计数器不为0，那么在整个程序运行过程，它占用的内存就不可能被回收(除非整个程序已经退出
)。

**(2)引用计数器操作**

为保证对象的存在，每当创建引用到对象需要给对象发送一条retain消息，可以使引用计数器值+1
( retain 方法返回对象本身)。

当不再需要对象时，通过给对象发送一条release消息，可以使引用计数器值-1。

给对象发送retainCount消息，可以获得当前的引用计数器值。

当对象的引用计数为0时，系统就知道这个对象不再需要使用了，所以可以释放它的内存，通过给对象发送dealloc消息发起这个过程。

>   int main(int argc, const char \* argv[]) {

>       \@autoreleasepool {

>           **//只要创建一个对象默认引用计数器的值就是1**

>           Person \*p = [[Person alloc] init];

>           NSLog(\@"retainCount = %lu", [p retainCount]); //1

>           **//只要给对象发送一个retain消息, 对象的引用计数器就会+1**

>           [p retain];

>           NSLog(\@"retainCount = %lu", [p retainCount]); //2

>           // 通过指针变量p,给p指向的对象发送一条release消息

>           // 只要对象接收到release消息, 引用计数器就会-1

>           // 只要一个对象的引用计数器为0, 系统就会释放对象

>           [p release];

>           **//release并不代表销毁\\回收对象, 仅仅是计数器-1**

>           NSLog(\@"retainCount = %lu", [p retainCount]); //1

>           [p release]; // 0

>           NSLog(\@"--------");

>       }

>       return 0;

>   }

>   **//Person Class**

>   \@implementation Person

>   \- (void)dealloc {

>       [super dealloc];

>       NSLog(\@"Person, dealloc");

>   }

>   \@end

>   2018-11-14 10:59:16.654252+0800 TestCommonLine[16089:371796] retainCount = 1

>   2018-11-14 10:59:16.654425+0800 TestCommonLine[16089:371796] retainCount = 2

>   2018-11-14 10:59:16.654458+0800 TestCommonLine[16089:371796] retainCount = 1

>   2018-11-14 10:59:16.654476+0800 TestCommonLine[16089:371796] Person, dealloc

>   2018-11-14 10:59:16.654484+0800 TestCommonLine[16089:371796] --------

>   Program ended with exit code: 0

**(3)dealloc方法**

当一个对象的引用计数器值为0时，这个对象即将被销毁，其占用的内存被系统回收

对象即将被销毁时系统会自动给对象发送一条dealloc消息。

**dealloc**方法的重写

一般会重写dealloc方法，在这里释放相关资源，dealloc就是对象的遗言

一旦重写了dealloc方法，就必须调用**[super dealloc]**，并且放在最后面调用

**(4)自动释放池**

当我们不再使用一个对象的时候应该将其空间释放，但是有时候我们不知道何时应该将其释放。为了解决这个问题，Objective-C提供了**autorelease**方法。

**autorelease**是一种支持引用计数的内存管理方式，只要给对象发送一条**autorelease消息**，会将对象放到一个自动释放池中，当自动释放池被销毁时，会对池子里面的所有对象做一次**release**操作。注意,这里只是发送release消息，如果当时的引用计数(reference-counted)依然不为0，则该对象依然不会被释放。

autorelease方法会返回对象本身，且调用完autorelease方法后，对象的计数器不变

>   Person \*p = [Person new];

>   p = [p autorelease];

>   NSLog(\@"count = %lu", [p retainCount]); **//计数还为1**

autorelease实际上只是把对release的调用延迟了，对于每一个autorelease，系统只是把该对象放入了当前的autorelease
pool中,当该pool被释放时,该pool中的所有对象会被调用release。

**(a)autorelease的创建方法**

1.使用NSAutoreleasePool来创建

>   NSAutoreleasePool \*pool = [[NSAutoreleasePool alloc] init]; //
>   创建自动释放池

>   [pool release]; // [pool drain]; 销毁自动释放池

2.使用\@autoreleasepool创建

>   \@autoreleasepool{ //开始代表创建自动释放池

>   } //结束代表销毁自动释放池

**(b)autorelease的使用方法**

>   NSAutoreleasePool \*autoreleasePool = [[NSAutoreleasePool alloc] init];

>   Person \*p = [[[Person alloc] init] autorelease];

>   [autoreleasePool drain];

>   \@autoreleasepool { // 创建一个自动释放池

>       Person \*p = [[Person new] autorelease];

>       **//将代码写到这里就放入了自动释放池**

>   } //销毁自动释放池(会给池子中所有对象发送一条release消息)

**(c)autorelease的注意事项**

并不是放到自动释放池代码中,都会自动加入到自动释放池

>   \@autoreleasepool {

>   //因为没有调用 autorelease 方法,所以对象没有加入到自动释放池

>   Person \*p = [[Person alloc] init];

>   [p run];

>   }

在自动释放池的外部发送autorelease
不会被加入到自动释放池中。autorelease是一个方法,只有在自动释 放池中调用才有效。

>   \@autoreleasepool {

>   }

>   // 没有与之对应的自动释放池, 只有在自动释放池中调用autorelease才会放到释放池

>   Person \*p = [[[Person alloc] init] autorelease];

>   [p run];

>   **//正确写法**

>   \@autoreleasepool {

>   Person \*p = [[[Person alloc] init] autorelease];

>   }

>   **//正确写法**

>   Person \*p = [[Person alloc] init];

>   \@autoreleasepool {

>   [p autorelease];

>   }

**自动释放池中不适宜放占用内存比较大的对象**，尽量避免对大内存使用该方法,对于这种**延迟释放机制**,还是尽量少用。不要把**大量循环**操作放到同一个
\@autoreleasepool 之间,这样会造成内存峰值的上升

>   **//内存暴涨**

>   \@autoreleasepool {

>       for (int i = 0; i \< 99999; ++i) {

>           Person \*p = [[[Person alloc] init] autorelease];

>       }

>   }

>   **//内存不会暴涨**

>   for (int i = 0; i \< 99999; ++i) {

>       \@autoreleasepool {

>           Person \*p = [[[Person alloc] init] autorelease];

>       }

>   }

**2.ARC 自动管理内存(Automatic Reference Counting)**

**Automatic Reference
Counting**，自动引用计数，即ARC，**WWDC2011**和**iOS5**所引入的最大的变革和最激动人心的变化。ARC是新的LLVM
3.0编译器的一项特性，使用ARC，可以说一
举解决了广大iOS开发者所憎恨的手动内存管理的麻烦。

使用ARC后，系统会检测出何时需要保持对象，何时需要自动释放对象，何时需要释放对象，**编译器会管理好对象的内存**，会在何时的地方插入**retain**,
**release**和**autorelease**，通过生成正确的代码去自动释放或者保持对象。我们完全不用担心编译器会出错。

**(1)ARC的判断原则**

ARC判断一个对象是否需要释放也是通过**引用计数**来进行判断的，但是这个计数是**强指针**。那么什么是强指针?

**(a)强指针**

1.**默认**所有对象的指针变量都是强指针

2.被**\__strong**修饰的指针

>   Person \*p1 = [[Person alloc] init];

>   **\__strong** Person \*p2 = [[Person alloc] init];

只要还有一个强指针变量指向对象，对象就会保持在内存中

>   int main(int argc, const char \* argv[]) {

>   **    //不用写release, main函数执行完毕后p会被自动释放**

>       Person \*p = [[Person alloc] init];

>       NSLog(\@"person identifier = %ld", p.identifier);

>       NSLog(\@"end");

>       return 0;

>   }

>   2018-11-14 11:29:13.653738+0800 TestCommonLine[16962:453858] person
>   identifier = 0

>   2018-11-14 11:29:13.653885+0800 TestCommonLine[16962:453858] end

>   2018-11-14 11:29:13.653897+0800 TestCommonLine[16962:453858] Person, dealloc

>   Program ended with exit code: 0

**(b)弱指针**

1.被**\__weak**修饰的指针

\__weak修饰的指针不会增加引用计数

>   int main(int argc, const char \* argv[]) {

>       **\__weak** Person \*p = [[Person alloc] init];

>       p.identifier = 101;

>       NSLog(\@"person identifier = %ld", p.identifier);

>       NSLog(\@"end");

>       return 0;

>   }

>   2018-11-14 11:33:42.156175+0800 TestCommonLine[17059:470096] Person, dealloc

>   2018-11-14 11:33:42.156333+0800 TestCommonLine[17059:470096] person
>   identifier = 0

>   2018-11-14 11:33:42.156345+0800 TestCommonLine[17059:470096] end

>   Program ended with exit code: 0

**(c)ARC的注意点**

**1.不允许调用对象的 release方法**

**2.不允许调用 autorelease方法**

**3.重写父类的dealloc方法时，不能再调用 [super dealloc]**

**2.ARC内存管理分析**

局部变量释放对象随之被释放

>   int main(int argc, const char \* argv[]) {

>       \@autoreleasepool {

>           Person \*p = [[Person alloc] init];

>           NSLog(\@"autoreleasepool end");

>       } **//执行到这一行局部变量p释放**

>       NSLog(\@"end");

>       return 0;

>   }

>   2018-11-14 11:40:52.022741+0800 TestCommonLine[17216:492310] autoreleasepool
>   end

>   2018-11-14 11:40:52.022898+0800 TestCommonLine[17216:492310] Person, dealloc

>   2018-11-14 11:40:52.022911+0800 TestCommonLine[17216:492310] end

>   Program ended with exit code: 0

清空指针对象随之被释放

>   int main(int argc, const char \* argv[]) {

>       \@autoreleasepool {

>           Person \*p = [[Person alloc] init];

>           p = nil; **//执行到这一行, 由于没有强指针指向对象, 所以对象被释放**

>           NSLog(\@"autoreleasepool end");

>       }

>       NSLog(\@"end");

>       return 0;

>   }

>   2018-11-14 11:39:29.065947+0800 TestCommonLine[17181:486922] Person, dealloc

>   2018-11-14 11:39:29.066091+0800 TestCommonLine[17181:486922] autoreleasepool
>   end

>   2018-11-14 11:39:29.066103+0800 TestCommonLine[17181:486922] end

>   Program ended with exit code: 0

弱指针无法持有对象

>   int main(int argc, const char \* argv[]) {

>       \@autoreleasepool {

>           //**p是弱指针, 对象会被立即释放**

>           \__weak Person \*p1 = [[Person alloc] init];

>       }

>       return 0;

>   }

**3.ARC的内部实现**

ARC背后的引用计数主要依赖于这三个方法：

>   \- (instancetype)retain OBJC_ARC_UNAVAILABLE;

>   \- (oneway void)release OBJC_ARC_UNAVAILABLE;

>   \- (instancetype)autorelease OBJC_ARC_UNAVAILABLE;

(a)**retain** 增加引用计数

(b)**release** 降低引用计数，引用计数为0的时候，释放对象。

(c)**autorelease** 在当前的auto release pool结束后，降低引用计数。

在**Cocoa Touch**中，NSObject协议中定义了这三个方法，由于Cocoa
Touch中，绝大部分类都继承自NSObject（NSObject类本身实现了NSObject协议），所以可以“免费”获得NSObject提供的运行时和ARC管理方法，这就是为什么适用OC开发iOS的时候，所写类要继承自NSObject。

**Retain**

既然ARC是引用计数，那么对应一个对象，内存中必然会有一个地方来存储这个对象的引用计数。这一点可以直接从retain方法入口。

>   \- (id)retain {

>   return ((id)self)-\>**rootRetain**();

>   }

>   inline id objc_object::rootRetain() {

>   if (isTaggedPointer()) return (id)this;

>   return **sidetable_retain**();

>   }

>   id objc_object::sidetable_retain() {

>   //获取table

>   **SideTable**& table = SideTables()[this];

>   //加锁

>   table.lock();

>   //获取引用计数

>   size_t& refcntStorage = table.refcnts[this];

>   if (! (refcntStorage & SIDE_TABLE_RC_PINNED)) {

>   //增加引用计数

>   refcntStorage += SIDE_TABLE_RC_ONE;

>   }

>   //解锁

>   table.unlock();

>   return (id)this;

>   }

到这里，retain如何实现就很清楚了，通过SideTable这个数据结构来存储引用计数。这个数据结构的实现如下：

>   typedef objc::DenseMap\<DisguisedPtr\<objc_object\>, size_t, true\>
>   RefcountMap;

>   struct SideTable {

>   spinlock_t slock;

>   **RefcountMap** refcnts;

>   **Weak_table_t** weak_table;

>   ……

>   };

可以看到，这个数据结构就是存储了一个自旋锁，一个引用计数map。这个引用计数的map以对象的地址作为key，引用计数作为value。到这里，引用计数的底层实现就很清楚了。

存在全局的map，这个map以地址作为key，引用计数的值作为value。

**Release**

然后看一下release的实现：

>   SideTable& table = SideTables()[this];

>   bool do_dealloc = false;

>   table.lock();

>   //找到对应地址的

>   RefcountMap::iterator it = table.refcnts.find(this);

>   if (it == table.refcnts.end()) { //找不到的话，执行dellloc

>   do_dealloc = true;

>   table.refcnts[this] = SIDE_TABLE_DEALLOCATING;

>   } else if (it-\>second \< SIDE_TABLE_DEALLOCATING)
>   {//引用计数小于阈值，dealloc

>   do_dealloc = true;

>   it-\>second \|= SIDE_TABLE_DEALLOCATING;

>   } else if (! (it-\>second & SIDE_TABLE_RC_PINNED)) {

>   //引用计数减去1

>   it-\>second -= SIDE_TABLE_RC_ONE;

>   }

>   table.unlock();

>   if (do_dealloc && performDealloc) {

>   //执行dealloc

>   ((void(\*)(objc_object \*, SEL))objc_msgSend)(this, SEL_dealloc);

>   }

>   return do_dealloc;

release的到这里也比较清楚了：查找map，对引用计数减1，如果引用计数小于阈值，则调用SEL_dealloc

**Autorelease pool**

上文提到了，autorelease方法的作用是把对象放到autorelease pool中，到pool
drain的时候，会释放池中的对象。如下

>   \__weak NSObject \* obj;

>   NSObject \* temp = [[NSObject alloc] init];

>   obj = temp;

>   NSLog(\@"%\@",obj); //非空

>   放到auto release pool中

>   \@autoreleasepool {

>   NSObject \* temp = [[NSObject alloc] init];

>   obj = temp;

>   }

>   NSLog(\@"%\@",obj); //nil

可以看到，放到自动释放池的对象是在超出自动释放池作用域后立即释放的。事实上在iOS
程序启动之后，主线程会启动一个Runloop，这个Runloop在每一次循环是被自动释放池包裹的，在合适的时候对池子进行清空。

对于Cocoa框架来说，提供了两种方式来把对象显式的放入AutoReleasePool.

**(a)NSAutoreleasePool(只能在MRC下使用)**

**(b)\@autoreleasepool {}代码块(ARC和MRC下均可以使用)**

那么AutoRelease pool又是如何实现的呢？首先看一下代码：

>   //autorelease方法

>   \- (id)autorelease {

>   return ((id)self)-\>**rootAutorelease**();

>   }

>   //rootAutorelease 方法

>   inline id objc_object::rootAutorelease()

>   {

>   if (isTaggedPointer()) return (id)this;

>   //检查是否可以优化

>   if (prepareOptimizedReturn(ReturnAtPlus1)) return (id)this;

>   //放到auto release pool中。

>   return **rootAutorelease2**();

>   }

>   // rootAutorelease2

>   id objc_object::rootAutorelease2()

>   {

>   assert(!isTaggedPointer());

>   return AutoreleasePoolPage::**autorelease**((id)this);

>   }

可以看到，把一个对象放到auto release
pool中是调用了AutoreleasePoolPage::autorelease这个方法。

>   public: static inline id autorelease(id obj)

>   {

>   assert(obj);

>   assert(!obj-\>isTaggedPointer());

>   id \*dest \__unused = autoreleaseFast(obj);

>   assert(!dest \|\| dest == EMPTY_POOL_PLACEHOLDER \|\| \*dest == obj);

>   return obj;

>   }

>   static inline id \*autoreleaseFast(id obj)

>   {

>   AutoreleasePoolPage \*page = hotPage(); //使用了线程局部存储

>   if (page && !page-\>full()) {

>   return page-\>add(obj); //添加到链表中

>   } else if (page) {

>   return autoreleaseFullPage(obj, page); //如果page满了会新建一个page

>   } else {

>   return autoreleaseNoPage(obj); //会新建hotPage

>   }

>   }

>   id \*add(id obj)

>   {

>   assert(!full());

>   unprotect();

>   id \*ret = next;

>   \*next++ = obj;

>   protect();

>   return ret;

>   }

autorelease方法会把对象存储到**AutoreleasePoolPage**的链表里。等到auto release
pool被释放的时候，把链表内存储的对象删除。所以，AutoreleasePoolPage就是自动释放池的内部实现。

**补充：**

\@autoreleasepool其实是在{}起止执行了以下两块代码：

>   **objc_autoreleasePoolPush()**

>   **objc_autoreleasePoolPop(obj)**

>   void \*objc_autoreleasePoolPush(void)

>   {

>       return AutoreleasePoolPage::push();

>   }

>   static inline void \*push()

>   {

>       id \*dest;

>       if (DebugPoolAllocation) {

>           // Each autorelease pool starts on a new pool page.

>           dest = autoreleaseNewPage(POOL_BOUNDARY);

>       } else {

>           dest = autoreleaseFast(POOL_BOUNDARY);

>       }

>       assert(dest == EMPTY_POOL_PLACEHOLDER \|\| \*dest == POOL_BOUNDARY);

>       return dest;

>   }

>   //\#define POOL_BOUNDARY nil

>   //\@autoreleasepool块结束时调用，此处为真正的释放时机

>   static inline void pop(void \*token)

>   {

>       AutoreleasePoolPage \*page;

>       id \*stop;

>       if (token == (void\*)EMPTY_POOL_PLACEHOLDER) {

>           if (hotPage()) {

>               pop(coldPage()-\>begin());

>           } else {

>               setHotPage(nil);

>           }

>           return;

>       }

>       page = pageForPointer(token);

>       stop = (id \*)token;

>       if (\*stop != POOL_BOUNDARY) {

>           if (stop == page-\>begin() && !page-\>parent) {

>           } else {

>               return badPop(token);

>           }

>       }

>       ...

>       page-\>releaseUntil(stop);

>       ...

>       if (page-\>child) {

>           if (page-\>lessThanHalfFull()) {

>               page-\>child-\>kill();

>           }

>           else if (page-\>child-\>child) {

>               page-\>child-\>child-\>kill();

>           }

>       }

>   }

>   class AutoreleasePoolPage

>   {

>       ...

>       static size_t const SIZE = PAGE_MAX_SIZE;

>       ...

>       magic_t const magic;                   // 16字节

>       id \*next;                              // 8字节

>       pthread_t const thread;                // 8字节

>       AutoreleasePoolPage \* const parent;    // 8字节

>       AutoreleasePoolPage \*child;            // 8字节

>       uint32_t const depth;                  // 4字节

>       uint32_t hiwat;                        // 4字节

>       ...

>   }

>   \#define PAGE_MAX_SIZE           PAGE_SIZE

>   \#define PAGE_SIZE              I386_PGBYTES

>   \#define I386_PGBYTES            4096

>   我们可以先把autoreleasepool运行的过程大概表示一下：

>   D  = ...

>   \@autoreleasepool {

>      \@autoreleasepool {

>        \@autoreleasepool {

>       A = ...

>        }

>        B = ...

>      }

>   }

>   token1 = push()

>   ...

>       token2 = push()

>       ...

>           token3 = push()

>           ...

>           pop(token3)

>       ...

>       pop(token2)

>   ...

>   pop(token1)

**\__weak与__strong**

用过block的一定写过类似的代码

>   **\__weak** typeSelf(self) weakSelf = self;

>   [object fetchSomeFromRemote:\^{

>   **\__strong** typeSelf(weakSelf) strongSelf = weakSelf;

>   //从这里开始用**strongSelf**

>   }];

那么，为什么要这么用呢？原因是：

block会捕获外部变量，用weakSelf保证self不会被block被捕获，防止引起循环引用或者不必要的额外生命周期。用strongSelf则保证在block的执行过程中，对象不会被释放掉。

待补充，详细的__weak & \__strong原理
