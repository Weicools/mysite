**android中几种多线程：Thread，AsyncTask, IntentService,
HandlerThread以及ThreadPool**

|               |                                                                                                                                                                           | 使用场景                                                                                                                                                            | 底层       |   |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|---|
| AsyncTask     | 为 UI 线程与工作线程之间进行快速的切换提供一种简单便捷的机制。                                                                                                            | 适用于当下立即需要启动，但是异步执行的生命周期短暂的使用场景。                                                                                                      | ThreadPool |   |
| HandlerThread | 是Thread的子类，是一个专属线程，可以长时间运行，不断从任务队列中获取复杂任务， 它在内部创建了消息队列，外界需要通过Handler的消息方式来通知HandlerThread执行一个具体的任务 | 比较适用于单线程+异步队列的场景，比如IO读写操作数据库、文件等，耗时不多而且也不会产生较大的阻塞。对于网络IO操作，并不适合，因为它只有一个线程，还得排队一个一个等着 | Thread     |   |
| IntentService | 是Service的子类，内部采用HandlerThread，执行完任务后自动退出                                                                                                              | 优先级比service高，适合执行高优先级的后台任务，不易被杀死，如下载                                                                                                   | Thread     |   |
| ThreadPool    |                                                                                                                                                                           | 适用于需要频繁创建和销毁的多线程并发任务                                                                                                                            |            |   |

**HandlerThread**

看看官方对它的解释：

>   Handy class for starting a new thread that has a looper. The looper can then
>   be used to create handler classes. Note that start() must still be called.

大致就是说HandlerThread可以创建一个带有looper的新线程,looper对象可以用于创建Handler类来进行来进行调度，而且start()方法必须被调用。

**特征：**

\- HandlerThread本质上是一个线程类，它继承了Thread； 

\- HandlerThread有自己的内部Looper对象，通过Looper.loop()进行looper循环； 

\- 通过获取HandlerThread的looper对象传递给Handler对象，然后在handleMessage()方法中执行异步任务； 

\- 创建HandlerThread后必须调用HandlerThread.start()方法来启动线程。

**使用：**

**public class** HandlerThreadActivity **extends** AppCompatActivity {

    **private** HandlerThread **handlerThread**;

    **private** Handler **mCheckMsgHandler**;

    \@Override

    **protected void** onCreate(Bundle savedInstanceState) {

        **super**.onCreate(savedInstanceState);

        initHandlerThread();

    }

    \@Override

    **protected void** onResume() {

        **super**.onResume();

        **mCheckMsgHandler**.sendEmptyMessage(MSG_UPDATE_INFO);

    }

    \@Override

    **protected void** onPause() {

        **super**.onPause();

        **mCheckMsgHandler**.removeMessages(MSG_UPDATE_INFO);

    }

    **private void** initHandlerThread() {

        **handlerThread** = **new** HandlerThread(**"check-message-coming"**);

        **handlerThread**.start();

        **mCheckMsgHandler** = **new** Handler(**handlerThread**.getLooper()) {

            \@Override

            **public void** handleMessage(Message msg) {

                *//执行具体操作*

*           * }

        };

    }

    \@Override

    **protected void** onDestroy() {

        **super**.onDestroy();

        *//释放资源*

*       * **handlerThread**.quit();

    }

}

**IntentService**

IntentService是Service的子类，比普通的Service增加了额外的功能。

**使用步骤：**

在Activity中：

Intent intent = new Intent(this, MyIntentService.class);   

startService(intent);  

 

继承IntentService类，并重写onHandleIntent()方法即可； 

public class MyIntentService extends IntentService {   

      public MyIntentService() {   

          super("MyIntentService");   

     }   

     \@Override   

     protected void onHandleIntent(Intent intent) {   

          // IntentService会使用单独的线程来执行该方法的代码   

          // 该方法内执行耗时任务，比如下载文件

     }   

  }  

**特征：** 

1.会创建独立的worker线程来处理所有的Intent请求；  

2.会创建独立的worker线程来处理onHandleIntent()方法实现的代码，无需处理多线程问题；  

3.所有请求处理完成后，IntentService会自动停止，无需调用stopSelf()方法停止Service；  

4.为Service的onBind()提供默认实现，返回null；  

5.为Service的onStartCommand提供默认实现，将请求Intent添加到队列中； 

**IntentService 源码:**

\@Override 

public void onCreate() { 

super.onCreate(); 

// HandlerThread 继承自 Thread，内部封装了
Looper，在这里新建线程并启动，所以启动 IntentService 不需要新建线程。 

HandlerThread thread = new HandlerThread("IntentService[" + mName + "]"); 

thread.start(); 

// 获得工作线程的 Looper，并维护自己的工作队列。 

mServiceLooper = thread.getLooper(); 

// mServiceHandler 是属于工作线程的。 

mServiceHandler = new ServiceHandler(mServiceLooper); 

} 

\@Override

public void onStart(\@Nullable Intent intent, int startId) {

    Message msg = mServiceHandler.obtainMessage();

    msg.arg1 = startId;

    msg.obj = intent;

    mServiceHandler.sendMessage(msg);

}

private volatile ServiceHandler mServiceHandler; 

private final class ServiceHandler extends Handler { 

public ServiceHandler(Looper looper) { 

super(looper); 

} 

\@Override 

public void handleMessage(Message msg) { 

// onHandleIntent 方法在工作线程中执行，执行完调用 stopSelf() 结束服务。 

onHandleIntent((Intent)msg.obj); 

stopSelf(msg.arg1); 

} 

} 

\@Override

public void onDestroy() {

    mServiceLooper.quit();

}

\@WorkerThread 

protected abstract void onHandleIntent(Intent intent);

启动IntentService的会使用单独的worker线程，因此不会阻塞前台的UI线程；而Service会。

Q&A：

1.启动 IntentService 为什么不需要新建线程？

见上面源码

 

2.为什么不建议通过 bindService() 启动 IntentService？

\@Override

public IBinder onBind(Intent intent){

    returnnull;

}

IntentService 源码中的 onBind() 默认返回 null；不适合 bindService() 启动服务，

如果你执意要 bindService() 来启动 IntentService，可能是希望想通过 Binder 或
Messenger 使得 IntentService 和 Activity 可以通信，那么 onHandleIntent()
不会被回调，相当于在你使用 Service 而不是 IntentService。

3.为什么多次启动 IntentService
会顺序执行事件，停止服务后，后续的事件得不到执行？

IntentService 中使用的 Handler、Looper、MessageQueue
机制把消息发送到线程中去执行的，所以多次启动 IntentService
不会重新创建新的服务和新的线程，只是把消息加入消息队列中等待执行，而如果服务停止，会清除消息队列中的消息，后续的事件得不到执行。

\@Override

public void onStart(Intent intent, int startId) {

    Message msg = mServiceHandler.obtainMessage();

    msg.arg1 = startId;

    msg.obj = intent;

    mServiceHandler.sendMessage(msg);

}

\@Override

public void onDestroy() {

    mServiceLooper.quit();

}

 
