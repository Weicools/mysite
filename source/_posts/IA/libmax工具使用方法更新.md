**libmax工具使用方法更新**

-   **将libmaxconfig.json文件移到B区对应的target目录下, 执行copyCtoA**  
      
    

    ![](media/aafb8317713ac70847b112c1bba73801.png)

-   **A区会生成对应的target目录，需修改改B区的build.gradle**  
      
    

    ![](media/87cfbe503343e6a63038f4817701c3ac.png)

oneAppMaxSecurityPro {  
java.srcDirs = ['src/main/java', 'libMax/oneAppMaxSecurityPro/java',
'src/oneAppMaxSecurityPro/java']  
  
res.srcDirs += pppLibmaxRes  
  
}

-   **当多个A区target发生改动时，不支持copyAtoC,把不需要的A区target删除后，可以继续执行copyAtoC**

-   **B区main目录下,target目录下都支持覆盖（已有同名文件时，不会拷贝到A区），和对比C区**

-   **A区对比，删除操作和以前一致**

-   **iaplugin.grale, checkCustomView.py,
    intruderCheck.py中的A区路径已经需改，暂未发现其他需要修改路径的工具**

%23%20libmax%E5%B7%A5%E5%85%B7%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95%E6%9B%B4%E6%96%B0%0A\*%20__%E5%B0%86libmaxconfig.json%E6%96%87%E4%BB%B6%E7%A7%BB%E5%88%B0B%E5%8C%BA%E5%AF%B9%E5%BA%94%E7%9A%84target%E7%9B%AE%E5%BD%95%E4%B8%8B%2C%20%E6%89%A7%E8%A1%8CcopyCtoA__%0A!%5B8942a14c9ac9d05c83823905a0f016d9.png%5D(evernotecid%3A%2F%2F52525B50-D7A6-448F-8220-C4E47EBBC8BF%2Fappyinxiangcom%2F19550952%2FENResource%2Fp1815)%0A\*%20__A%E5%8C%BA%E4%BC%9A%E7%94%9F%E6%88%90%E5%AF%B9%E5%BA%94%E7%9A%84target%E7%9B%AE%E5%BD%95%EF%BC%8C%E9%9C%80%E4%BF%AE%E6%94%B9%E6%94%B9B%E5%8C%BA%E7%9A%84build.gradle__%0A!%5B29ff32fa259613664c287d4a67ffaee7.png%5D(evernotecid%3A%2F%2F52525B50-D7A6-448F-8220-C4E47EBBC8BF%2Fappyinxiangcom%2F19550952%2FENResource%2Fp1816)%0A%0A%60%60%60%20gradle%0A%20oneAppMaxSecurityPro%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20java.srcDirs%20%3D%20%5B'src%2Fmain%2Fjava'%2C%20'libMax%2FoneAppMaxSecurityPro%2Fjava'%2C%20'src%2FoneAppMaxSecurityPro%2Fjava'%5D%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20res.srcDirs%20%2B%3D%20pppLibmaxRes%0A%0A%20%20%20%20%20%20%20%20%7D%0A%60%60%60%0A\*%20__%E5%BD%93%E5%A4%9A%E4%B8%AAA%E5%8C%BAtarget%E5%8F%91%E7%94%9F%E6%94%B9%E5%8A%A8%E6%97%B6%EF%BC%8C%E4%B8%8D%E6%94%AF%E6%8C%81copyAtoC%2C%E6%8A%8A%E4%B8%8D%E9%9C%80%E8%A6%81%E7%9A%84A%E5%8C%BAtarget%E5%88%A0%E9%99%A4%E5%90%8E%EF%BC%8C%E5%8F%AF%E4%BB%A5%E7%BB%A7%E7%BB%AD%E6%89%A7%E8%A1%8CcopyAtoC__%0A\*%20__B%E5%8C%BAmain%E7%9B%AE%E5%BD%95%E4%B8%8B%2Ctarget%E7%9B%AE%E5%BD%95%E4%B8%8B%E9%83%BD%E6%94%AF%E6%8C%81%E8%A6%86%E7%9B%96%EF%BC%88%E5%B7%B2%E6%9C%89%E5%90%8C%E5%90%8D%E6%96%87%E4%BB%B6%E6%97%B6%EF%BC%8C%E4%B8%8D%E4%BC%9A%E6%8B%B7%E8%B4%9D%E5%88%B0A%E5%8C%BA%EF%BC%89%EF%BC%8C%E5%92%8C%E5%AF%B9%E6%AF%94C%E5%8C%BA__%0A\*%20__A%E5%8C%BA%E5%AF%B9%E6%AF%94%EF%BC%8C%E5%88%A0%E9%99%A4%E6%93%8D%E4%BD%9C%E5%92%8C%E4%BB%A5%E5%89%8D%E4%B8%80%E8%87%B4__%0A\*%20__iaplugin.grale%2C%20checkCustomView.py%2C%20intruderCheck.py%E4%B8%AD%E7%9A%84A%E5%8C%BA%E8%B7%AF%E5%BE%84%E5%B7%B2%E7%BB%8F%E9%9C%80%E6%94%B9%EF%BC%8C%E6%9A%82%E6%9C%AA%E5%8F%91%E7%8E%B0%E5%85%B6%E4%BB%96%E9%9C%80%E8%A6%81%E4%BF%AE%E6%94%B9%E8%B7%AF%E5%BE%84%E7%9A%84%E5%B7%A5%E5%85%B7_\_
