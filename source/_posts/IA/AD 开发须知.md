1 申请一个广告位配置

2 获取广告资源

3 展示广告

4 释放广告资源

5 记录flurry

Constants 中定义了可以使用的广告位配置

*// ad placement*

**public static
final **String *AD_PLACEMENT_NAME_MANY_IN_ONE *= **"ManyInOne"**;

**public static final **String *AD_PLACEMENT_NAME_BOOST_DONE *= **"BoostDone"**;

**public static final **String *AD_PLACEMENT_NAME_LUCKY_DRAW *= **"LuckyDraw"**;

**public static final **String *AD_PLACEMENT_NAME_CABLE *= **"Cable"**;

**public static
final **String *AD_PLACEMENT_NAME_PROMOTE_CARD *= **"PromoteCard"**;

**public static final **String *AD_PLACEMENT_NAME_FREEZE *= **"Freeze"**;

激活广告

AcbNativeAdManager.*sharedInstance*().init(OptimizerApplication.**this**);

AcbNativeAdManager.*sharedInstance*().activePlacementInProcess(String...);

关闭广告

AcbNativeAdManager.*getInstance*().deactivePlacementInProcess(String...);

获取广告

**nativeAdLoader**.load(1, **new **AcbNativeAdLoader.AcbNativeAdLoadListener() {

    \@Override

   **public void **onAdReceived(AcbNativeAdLoader acbNativeAdLoader,
List\<AcbNativeAd\> list) {

    }

    \@Override

   **public void **onAdFinished(AcbNativeAdLoader acbNativeAdLoader, HSError
hsError) {

    }

});

使用广告

AcbNativeAdContainerView containerView
= **new **AcbNativeAdContainerView(HSApplication.*getContext*());

View adContentView
= LayoutInflater.*from*(HSApplication.*getContext*()).inflate(R.layout.*item_ad_card_intruder*, **null**);

containerView.addContentView(adContentView);

containerView.setAdActionView(adContentView.findViewById(R.id.*ad_action*));

containerView.setAdBodyView((TextView)
adContentView.findViewById(R.id.*ad_body*));

containerView.setAdIconView((AcbNativeAdIconView)
adContentView.findViewById(R.id.*ad_icon*));

containerView.setAdPrimaryView((AcbNativeAdPrimaryView)
adContentView.findViewById(R.id.*ad_icon*));

containerView.setAdTitleView((TextView)
adContentView.findViewById(R.id.*ad_title*));

containerView.setAdChoiceView((ViewGroup)
adContentView.findViewById(R.id.*ad_choice*));

**adsLayout**.removeAllViews();

**adsLayout**.addView(containerView);

containerView.fillNativeAd(acbNativeAd);

Flurry

AcbNativeAdAnalytics.*logAppViewEvent*(placementName, **true**); //广告展示

AcbNativeAdAnalytics.*logAppViewEvent*(placementName, **false**); //广告load失败

释放广告

acbNativeAd.release()

acbNativeAdLoader.cancel()

注意事项

一般情况下广告元素需要展示完整，特殊情况分析

布局上广告元素层级属于顶层，广告上方无任何view，即使设置invisible或gone也得remove

广告布局上需要添加AD角标

在开发中如果总是不出广告，可以临时使用其他广告位进行测试，比如PromoteCard，BoostDone
这些经常出广告的PlacmentName，或者修改pList文件中这些广告位名字，但是需要注意的是提交代码前修正
