---
title: about
date: 2018-05-30 17:25:30
type: "about"
layout: "about"
---

# About
大家好，我是Weicools，2017年本科毕业于四川大学软件工程系，目前主要从事的是移动开发，这几年都是Android开发为主，博客主要记录了我平时一些关于编程技术的心得体会或读书笔记，当然偶尔也会记录下生活总结，除了写代码还喜欢看看动漫、听歌、开开黑玩玩游戏。

## Planning
我从事Android平台开发近2年，正在沉淀自己的技术，同时也保持视野开阔，了解当前各个技术领域的前沿。未来我将会扩展自己从事的技术栈，紧随技术发展的浪潮，努力成为一个技术大牛专家。

## Contact Me
Email: [Weicools.me@gmail.com](mailto: lecymeng@outlook.com)
Weibo: [@Weicools](https://weibo.com/onewinnie)
Twitter: [@Weicools](https://www.instagram.com/weicools/)
Telegram: [@Weicools](https://t.me/Weicools)
GitHub: [@lecymeng](https://github.com/lecymeng)